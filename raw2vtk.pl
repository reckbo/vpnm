#!/usr/bin/perl -w
#
#This small script converts the raw files (text files containing voxel coordinates and
#values) produced by vfsim into vtk format.  They can then be visualized using a program 
#such as Mayavi.

print "Number of input files: " . (@ARGV - 1) ."\n";

for ($is = 1; $is <= $#ARGV; ++$is)
{
	# Read in input file contents
	open(fhInput, $ARGV[$is]) || die "couldn't open input file.\n";		
	@lines = <fhInput>;		
	$size = @lines;
	close(fhInput);			

	@rgsOutput = ();
	push( @rgsOutput, "# vtk DataFile Version 3.1\n");
	push( @rgsOutput, "Created by raw2vtk.pl\n");
	push( @rgsOutput, "ASCII\n");
	push( @rgsOutput, "DATASET UNSTRUCTURED_GRID\n");
	push( @rgsOutput, "POINTS $size FlOAT\n");

	$fileOutput = $ARGV[$is];
	$fileOutput =~ s/\.raw/\.vtk/;

	if ($ARGV[0] eq 'f') # for filament data
	{
    $fileOutput =~ s/\.dat/\.vtk/;
		push(@rgsOutput, @lines);
	}
	elsif ($ARGV[0] eq 'n') # for narrow band data
	{
		@rgvl = ();
		@rgchCoord = ();
		foreach $line (@lines)
		{
			chomp($line);
			@values = split(/ +/, $line);
			push(@rgvl, "$values[3]\n");
			push(@rgchCoord, "$values[0] $values[1] $values[2]\n");
		}
		push( @rgsOutput, @rgchCoord );			
		push( @rgsOutput, "\n" );
		push( @rgsOutput, "POINT_DATA $size\n" );
		push( @rgsOutput, "SCALARS NarrowBandValue float\n" );
		push( @rgsOutput, "LOOKUP_TABLE default\n" );
		push( @rgsOutput, @rgvl );
	}
	else
	{
		die "Specify flag: either \"f\" for filaments or \"n\" for narrow band.\n";
	}
	
	if (-e $fileOutput)
	{
		die "Cannot write to $fileOutput, it already exists!\n";
	}
	print "Writing $fileOutput\n";
	open(fhOutput, ">$fileOutput") || die "couldn't open file to write to: $!\n";
	print fhOutput @rgsOutput;
	close(fhOutput);
}
