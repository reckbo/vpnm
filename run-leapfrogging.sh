#!/bin/bash
#Leap frogging example to show core conservation.  Diffusion commented out.
./mkellipse -x 0.0 -y 0.0 -z 0.5 -t 0 -e 0 -o 1 -n 700 fil1.dat
./mkellipse -x 0.0 -y 0.0 -z 1.0 -t 0 -e 0 -o 1 -n 700 fil2.dat

OPTIONS="--hybrid 
	  	 --dump-every=0.005 
 	  	 --timestep=0.005   
       --small-timestep=0.0025
	  	 --time-begin=0.000 
       --time-end=0.005 
	  	 --resolution=120 
	  	 --circulation=0.5 
       --biotsavart-spline
	  	 --high-order-integration 
	  	 --subsample-res=1 
       --spline-weight=120
	  	 --narrowband-radius=4.0 
	  	 --narrowband-boundary=1.5 
	  	 --thin-every=0.035 
	  	 fil1.dat fil2.dat"

#./vfsim $OPTIONS
#~/vp/vpnm-2-18/vfsim $OPTIONS 
gdb --args vfsim $OPTIONS 
