#!/usr/bin/python

from filvel import *
import sys

rgspd = []
rgspd.append(filspeed(sys.argv[1], 0, 1000))
rgspd.append(filspeed(sys.argv[2], 0, 1000))
rgspd.append(filspeed(sys.argv[3], 0, 1000))
rgspd.append(filspeed(sys.argv[4], 0, 1000))
plot([60,80,100,120],rgspd, 'go-', label='ss=1')
#rgspd = []
#rgspd.append(-1*filspeed(sys.argv[7], 0, 500))
#rgspd.append(-1*filspeed(sys.argv[8], 0, 500))
#rgspd.append(-1*filspeed(sys.argv[9], 0, 500))
#rgspd.append(-1*filspeed(sys.argv[10], 0, 70))
#plot([40,60,80,100],rgspd, 'bo-', label='ss=3')
plot([40,60,80,100,120,140],0.304*ones([1,6]), 'r-', label='theoretical speed')
title('Average Circular Filament Speeds for Different Resolutions, dt=0.01')
xlabel('Resolution (number of voxels/unit)')
ylabel('Avg. Speed (units/s)')
legend(loc=2)
#axis([0,180,0.20,0.345])
show()
