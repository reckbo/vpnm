#!/usr/bin/python
from Numeric import *
from pylab import *

sFile = sys.argv[1]
f=open('%s'%sFile,'r')
rg= [ map(float, line.split()) for line in f ]
arr= array(rg)
print arr

# We want to plot velocity field in the y=0 plane
X = arr[:,0].copy()
Z = arr[:,2].copy()
rgdxdtFluid = arr[:,3].copy()
rgdzdtFluid = arr[:,5].copy()
rgdxdtNormal = arr[:,6].copy()
rgdzdtNormal = arr[:,8].copy()
print rgdzdtNormal
#rg1= [ map(print, i) for i in rgdzdtFluid ]
#rg1 = array('f', rgdzdtFluid);
#print rg1
quiver(X,Z, rgdxdtFluid, rgdzdtFluid, scale=40.0)
#quiver(X,Z, rgdxdtNormal, rgdzdtNormal, color='r', scale=40.0)
quiver(X,Z, rgdxdtNormal, rgdzdtNormal, color='r', scale=15.0)
quiver([-120], [0], [0], [-0.304], color='b', scale=40.0)
axis('scaled')
xlabel('x axis')
ylabel('z axis')
#rgdxdtNormal = arr[:,
show()
