#!/usr/bin/python

import os
import sys
from filvel import *

sDirVortex = sys.argv[1]
sDirHybrid = sys.argv[2]
tStart = int(sys.argv[3])
tEnd = int(sys.argv[4])
dt = int(sys.argv[5])

rgt = arange(tStart, tEnd+dt, dt) #array: tStart...tEnd
f = []
arr = []
dts = dt/1000.0
rgdist=[[],[]]
vecCentre=[0,0]
for t in rgt: #from tStart to tEnd
		for i in (1,2):
				for ifils in (1,2):
						if os.path.exists('%sfil%d-%04d.raw'%(sys.argv[i],ifils,t)):
								f=open('%sfil%d-%04d.raw'%(sys.argv[i],ifils,t))
						else:
								f=open('%sfil%d-%05d.raw'%(sys.argv[i],ifils,t))
						arr = [map(float,line.split()) for line in f]
						f.close()
						arr = array(arr)
						vecCentre[ifils-1] = sum(arr)/float(len(arr))
				rgdist[i-1].append(sqrt(sum((vecCentre[0] - vecCentre[1])**2)))
rgt = rgt/1000.0
plot(rgt,rgdist[0], 'ro-', label='Vortex Method')		
plot(rgt,rgdist[1], 'bo-', label='Hybrid Method')		
#axis([tStart, tEnd, 0.975, 1.001])
xlabel('Time')
ylabel('Distance Between Filaments')
title('Leap Frogging Simulation\nDistance Between Filaments vs Time')
legend()
show()
