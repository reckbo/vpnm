~/vp/vpnm/mkellipse -x 0.0 -y 0.0 -z 0.0 -e 0 -n 700 fil.dat

OPTIONS="--hybrid 
	  	 --dump-every=0.005 
 	  	 --timestep=0.005   
	  	 --time-begin=0 
	 	 --time-end=2.000 
	  	 --resolution=60 
	  	 --circulation=1 
	  	 --core-radius=0.1  
		 --biotsavart-spline
	  	 --high-order-integration 
	  	 --subsample-res=1 
		 --spline-weight=60
	  	 --narrowband-radius=2.5 
	  	 --narrowband-boundary=0.5 
	  	 --thin-every=4000 
		 --debug
		 fil.dat"

gdb --args ~/vp/vpnm/vfsim $OPTIONS
