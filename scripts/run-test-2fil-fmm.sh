#~/vp/vpnm/mkellipse -x 1.2 -y 1.2 -z 1.0 -e 0 -t 15 -n 700 -o 1 fil1.dat
#~/vp/vpnm/mkellipse -x 3.4 -y 1.2 -z 1.0 -e 0 -t -15 -n 700 -o 1 fil2.dat

OPTIONS="--hybrid 
	  	 --dump-every=0.01 
 	  	 --timestep=0.01   
	  	 --time-begin=3.210 
	 	 --time-end=4.000 
	  	 --resolution=60 
	  	 --circulation=1 
	  	 --core-radius=0.1  
		 --biotsavart-spline
	  	 --high-order-integration 
	  	 --subsample-res=1 
		 --spline-weight=60
	  	 --narrowband-radius=2.5 
	  	 --narrowband-boundary=0.5 
	  	 --thin-every=4000 
		 sim/11/b/fil1-3660.raw sim/11/b/fil2-3660.raw"

gdb --args ~/vp/vpnm/vfsim $OPTIONS
