#!/usr/bin/python
import sys
import mayavi
import time
import vtk
from Numeric import *
#import ivtk

v = mayavi.mayavi()
rgt = arange(float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]))
sDir = sys.argv[1]
rgifils = range(1,int(sys.argv[5])+1)
p=[]
fFirstIteration = 1
for t in rgt:
		for ifils in rgifils:
				f=v.open_vtk('%sfil%d-%04d.vtk'%(sDir,ifils,t),config=0)
				g=v.load_module('Glyph',0)
				g.glyph_src=vtk.vtkSphereSource()
				g.glyph.SetSource (g.glyph_src.GetOutput())
				g.glyph_src.SetRadius(0.005)
				v.renwin.x_plus_view()
				v.renwin.camera.Zoom(2)
				#v.renwin.camera.SetPosition((15,1.5,0.0))
				v.renwin.camera.SetParallelProjection(1)
				if fFirstIteration == 1:
						v.renwin.camera.Zoom(2)
						fFirstIteration = 0
						p = v.renwin.camera.GetPosition()
				v.Render()
		#v.close_all()
		time.sleep(1)
v.renwin.save_png('%sfils-evolution.png'%sDir)
v.close_all()
