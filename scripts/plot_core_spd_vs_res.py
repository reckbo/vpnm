#!/usr/bin/python

from Numeric import *
from pylab import *
import sys

sTitle = sys.argv[1]
y = array([float(sys.argv[2])/0.304,float(sys.argv[3])/0.304,float(sys.argv[4])/0.304,float(sys.argv[5])/0.304])
plot([60,90,120,180], y, 'gx-', label='Average tube speed (tube voxel radius fixed)')
plot([60,90,120,180],ones([1,4]), 'r-', label='Theoretical speed')
plot([120], [float(sys.argv[6])/0.3041], 'ro', label='Average tube speed, with regular thinning')
plot([120], [float(sys.argv[7])/0.3041], 'bo', label='Average tube speed, with sub-sampled thinning')
#plot([60,90,120,180], y2, 'bo-', label='Experiment 1: Avg. tube speed, no thinning, 10 iterations')
#plot([120,180], y3, 'mo-', label='Experiment 2: Avg. tube speed, thinning, ss=1, 1 thinning, 10 iterations')
#plot([120,180], y4, 'yo-', label='Experiment 3: Avg. tube speed, thinning, ss=3, 1 thinning, 10 iterations')
#plot([120], y5, 'ro-', 	   label='ations')
title('%s\nFilament Radius=1, Circulation=1, Core Radius=0.1, Tube Radius=6 voxels'%sTitle)
xlabel('Resolution (voxels/unit)')
ylabel('Normalized Average Speed (units/s)')
legend(loc='lower right')
axis([60,180,0.70,1.02])
show()



# Old script 
#sTitle = sys.argv[1]
#y = array([sys.argv[1],sys.argv[2],sys.argv[3],sys.argv[4]])
#y = [0.255, 0.27665, 0.28632, 0.295129] #the first iteration
#y1 = [0.266, 0.259, 0.258, 0.257] #core, after 10 iterations
#y2 = [0.266, 0.281, 0.291, 0.296]  # tube, after 10 iterations
#y3 = [0.287300, 0.293707]  # tube, ss=1, after 10 iterations
#y4 = [0.287266, 0.293727]  # tube, ss=3, after 10 iterations
#y5 = [0.287815]  # res=120, tube, ss=1, after 62 iterations
#plot([60,90,120,180], y1, 'go-', label='Experiment 1: Average core speed (voxel radius varies)')
#plot([60,90,120,180], y2, 'bo-', label='Experiment 1: Avg. tube speed, no thinning, 10 iterations')
#plot([120,180], y3, 'mo-', label='Experiment 2: Avg. tube speed, thinning, ss=1, 1 thinning, 10 iterations')
#plot([120,180], y4, 'yo-', label='Experiment 3: Avg. tube speed, thinning, ss=3, 1 thinning, 10 iterations')
#plot([120], y5, 'ro-', 	   label='Experiment 4: Avg. tube speed, thinning, ss=1, 9 thinnings, 62 iterations')
#plot([60,90,120,180],0.304*ones([1,4]), 'r-', label='Theoretical speed')
#title('%s\nFilament Radius=1, Circulation=1, Core Radius=0.1, Tube Radius=6 voxels'%sTitle)
#xlabel('Resolution (voxels/unit)')
#ylabel('Average Speed (units/s)')
#legend(loc='lower right')
#axis([0,180,0.20,0.345])
#show()
