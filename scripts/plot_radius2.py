#!/usr/bin/python

from filvel import *
import sys

rgrad = []
rgrad.append(filradius(sys.argv[1], 500))
rgrad.append(filradius(sys.argv[2], 500))
rgrad.append(filradius(sys.argv[3], 500))
rgrad.append(filradius(sys.argv[4], 500))
plot([60,80,100,140],rgrad, 'go-', label='ss=1')
#rgrad = []
#rgrad.append(filradius(sys.argv[7], 500))
#rgrad.append(filradius(sys.argv[8], 500))
#rgrad.append(filradius(sys.argv[9], 500))
#plot([40,60,80],rgrad, 'bo-', label='ss=3')
title('Circular Filament Radius after 1000 ms, dt=0.01 (Initial Radius = 1.000)')
xlabel('Resolution (voxels/unit)')
ylabel('Filament Radius (units)')
legend(loc=2)
#axis([0,180,0.20,0.345])
show()

