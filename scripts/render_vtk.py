#!/usr/bin/python
import sys
import mayavi
import time
import vtk
from Numeric import *
#import ivtk

v = mayavi.mayavi()
sDir = sys.argv[1]
sFile = sys.argv[2]

f=v.open_vtk('%s%s'%(sDir,sFile),config=0)
g=v.load_module('Glyph',0)

if sys.argv[3] == 's':
  g.glyph_src=vtk.vtkSphereSource()
  g.glyph.SetSource (g.glyph_src.GetOutput())
  g.glyph_src.SetRadius(0.005)

if sys.argv[3] == 'c':
  g.glyph_src=vtk.vtkCubeSource()
  g.glyph.SetSource (g.glyph_src.GetOutput())
  g.glyph.SetScaling(0)

if sys.argv[4] == 'z':
  v.renwin.z_plus_view()
elif sys.argv[4] == 'm': 
  v.renwin.camera.SetFocalPoint(0,0,0)
  v.renwin.camera.SetPosition(0,1,0.5)
  v.renwin.camera.SetViewUp(0,0,1)
  v.renwin.ren.ResetCamera()
elif sys.argv[4] == 'd': 
  #v.renwin.x_plus_view()
  v.renwin.camera.SetFocalPoint(0.0,1.0,1.8)
  #v.renwin.camera.SetPosition(+10,+0.5,+6.5)
  v.renwin.camera.SetViewUp(0,0.3,1)
  v.renwin.ren.ResetCamera()
elif sys.argv[4] == 't':  #test
  v.renwin.camera.SetFocalPoint(0,0,0)
  v.renwin.camera.SetPosition(0.7,+2.0,-1.0)
  v.renwin.camera.SetViewUp(0,1,0)
  v.renwin.ren.ResetCamera()
  v.renwin.camera.Zoom(1.0)
#v.renwin.camera.Zoom(1.4)  #recropping 34 degree filament exp, t = 20 to 23
#v.renwin.camera.Zoom(1.6) #redid t = 2 with this, I dont' know what it was previously
v.Render()

#if sys.argv[4] == 'z':
#  v.renwin.save_png('%s%s-z.png'%(sDir,sFile))
#else: 
v.renwin.save_png('%s%s.png'%(sDir,sFile))

#v.close_all()
