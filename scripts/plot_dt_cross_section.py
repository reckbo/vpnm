#!/usr/bin/python
from Numeric import *
from pylab import *

f=open('fmm-dt-cross-section.raw','r')
arr = array([map(float,line.split()) for line in f])
f.close()
plot(array(arr[:,0]), array(arr[:,1]), 'ro-')		
show()
