#!/usr/bin/python
from Numeric import *
from pylab import *
import matplotlib.axes3d as p3

#tStart = float(sys.argv[1])
#tEnd = float(sys.argv[2])
#dt = float(sys.argv[3])
#rgt = arange(tStart, tEnd, dt)
#f = []
#for t in rgt:
#f=open('%snb-%04d.raw'%(sDir,t), 'r')

circulation = []
for i in range(1,9):
		sFile = sys.argv[i]
		f=open('%s'%sFile,'r')
		rg= array([map(float, line.split()) for line in f])
		f.close()

		#Get two vectors
		v1 = rg[0,:].copy()
		v2 = rg[50,:].copy()

		#Take cross product
		v3 = array([1+i, 2, 3])

		#Append to list
		circulation.append(v3)


circulation = array(circulation, Float)
print circulation[:,1]
axis('scaled')
quiver(zeros(8), zeros(8), circulation[:,0], circulation[:,1], scale=40.0, pivot='tail', color='black')
#xlabel('x axis')
#ylabel
show()
