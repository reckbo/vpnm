~/vp/vpnm/mkellipse -x 3.4 -y 1.2 -z 1.0 -e 0 -n 700 -o 1 fil.dat

OPTIONS="--vortex-method 
	  	 --dump-every=0.10 
 	  	 --timestep=0.100   
	  	 --time-begin=0.00 
	 	 --time-end=100.000 
	  	 --circulation=1 
	  	 --core-radius=0.1  
		 --biotsavart-spline
	  	 --high-order-integration 
		 fil.dat"

#~/vp/vpnm/vfsim $OPTIONS &> output.txt &
~/vp/vpnm/vfsim $OPTIONS
