#!/usr/bin/python
from Numeric import *
from pylab import *

def filspeed(sDir, tStart, tEnd):
	fStart=open('%(str)sfil1-%(t)04d.raw'%{"str":sDir,"t":tStart},'r')
	fEnd=open('%(str)sfil1-%(t)04d.raw'%{"str":sDir,"t":tEnd},'r')
	lStart = [map(float,line.split()) for line in fStart]
	lEnd = [map(float,line.split()) for line in fEnd]
	arrStart = array(lStart)
	arrEnd = array(lEnd)
	vecStart = sum(arrStart)/float(len(arrStart))
	vecEnd = sum(arrEnd)/float(len(arrEnd))
	vecDiff = (vecEnd - vecStart)
	fStart.close()
	fEnd.close()
	return vecDiff[2]/((float(tEnd)-float(tStart))/1000.0)

def plotvel(sDir, tStart, tEnd, dt):
	#rgt = dt*array(range((tEnd-tStart)/dt))
	rgt = arange(tStart, tEnd, dt)
	f = []
	arrEn = []
	dts = dt/1000.0
	rgspd=[]
	f=open('%(str)sfil1-%(t)04d.raw'%{"str":sDir,"t":tStart},'r')
	arrEn = [map(float,line.split()) for line in f]
	f.close()
	arrEn = array(arrEn)
	vecSt = sum(arrEn)/float(len(arrEn))
	for t in rgt:
		f=open('%(str)sfil1-%(t)04d.raw'%{"str":sDir,"t":t+dt},'r')
		arrEn = [map(float,line.split()) for line in f]
		f.close()
		arrEn = array(arrEn)
		vecEn = sum(arrEn)/float(len(arrEn))
		rgspd.append((vecEn[2] - vecSt[2])/dts)
		vecSt = vecEn
	spdAvg = filspeed(tStart, tEnd)	
	#plot(rgt,rgspd,rgt,spdAvg*ones([1,len(rgt)]), rgt, -0.3041*ones([1,len(rgt)]), 'r')		
	figure()
	plot(rgt,rgspd, label='_nolegend_')		
	plot(rgt,spdAvg*ones([1,len(rgt)]), 'g', label='avg. speed')		
	plot(rgt,-0.3041*ones([1,len(rgt)]), 'r', label='theoretical speed')		
	xlabel('time (ms)')
	ylabel('z-axis speed (units/s)')
	title('Hybrid Method\nSingle Ring Filament Speed along Z-Axis vs Time')
	legend()
	#legend('avg', 'theoretical', 'test')
	#show()
	return 


#print filspeed(0, 500)
#plotvel(0, 500, 10)



