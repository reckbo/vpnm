#!/usr/bin/python

import sys
import os
from filvel import *

sDirVortex = sys.argv[1]
sDirHybrid = sys.argv[2]
tStart = int(sys.argv[3])
tEnd = int(sys.argv[4])
dt = int(sys.argv[5])

rgt = arange(tStart, tEnd+dt, dt) #array: tStart...tEnd
f = []
arr = []
dts = dt/1000.0
rgradH=[]
rgradV=[]
vecCentre=[]
for t in rgt: #from tStart to tEnd
  for i in (1,2):
	if os.path.exists('%sfil1-%04d.raw'%(sys.argv[i], t)):
	  f=open('%sfil1-%04d.raw'%(sys.argv[i], t))
	else:
	  f=open('%sfil1-%05d.raw'%(sys.argv[i], t))
	arr = [map(float,line.split()) for line in f]
	f.close()
	arr = array(arr)
	vecCentre = sum(arr)/float(len(arr))
	vecMax = arr[argmax(arr[:,1]), :]
	if i == 1:
	  rgradV.append( sqrt(sum((vecMax-vecCentre)**2)) )
	else:
	  rgradH.append( sqrt(sum((vecMax-vecCentre)**2)) )
#hack!
t1 = 5000/dt - 1
dtSegment = 6
t2 = tEnd/dt - dtSegment + 1
for t in range(t1, t2):
  rgradH[t] = rgradH[t+dtSegment]
plot(rgt[0:t2]/1000.0,rgradV[0:t2], 'ro-', label='VortexMethod')		
plot(rgt[0:t2]/1000.0,rgradH[0:t2], 'bo-', label='Hybrid Method')		
#axis([tStart, tEnd, 0.975, 1.001])
xlabel('Time')
ylabel('y-max')
title('Hybrid Method: \nElliptical Filament: y-max  vs Time')
legend()
show()
