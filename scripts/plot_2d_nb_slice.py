#!/usr/bin/python
from Numeric import *
from pylab import *
import matplotlib.axes3d as p3

#tStart = float(sys.argv[1])
#tEnd = float(sys.argv[2])
#dt = float(sys.argv[3])
#rgt = arange(tStart, tEnd, dt)
#f = []
#for t in rgt:
#f=open('%snb-%04d.raw'%(sDir,t), 'r')

sFile = sys.argv[1]

f=open('%s'%sFile,'r')
rg= [map(float, line.split()) for line in f]
f.close()

# We want to plot field in the y=0 plane
lst = [w 
		for w in rg 
		if int(w[1]) == 0]
#if int(w[0]) > 0]

rgScatter = array(lst);
X = rgScatter[:,0].copy()
Z = rgScatter[:,2].copy()

#minX = min(X)
#minZ = min(Z)
#maxX = max(X)
#maxZ = max(Z)

res = float(sys.argv[2])
NBwidth = float(sys.argv[3])
du = NBwidth + 10
dv = res + NBwidth + 10

U = zeros((2*du,2*dv), Float)
for w in rgScatter:
		U[int(w[2])+du, int(w[0])+dv] = w[3]

# Contour Plot
contour(U,[0.05, 0.75, 0.1, 0.125, 0.15], origin='lower', colors=('blue','red','black'))
#contour(U, origin='lower', colors=('blue','red','black'))
hold(True)

# 2D Psi Plot
#imshow(U, origin="lower", alpha=0.9, interpolation='Nearest', extent=[-80,80,-15,15])
imshow(U, alpha=0.9, interpolation='Nearest', origin='lower')
#gray()
bone()
colorbar()
axis('scaled')
hold(True)

# Fluid Velocity Field Plot
rgdxdtFluid = rgScatter[:,4].copy()
rgdzdtFluid = rgScatter[:,6].copy()
quiver(X+dv+0.5, Z+du+0.5, rgdxdtFluid, rgdzdtFluid, scale=40.0, pivot='tail', color='yellow')
#print "Average z fluid velocity: %s"%(add.reduce(rgdzdtFluid)/len(rgdzdtFluid))
#print "Average z fluid velocity within 0.1 contour: %s"%( add.reduce([w[6] for w in rgScatter if w[3] < 0.1]) / len(rgdzdtFluid))
#quiver([20.5], [14.5], [0], [-0.304], color='b', scale=20.0)

# Normal Velocity Plot
rgdxdtNormal = rgScatter[:,7].copy()
rgdzdtNormal = rgScatter[:,9].copy()
quiver(X+dv+0.5, Z+du+0.5, rgdxdtNormal, rgdzdtNormal, scale=40.0, pivot='tail', color='black')

# Gradient Plot
rgGradientX = rgScatter[:,10].copy()
rgGradientZ = rgScatter[:,12].copy()
quiver(X+dv+0.5, Z+du+0.5, rgGradientX, rgGradientZ, scale=40.0, pivot='tail', color='red')

xlabel('x axis')
ylabel('z axis')

title('%s'%sFile)
axis([5,2*du-4, 5, 2*du-4])
if sys.argv[4] == "show":
		show()
if sys.argv[4] == "save-image":
		savefig('vel_fields_%s.png'%sFile, format='png')
