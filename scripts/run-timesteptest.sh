~/vp/vpnm/mkellipse -x 1.0 -y 0.0 -z 0.5 -e 0 -t 0 -n 700 fil.dat

OPTIONS="--hybrid 
	  	 --dump-every=0.01 
 	  	 --timestep=0.01   
	  	 --time-begin=0.000 
	 	 --time-end=0.050 
	  	 --resolution=60 
	  	 --circulation=1 
	  	 --core-radius=0.1  
		 --biotsavart-spline
	  	 --high-order-integration 
	  	 --subsample-res=1 
		 --spline-weight=60
	  	 --narrowband-radius=2.5 
	  	 --narrowband-boundary=0.5 
	  	 --thin-every=4000 
		 fil.dat"

gdb --args ~/vp/vpnm/vfsim $OPTIONS
