function [] = render_tubes(tStart, dt, tEnd)

for i = tStart:dt:tEnd
		x = load(sprintf('ptnSS1-%04i.raw', i));
		y = load(sprintf('ptnSS2-%04i.raw', i));
		plot3(x(:,1), x(:,2), x(:,3), 'gs'); 
		hold on; 
		plot3(y(:,1), y(:,2), y(:,3), 'ys'); 
		axis equal;
end

