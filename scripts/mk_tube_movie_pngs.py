#!/usr/bin/python
import sys
import mayavi
import time
import vtk
from Numeric import *
#import ivtk

v = mayavi.mayavi()
rgt = arange(float(sys.argv[2]), float(sys.argv[3]), float(sys.argv[4]))
sDir = sys.argv[1]
p=[]
fFirstIteration = 1
for t in rgt:
		f=v.open_vtk('%sptnSS-%04d.vtk'%(sDir,t),config=0)
		g=v.load_module('Glyph',0)
		g.glyph_src=vtk.vtkCubeSource()
		g.glyph.SetSource (g.glyph_src.GetOutput())
		#g.glyph_src.SetRadius(0.005)
		#v.renwin.y_plus_view()
		#v.renwin.x_plus_view()
		#v.renwin.camera.Pitch(5)
		v.renwin.camera.SetFocalPoint(0,0,0)
		v.renwin.camera.SetPosition(0,1,0.5)
		v.renwin.camera.SetViewUp(0,0,1)
		v.renwin.ren.ResetCamera()
		#p = v.renwin.camera.GetPosition()
		#v.renwin.camera.SetPosition((p(1),p(2),0.0))
		#if fFirstIteration == 1:
				#v.renwin.camera.SetParallelProjection(1)
				#v.renwin.camera.Zoom(2)
				#fFirstIteration = 1
		v.Render()
		#v.renwin.y_plus_view()
		#c = ivtk.GetActiveCamera()
		#c.SetDistance(3.5)
		v.renwin.save_png('%sptnSS-%04d.png'%(sDir,t))
		v.close_all()
		time.sleep(1)
