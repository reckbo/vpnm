#!/usr/bin/python

from Numeric import *
from pylab import *
import sys

sTitle = sys.argv[1]
y = array([float(sys.argv[2]),float(sys.argv[3]),float(sys.argv[4]),float(sys.argv[5])])
plot([60,90,120,180], y, 'rx-')
title('%s\nFilament Radius=1, Circulation=1, Core Radius=0.1'%sTitle)
xlabel('Resolution (voxels/unit)')
ylabel('Filament Tube Radius (units)')
#legend(loc='lower right')
axis([60,180,0.00,0.15])
show()
