#!/usr/bin/python
from Numeric import *
from pylab import *

def filradius(sDir, t):
	f=open('%sfil1-%04d.raw'%(sDir,t))
	arr = [map(float,line.split()) for line in f]
	f.close()
	arr = array(arr)
	vec = sum(arr)/float(len(arr))
	arrRadii = sqrt( sum( (vec*ones([len(arr),3]) - arr)**2,  1 ) )
	avgRadius = sum(arrRadii)/float(len(arr))
	return avgRadius

def filspeed(sDir, tStart, tEnd):
	fStart=open('%(str)sfil1-%(t)04d.raw'%{"str":sDir,"t":tStart},'r')
	fEnd=open('%(str)sfil1-%(t)04d.raw'%{"str":sDir,"t":tEnd},'r')
	lStart = [map(float,line.split()) for line in fStart]
	lEnd = [map(float,line.split()) for line in fEnd]
	arrStart = array(lStart)
	arrd = array(lEnd)
	vecForwardart = sum(arrStart)/float(len(arrStart))
	vecBackd = sum(arrd)/float(len(arrd))
	vecDiff = (vecBackd - vecForwardart)
	fStart.close()
	fEnd.close()
	return vecDiff[2]/((float(tEnd)-float(tStart))/1000.0)

def plotvel(sSubTitle, sDir, tStart, tEnd, dt):
	#rgt = dt*array(range((tEnd-tStart)/dt))
	rgt = arange(tStart, tEnd+dt, dt) #array: tStart...tEnd
	f = []
	arr = []
	dts = dt/1000.0
	rgspd=[]
	rgzpos=[]
	rgrad =[]
	vecForward=[]
	vecBack=[]
	for t in rgt: #from tStart to tEnd
		if t == 0:
			f=open('%(str)sfil1-%(t)04d.raw'%{"str":sDir,"t":tStart},'r')
			arr = [map(float,line.split()) for line in f]
			f.close()
			arr = array(arr)
			vecBack = sum(arr)/float(len(arr))
			# Add z position to position array
			rgzpos.append(vecBack[2])
			# Calculate and add average radius to radius array. This should give 1.0.
			arrRadii = sqrt( sum( (vecBack*ones([len(arr),3]) - arr)**2,  1 ) )
			avgRadius = sum(arrRadii)/float(len(arr))
			rgrad.append(avgRadius)
		else:
			f=open('%(str)sfil1-%(t)04d.raw'%{"str":sDir,"t":t},'r')
			arr = [map(float,line.split()) for line in f]
			f.close()
			arr = array(arr)
			vecForward = sum(arr)/float(len(arr))
			# Add z position to position array
			rgzpos.append(vecForward[2])
			# Calculate and add average radius to radius array
			arrRadii = sqrt( sum( (vecForward*ones([len(arr),3]) - arr)**2,  1 ) )
			avgRadius = sum(arrRadii)/float(len(arr))
			rgrad.append(avgRadius)
			# Calculate and add velocity to velocity array
			rgspd.append((vecForward[2] - vecBack[2])/dts)
			vecBack = vecForward
	#Plot velocity
	spdAvg = filspeed(sDir,tStart,tEnd)
	figure()
	subplot(221)
	plot(rgt[1:],rgspd, 'bo-', label='_nolegend_')		
	plot(rgt[1:],spdAvg*ones([1,len(rgt)-1]), 'g', label='avg. speed (%.3f units/s)'%spdAvg)		
	plot(rgt[1:],-0.3041*ones([1,len(rgt)-1]), 'r', label='theoretical speed (%s units/s)'%-0.304)		
	#ax = axis()
	#ax[2]=-0.35
	#ax[3]=0.0
	#axis([tStart, tEnd, -0.4, 0.2])
	axis([tStart, tEnd, -0.5, 0.5])
	xlabel('time (ms)')
	ylabel('z-axis speed (units/s)')
	#title('Hybrid Method: %(str)s\nSingle Ring Filament Speed Along Z-Axis vs Time'%{"str":sSubTitle})
	title('Single Ring Filament Speed Along Z-Axis vs Time'%{"str":sSubTitle})
	legend()
	#Plot position
	subplot(223)
	plot(rgt,rgzpos, 'mo-', label='_nolegend_')		
	xlabel('time (ms)')
	ylabel('z-axis position (units)')
	#title('Hybrid Method: %(str)s\nSingle Ring Filament Z-Axis Position vs Time'%{"str":sSubTitle})
	title('Single Ring Filament Z-Axis Position vs Time'%{"str":sSubTitle})
	#Plot radius
	a=axes([0.6,0.1,0.35,0.8])
	#subplot(223)
	plot(rgt,rgrad, 'ro-', label='_nolegend_')		
	axis([tStart, tEnd, 0.975, 1.001])
	xlabel('time (ms)')
	ylabel('average radius (units)')
	title('Hybrid Method: %s\nSingle Ring Filament Average Radius vs Time'%sSubTitle)
	return 


	#plot(rgt,rgspd,rgt,spdAvg*ones([1,len(rgt)]), rgt, -0.3041*ones([1,len(rgt)]), 'r')		
#plotvel(0, 500, 10)



