#!/usr/bin/python
from Numeric import *
from pylab import *
import matplotlib.axes3d as p3

sFile = sys.argv[1]
ySlice = int(sys.argv[2])

f=open('%s'%sFile,'r')
rg= [map(float, line.split()) for line in f]
f.close()

lst = [w 
		for w in rg 
		if int(w[1]) == ySlice]

rg= array(lst);

X = rg[:,0].copy()
Z = rg[:,2].copy()

minZ = min(Z)
maxZ = max(Z)

minX = min(X)
maxX = max(X)

U = zeros((maxZ+45, maxX+45), Float)
for w in rg:
		#U[int(w[2]), int(w[0])] = w[3]
		U[int(w[2])+30, int(w[0]+30)] = 1

# Contour Plot
#contour(U,[0.05, 0.75, 0.1, 0.125, 0.15], origin='lower', colors=('blue','red','black'))
#contour(U, origin='lower', colors=('blue','red','black'))
#hold(True)

# 2D Psi Plot
#imshow(U, origin="lower", alpha=0.9, interpolation='Nearest', extent=[-80,80,-15,15])
imshow(U, alpha=0.9, interpolation='Nearest', origin='lower')
#gray()
bone()
colorbar()
axis('scaled')
hold(True)

xlabel('x axis')
ylabel('z axis')

title('y = %s Voxel Plane, The Centre of the Approach Region'%ySlice)
show()
