#include <ctype.h>
#include <malloc.h>
#include <stdio.h>
#include <math.h>
#include <unistd.h>
#include <stdlib.h>

void 
WriteFilamentToFile(float xC, float yC, float zC, float a, float e, float t, int cfn, int o, int p, int s, char *szFilename)
{
	FILE *fpt;
	fpt=fopen(szFilename, "w+");
	if (!fpt)
		{
		printf("Couldn't open file to write.\n");
		exit(1);
		}

	float b = sqrt(1-pow(e, 2));
	float x = 0.0;
	float y = 0.0;
	float z = 0.0;
	//float z = zC;
	float alpha = (t/360.0)*2*M_PI; //tilt angle about y-axis
	float xr = 0.0;
	float zr = 0.0;

	float dtheta=(2.0*M_PI)/cfn;
	int i;
	if(o)
		{
		for(i=0; i < cfn; i++)
			{
			x = a*cos((float)i*dtheta);
			y = a*b*sin((float)i*dtheta);
			if (p > 0)
				z =  (M_PI/(float)p)*sin( (2.0*M_PI*(float)i)/((float)cfn/(float)p) - (float)s*(M_PI));
			else
				z = 0;
			xr = cos(alpha)*x - sin(alpha)*z;
			zr = sin(alpha)*x + cos(alpha)*z;
			x = xr + xC;
			y = y + yC;
			z = zr + zC;

			fprintf(fpt,"%f  %f  %f\n", x, y, z);
			}
		}
	else
		{
		for(i=cfn-1; i >= 0; i--)
			{
			x = a*cos((float)i*dtheta);
			y = a*b*sin((float)i*dtheta);
			if (p > 0)
				z =  (M_PI/(float)p)*sin( (2.0*M_PI*(float)i)/((float)cfn/(float)p) );
			else
				z = 0;
			xr = cos(alpha)*x - sin(alpha)*z;
			zr = sin(alpha)*x + cos(alpha)*z;
			x = xr + xC;
			y = y + yC;
			z = zr + zC;
			fprintf(fpt,"%f  %f  %f\n", x, y, z);
			}
		}
	fclose(fpt);
	printf("Created file `%s'\n", szFilename);
}
int 
main(int argc, char *argv[])
{
	int ch;

	opterr = 0;

	float x = 0.0;
	float y = 0.0;
	float z = 0.0;
	float a = 1.0;
	float e = 0.0;
	float t = 0.0;
	int n = 0;
	int o = 0;
	int p = 0;
	int s = 0;

	while ((ch = getopt (argc, argv, "x:y:z:a:e:t:n:o:p:s:f:")) != -1)
		switch (ch)
			{
			case 'x':
				x = atof(optarg); 
				break;
			case 'y':
				y = atof(optarg);
				break;
			case 'z':
				z = atof(optarg);
				break;
			case 'a':
				a = atof(optarg);
				break;
			case 'e':
				e = atof(optarg);
				break;
			case 't':
				t = atof(optarg);
				break;
			case 'n':
				n = atoi(optarg);
				break;
			case 'o':
				o = atoi(optarg);
				break;
			case 'p':
				p = atoi(optarg);
				break;
			case 's':
				s = atoi(optarg);
				break;
			case '?':
				if (isprint (optopt))
					fprintf (stderr, "Unknown option `-%c'.\n", optopt);
				else
					fprintf (stderr, "Unknown option character `\\x%x'.\n",	optopt);
				return 1;
			default:
				abort ();
			}

	if (optind == argc)
		{
		printf ("Specify the name of the file to create.\n");
		return 1;
		}
	char *szFilename = argv[optind];

	int index;
	for (index = optind+1; index < argc; index++)
		printf ("Non-option argument %s\n", argv[index]);


	WriteFilamentToFile(x, y, z, a, e, t, n, o, p, s, szFilename);

	return 0;
}
