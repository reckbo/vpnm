# README #

This is the implementation of my M.Sc. thesis, "Simulating Vortex Ring Collisions: Extending the Hybrid Method", where we propose a new hybrid method for simulating vortex filament motion with possible topological
changes. It's written in C, but uses pre-existing numerical routines written in Fortran for solving the Navier-Stokes equations.