CC = gcc -g -Wall -DDEBUGMEM -pg
ROOTDIR = .
INCLUDE = -O0 -I$(ROOTDIR)/vfsim-src
PROFILE = 
CFLAGS  = $(INCLUDE)

OBJ = IM_access.o IM_convert.o thin_ryan.o heap.o list.o Dump_Info.o  NarrowBand_RungeKutta.o main.o VoxelTree.o voxel.o fibheap.o fmm.o FluidVelocityRoutines.o smoothing.o debug.o mymath.o


install: $(OBJ)
	$(CC) $(OBJ) -o vfsim $(PROFILE) -lm -lpthread
	ctags vfsim-src/*.c vfsim-src/*.h

#ExtendedVoxelContainer.o: vfsim-src/ExtendedVoxelContainer.c vfsim-src/ExtendedVoxelContainerInterface.h
#	$(CC) $(CFLAGS) -c  vfsim-src/ExtendedVoxelContainer.c -o ExtendedVoxelContainer.o $(PROFILE)

VoxelTree.o: vfsim-src/VoxelTree.c vfsim-src/VoxelTree.h
	$(CC) $(CFLAGS) -c  vfsim-src/VoxelTree.c -o VoxelTree.o $(PROFILE)

debug.o: vfsim-src/debug.c vfsim-src/debug.h
	$(CC) $(CFLAGS) -c  vfsim-src/debug.c -o debug.o $(PROFILE)

smoothing.o: vfsim-src/smoothing.c 
	$(CC) $(CFLAGS) -c  vfsim-src/smoothing.c -o smoothing.o $(PROFILE)

voxel.o: vfsim-src/voxel.c vfsim-src/voxel.h
	$(CC) $(CFLAGS) -c  vfsim-src/voxel.c -o voxel.o $(PROFILE)

FluidVelocityRoutines.o: vfsim-src/FluidVelocityRoutines.c vfsim-src/FluidVelocityRoutines.h
	$(CC) $(CFLAGS) -c  vfsim-src/FluidVelocityRoutines.c -o FluidVelocityRoutines.o $(PROFILE)

#bs_tree.o: vfsim-src/bs_tree.c vfsim-src/bs_tree.h
	#$(CC) $(CFLAGS) -c  vfsim-src/bs_tree.c -o bs_tree.o $(PROFILE)

#tree_and_voxel_based_functions.o: vfsim-src/tree_and_voxel_based_functions.c vfsim-src/tree_and_voxel_based_functions.h
#	$(CC) $(CFLAGS) -c  vfsim-src/tree_and_voxel_based_functions.c -o tree_and_voxel_based_functions.o $(PROFILE)

fibheap.o: vfsim-src/fibheap.c vfsim-src/fibheap.h 
	$(CC) $(CFLAGS) -c  vfsim-src/fibheap.c -o fibheap.o $(PROFILE)

mymath.o: vfsim-src/mymath.c vfsim-src/mymath.h 
	$(CC) $(CFLAGS) -c  vfsim-src/mymath.c -o mymath.o $(PROFILE)

#fheap.o: vfsim-src/fheap.c vfsim-src/fheap.h 
#	$(CC) $(CFLAGS) -c  vfsim-src/fheap.c -o fheap.o $(PROFILE)

fmm.o: vfsim-src/fmm.c vfsim-src/fmm.h 
	$(CC) $(CFLAGS) -c  vfsim-src/fmm.c -o fmm.o $(PROFILE)

IM_access.o: vfsim-src/IM_access.c vfsim-src/IM_convert.h 
	$(CC) $(CFLAGS) -c  vfsim-src/IM_access.c -o IM_access.o $(PROFILE)

IM_convert.o: vfsim-src/IM_convert.c vfsim-src/IM_convert.h
	$(CC) $(CFLAGS) -c  vfsim-src/IM_convert.c -o IM_convert.o $(PROFILE)

thin_ryan.o: vfsim-src/thin_ryan.c vfsim-src/thin_ryan.h vfsim-src/heap.h vfsim-src/list.h
	$(CC) $(CFLAGS) -c  vfsim-src/thin_ryan.c -o thin_ryan.o $(PROFILE)

heap.o: vfsim-src/heap.c vfsim-src/heap.h
	$(CC) $(CFLAGS) -c  vfsim-src/heap.c -o heap.o $(PROFILE)

list.o: vfsim-src/list.c vfsim-src/list.h
	$(CC) $(CFLAGS) -c  vfsim-src/list.c -o list.o $(PROFILE)

#gauss.o: vfsim-src/gauss.c vfsim-src/gauss.h
	#$(CC) $(CFLAGS) -c  vfsim-src/gauss.c -o gauss.o $(PROFILE)

#3D_DT_Support.o: vfsim-src/3D_DT_Support.c vfsim-src/3D_DT_Support.h
	#$(CC) $(CFLAGS) -c  vfsim-src/3D_DT_Support.c -o 3D_DT_Support.o $(PROFILE)

#Filament_Functions.o: vfsim-src/Filament_Functions.c vfsim-src/Filament_Functions.h
	#$(CC) $(CFLAGS) -c  vfsim-src/Filament_Functions.c -o Filament_Functions.o $(PROFILE)

Dump_Info.o: vfsim-src/Dump_Info.c vfsim-src/Dump_Info.h
	$(CC) $(CFLAGS) -c  vfsim-src/Dump_Info.c -o Dump_Info.o $(PROFILE)

#LevelSet_NarrowBand_Sim.o: vfsim-src/LevelSet_NarrowBand_Sim.c vfsim-src/LevelSet_NarrowBand_Sim.h vfsim-src/VoxelAndVelocityContainerInterface.h
#	$(CC) $(CFLAGS) -c  vfsim-src/LevelSet_NarrowBand_Sim.c -o LevelSet_NarrowBand_Sim.o $(PROFILE)

NarrowBand_RungeKutta.o: vfsim-src/NarrowBand_RungeKutta.c vfsim-src/NarrowBand_RungeKutta.h
	$(CC) $(CFLAGS) -c  vfsim-src/NarrowBand_RungeKutta.c -o NarrowBand_RungeKutta.o $(PROFILE)

main.o: vfsim-src/main.c vfsim-src/Global_Parameters.h 
	$(CC) $(CFLAGS) -c vfsim-src/main.c -o main.o $(PROFILE)
	
clean::
	rm -f *.o

tags: vfsim-src/*.c vfsim-src/*.h
	ctags vfsim-src/*.c vfsim-src/*.h

mkellipse: mkellipse-src/mkellipse.c 
	$(CC) $(CFLAGS) mkellipse-src/mkellipse.c -o mkellipse -lm
