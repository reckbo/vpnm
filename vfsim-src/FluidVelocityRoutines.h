void ComputeBiotSavartIntegral(TN *, void *);
void ComputeFluidVelocityAtVoxels(TN *, Coordinates **, int);
void Accurate_NewFilament_Position(Coordinates **, Coordinates **, double, int);
