#include <stdlib.h>
#include <malloc.h>
#include <stdio.h>
#include <math.h>
#include "mymath.h"
#include "assert.h"
#include "voxel.h"
#include "VoxelTree.h"
#include "Global_Parameters.h"
#include "NarrowBand_RungeKutta.h"
#include "FluidVelocityRoutines.h"
#include "debug.h"
#define TRUE 1
#define FALSE 0

extern double *Rgdvl_dcoComputeDifferencesAtVoxel(VX *, char, int);

void
Test26VoxelNeighbours(TN *ptn, void *pv)
{
	VX *pvx = PvxFromPtn(ptn);
	VX **rgpvx = (VX **) PvGetVoxelData(pvx); 
	if (rgpvx == NULL)
		return;
	VX *pvxStart = PvxGetNeighbour(PvxGetNeighbour(PvxGetNeighbour(pvx, wyZMinus1), wyYMinus1), wyXMinus1); 
	VX *pvxX = pvxStart;
	VX *pvxY = pvxStart;
	VX *pvxZ = pvxStart;
	int k,j,i;
	for(k=0; k<3; k++)
		{
		for (j=0; j<3; j++)
			{
			for (i=0; i<3; i++)
				{
				if (rgpvx[i+3*j+9*k] != pvxX)
					{
					printf("26 neighbourhood test failed! rgpvx[%i] != pvxX", i+3*j+9*k);
					exit(0);
					}
				pvxX = PvxGetNeighbour(pvxX, wyXPlus1);
				}
			pvxY = PvxGetNeighbour(pvxY, wyYPlus1);
			pvxX = pvxY;
			}
		pvxZ = PvxGetNeighbour(pvxZ, wyZPlus1);
		pvxX = pvxZ;
		pvxY = pvxZ;
		}
}
void
Test6Neighbours(TN *ptn, void *pv)
{
	VX *pvx = PvxFromPtn(ptn);
	int x = pvx->rgco[0];
	int y = pvx->rgco[1];
	int z = pvx->rgco[2];

	TN *ptnRoot = (TN *) pv;
	VX *pvxNeigh = NULL;

	pvxNeigh = PvxGetNeighbour(pvx, wyXPlus1);
	assert(PvxFindVoxel(ptnRoot, x+1, y, z) == pvxNeigh);

	pvxNeigh = PvxGetNeighbour(pvx, wyXMinus1);
	assert(PvxFindVoxel(ptnRoot, x-1, y, z) == pvxNeigh);

	pvxNeigh = PvxGetNeighbour(pvx, wyYPlus1);
	assert(PvxFindVoxel(ptnRoot, x, y+1, z) == pvxNeigh);

	pvxNeigh = PvxGetNeighbour(pvx, wyYMinus1);
	assert(PvxFindVoxel(ptnRoot, x, y-1, z) == pvxNeigh);

	pvxNeigh = PvxGetNeighbour(pvx, wyZPlus1);
	assert(PvxFindVoxel(ptnRoot, x, y, z+1) == pvxNeigh);

	pvxNeigh = PvxGetNeighbour(pvx, wyZMinus1);
	assert(PvxFindVoxel(ptnRoot, x, y, z-1) == pvxNeigh);
}
void
NeighbourTest(TN *ptn, void *pv)
{
	VX *pvx = PvxFromPtn(ptn);
	VX **rgpvx = (VX **) PvGetVoxelData(pvx); 
	VX *pvx26Neigh = PvxGetNeighbour(PvxGetNeighbour(PvxGetNeighbour(pvx, wyZMinus1), wyYMinus1), wyXMinus1);
	VX *pvx26Neigh1 = PvxGetNeighbour(PvxGetNeighbour(pvx, wyYMinus1), wyZMinus1);
	VX *pvx26Neigh2 = PvxGetNeighbour(PvxGetNeighbour(PvxGetNeighbour(pvx, wyZMinus1), wyYMinus1), wyXPlus1);
	if (rgpvx != NULL && rgpvx[0] != pvx26Neigh && rgpvx[1] != pvx26Neigh1 && rgpvx[2] != pvx26Neigh2)
		{
		printf("pvx26Neigh: x=%i,y=%i,z=%i\n",XFromPvx(pvx26Neigh),YFromPvx(pvx26Neigh),ZFromPvx(pvx26Neigh));
		printf("rgpvx[0]:   x=%i,y=%i,z=%i\n",XFromPvx(rgpvx[0]),YFromPvx(rgpvx[0]),ZFromPvx(rgpvx[0]));
		}
}
void
FindTest(TN *ptn, void *pv)
{
	printf("voxel visit\n");
	VX *pvx = PvxFromPtn(ptn);
	int x = XFromPvx(pvx);
	int y = YFromPvx(pvx);
	int z = ZFromPvx(pvx);
	
	VX *pvxTest = (VX *) PvxFindVoxel(ptn, x, y, z); 
	if (pvxTest == NULL);
		{
		printf("Voxel couldn't find itself in tree\n");
		printf("x=%i, y=%i, z=%i\n",x, y, z);	
		exit(1);
		}
}
void
TestBlur(TN *ptn, void *pv)
{
	VX *pvx = PvxFromPtn(ptn);
	if (VlFromPvx(pvx)<=lCoreRadius*cvxResolution) 
		SetVoxelValue(pvx, 1);
	 else
		SetVoxelValue(pvx, 0); 
}
void
TestTreeContainment(TN *ptn, void *pv)
{
	TN *ptnRoot = (TN *) pv;
	VX *pvx = PvxFromPtn(ptn);
	VX *pvxNeigh = NULL;
	VX *pvxSav = NULL;
	int wy = 0;
	for(wy=0; wy<6; wy++)
		{
		pvxNeigh = PvxGetNeighbour(pvx, wy);
		if (pvxNeigh != NULL)
			{
			pvxSav = PvxFindVoxel(ptnRoot, XFromPvx(pvxNeigh), YFromPvx(pvxNeigh), ZFromPvx(pvxNeigh));
			if (pvxSav == NULL)
				printf("Voxel has a neighbour set thats not in the voxel tree\n");
			}
		}
}

void
SetTrialStep(TN *ptn, void *pv)
{
	VX *pvxCurrVoxel = PvxFromPtn(ptn);
	RK *prk = (RK *) PvGetVoxelData(pvxCurrVoxel);
	prk->vlTrialStep = VlFromPvx(pvxCurrVoxel);
}
void
CalculateGradient(TN *ptn, void *pv)
{
	VX *pvxCurrVoxel = PvxFromPtn(ptn);

	double *rgdvl_dcoCentral = Rgdvl_dcoComputeDifferencesAtVoxel(pvxCurrVoxel, '0', FALSE);
	//double *rgdvl_dcoCentral = Rgdvl_dcoComputeDifferencesAtVoxel(pvxCurrVoxel, '0', TRUE);
	
	RK *prk = (RK *) PvGetVoxelData(pvxCurrVoxel);

	if (rgdvl_dcoCentral == NULL)
		{
		prk->rgdvl_dt[1] = 0;
		return;
		}

	//prk->rgdvl_dt[1] = sqrt(cvxResolution*cvxResolution*(rgdvl_dcoCentral[0]*rgdvl_dcoCentral[0] + rgdvl_dcoCentral[1]*rgdvl_dcoCentral[1] + rgdvl_dcoCentral[2]*rgdvl_dcoCentral[2])/4);

	double gradient = sqrt(pow(rgdvl_dcoCentral[0]/(2/cvxResolution),2) + pow(rgdvl_dcoCentral[1]/(2/cvxResolution),2) + pow(rgdvl_dcoCentral[2]/(2/cvxResolution),2));
	//double gradient = sqrt(pow(rgdvl_dcoCentral[0]/2,2) + pow(rgdvl_dcoCentral[1]/2,2) + pow(rgdvl_dcoCentral[2]/2,2));

	printf("gradient=%f\n", gradient);
	//printf("gradient=%f\n", prk->rgdvl_dt[1]);
	free(rgdvl_dcoCentral);
}
void
SetGradient(TN *ptn, void *pv)
{
	VX *pvx = PvxFromPtn(ptn);
	RK *prk = (RK *) PvGetVoxelData(pvx);	

	SetVoxelValue(pvx, prk->rgdvl_dt[1]);
}
void
Get26VoxelNeighbours2(TN *ptn, VX *pvx, VX **rgpvx)
{
	int x = XFromPvx(pvx);
	int y = YFromPvx(pvx);
	int z = ZFromPvx(pvx);

	VX *pvx26Neigh = NULL;
	int k,j,i;
	for(k=0; k<3; k++)
		for (j=0; j<3; j++)
			for (i=0; i<3; i++)
				{
				pvx26Neigh = PvxFindVoxel(ptn, x-1+i,y-1+j,z-1+k);
				rgpvx[i+3*j+9*k] = pvx26Neigh;
				}
}
void
BoundaryVoxelTest(TN *ptn, void *pv)
{
	VX *pvx = PvxFromPtn(ptn);
	TN *ptnRoot = (TN *) pv;

	if (!FVoxelHasAllNeighbours(pvx))
		{
		SetVoxelValue(pvx, 500);

		VX **rgpvx = (VX **) malloc(sizeof(VX *) * 27);
		Get26VoxelNeighbours2(ptnRoot, pvx, rgpvx);
		//VX *pvxNeighToCopy = NULL;
		//double vlMin = 1e9;
		int ipvx = 0;
//
		for (ipvx=0; ipvx<27; ipvx++)
		{
			//if (rgpvx[ipvx] != NULL && FVoxelHasAllNeighbours(rgpvx[ipvx]) && VlFromPvx(rgpvx[ipvx]) < vlMin) 
			if (rgpvx[ipvx] != NULL && !FVoxelHasAllNeighbours(rgpvx[ipvx]) )
				{
				SetVoxelValue(rgpvx[ipvx], 1000);
				//pvxNeighToCopy = rgpvx[ipvx];
				//vlMin = VlFromPvx(pvxNeighToCopy);
				}
			}

		//free(rgpvx);
//
		//if (pvxNeighToCopy != NULL)
			//{	
			//RK *prkNeighToCopy = (RK *) PvGetVoxelData(pvxNeighToCopy);
			//prk->rgdvl_dt[2] = prkNeighToCopy->rgdvl_dt[2];
			//}
		//else
			//prk->rgdvl_dt[2] = 500;
		}

}
void
ExclusivityTest(TN *ptn, void *pv)
{
	TN *ptnOther = (TN *) pv;
	VX *pvx = PvxFromPtn(ptn);
	assert(!PvxFindVoxel(ptnOther, XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx)));
}
void
BoundaryTest(TN *ptn, void *pv)
{
	//TN *ptnBoundary = (TN *) pv;
	VX *pvx = PvxFromPtn(ptn);
	double *rgdvl_dcoCentral = Rgdvl_dcoComputeDifferencesAtVoxel(pvx, '0', 1);
	SetVoxelValue(pvx, rgdvl_dcoCentral[0]);
	//int wy=0;
	//for (wy=0; wy<6; wy++)
		//{
		//if (!PvGetVoxelData(pvxNeigh))
		//}
}
void
Neighbour26Test(TN *ptn, void *pv)
{
	printf("in test\n");
	TN *ptnRoot = (TN *) pv;

	VX *pvx = PvxFromPtn(ptn);
	if (TgFromPvx(pvx) == tgBoundary);
	return;


	VX *pvxStart = PvxGetNeighbour(PvxGetNeighbour(PvxGetNeighbour(pvx, wyZMinus1), wyYMinus1), wyXMinus1); 
	VX *pvxFind = PvxFindVoxel(ptnRoot,XFromPvx(pvx)-1, YFromPvx(pvx)-1, ZFromPvx(pvx)-1); 


	printf("v:x=%i,y=%i,z=%i\n",XFromPvx(pvx),YFromPvx(pvx),ZFromPvx(pvx));
	printf("s:x=%i,y=%i,z=%i\n",XFromPvx(pvxStart),YFromPvx(pvxStart),ZFromPvx(pvxStart));
	printf("f:x=%i,y=%i,z=%i\n\n",XFromPvx(pvxFind),YFromPvx(pvxFind),ZFromPvx(pvxFind));
	assert(pvxFind == pvxStart);


	VX **rgpvx2 = (VX **) malloc(sizeof(VX *)*27);
	VX **rgpvx3 = (VX **) malloc(sizeof(VX *)*27);
	//Get26VoxelNeighbours3(pvx, rgpvx3);
	Get26VoxelNeighbours2(ptnRoot, pvx, rgpvx2);
	int ipvx;
	for (ipvx=0; ipvx<27; ipvx++)
		{
		if (rgpvx3[ipvx] != rgpvx2[ipvx])
			{
			printf("failed on ipvx=%i\n", ipvx);
			exit(0);
			}
		}
}
void
TestObjectForHoles_6Neighbours(TN *ptn, void *pv)
{
	VX *pvx = PvxFromPtn(ptn);
	int wy;
	//printf("Checking neighbours..\n");
	for (wy=0; wy<6; wy++)
		{
		if (PvxGetNeighbour(pvx, wy) == NULL) //if its a boundary voxel
			{
			assert(double_EQ(pvx->vl, 0.0)); 
			}
		}
}
void
TestObjectForHoles_26Neighbours(TN *ptn, void *pv)
{
	TN *ptnRoot = (TN *) pv;
	VX *pvx = PvxFromPtn(ptn);

	VX **rgpvx = (VX **) PvGetVoxelData(pvx); 

	if (rgpvx == NULL)
		{
			if (!double_EQ(pvx->vl, 0.0) )
				printf("pvx->vl is %f and rgpvx is NULL!\n", pvx->vl);
			return;
		}

	VX *pvxN = NULL;
	int k,j,i;
	for(k=0; k<3; k++)
		for (j=0; j<3; j++)
			for (i=0; i<3; i++)
				{
				//if (TgFromPvx(rgpvx[i+3*j+9*k]) == 0 && double_EQ(pvx->vl, 0.0)) 
				pvxN = rgpvx[i+3*j+9*k];
				if (TgFromPvx(pvxN) == 0 && pvx->vl < -1.8) 
					{
					printf("26-neighbour is set as BACKGROUND, voxel value is %f\n", pvx->vl);
					printf("26-neighbour  voxel value is %f\n", pvxN->vl);
					assert(PvGetVoxelData(pvxN) == NULL);
					assert(PvxFindVoxel(ptnRoot, XFromPvx(pvxN), YFromPvx(pvxN), ZFromPvx(pvxN)) != NULL); 
					}
				}
}
