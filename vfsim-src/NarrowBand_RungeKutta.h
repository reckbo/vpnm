typedef struct double_coordinates
{
  double xcord;
  double ycord;
  double zcord;

  double dxc_ds;
  double dyc_ds;
  double dzc_ds;

  double ho2;
  //double rdfxdr[4];
  double sigma;
  int iFilament;
}Coordinates;
 
//typedef struct double_coordinates
//{
 // double xcord;
  //double ycord;
  //double zcord;
//}double_Coordinates;


typedef struct RK RK;
struct RK
{
	double vlTrialStep;
	double rgdvl_dt[5]; //Runge-Kutta derivatives
	double rgdco_dtFluidVelocity[3][3];
	double rgdco_dtNormalVelocity[3][3];
	double rgdvl_dcoGradient[3][3];
	VX *pvxNeighToCopy;
};

void RunVortexMethod(Coordinates **P);
void RunHybridMethod(Coordinates **P);
void RunDebug(Coordinates **P);
double lComputeNodeDistance(Coordinates , Coordinates );
