#include <IM.h>
#include <math.h>
#include <assert.h>
#include <malloc.h>
#include "Global_Parameters.h"
#include "voxel.h"
#include "VoxelTree.h"
#include "list.h"
#include "3Dskel.h"
//#include "fibheap.h"
#include "heap.h"
#include "thin_ryan.h"
#include "debug.h"


#define WHITE 0
#define GRAY  1
#define BLACK 2

list Neighbours26[27];
list Neighbours6[27];
//TN *ptnTreeRoot = NULL;
int skelCount = 0;

/*Debug*/
int cvxBackgroundVoxels = 0;
int cvxBackgroundObject = 0;

static int planes[9][8][3]= {
{ {0, -1, -1}, {0, 0, -1}, {0, 1, -1}, {0, 1, 0},
  {0, 1, 1},   {0, 0, 1}, {0, -1, 1},  {0, -1, 0}
},

{ {-1, 0, -1}, {0, 0, -1}, {1, 0, -1}, {1, 0, 0},  
  {1, 0, 1},   {0, 0, 1}, {-1, 0, 1},  {-1, 0, 0}
},

{
{-1, -1, 0}, {0, -1, 0}, {1, -1, 0}, {1, 0, 0},
{1, 1, 0}, {0, 1, 0}, {-1, 1, 0}, {-1, 0, 0}
},

{
{-1, -1, -1}, {0, 0, -1}, {1, 1, -1}, {1, 1, 0},
{1, 1, 1}, {0, 0, 1}, {-1, -1, 1},{-1, -1, 0} 
},

{
{1, -1, -1}, {0, 0, -1}, {-1, 1, -1}, {-1, 1, 0},
{-1, 1, 1}, {0, 0, 1}, {1, -1, 1}, {1, -1, 0}
},

{
{-1, -1, -1}, {-1, 0, -1}, {-1, 1, -1}, {0, 1, 0}, 
{1, 1, 1}, {1, 0, 1}, {1, -1, 1}, {0, -1, 0}
},

{
{1, -1, -1}, {1, 0, -1}, {1, 1, -1}, {0, 1, 0}, 
{-1, 1, 1},  {-1, 0, 1}, {-1, -1, 1},{0, -1, 0}
},

{
{-1, -1, -1},{0, -1, -1},{1, -1, -1},{1, 0, 0},
{1, 1, 1}, {0, 1, 1}, {-1, 1, 1}, {-1, 0, 0}
},

{
{-1, 1, -1},{0, 1, -1},{1, 1, -1},{1, 0, 0},
{1, -1, 1},{0, -1, 1},{-1, -1, 1}, {-1, 0, 0}
}
};




/*--------------------------------------------------------------------------
  Print information about a Graph
--------------------------------------------------------------------------*/
void printGraph(list *graph) 
{
  int i,j,k;
  list temp;
  
  for(i=0; i<3; i++) {
    for (j=0; j<3; j++) {
      for (k=0; k<3; k++) {
	printf("(%d %d %d):\n -> ", i,j,k);
	temp = graph[i+3*j+9*k];
	while (temp!=NULL) {
	  printf ("(%d %d %d) ", temp->x, temp->y, temp->z);
	  temp = temp->next;
	}
	printf("\n");
      }
    }
  }
}
/*--------------------------------------------------------------------------
  Free the memory allocated for the creation of a graph
--------------------------------------------------------------------------*/
void freeGraph(list *graph) 
{
  int i;
  list temp;

  for(i=0; i<27; i++) {
    while (graph[i]!=NULL) {
      temp = graph[i];
      graph[i]=deleteCell(temp, graph[i]);
      free(temp);
    }
  }
}  

/*--------------------------------------------------------------------------
  Create the list of all possible Neighbours (in a 26 connected sense)
  for each node in a 3x3x3 lattice
--------------------------------------------------------------------------*/
void createNeighbours()
{
  int k,j,i;

  int n,m,l;
  cell *aCell;
  double distance;
  int d2;

  for (k=0; k<3; k++) {
    for (j=0; j<3; j++) {
      for (i=0; i<3; i++) {
	Neighbours26[i+3*j+9*k]=createList();
	for (n=0; n<3; n++) {
	  for (m=0; m<3; m++) {
	    for (l=0; l<3; l++) {
	      distance = sqrt((double)((n-k)*(n-k) + (m-j)*(m-j) + (l-i)*(l-i)));
	      if ((distance < 2.0) && (distance > 0.0)){
		aCell = (cell *) malloc(sizeof(cell));
		assert(aCell!=NULL);
		aCell->x = l;
		aCell->y = m;
		aCell->z = n;
		aCell->value=l+3*m+9*n;
		aCell->prev = NULL;
		aCell->next = NULL;
		Neighbours26[i+3*j+9*k]=insertCell(aCell, Neighbours26[i+3*j+9*k]);
	      }
	    }
	  }
	}
	Neighbours6[i+3*j+9*k]=createList();
	if( !(((i==0)&&(j==0)&&(k==0))||
	      ((i==0)&&(j==0)&&(k==2))||
	      ((i==0)&&(j==2)&&(k==0))||
	      ((i==0)&&(j==2)&&(k==2))||
	      ((i==2)&&(j==0)&&(k==0))||
	      ((i==2)&&(j==0)&&(k==2))||
	      ((i==2)&&(j==2)&&(k==0))||
	      ((i==2)&&(j==2)&&(k==2)))){
	  for (n=0; n<3; n++) {
	    for (m=0; m<3; m++) {
	      for (l=0; l<3; l++) {
		if( !(((l==0)&&(m==0)&&(n==0))||
		      ((l==0)&&(m==0)&&(n==2))||
		      ((l==0)&&(m==2)&&(n==0))||
		      ((l==0)&&(m==2)&&(n==2))||
		      ((l==2)&&(m==0)&&(n==0))||
		      ((l==2)&&(m==0)&&(n==2))||
		      ((l==2)&&(m==2)&&(n==0))||
		      ((l==2)&&(m==2)&&(n==2)))){	
		  d2 = ((n-k)*(n-k) + (m-j)*(m-j) + (l-i)*(l-i));
		  if (d2==1){
		    aCell = (cell *) malloc(sizeof(cell));
		    assert(aCell!=NULL);
		    aCell->x = l;
		    aCell->y = m;
		    aCell->z = n;
		    aCell->value=l+3*m+9*n;
		    aCell->prev = NULL;
		    aCell->next = NULL;
		    Neighbours6[i+3*j+9*k]=insertCell(aCell, Neighbours6[i+3*j+9*k]);
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
}

void
Get26VoxelNeighbours_UsingFind(VX *pvx, VX **rgpvx, TN **pptnTreeRoot)
{
	//printf("here\n");
	if (*pptnTreeRoot == NULL)
		printf("tree is empty\n");

	//int x = XFromPvx(pvx);
	//int y = YFromPvx(pvx);
	//int z = ZFromPvx(pvx);
	int x = pvx->rgco[0];
	int y = pvx->rgco[1];
	int z = pvx->rgco[2];
	VX *pvx26Neigh = NULL;

	int k,j,i;
	for(k=0; k<3; k++)
		for (j=0; j<3; j++)
			for (i=0; i<3; i++)
				{
				pvx26Neigh = PvxFindVoxel(*pptnTreeRoot, x-1+i,y-1+j,z-1+k);
				if (pvx26Neigh == NULL)
					{
					pvx26Neigh = PvxCreateVoxel(x-1+i,y-1+j,z-1+k, 0.0);
					/* Debug */
					//pvx26Neigh = PvxCreateVoxel(x-1+i,y-1+j,z-1+k, 3.0);
					/* Debug */
					SetVoxelTag(pvx26Neigh, BACKGROUND);
					CreateLinksWithVoxelsInTree(*pptnTreeRoot, pvx26Neigh);
					InsertVoxel(pptnTreeRoot, pvx26Neigh);
					//printf("Creating background voxel in thinning code.\n");
					//printf("voxel does not have all of its 26 neighbours set. Exiting.\n");
					//exit(1);
					}
				rgpvx[i+3*j+9*k] = pvx26Neigh;
				}
}

void
Get26VoxelNeighbours_UsingPointers(VX *pvx, VX **rgpvx, TN **pptnTreeRoot)
{
	VX *pvxStart = PvxGetNeighbour(PvxGetNeighbour(PvxGetNeighbour(pvx, wyZMinus1), wyYMinus1), wyXMinus1); 
	VX *pvxX = pvxStart;
	VX *pvxY = pvxStart;
	VX *pvxZ = pvxStart;

	int x = pvx->rgco[0];
	int y = pvx->rgco[1];
	int z = pvx->rgco[2];

	int k,j,i;
	for(k=0; k<3; k++)
		{
		for (j=0; j<3; j++)
			{
			for (i=0; i<3; i++)
				{
				if (pvxX == NULL)
					{					
					assert(PvxFindVoxel(*pptnTreeRoot, x-1+i, y-1+j, z-1+k) == NULL);
					pvxX = PvxCreateVoxel(x-1+i,y-1+j,z-1+k, 0.0);
					SetVoxelTag(pvxX, BACKGROUND);
					CreateLinksWithVoxelsInTree(*pptnTreeRoot, pvxX);
					InsertVoxel(pptnTreeRoot, pvxX);
					}
				rgpvx[i+3*j+9*k] = pvxX;
				pvxX = PvxGetNeighbour(pvxX, wyXPlus1);
				}
			pvxY = PvxGetNeighbour(pvxY, wyYPlus1);
			pvxX = pvxY;
			}
		pvxZ = PvxGetNeighbour(pvxZ, wyZPlus1);
		pvxX = pvxZ;
		pvxY = pvxZ;
		}
}


/*--------------------------------------------------------------------------
  Create the graph representing the object in a 3x3x3 neighbourhood
  Returns the number of nodes in the graph
--------------------------------------------------------------------------*/
int createObjectGraph(VX *pvx, list *objectGraph) 
{
  int k,j,i;
  int count=0;
  list temp;
  cell *aCell;

  VX **rgpvx = (VX **) PvGetVoxelData(pvx);
  //VX *rgpvx[27];
  //Get26VoxelNeighbours(pvx, rgpvx);
  //VX *pvxNeigh = PvxGetNeighbour(PvxGetNeighbour(PvxGetNeighbour(pvx, wyXMinus1), wyMinus1), wyZMinus1); 
  for(k=0; k<3; k++)
	  for(j=0; j<3; j++)
		  for(i=0; i<3; i++)
			  {
			  /* Check if the point belongs to object */
			  if (TgFromPvx(rgpvx[i+3*j+9*k]) != BACKGROUND)
				  {
				  count++;
				  temp = Neighbours26[i+3*j+9*k];
				  /* Look for neighbours belonging to the object */
				  while (temp!=NULL) 
					  { 
					  if (TgFromPvx(rgpvx[(int)temp->value]) != BACKGROUND)
						  {
						  //printf("just before malloc, i=%i, j=%i, k=%i, index=%i\n",i,j,k,(i+3*j+9*k));
						  aCell = (cell *) malloc(sizeof(cell));
						  assert(aCell!=NULL);
						  aCell->x = temp->x;
						  aCell->y = temp->y;
						  aCell->z = temp->z;
						  aCell->value= temp->value;
						  aCell->prev = NULL;
						  aCell->next = NULL;
						  objectGraph[i+3*j+9*k] = insertCell(aCell,objectGraph[i+3*j+9*k]);
						  }
					  temp = temp->next;
					  }
				  }
			  else
				  objectGraph[i+3*j+9*k]=NULL;
			  }
  //free(rgpvx);

  //printf("finished\n");
  return count;
}
/*--------------------------------------------------------------------------
  Check if the neighbour is a background 6 neighbour
--------------------------------------------------------------------------*/
int checkValid(VX **rgpvx, int i,int j, int k) 
{
  int dist;
  int a,b,c;


  a = 1; b = 1; c = 0; 
  dist = (a-i)*(a-i)+(b-j)*(b-j)+(c-k)*(c-k);
  if ((dist<=1)&&(TgFromPvx(rgpvx[a+3*b+9*c])==BACKGROUND))
    return TRUE;

  a = 1; b = 1; c = 2; 
  dist = (a-i)*(a-i)+(b-j)*(b-j)+(c-k)*(c-k);
  //if ((dist<=1)&&(VlGetVoxelValueXYZ(pvc, z-1+c, y-1+b, x-1+a)==BACKGROUND))
  if ((dist<=1)&&(TgFromPvx(rgpvx[a+3*b+9*c])==BACKGROUND))
    return TRUE;

  a = 1; b = 0; c = 1; 
  dist = (a-i)*(a-i)+(b-j)*(b-j)+(c-k)*(c-k);
  //if ((dist<=1)&&(VlGetVoxelValueXYZ(pvc, z-1+c, y-1+b, x-1+a)==BACKGROUND))
  if ((dist<=1)&&(TgFromPvx(rgpvx[a+3*b+9*c])==BACKGROUND))
    return TRUE;

  a = 1; b = 2; c = 1; 
  dist = (a-i)*(a-i)+(b-j)*(b-j)+(c-k)*(c-k);
  //if ((dist<=1)&&(VlGetVoxelValueXYZ(pvc, z-1+c, y-1+b, x-1+a)==BACKGROUND))
  if ((dist<=1)&&(TgFromPvx(rgpvx[a+3*b+9*c])==BACKGROUND))
    return TRUE;

  a = 0; b = 1; c = 1; 
  dist = (a-i)*(a-i)+(b-j)*(b-j)+(c-k)*(c-k);
  //if ((dist<=1)&&(VlGetVoxelValueXYZ(pvc, z-1+c, y-1+b, x-1+a)==BACKGROUND))
  if ((dist<=1)&&(TgFromPvx(rgpvx[a+3*b+9*c])==BACKGROUND))
    return TRUE;

  a = 2; b = 1; c = 1; 
  dist = (a-i)*(a-i)+(b-j)*(b-j)+(c-k)*(c-k);
  //if ((dist<=1)&&(VlGetVoxelValueXYZ(pvc, z-1+c, y-1+b, x-1+a)==BACKGROUND))
  if ((dist<=1)&&(TgFromPvx(rgpvx[a+3*b+9*c])==BACKGROUND))
    return TRUE;

  return FALSE;
}
/*--------------------------------------------------------------------------
  Create the graph representing the background in a 3x3x3
  neighbourhood Returns the number of nodes in the graph
--------------------------------------------------------------------------*/
int createBackgroundGraph(VX *pvx, list *backgroundGraph) 
{
	int k,j,i;
	int count=0;
	list temp;
	cell *aCell;

	VX **rgpvx = (VX **) PvGetVoxelData(pvx);
	assert(rgpvx != NULL);

	for (k=0; k<3; k++) 
		for (j=0; j<3; j++) 
			for (i=0; i<3; i++) 
				{
				/* Check if the point belongs to object */
				if( !(((i==0)&&(j==0)&&(k==0))||
							((i==0)&&(j==0)&&(k==2))||
							((i==0)&&(j==2)&&(k==0))||
							((i==0)&&(j==2)&&(k==2))||
							((i==2)&&(j==0)&&(k==0))||
							((i==2)&&(j==0)&&(k==2))||
							((i==2)&&(j==2)&&(k==0))||
							((i==2)&&(j==2)&&(k==2))))
					{
					if ( TgFromPvx(rgpvx[i+3*j+9*k]) == BACKGROUND ) 
						{
						if (checkValid(rgpvx, i,j,k) == TRUE)
							count++;
						temp = Neighbours6[i+3*j+9*k];
						/* Look for neighbours belongin to the object */
						while (temp!=NULL) 
							{ 
							if (TgFromPvx(rgpvx[(int)temp->value]) == BACKGROUND) 
								{
								aCell = (cell *) malloc(sizeof(cell));
								assert(aCell!=NULL);
								aCell->x = temp->x;
								aCell->y = temp->y;
								aCell->z = temp->z;
								aCell->value= temp->value;
								aCell->prev = NULL;
								aCell->next = NULL;
								backgroundGraph[i+3*j+9*k]=insertCell(aCell,backgroundGraph[i+3*j+9*k]);
								}
							temp = temp->next;
							}
						}
					else
						backgroundGraph[i+3*j+9*k] = NULL;
					}
				}
	return count;
}

/*--------------------------------------------------------------------------
  Visit a node in a graph using a Depth First Strategy It has been
  modified to only count the number of nodes seen starting from the
  initial node
--------------------------------------------------------------------------*/
void DFSVisit( int node, list *adj, int *color, int *count) 
{
  list temp=adj[node];
  color[node] = GRAY;
  (*count)++;
  while (temp!=NULL) {
    if (color[(int)temp->value] == WHITE) {
      DFSVisit((int)temp->value, adj, color, count);
    }
    temp = temp->next;
  }
  color[node] = BLACK;
} 
/*--------------------------------------------------------------------------
  Check if a graph is connected looks for the first node which has
  neighbours in the graph return true if the number of nodes visited
  through this node is equal the total number of nodes in the graph
--------------------------------------------------------------------------*/
int checkConnected(int count, list *graph) 
{

  int i;
  int color[27];
  int visited=0;

  for (i=0; i<27; i++)
    color[i] = WHITE;

  /* find the first node which has neighbours */ 
  i=0;
  while ((i<27)&&(graph[i]==NULL))
    i++; 
  
  if ((i<27)&&(graph[i]!=NULL))
    DFSVisit(i,graph, color, &visited);

  if ((visited == count)||(count==1))
    return TRUE;

  return FALSE;
}

/*--------------------------------------------------------------------------
  Check if the middle point can see the background 
--------------------------------------------------------------------------*/
int checkOne(VX *pvx) 
{
	if (TgFromPvx(PvxGetNeighbour(pvx, wyZMinus1)) != BACKGROUND &&
		TgFromPvx(PvxGetNeighbour(pvx, wyZPlus1 )) != BACKGROUND &&
		TgFromPvx(PvxGetNeighbour(pvx, wyYMinus1)) != BACKGROUND &&
		TgFromPvx(PvxGetNeighbour(pvx, wyYPlus1 )) != BACKGROUND &&
		TgFromPvx(PvxGetNeighbour(pvx, wyXMinus1)) != BACKGROUND &&
		TgFromPvx(PvxGetNeighbour(pvx, wyXPlus1 )) != BACKGROUND) 
			return FALSE;
	return TRUE;

	//if (TgGetVoxelTagXYZ(pvc, x, y, z-1) != BACKGROUND &&
	//	TgGetVoxelTagXYZ(pvc, x, y, z+1) != BACKGROUND &&
	//	TgGetVoxelTagXYZ(pvc, x, y-1, z) != BACKGROUND &&
	//	TgGetVoxelTagXYZ(pvc, x, y+1, z) != BACKGROUND &&
	//	TgGetVoxelTagXYZ(pvc, x-1, y, z) != BACKGROUND &&
	//	TgGetVoxelTagXYZ(pvc, x+1, y, z) != BACKGROUND) 
	//	return FALSE;
	//return TRUE;
}
/*--------------------------------------------------------------------------
  Check if the removal of the point changes connectedness of object
--------------------------------------------------------------------------*/
int checkTwo(VX *pvx) 
{
  int i,count;
  int tgSave = TgFromPvx(pvx);
  list objectGraph[27];
  int check;

  SetVoxelTag(pvx, BACKGROUND); 

  for (i=0; i<27; i++)
    objectGraph[i]=NULL;

  count = createObjectGraph(pvx, objectGraph);
  SetVoxelTag(pvx, tgSave);

  check = checkConnected(count,objectGraph);
  freeGraph(objectGraph);
  return check;
}
/*--------------------------------------------------------------------------
  Check if the removal of the point changes connectedness of background  
--------------------------------------------------------------------------*/
int checkThree(VX *pvx) 
{  
  int i,count;
  list backgroundGraph[27];
  int check;

  for (i=0; i<27; i++)
    backgroundGraph[i]=NULL;
  
  count = createBackgroundGraph(pvx,backgroundGraph);
  
  check = checkConnected(count,backgroundGraph);  
  freeGraph(backgroundGraph);
  return check;
}
/*--------------------------------------------------------------------------
  Check if a point is removable
--------------------------------------------------------------------------*/
int checkRemovable(VX *pvx) 
{
	//printf("checking if voxel is removable");
  if ( (checkOne(pvx)==TRUE) && (checkTwo(pvx)==TRUE) && (checkThree(pvx)==TRUE) )
    return TRUE;

  /* Debug */

  //if (pvx->vl > -2.0 && pvx->vl < -1.8)
	  //{
	  //printf("voxel value that's not removable: %f\n", pvx->vl);
	  //VX **rgpvx = (VX **) PvGetVoxelData(pvx); 
	  //if (rgpvx == NULL)
		  //{
		  //printf("rgpvx is NULL\n");
		  //}
	  //VX *pvxStart = PvxGetNeighbour(PvxGetNeighbour(PvxGetNeighbour(pvx, wyZMinus1), wyYMinus1), wyXMinus1); 
	  //VX *pvxX = pvxStart;
	  //VX *pvxY = pvxStart;
	  //VX *pvxZ = pvxStart;
	  //int k,j,i;
	  //for(k=0; k<3; k++)
		  //{
		  //for (j=0; j<3; j++)
			  //{
			  //for (i=0; i<3; i++)
				  //{
				  //if (rgpvx[i+3*j+9*k] != pvxX)
					  //{
					  //printf("26 neighbourhood test failed! rgpvx[%i] != pvxX", i+3*j+9*k);
					  //exit(0);
					  //}
				  //pvxX = PvxGetNeighbour(pvxX, wyXPlus1);
				  //}
			  //pvxY = PvxGetNeighbour(pvxY, wyYPlus1);
			  //pvxX = pvxY;
			  //}
		  //pvxZ = PvxGetNeighbour(pvxZ, wyZPlus1);
		  //pvxX = pvxZ;
		  //pvxY = pvxZ;
		  //}
	  ////printf("checkOne, checkTwo, checkThree: %i %i %i\n", checkOne(pvx), checkTwo(pvx), checkThree(pvx) );
	  //}
  return FALSE;
}
/*--------------------------------------------------------------------------
  Check if a point is a border point: 6-connected case
--------------------------------------------------------------------------*/
int checkBorder6(int x, int y, int z, FLOAT_TYPE ***data) 
{
  
  if ((data[z-1][y  ][x  ]==BACKGROUND)&&
      (data[z  ][y  ][x-1]==BACKGROUND)&&
      (data[z  ][y  ][x+1]==BACKGROUND))
    return TRUE;

  if ((data[z-1][y  ][x  ]==BACKGROUND)&&
      (data[z  ][y-1][x  ]==BACKGROUND)&&
      (data[z  ][y+1][x  ]==BACKGROUND))
    return TRUE;  

  if ((data[z+1][y  ][x  ]==BACKGROUND)&&
      (data[z  ][y  ][x-1]==BACKGROUND)&&
      (data[z  ][y  ][x+1]==BACKGROUND))
    return TRUE;      
  if ((data[z+1][y  ][x  ]==BACKGROUND)&&
      (data[z  ][y-1][x  ]==BACKGROUND)&&
      (data[z  ][y+1][x  ]==BACKGROUND))
    return TRUE;       

  if ((data[z  ][y-1][x  ]==BACKGROUND)&&
      (data[z  ][y  ][x-1]==BACKGROUND)&&
      (data[z  ][y  ][x+1]==BACKGROUND))
    return TRUE;
  if ((data[z  ][y-1][x  ]==BACKGROUND)&&
      (data[z-1][y  ][x  ]==BACKGROUND)&&
      (data[z+1][y  ][x  ]==BACKGROUND))
    return TRUE;  

  if ((data[z  ][y+1][x  ]==BACKGROUND)&&
      (data[z  ][y  ][x-1]==BACKGROUND)&&
      (data[z  ][y  ][x+1]==BACKGROUND))
    return TRUE;      
  if ((data[z  ][y+1][x  ]==BACKGROUND)&&
      (data[z-1][y  ][x  ]==BACKGROUND)&&
      (data[z+1][y  ][x  ]==BACKGROUND))
    return TRUE;       

  if ((data[z  ][y  ][x-1]==BACKGROUND)&&
      (data[z  ][y-1][x  ]==BACKGROUND)&&
      (data[z  ][y+1][x  ]==BACKGROUND))
    return TRUE;
  if ((data[z  ][y  ][x-1]==BACKGROUND)&&
      (data[z-1][y  ][x  ]==BACKGROUND)&&
      (data[z+1][y  ][x  ]==BACKGROUND))
    return TRUE;  

  if ((data[z  ][y  ][x+1]==BACKGROUND)&&
      (data[z  ][y-1][x  ]==BACKGROUND)&&
      (data[z  ][y+1][x  ]==BACKGROUND))
    return TRUE;      
  if ((data[z  ][y  ][x+1]==BACKGROUND)&&
      (data[z-1][y  ][x  ]==BACKGROUND)&&
      (data[z+1][y  ][x  ]==BACKGROUND))
    return TRUE;       
    
  return FALSE;
}
/*--------------------------------------------------------------------------
  Check if a point is a border point: 26-connected case
--------------------------------------------------------------------------*/
int checkBorder26(int x, int y, int z, FLOAT_TYPE ***data) 
{
  int i, j, count = 0;
  int X,Y,Z;
  
  for (j=0; j<9; j++) { 
    count = 0;
    for (i=0; i<8; i++) {
      X=x+planes[j][i][0]; 
      Y=y+planes[j][i][1]; 
      Z=z+planes[j][i][2];
      if (data[Z][Y][X]!=BACKGROUND)
	count++;
    }
    if (count==1) return TRUE;
  }
  return FALSE;
}
/*--------------------------------------------------------------------------
  Check if a point is a border point
--------------------------------------------------------------------------*/
int checkBorder(int connectedness, int x, int y, int z, FLOAT_TYPE ***data) 
{
  int i,j,k;
  int count = 0;

  for (i=-1; i<=1; i++)
    for (j=-1; j<=1; j++)
      for (k=-1; k<=1; k++)
	if (!((i==0)&&(j==0)&&(k==0)))
	  if (data[z+i][y+j][x+k]!=BACKGROUND)
	    count++;
  
  if (count==1) return FALSE;

  if (connectedness == 6)
    return checkBorder6(x,y,z,data);
  else
    return checkBorder26(x,y,z,data);
}
void
InsertRemovablePointsIntoHeap(TN *ptn, void *pvHeap)
{
	//fibheap_t fh = (fibheap_t) pvHeap;
	heap *fh = (heap *) pvHeap;
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	 
	if (TgFromPvx(pvx) == SKEL && checkRemovable(pvx) == TRUE)
		{
		//printf("inserting voxel\n");
		point aPoint;
		aPoint.x = XFromPvx(pvx);
		aPoint.y = YFromPvx(pvx);
		aPoint.z = ZFromPvx(pvx);
		aPoint.value= VlFromPvx(pvx);	 
		aPoint.pvx = pvx;
		//fibheap_insert(fh, VlFromPvx(pvx), pvx);
		//fibheap_insert(fh, (aPoint->value), aPoint);
		heapInsert(fh, aPoint);
		SetVoxelTag(pvx, INSERTED);

		///* debug */
		//VX *pvxF = PvxFindVoxel(ptn, aPoint.x, aPoint.y, aPoint.z);
		//assert(pvxF == pvx);
		//printf("checkOne=%i,checkTwo=%i,checkThree=%i\n", checkOne(pvx),checkTwo(pvx),checkThree(pvx));
		}
}
void
MarkSkelVoxels(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	SetVoxelTag(pvx, SKEL);
	skelCount++;

	//if (VlFromPvx(pvx) <= *pvlObjBoundary) 
		//SetVoxelTag(pvx, SKEL);
	//else
		//SetVoxelTag(pvx, BACKGROUND); 
}
void 
DebugFunction(TN *ptn, void *pv)
{
	//printf("x=%i \n",XFromPvx(PvxFromPtn(ptnTreeRoot)));
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	//VX *rgpvx[27];
	//if (TgFromPvx(pvx) == SKEL)
		//Get26VoxelNeighbours(pvx, rgpvx);
	if (checkRemovable(pvx) == TRUE)
		SetVoxelValue(pvx, 2);
	
	//checkRemovable(pvx);
}
void
Set26VoxelNeighbours_UsingFind(TN *ptn, void *pv)
{
	TN **pptnTreeRoot = (TN **) pv;
	if (*pptnTreeRoot == NULL)
		printf("tree is empty\n");

	VX **rgpvx = (VX **) malloc(sizeof(VX *)*27);
	assert(rgpvx != NULL);

	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	int x = pvx->rgco[0];
	int y = pvx->rgco[1];
	int z = pvx->rgco[2];
	VX *pvx26Neigh = NULL;

	int k,j,i;
	for(k=0; k<3; k++)
		for (j=0; j<3; j++)
			for (i=0; i<3; i++)
				{
				pvx26Neigh = PvxFindVoxel(*pptnTreeRoot, x-1+i,y-1+j,z-1+k);
				rgpvx[i+3*j+9*k] = pvx26Neigh;
				}
	SetVoxelData(pvx, rgpvx);
}
void
CreateAndSet26VoxelNeighbours(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	assert(pvx != NULL);
	if (pvx->tg != SKEL)
		{
		assert(pvx->tg == BACKGROUND);
		return;
		}

	VX **rgpvx = (VX **) malloc(sizeof(VX *)*27);
	assert(rgpvx != NULL);

	/* This function will create 26 neighbours if they do not exist. This creates
	 * a boundary of BACKGROUND voxels surrounding the core of voxels to be thinned.
	 */
	TN **pptnRoot = (TN **) pv;
	Get26VoxelNeighbours_UsingFind(pvx, rgpvx, pptnRoot);
	//Get26VoxelNeighbours_UsingPointers(pvx, rgpvx, pptnRoot);
	SetVoxelData(pvx, rgpvx);
}
void
GetSkelPoints(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	if (TgFromPvx(pvx) == SKEL) 
		{
		TN **pptnNewTree = (TN **) pv;
		//printf("x=%i,y=%i,z=%i\n",XFromPvx(pvx),YFromPvx(pvx),ZFromPvx(pvx));
		//if (checkRemovable(pvx) == TRUE)
		//	InsertVoxel(pptnNewTree, PvxCreateVoxel(XFromPvx(pvx),YFromPvx(pvx),ZFromPvx(pvx),1));
		VX *pvxNew = PvxCreateVoxel(XFromPvx(pvx),YFromPvx(pvx),ZFromPvx(pvx), 0.0);
		pvxNew->iFilament = pvx->iFilament;
		CreateLinksWithVoxelsInTree(*pptnNewTree, pvxNew);
		InsertVoxel(pptnNewTree, pvxNew);
		}
}
//void
//InsertBoundaryVoxels(TN *ptn, void *pv)
//{
	//TN **pptnBoundary = (TN **) pv;
	//VX *pvx= PvxFromPtn(ptn);	
//
	//int wy;
	//for(wy=0; wy<6; wy++)
		//{
		///* if boundary voxel */
		//if (PvxGetNeighbour(pvx, wy) == NULL)
			//{
			//InsertVoxel(pptnBoundary, pvx);
			//return;
			//}
		//}
//}
void
FindMinDist(TN *ptn, void *pv)
{
	/*VX *pvxBoundary = PvxFromPtn(ptn);	*/
	VX *pvxBoundary = (ptn == NULL) ? NULL : ptn->pvx;
	VX *pvx = (VX *) pv;
	double dxc = (double)(XFromPvx(pvxBoundary) - XFromPvx(pvx));
	double dyc = (double)(YFromPvx(pvxBoundary) - YFromPvx(pvx));
	double dzc = (double)(ZFromPvx(pvxBoundary) - ZFromPvx(pvx));
	double vlDist = sqrt((dxc*dxc) + (dyc*dyc) + (dzc*dzc));
	if(vlDist < VlFromPvx(pvx))
		SetVoxelValue(pvx, vlDist);
}
void
ComputeDTFromBoundary(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	SetVoxelValue(pvx, 1e7);
	TN *ptnBoundary = (TN *) pv;
	TraverseTree(ptnBoundary, &FindMinDist, pvx);
}
void
CheckStorage(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	if (PvGetVoxelData(pvx) == NULL && TgFromPvx(pvx) == SKEL)
		{
		printf("voxel doesn't have data set\n");
		printf("x=%i \n",XFromPvx(pvx));
		}
}
void
WriteVoxelTreeT2(TN *ptn, char *szPrefix, double t)
{
	char szFilename[40];
	sprintf(szFilename, "%s-%04i.raw", szPrefix, 0); 
	printf("Saving %s\n", szFilename);
	WriteVoxelTree(ptn, szFilename);
}
int
CvxGetNumberOfSkelNeighbours(VX *pvx)
{
	VX **rgpvx = (VX **) PvGetVoxelData(pvx);
	int cvx = 0;
	int ipvx=0;
	/* for each 26 neighbour */
	for (ipvx=0; ipvx<27; ipvx++)
		{
		if ((rgpvx[ipvx])->tg == SKEL && rgpvx[ipvx] != pvx)
			cvx++;
		}
	return cvx;
}
void
PrintSkelNeighbours(VX *pvx)
{
	VX **rgpvx = (VX **) PvGetVoxelData(pvx);
	int ipvx=0;
	/* for each 26 neighbour */
	for (ipvx=0; ipvx<27; ipvx++)
		{
		if ((rgpvx[ipvx])->tg == SKEL && rgpvx[ipvx] != pvx)
			{
			printf(" 	(SKEL Neighbour coord: (%i, %i, %i), tg == %i\n", XFromPvx(rgpvx[ipvx]), YFromPvx(rgpvx[ipvx]), ZFromPvx(rgpvx[ipvx]),(rgpvx[ipvx])->tg);
			}
		if ((rgpvx[ipvx])->tg == INSERTED && rgpvx[ipvx] != pvx)
			{
			printf(" 	(INSERTED Neighbour coord: (%i, %i, %i), tg == %i\n", XFromPvx(rgpvx[ipvx]), YFromPvx(rgpvx[ipvx]), ZFromPvx(rgpvx[ipvx]),(rgpvx[ipvx])->tg);
			}
		}
}
void
FindJunctions2(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;

	if (pvx->tg == BACKGROUND)
		return;
	assert(pvx->tg == SKEL);

	/* return if voxel is a filament branch voxel */
	int cvx = CvxGetNumberOfSkelNeighbours(pvx);
	if (cvx == 2)
		return;
	if (cvx > 2)
		{
		printf("Found voxel with %i SKEL neighbours: tag == %i and checkRemovable(pvx) == %i\n", cvx, pvx->tg, checkRemovable(pvx));
		printf("voxel coordinates: %i %i %i \n", XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx));
		PrintSkelNeighbours(pvx);
		}
	if (CvxGetNumberOfSkelNeighbours(pvx) == 1)
		printf("Found voxel with 1 neighbour: tag == %i and checkRemovable(pvx) == %i\n", pvx->tg, checkRemovable(pvx));
}
void
InsertBackgroundVoxels(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	if (pvx->tg == BACKGROUND)
		{
		TN **pptnBackgroundVoxels = (TN **) pv;
		VX *pvxNew = PvxCreateVoxel(XFromPvx(pvx),YFromPvx(pvx),ZFromPvx(pvx), 0.0);
		pvxNew->tg = 0;
		CreateLinksWithVoxelsInTree(*pptnBackgroundVoxels, pvxNew);
		InsertVoxel(pptnBackgroundVoxels, pvx);
		cvxBackgroundVoxels++;
		}
}
void
TraverseObject(VX *pvx)
{
	if(pvx->tg == 1)
		return;
	pvx->tg = 1;
	printf("Object voxel (%i, %i, %i) marked\n", XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx));
	cvxBackgroundObject++;
	VX *pvxNeighbour = NULL;
	int wy;
	for(wy=0; wy<6; wy++)
		{
		pvxNeighbour = PvxGetNeighbour(pvx, wy);
		if (pvxNeighbour != NULL && pvxNeighbour->tg == 0)
			{
			TraverseObject(pvxNeighbour);
			}
		}
}
void
thin(TN **pptn)
{
   /* Create adjacency graph of a 26-connected 3x3x3 neighbourhood */
   createNeighbours();

   /* Initialize the Heap */
   heap Skeleton;
   Skeleton.size = 0;
   Skeleton.keys = (point *) malloc (sizeof(point)*50000);
   assert(Skeleton.keys!=NULL);
   
   /* Set/create 26 neighbours of the skeleton voxels */
   TN *ptn = *pptn;
   skelCount = 0;
   TraverseTree(ptn, &MarkSkelVoxels, NULL);

   /* Debug */
   printf("\t DEBUG: Counted %i object voxels\n", skelCount); 
   /* Debug */

   printf("\t Storing pointers to 26 neighbours at each voxel, and creating neighbours at boundary voxels...");
   TraverseTree(ptn, &CreateAndSet26VoxelNeighbours, pptn);
   printf("done.\n");

   /* Debug */
   //WriteVoxelTreeT2(*pptn, "ptnSS", 0.0);
   //TN *ptnBackgroundVoxels = NULL;
   //printf("Inserting background voxels into background voxel tree...");
   //TraverseTree(ptn, &InsertBackgroundVoxels, &ptnBackgroundVoxels);
   //TraverseTree(ptn, &Set26VoxelNeighbours_UsingFind, &ptnBackgroundVoxels);
   //printf("done.\n");
   //TraverseObject(PvxFromPtn(ptnBackgroundVoxels));
   //printf("Total number of background voxels: %i\n", cvxBackgroundVoxels);
   //printf("Total number of object voxels: %i\n", cvxBackgroundObject);
   //WriteVoxelTree(ptnBackgroundVoxels, "ptnSS_background_voxels.raw");
   TraverseTree(ptn, &Test26VoxelNeighbours, NULL);
   //TraverseTree(ptn, &TestObjectForHoles_26Neighbours, ptn);
   /* End Debug */

   /* Insert removable skeleton voxels into the heap */
   TraverseTree(ptn, &InsertRemovablePointsIntoHeap, &Skeleton);
   
   /* Start the removal of points from skeleton Heap */
   printf("Removing voxels from filament tube(s)..."); 
   int Count  = 0;
   VX *pvx = NULL;
   VX *pvx26Neigh = NULL;
   point aPoint; 
   point newPoint;
   
   int k,j,i;
   while (Skeleton.size != 0) 
	   {
	   printf("in while loop\n");
	   aPoint = heapExtractMax(&Skeleton);
	   //aPoint = (point *) fibheap_extract_min(fhSkeleton);
	   //pvx = (VX *) fibheap_extract_min(fhSkeleton);

	   /* Check if the point is Simple */
	   //pvx = PvxFindVoxel(ptn, aPoint.x, aPoint.y, aPoint.z);
	   pvx = aPoint.pvx;
	   assert(pvx->tg == INSERTED);
	   VX **rgpvx = (VX **) PvGetVoxelData(pvx);
	   assert(rgpvx != NULL);
	   /* Debug */
	   if (XFromPvx(pvx) == 1 && YFromPvx(pvx) == 178 && ZFromPvx(pvx) == 131)
		   {
	   	   printf("\nNumber of SKEL neighbours = %i\n", CvxGetNumberOfSkelNeighbours(pvx));
		   printf("Tracked voxel: checkOne == %i checkTwo == %i checkThree == %i\n", checkOne(pvx), checkTwo(pvx), checkThree(pvx));
		   PrintSkelNeighbours(pvx);
		   }
	   /* Debug */
	   if(checkRemovable(pvx) == TRUE)
		   {
		   /* Finally, we can remove the point! */
		   SetVoxelTag(pvx, BACKGROUND);
		   Count++;
		   //printf("vl=%f\n",VlFromPvx(pvx));
		   //Z = aPoint.z;
		   //Y = aPoint.y;
		   //X = aPoint.x;
		   /* Add removable neighbours of the point we just removed in the heap */
		   for(k=0; k<3; k++)
			   for (j=0; j<3; j++)
				   for (i=0; i<3; i++)
					   {
					   //pvxNeigh = PvxGetNeighbour(pvx, wy);
					   pvx26Neigh = rgpvx[i+3*j+9*k];
					   if (TgFromPvx(pvx26Neigh) == SKEL && checkRemovable(pvx26Neigh)==TRUE) 
						   {
						   newPoint.x = XFromPvx(pvx26Neigh);
						   newPoint.y = YFromPvx(pvx26Neigh);
						   newPoint.z = ZFromPvx(pvx26Neigh);
						   newPoint.value=  VlFromPvx(pvx26Neigh);
						   newPoint.pvx = pvx26Neigh;
						   heapInsert(&Skeleton, newPoint);
						   //fibheap_insert(fhSkeleton, newPoint->value, pvxNeigh);
						   //fibheap_insert(fhSkeleton, newPoint->value, newPoint);
						   SetVoxelTag(pvx26Neigh, INSERTED);
						   }
					   }
		   }
	   else 
		   {
		   SetVoxelTag(pvx, SKEL);
		   //if (pvx->vl > -1.5)
			   //printf("A non removable voxel has value %f\n", pvx->vl);
		   }
	   }

   assert(Skeleton.size == 0);

   free(Skeleton.keys);

   TN *ptnNewTree = NULL;
   TraverseTree(ptn, &GetSkelPoints, &ptnNewTree);
   printf("done. (%d voxels removed)\n",Count);

   /* Debug */
   TraverseTree(ptn, &FindJunctions2, NULL);
   /* End Debug */

   FreeTreeAndVoxels(ptn);
   ptn = NULL;
	
   *pptn = ptnNewTree;
   //return ptnNewTree;
  }
