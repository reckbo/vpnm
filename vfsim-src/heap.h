/**************************************************************************

   File:                heap.h

   Author(s):           Sylvain Bouix and Kaleem Siddiqi

   Created:             Feb 2000

   Last Revision:       $Date: 2006/09/18 13:39:56 $

   Description: 

   $Revision: 1.1.1.1 $

   $Log: heap.h,v $
   Revision 1.1.1.1  2006/09/18 13:39:56  eckbo
   Imported source code.

   Revision 1.1.1.1  2006-09-17 23:43:31  ryan
   Imported source code.

   Revision 1.1.1.1  2006-09-17 02:39:57  ryan
   Import of vortex filament project

   Revision 1.1.1.1  2006-09-16 23:13:56  ryan


   Revision 1.1.1.1  2006/04/19 19:51:21  eckbo
   Previously backed up code before the version that made the single elliptical
   vortex movie broke.


	
   Copyright (c) 2000 by Sylvain Bouix and Kaleem Siddiqi, Centre for
   Intelligent Machines, McGill University, Montreal, QC.  Please see
   the copyright notice included in this distribution for full
   details.

 **************************************************************************/
typedef struct poin {
  int x;
  int y;
  int z;
  double value;
  VX *pvx; // a quick hack
} point;

typedef struct h {
  point *keys;
  unsigned int size;
} heap;

unsigned int parent (unsigned int i);
unsigned int left (unsigned int i);
unsigned int right(unsigned int i);

void heapify(heap *, unsigned int);
void buildHeap (heap *, point *, unsigned int);
point heapExtractMax (heap *);
void heapInsert (heap *, point);
