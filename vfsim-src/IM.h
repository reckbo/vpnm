/*---------------------------------------------------------------------------*/
/* Program:  IM.H                                                            */
/*                                                                           */
/* Purpose:  This file contains the constant and type definitions for        */
/*           a new image format called "KUIM".  This image format            */
/*           supports 1D, 2D and 3D images of a variety of data types.       */
/*           The basic image access routines are in image.c.                 */
/*                                                                           */
/* Author:   John Gauch                                                      */
/*                                                                           */
/* Date:     Oct 25, 1995  (JPEG part by Chunyen Liu)                        */
/*                                                                           */
/* Note:     Copyright (C) The University of Kansas, 1995                    */
/*---------------------------------------------------------------------------*/
//#include <sys/file.h>
#include <stdio.h>
#include <strings.h>
#include <math.h>

/* Boolean values */
#define TRUE 1
#define FALSE 0
#define VALID 1
#define INVALID 0

/* Pixel format codes */
#define MINTYPE 1
#define BYTE 1
#define SHORT 2
#define INT 3
#define FLOAT 4
#define DOUBLE 5
#define COMPLEX 6
#define COLOR 7
#define PSEUDO 8
#define JPEG_GRAY 9
#define JPEG_RGB 10
#define MAXTYPE 10

/* Pixel format types */
typedef unsigned char BYTE_TYPE;
typedef short SHORT_TYPE;
typedef int INT_TYPE;
typedef float FLOAT_TYPE;
typedef double DOUBLE_TYPE;
typedef struct
{
   float re, im;
}
COMPLEX_TYPE;
typedef struct
{
   unsigned char r, g, b;
}
COLOR_TYPE;
typedef unsigned char PSEUDO_TYPE;
typedef unsigned char JPEG_GRAY_TYPE;
typedef unsigned char JPEG_RGB_TYPE;

/* Length of header arrays */
#define nUNUSED 116
#define nTITLE 128
#define nCMAP 256

/* Structure for 1024 byte image header */
typedef struct
{
   short Machine;
   short Version;
   short PixType;
   short Xdim;
   short Ydim;
   short Zdim;
   char Unused[nUNUSED];	/* This field is unused */
   char Title[nTITLE];
   unsigned char Red[nCMAP];
   unsigned char Green[nCMAP];
   unsigned char Blue[nCMAP];
   FILE *Fd;			/* This field not stored to disk */
}
IM_TYPE;

/* Forward declarations */
//extern "C"
//{
IM_TYPE *im_create(char *, int, int, int, int);
IM_TYPE *im_open(char *, int *, int *, int *, int *, int *);
void im_get_title(IM_TYPE *, char *);
void im_put_title(IM_TYPE *, char *);
void im_get_cmap(IM_TYPE *, unsigned char *, unsigned char *, unsigned char *);
void im_put_cmap(IM_TYPE *, unsigned char *, unsigned char *, unsigned char *);
char *im_alloc1D(IM_TYPE *, int);
char **im_alloc2D(IM_TYPE *, int);
char ***im_alloc3D(IM_TYPE *, int);
void im_free1D(char *);
void im_free2D(char **);
void im_free3D(char ***);
void im_read(IM_TYPE *, int, char *);
void im_write(IM_TYPE *, int, char *);
void Error(char *);
//}
