#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "mymath.h"
#include "voxel.h"
#include "VoxelTree.h"
#include "NarrowBand_RungeKutta.h"
#include "Global_Parameters.h"
#include "Dump_Info.h"
#include "thin_ryan.h"


/*************************************************************************************************************	      
 The following function writes *.dat file containing the coordinates of the nodes of the 
 current REAL filament evolved using Biot_Savart Law. It dumps filament number Fil_Num in 
 case of multiple filaments.
****************************************************************************************************************/
void Dump_Position_of_Real_Filament(Coordinates **position, int Fil_Num, double t)
{

  int m;
  char filename[250];
  FILE *fpt;


  printf("\n\nSaving current smoothed filament coordinates to file ...\n");


  //set up the name of the file to be written
  sprintf(filename,"fil_%i_%.3f.dat", Fil_Num, t);

  
  //Write out the position of the filament nodes to a data file
  fpt=fopen(filename, "w+");

 
  for(m=1;m<=(int)position[Fil_Num][0].xcord;m++)
    fprintf(fpt,"%f  %f  %f\n",
	    position[Fil_Num][m].xcord, position[Fil_Num][m].ycord, position[Fil_Num][m].zcord);
	

  fclose(fpt);

}





/*************************************************************************************************************	      
 The following function writes *.dat file containing the coordinates of the nodes of the 
 current thinned smoothed filament. It dumps filament number Fil_Num in 
 case of multiple filaments.
****************************************************************************************************************/
void Dump_Position_of_Thinned_Filament(Coordinates **position, int Fil_Num, int Iteration)
{

  int m;
  char filename[250];
  FILE *fpt;


  printf("\n\nSaving current smoothed filament coordinates to file ...\n");

  
  //set up the name of the file to be written
  sprintf(filename,"%s/Thin_%d_iter_%d.dat", THIN_FIL_DIRECTORY, Fil_Num, Iteration);
  printf("%s\n",filename);
  
  //Write out the position of the filament nodes to a data file
  fpt=fopen(filename, "w+");

  //printf( "got here." );
 
  for(m=1;m<=(int)position[Fil_Num][0].xcord;m++)
    fprintf(fpt,"%e             %e             %e       \n",
	    (position[Fil_Num][m].xcord),(position[Fil_Num][m].ycord),(position[Fil_Num][m].zcord));
	


  printf("Finished writing filament coordinates to file.\n");


  fclose(fpt);

}





/*************************************************************************************************************	      
 The following is function solves a linear system of equations having a symmetric, cyclically five-diagonal
 (pentadiagonal) coefficient matrix. The function is given the size of the square matrix, given by n, where n
 in our current application denotes the number of nodes of the filament we want to smooth. 
 Array A contains the diagonal entries of the five-diagonal matrix. Array B contains the first subdiagonal
 (i.e. the diagonal directly under the main diagonal) of the matrix and array C contains the entries on the 
 second subdiagonal. Initially, array F contains the right hand side values of the linear equations.
 The solution vector is stored in array F by the time the function terminates. In addition, in the course
 of the function, arrays A, B and C are overwritten with useful information to used in solving the system.
 The approach taken to solve the system is by decomposing our five-diagonal matrix, M, into 2 matrices, L and
 U where L is a lower triangular matrix and U is an upper triangular matrix, such that M=LU.
 This function is a direct implementaion of the Fortran program PENPES found in the book "One Dimensional
 Spline Interpolation Algorithms" by Helmuth Spath. 
****************************************************************************************************************/
void Solve_Linear_System(int n, double A[NODES], double B[NODES], double C[NODES], double F[NODES])
{

  //working space arrays
  double D[NODES], E[NODES], H[NODES];

  double z, h1, h3, h2, h4, h5, hh1, hh3, hhh1, sum;
  int nm1, nm2, nm3, nm4, nm5, k, km1, km2, kp1, i;

  nm1=n-1;
  nm2=n-2;
  nm3=n-3;
  nm4=n-4;

  z=A[1];



  A[1]=F[1]/z;
  D[1]=C[nm1]/z;
  E[1]=B[n]/z;
  F[1]=C[nm1];
  H[1]=B[n];
  h3=B[1];
  h1=C[1];
  h4=B[1]/z;
  B[1]=h4;
  C[1]=C[1]/z;

  z=A[2] - (h3*h4);



  A[2]=(F[2]-h3*A[1])/z;
  D[2]=-(h3*D[1])/z;
  E[2]=(C[n]- h3*E[1])/z;
  F[2]=-F[1]*h4;
  H[2]=C[n]-H[1]*h4;
  hh3=h3;
  h3=B[2]-h1*h4;
  h4=(B[2]-hh3*C[1])/z;
  B[2]=h4;

  if(n > 5)
    {
      nm5=n-5;

      for(k=3;k<=nm3;k++)
	{
	  km1=k-1;
	  km2=k-2;
	  kp1=k+1;
	  hh1=h1;
	  h1=C[km1];
	  C[km1]=C[km1]/z;
	  h2=C[km2];
	  z=A[k] - hh1*h2 - h3*h4;



	  A[k]=(F[k]-hh1*A[km2]-h3*A[km1])/z;
	  D[k]=-(hh1*D[km2] + h3*D[km1])/z;
	  E[k]=-(hh1*E[km2] + h3*E[km1])/z;
	  F[k]=-F[km2]*h2-F[km1]*h4;
	  H[k]=-H[km2]*h2-H[km1]*h4;
	  hh3=h3;
	  h3=B[k]- h1*h4;
	  h4=(B[k]-hh3*C[km1])/z;
	  B[k]=h4;
	}

      hhh1=hh1;
      hh1=h1;
      h1=C[nm3]-F[nm5]*C[nm5]-F[nm4]*B[nm4];
      C[nm3]=(C[nm3]-hhh1*D[nm5]-hh3*D[nm4])/z;
    }//end if

  else
    {
      hh1=h1;
      h1=C[nm3]-F[nm4]*B[nm4];
      C[nm3]=(C[nm3]-hh3*D[nm4])/z;
    }

  h2=C[nm4];
  z=A[nm2]-hh1*h2 - h3*h4;



  A[nm2]=(F[nm2]-hh1*A[nm4]-h3*A[nm3])/z;
  hh3=h3;
  h3=B[nm2]-F[nm4]*h2-h1*h4;
  h4=(B[nm2]-D[nm4]*hh1-hh3*C[nm3])/z;
  B[nm2]=h4;
  hhh1=hh1;
  hh1=h1;
  h1=C[nm2]-H[nm4]*h2-H[nm3]*B[nm3];
  C[nm2]=(C[nm2]-hhh1*E[nm4]-hh3*E[nm3])/z;

  sum=0.0;
  
  for(i=1;i<=nm4;i++)
    sum=sum+F[i]*D[i];

  z=A[nm1]-hh1*C[nm3]-h3*h4-sum;



  sum=0.0;

  for(i=1;i<=nm4;i++)
    sum=sum+F[i]*A[i];


  A[nm1]=(F[nm1]-sum-hh1*A[nm3]-h3*A[nm2])/z;

  sum=0.0;

  for(i=1;i<=nm4;i++)
    sum=sum + F[i]*E[i];


  h5=B[nm1];
  h4=(h5-h3*C[nm2]-sum-hh1*E[nm3])/z;
  B[nm1]=h4;

  sum=0.0;

  for(i=1;i<=nm4;i++)
      sum=sum+H[i]*D[i];

  hh3=h3;
  h3=h5-h1*B[nm2]-sum-H[nm3]*C[nm3];

  sum=0.0;

  for(i=1;i<=nm3;i++)
    sum=sum+H[i]*E[i];



  z=A[n]-h1*C[nm2]-h3*h4-sum;


  sum=0.0;
  for(i=1;i<=nm3;i++)
    sum=sum+H[i]*A[i];


  A[n]=(F[n]-sum-h1*A[nm2]-h3*A[nm1])/z;
  F[n]=A[n];
  F[nm1]=A[nm1]-B[nm1]*F[n];
  F[nm2]=A[nm2]-B[nm2]*F[nm1]-C[nm2]*F[n];
  F[nm3]=A[nm3]-B[nm3]*F[nm2]-C[nm3]*F[nm1]-E[nm3]*F[n];

 for(i=nm4;i>=1;i=i-1)
   F[i]=A[i]-B[i]*F[i+1]-C[i]*F[i+2]-D[i]*F[nm1]-E[i]*F[n];


}




/*************************************************************************************************************	      
 The following function smooths a number of filaments using a gievn wieght, weight. The filaments are
 assumed to be 3-dimensional and the filament node locations are stored in the 2D array Position (the standard 
 structure used in this application for storing filament nodes). The function returns the smoothed out nodes
 in the given array Position (and therefore updating it).
 The smoothing is done by using smoothing cubic splines where, for each closed filament, the original nodes 
 are used as "knots" for the splining. Since we assume all filaments are CLOSED, periodic smoothing is used where
 the last node and first node are the same. 
 Since our filaments are 3dimensional, we need to perform the smoothing splines in each coordinate. Therefore, we
 need to parametrize our filaments using a parameter, S, that runs over the length of each filament.
 Since smooothing splines smooth out a 1dimensional function (x,f(x)), we have to perform smoothing splines
 3 times for each filament such that each coordinate is smoothed out i.e. (s,X(s)), (s,Y(s)) and (s,Z(s)).
 The parameter values are stored in array S. The wieght determines how close our fit is. As wieght->0, maximum
 smoothing is reached and our filaments will become 1 point. As weight->infinitiy, our splines become
 INTERPOLATING splines and therefore going through the original points. The heart of this algorithm is
 an implementation of the Fortran program CUBSM2 in the book "One dimensional interpolating algorithms" by
 Helmuth Spath.  
****************************************************************************************************************/
void Periodic_Smoothing_Spline(Coordinates **Position, double weight)
{
	printf("in periodic smoothing spline...\n");
	//the following is the data structure needed as workspace for the algorithm
	double Filament[NODES][4], A[NODES], B[NODES], C[NODES], D[NODES], Y2[NODES], S[NODES];
	int Fil_Num, i, nodes, n1, n2, j, w, k, k1;
	double seg_length, ds, h, h1, h2, h3, h4, r1, r2, p, s; 
	//int ns;
	//double dsavg, snew;

	//set the weights; these are the same value for all nodes i.e. polynomials
	p=1.0/weight;  
	//get the total number of filaments
	Fil_Num=(int)Position[0][0].xcord;

	//smooth each filament (we assume each filament is closed 3D curve)
	for(i=1;i<=Fil_Num;i++)
		{

		//get the number of nodes in the current filament such that the last node is equal to the first node i.e. periodic 
		nodes=(int)Position[i][0].xcord + 1;

		n1=nodes-1;
		n2=nodes-2;

		/*transfer the nodes of the current filament to a temporary storage 2d-array of doubles
		  This is done in order to make the subsequent calculations easier since the x, y and z 
		  coordinates of node j are accessed by Filament[j][1], Filament[j][2] and Filament[j][3].*/
		for(j=1;j<=n1;j++)
			{
			Filament[j][1]=Position[i][j].xcord;
			Filament[j][2]=Position[i][j].ycord;
			Filament[j][3]=Position[i][j].zcord;
			}

		//let the last element in array Filament be identical to the first one (this structure is needed for algorithm)
		Filament[nodes][1]=Position[i][1].xcord;
		Filament[nodes][2]=Position[i][1].ycord;
		Filament[nodes][3]=Position[i][1].zcord;


		//we need to reparametrize our 3d-curve (filament) by S 
		//we store the cumulative length of our filament in array S as such
		S[1]=0.0;
		for(j=2;j<=n1;j++)
			{
			seg_length = sqr(Filament[j][1]-Filament[j-1][1])
				+  sqr(Filament[j][2]-Filament[j-1][2])
				+  sqr(Filament[j][3]-Filament[j-1][3]);

			ds=sqrt(seg_length);

			//add the length of this node to the total length of the filament
			S[j]= S[j-1] + ds;
			}

		//fill-up the last entry of array S (it's more efficient this way)
		seg_length = sqr(Filament[1][1]-Filament[n1][1])
			+  sqr(Filament[1][2]-Filament[n1][2])
			+  sqr(Filament[1][3]-Filament[n1][3]);

		ds=sqrt(seg_length);

		//add the length of this node to the total length of the filament
		S[nodes]= S[n1] + ds;   

		//smooth each coordinate (x,y,z) using splines
		for(w=1;w<=3;w++)
			{
			h1 = 1.0/(S[nodes]-S[n1]);
			h2 = 1.0/(S[2]-S[1]);
			r1=(Filament[1][w] - Filament[n1][w])*h1;

			//compute preparations 
			for(k=1;k<=n1;k++)
				{
				k1=k+1;
				s=h1+h2;

				if(k < n1)
					{
					h3=1.0/(S[k+2]-S[k1]);
					r2=(Filament[k1][w] - Filament[k][w])*h2;
					}
				else
					{
					h3=1.0/(S[2]-S[1]);
					r2=(Filament[1][w] - Filament[n1][w])*h2;
					}

				A[k]=(2.0/h1) + (2.0/h2) + 6.0*(h1*h1*p + s*s*p + h2*h2*p);

				if(k < n1)
					B[k]=(1.0/h2) - 6.0*h2*(p*s + p*(h2+h3));

				if(k < n2)
					C[k]=6.0*p*h2*h3;

				//array Y2 contains the values of the second derivatives
				Y2[k]=6.0*(r2-r1);

				if(k==1)
					{
					h4=1.0/(S[n1]-S[n2]);
					B[n1]=1.0/h1 - 6.0*h1*(p*(h1+h4) + p*(h1+h2));
					C[n2]=6.0*p*h4*h1;
					}

				if(k==2)
					C[n1]=6.0*p*h1/(S[nodes]-S[n1]);

				h1=h2;
				h2=h3;
				r1=r2;

				}//end for loop over k

			/*now that we have the values of the second derivatives stored in Y2, we proceed
			  to solve the system of linear equations. The solution will be outputed in array Y2 too*/
			printf("about to enter solve linear system\n");
			Solve_Linear_System(n1, A, B, C, Y2);
			printf("done solve linear system\n");

			Y2[nodes]=Y2[1];

			h1=(Y2[1]-Y2[n1])/(S[nodes]-S[n1]);

			//now that we've solved th equations, we proceed to calculate the polynomial coefficients

			for(k=1;k<=n1;k++)
				{
				k1=k+1;
				h=1.0/(S[k1]-S[k]);
				B[k]=h;
				h2=h*(Y2[k1]-Y2[k]);
				D[k]=h2/6.0;
				A[k]=Filament[k][w]-(h2-h1)*p;
				C[k]=Y2[k]/2.0;
				h1=h2;
				}
			A[nodes]=A[1];

			for(k=1;k<=n1;k++)
				{
				k1=k+1;
				h=B[k];
				B[k]=(A[k1]-A[k])*h - (Y2[k1] + 2.0*Y2[k])/(6.0*h);
				}

			/*we now have the coefficient of the polynomials for the given filament and given coordinate. We can update our nodes and 
			  adjust the coordinate value of all nodes in order to smooth them. Array A, B, C and D contain the coefficients. We'll update the 
			  original array Position to impose lasting changes.*/



			//let's try to place nodes ON the produced smoothing spline
			if(w==1)
				{
				//smooth-out the xcord. of the current filament's nodes
				for(j=1;j<=n1;j++)
					{
					//place a node on the cubic polynomial, halfway between each pair of original knots
					ds=(S[j+1]-S[j])/2.0;
					Position[i][j].xcord= A[j] + ds*B[j] + ds*ds*C[j] + ds*ds*ds*D[j];		
					}	  
				}

			else if(w==2)
				{	      
				//smooth-out the ycord. of the current filament's nodes
				for(j=1;j<=n1;j++)
					{
					//place a node on the cubic polynomial, halfway between each pair of original knots
					ds=(S[j+1]-S[j])/2.0;
					Position[i][j].ycord= A[j] + ds*B[j] + ds*ds*C[j] + ds*ds*ds*D[j];		
					}	  
				}

			else if(w==3)
				{	     
				//smooth-out the zcord. of the current filament's nodes
				for(j=1;j<=n1;j++)
					{
					//place a node on the cubic polynomial, halfway between each pair of original knots
					ds=(S[j+1]-S[j])/2.0;
					Position[i][j].zcord= A[j] + ds*B[j] + ds*ds*C[j] + ds*ds*ds*D[j];		
					}	  	      
				}	  
			}//finish smoothing over all coordinates for the given filament
		}//end smoothing all filaments
			printf("here i am here\n");
}



/*************************************************************************************************************	      
 The following function is for naive display of the filament corresponding to a given value of the Height 
 function. It takes a 3D array containing the current Height function in addition to the "thickness"
 which coresponds to the level-set whose evolution you want to follow. Thresholding for thinning is done
 based on this thickness. The Iteration_Number is also given in order to dump the resulting filament as a
 *.im file and as a data file of x,y and z coordinates in order to plot it later in matlab (shifted).
 It also dumps the smoothing parameters used in case of variable smoothing.
****************************************************************************************************************/
/* void Thin_Height_For_Display(double_TYPE ***Height, int Iteration_Number)   */
/* {    */
  /*  */
  /*  */
  /* //3D arrays for containing data     */
  /* IM_TYPE *Temp_Pointer; */
  /* double_TYPE ***Temp; */
  /*  */
  /*  */
  /* int x, y, z, Fil_num, i, j; */
  /* Coordinates Thinned_Position[NumOfFil][NODES]; */
  /* double  sigma, range, Nodes; */
  /*  */
/*  */
/*  */
  /* //create a temporary array needed for thinning */
  /* Temp_Pointer = im_create("/dev/null", double, X_Res, Y_Res, Z_Res);  */
  /* Temp = (double_TYPE ***) im_alloc3D(Temp_Pointer, double); */
  /*  */
  /*  */
  /*  */
  /* //Thin the current evolved surface to obtain the corresponding filament */
  /* printf("\n\nStart the thinning of the current evolved surface...\n"); */
  /* //thin(pvc);  */
  /* printf("Finished the thinning of the current evolved surface\n"); */
  /*  */
  /* //testing */
  /* //Dump_Height(Temp, 1000); */
  /*  */
  /* //Locate and order filament voxels and store their ordered positions in 2D array Thinned_Position */
  /* Order_Voxels(Thinned_Position, Thinned_Position, Temp, X_Res, Y_Res, Z_Res);   */
  /*  */
  /* //free the space used to store the temp 3D array used for thinning, smoothing and storing temporary data */
  /* im_free3D((char ***) Temp); */
  /*  */
  /*  */
  /* Fil_num = (int)Thinned_Position[0][0].xcord; */
  /*  */
  /* //smooth out the filament using smoothing splines */
  /* Periodic_Smoothing_Spline(Thinned_Position, WEIGHT); */
  /*  */
  /* //dump the smoothed filament */
  /* for(i=1;i<=Fil_num;i++) */
    /* Dump_Position_of_Thinned_Filament(Thinned_Position, i, Iteration_Number); */
  /*  */
 /*  */
/* } */





/*************************************************************************************************************	      
 This function takes a 2D array containing filament/s positions and reverses the order of the filament
 given by FilNum.
****************************************************************************************************************/
void Swap_Filament(Coordinates **Fil, int FilNum)
{
  int i, nodes;
  Coordinates Temp[NODES];


  nodes = (int)Fil[FilNum][0].xcord;

  for(i=1;i<=nodes;i++)
    {
      Temp[i].xcord = Fil[FilNum][nodes-(i-1)].xcord;
      Temp[i].ycord = Fil[FilNum][nodes-(i-1)].ycord;
      Temp[i].zcord = Fil[FilNum][nodes-(i-1)].zcord;
    }
  
 
  //update the current array
  for(i=1;i<=nodes;i++)
    {
      Fil[FilNum][i].xcord = Temp[i].xcord;
      Fil[FilNum][i].ycord = Temp[i].ycord;
      Fil[FilNum][i].zcord = Temp[i].zcord;
    }
  
}









/*************************************************************************************************************	      
 This is an auxliary function for funciton Robustly_Check_Direction_of_Ordering below. 
 Given a node position given by xcord, ycord and zcord, this function finds the node in array 
 Filament_Array whose position is closest to the given position. It returns the array position of this node.
 The function also updates the integer "which_fil" that indicates the number of the filament where we found the
 current node.
****************************************************************************************************************/
int Locate_Closest_Voxel(Coordinates **Filament_Array, double xcord, double ycord, double zcord, int *which_fil)
{
  int filnum, i, j, index, fil_index;
  double Min_Dist, xdist, ydist, zdist, distance;

  //get the total number of filaments stored in Filament_Array
  filnum = (int)Filament_Array[0][0].xcord;

  //initialize Min_Dist to a very large number
  Min_Dist = LARGE_NUMBER;


  //go through every voxel Filament_Array and find the array position of the closest voxel to the given location
  for(i=1;i<=filnum;i++)
    for(j=1;j<=(int)Filament_Array[i][0].xcord;j++)
      {
	xdist = xcord - Filament_Array[i][j].xcord;
	ydist = ycord - Filament_Array[i][j].ycord;
	zdist = zcord - Filament_Array[i][j].zcord;

	distance = sqrt((xdist*xdist) + (ydist*ydist) + (zdist*zdist));

	if(distance<Min_Dist)
	  {
	    Min_Dist = distance;
	    index = j;
	    fil_index = i;
	  }
      }

  
  //update the number of the filament where the current node was found
  *which_fil = fil_index;

  //return the closest array position of the given voxel
  return(index);
}




/*************************************************************************************************************	      
 This is an auxliary function for funciton Robustly_Check_Direction_of_Ordering below. 
 Given a node location described by xcord, ycord and zcord, this function finds the node in array 
 Filament_Array whose position is closest to the given location. It returns the array position of this node
 provided that it's location is in the "middle" of the located filament i.e. at some distance from both
 the start and end voxel locations of the coresponding filament. This critical distance is 20 voxels 
 since all our filaments contain at least 400 voxels (400>>20). If the closet array location turns out to be
 too close to either the start or end locations, then the function returns 0.(the 2D array  Filament_Array
 is not zero-indexed so node location 0 doesn't represent a valid array entry)
 The function also updates the integer "which_fil" that indicates the number of filament where we found the
 current node (once we find a suitable first node). If the current node is not a "suitable" first
 node, then we don't modify "which_fil" and we leave it with it's initial value. 
****************************************************************************************************************/
int 
Locate_Closest_First_Voxel(Coordinates **Filament_Array, double xcord, double ycord, double zcord, int *which_fil)
{
	int filnum, i, j, node_index, fil_index, fil_nodes;
	double Min_Dist, xdist, ydist, zdist, distance;

	//get the total number of filaments stored in Filament_Array
	filnum = (int)Filament_Array[0][0].xcord;

	//initialize Min_Dist to a very large number
	Min_Dist = LARGE_NUMBER;

	//go through every voxel Filament_Array and find the array position of the closest voxel to the given location
	for(i=1;i<=filnum;i++)
		for(j=1;j<=(int)Filament_Array[i][0].xcord;j++)
			{
			xdist = xcord - Filament_Array[i][j].xcord;
			ydist = ycord - Filament_Array[i][j].ycord;
			zdist = zcord - Filament_Array[i][j].zcord;

			distance = sqrt((xdist*xdist) + (ydist*ydist) + (zdist*zdist));

			if(distance<Min_Dist)
				{
				Min_Dist = distance;
				node_index = j;
				fil_index = i;
				}
			}

	/*return the closest array position of the given voxel provided this location is 
	  within 20 voxels from the start and end locations of the filament, otherwise return 0.
	  We need to find a first location which is some distance away from the danger zones
	  of the start and end locations, that's why we pick a "first" location which is in
	  the "middle" of our given filament.*/
	fil_nodes = (int)Filament_Array[fil_index][0].xcord;

	if((node_index>20)&&(node_index<(fil_nodes-20)))
		{
		*which_fil=fil_index;
		return(node_index);
		}
	else
		return(0);

}
void  
My_Robustly_Check_Direction_of_Ordering(Coordinates **Previous_Filament, Coordinates **Current_Filament, int Fil)
{
  int  found_it=0, i=0, which_fil1;
  double xcord, ycord, zcord;

  while(found_it==0)
	  {
	  //go to the next node location in our current ordering 
	  i++;

	  //small check that we haven't exceeded the number of nodes in the current filament
	  //printf("number of nodes in filament=%f\n", Current_Filament[Fil][0].xcord);
	  if(i > (int)Current_Filament[Fil][0].xcord)
		  {
		  printf("\n!!There is some defect in the ordering code!!\n");
		  exit(0);
		  }

	  xcord = Current_Filament[Fil][i].xcord;
	  ycord = Current_Filament[Fil][i].ycord;
	  zcord = Current_Filament[Fil][i].zcord;

	  /*return the closest array location in array Previous_Filament corresponding to the given position 
		provided it's some distance from the edges of the corresponding filament.If it's too close to
		the edges, then zero will be returned*/
	  found_it = Locate_Closest_First_Voxel(Previous_Filament, xcord, ycord, zcord, &which_fil1);
	  }

  double v_previous[3]; 
  v_previous[0] = Current_Filament[Fil][i+1].xcord - xcord;
  v_previous[1] = Current_Filament[Fil][i+1].ycord - ycord;
  v_previous[2] = Current_Filament[Fil][i+1].zcord - zcord;
  double v_current[3];  
  v_current[0] = Previous_Filament[which_fil1][found_it+1].xcord - Previous_Filament[which_fil1][found_it].xcord;
  v_current[1] = Previous_Filament[which_fil1][found_it+1].ycord - Previous_Filament[which_fil1][found_it].ycord;
  v_current[2] = Previous_Filament[which_fil1][found_it+1].zcord - Previous_Filament[which_fil1][found_it].zcord;

  double dot_product = v_previous[0]*v_current[0] +  v_previous[1]*v_current[1] + v_previous[2]*v_current[2];
  if (dot_product < 0)
	  {
	  printf(" 		Swapping node ordering for filament %i\n", Fil);
	  Swap_Filament(Current_Filament, Fil);
	  }
  else
	  printf(" 		Node ordering for filament %i is fine. \n", Fil);

  /* Hack!!*/
  //if (Fil == 3)
	  //Swap_Filament(Current_Filament, Fil);
  //if (Fil == 7)
	  //Swap_Filament(Current_Filament, Fil);

}


/*************************************************************************************************************	      
 This function provides a second method for checking that the thinned filament/s nodes are ordered in 
 a consistent way. We need to make sure that the sequence in which the nodes are placed in array Current_Filament
 are consistent with the initial ordering. Current_Filament contains the node positions whose ordering we need
 to check. Previous_Filament contains the evolved real filament corresponding to the thinned torus contained
 in Current_FIlament. We assume that the ordering in Previous_Filament is correct and we use it make sure
 that the ordering in Current_Filament is correct. 
 This is done by taking 2 "consecutive" nodes from Previous_Filament and making sure that the nodes closest
 to these 2 nodes in array Current_Filament are also consecutive in the same way.
****************************************************************************************************************/
void  
Robustly_Check_Direction_of_Ordering(Coordinates **Previous_Filament, Coordinates **Current_Filament, int Fil)
{
  int first_location, second_location, found_it=0, i=0, j, equal=1, which_fil1,  which_fil2, Different_fils=1;
  double xcord, ycord, zcord;

  /*Pick a suitable "first" voxel to be a reference point in the quest for the right direction of ordering.
    This is achieved by going through the nodes in our current filament ordering, and making picking
    a node whose corresponding node in the previous filament ordering, is located at some distance
    from the start and end voxels of the corresponding filament. This ensures that we start with 
    "first" voxel which is some distance away from the danger zones since the start and end locations
     are limiting cases and could introduce some problems for this method of order checking.*/

  while(Different_fils)    //we can only compare the 2 node locations provided they come from the SAME original filament
	  {
	  while(found_it==0)
		  {
		  //go to the next node location in our current ordering 
		  i++;

		  //small check that we haven't exceeded the number of nodes in the current filament
		  //printf("number of nodes in filament=%f\n", Current_Filament[Fil][0].xcord);
		  if(i > (int)Current_Filament[Fil][0].xcord)
			  {
			  printf("\n!!There is some defect in the ordering code!!\n");
			  exit(0);
			  }

		  xcord = Current_Filament[Fil][i].xcord;
		  ycord = Current_Filament[Fil][i].ycord;
		  zcord = Current_Filament[Fil][i].zcord;

		  /*return the closest array location in array Previous_Filament corresponding to the given position 
			provided it's some distance from the edges of the corresponding filament.If it's too close to
			the edges, then zero will be returned*/
		  found_it = Locate_Closest_First_Voxel(Previous_Filament, xcord, ycord, zcord, &which_fil1);
		  }

	  /*once a good "first" location is found, we proceed to check the consistency of the ordering by checking 
		the next voxels in our current ordering where also the "next" voxels in our previous ordering*/
	  first_location = found_it;

	  //let j denote our valid location in the current filament ordering
	  j = i;

	  while(equal)
		  {
		  //proceed to the next voxels
		  ++j;

		  //get the current locations of the second voxel in the array containing the current ordering of our voxels 
		  xcord = Current_Filament[Fil][j].xcord;
		  ycord = Current_Filament[Fil][j].ycord;
		  zcord = Current_Filament[Fil][j].zcord;

		  //take the second voxel in the array containing the current ordering of voxels
		  second_location = Locate_Closest_Voxel(Previous_Filament, xcord, ycord, zcord, &which_fil2);

		  //check whether the closest second location is different from the one closest to the first location
		  if(second_location!=first_location) 
			  equal=0;
		  }

	  /*Now we can check that both nodes came from the SAME original filament because we can only compare order 
		of nodes provided they came from the same filament in the previous iteration*/
	  if(which_fil1!=which_fil2)
		  {
		  //need to repeat the above but we set our first voxel to be the current second location
		  i=j-1;
		  found_it=0;
		  equal=1;
		  }
	  else   //exit loop since we've found a suitable first and second locations
		  Different_fils=0;

	  }

  //check that the "second" location is in the correct position, if not-> swap current voxel locations
  if(second_location<first_location)
	  {
	  printf(" 		Swapping node ordering for filament %i\n", Fil);
	  Swap_Filament(Current_Filament, Fil);
	  }
  else
	  printf(" 		Node ordering for filament %i is fine. \n", Fil);
}

/*************************************************************************************************************	      
 The following function is exactly like function Thin_Height_For_Display above. The only difference
 is that it updates the "real" filament we're depending on by the actual current filament which is the
 result of thinning the current height function. Given the 2D array containing the current real filament
 it replaces it by the current thinned-smoothed Height function. It doesn't dump any filament files.
 It dumps the smoothing parameters used in the case of variable smoothing.
****************************************************************************************************************/
/* void Update_Current_Filament(TN *ptn, Coordinates **P, int Iteration_Number)   */
 /* {    */
  /*  */
    /* //3D arrays for containing data     */
    /* IM_TYPE *Temp_Pointer; */
    /* double_TYPE ***Temp; */
/*  */
    /* int x, y, z, Fil_num, l, m, nodes, i, j; */
    /* double Nodes, sigma, range; */
/*  */
    /* Coordinates Thinned_Position[NumOfFil][NODES], Velocity[NumOfFil][NODES]; */
/*  */
/*  */
   /*  */
    /* //create a temporary array needed for 3D smoothing and store temporary data */
    /* //Temp_Pointer = im_create("/dev/null", double, X_Res, Y_Res, Z_Res);  */
    /* //Temp = (double_TYPE ***) im_alloc3D(Temp_Pointer, double); */
    /*  */
/*  */
    /* //Thin the current evolved surface to obtain the corresponding filament */
    /* printf("\n\nStart the thinning of the current evolved surface...\n"); */
    /* //thin(ptn);  */
    /* printf("Finished the thinning of the current evolved surface\n"); */
      /*  */
      /*  */
    /* //Locate and order filament voxels and store their ordered positions in 2D array Position */
    /* Order_Voxels(P, Thinned_Position, Temp, X_Res, Y_Res, Z_Res);   */
      /*  */
    /*  */
    /* //free the space used to store the temp 3D array used for thinning */
    /* //im_free3D((char ***) Temp); */
/*  */
    /*  */
    /* Fil_num = (int)Thinned_Position[0][0].xcord; */
/*  */
    /* //smooth out the filament */
    /* Periodic_Smoothing_Spline(Thinned_Position, WEIGHT); */
    /*  */
/*  */
    /* //Check that the direction of ordering of EACH filament is consistent with the desired direction of ordering */
    /* for(i=1;i<=Fil_num;i++) */
      /* { */
	/* printf("\nStart checking direction of ordering for filament %d...\n",i); */
	/* Robustly_Check_Direction_of_Ordering(P, Thinned_Position, i); */
    	/* printf("Finished checking direction of ordering\n"); */
      /* } */
    /*  */
    /*SmoothP now contains the correct ordering over voxels therefore we need to transfer 
      it's contents to our array P in order to update our fil. to the current, correctly ordered
      thinned filament*/
    /*  */
    /* //first, transfer the total number of filaments  */
    /* P[0][0].xcord = Thinned_Position[0][0].xcord; */
/*  */
    /* for(i=1;i<=(int)Thinned_Position[0][0].xcord;i++) */
      /* {		     */
	/* //transfer the number of nodes of the current fil. */
	/* P[i][0].xcord = Thinned_Position[i][0].xcord; */
	/*  */
	/* for(j=1;j<=(int)Thinned_Position[i][0].xcord;j++) */
	  /* { */
	    /*  */
	    /* //transfer the node positions of the current fil. to new array */
	    /* P[i][j].xcord = Thinned_Position[i][j].xcord; */
	    /* P[i][j].ycord = Thinned_Position[i][j].ycord; */
	    /* P[i][j].zcord = Thinned_Position[i][j].zcord; */
	    /*  */
	  /* }//end for  */
	/*  */
      /* }//end for  */
    /*  */
    /* //With that we would have updated the current filament P with the correctly ordered thinned filament */
    /*  */
    /*  */
 /* } */
/*  */











