#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <malloc.h>
#include "Global_Parameters.h"
#include "voxel.h"
#include "VoxelTree.h"


#define tgNarrowBand 7

/* #####  DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ##################### */

int
CompareVoxels(xComp, yComp, zComp, x, y, z)
{
	if (xComp < x)
		return -1;
	if (xComp > x)
		return 1;

	if (yComp < y)
		return -1;
	if (yComp > y)
		return 1;

	if (zComp < z)
		return -1;
	if (zComp > z)
		return 1;

	return 0;
}
TN *
PtnCreateTreeNode(VX *pvx)
{
	TN *ptnNewNode = (TN *) malloc(sizeof(TN));
	if (ptnNewNode == NULL)
		{
		printf("Couldn't create tree node.\n");
		exit(0);
		}
	ptnNewNode->pvx = pvx;
	ptnNewNode->ptnLeft = NULL;
	ptnNewNode->ptnRight = NULL;
	return ptnNewNode;
}
void
InsertTreeNode(TN **pptn, VX *pvx)
{
	assert(pvx != NULL);
	TN *ptn = *pptn;

    if (ptn == NULL)
		{
		TN *ptnNewNode = PtnCreateTreeNode(pvx);
		*pptn = ptnNewNode;
		if (*pptn== NULL)
			printf("Problem creating (sub)tree!\n");
		}
	else
		{
		/*VX *pvxCur = PvxFromPtn(ptn);*/
      VX *pvxCur = (ptn == NULL) ? NULL : ptn->pvx;
		int x = pvx->rgco[0];
		int y = pvx->rgco[1];
		int z = pvx->rgco[2];
		int xCur = pvxCur->rgco[0];
		int yCur = pvxCur->rgco[1];
		int zCur = pvxCur->rgco[2];
		if (CompareVoxels(x, y, z, xCur, yCur, zCur) < 0)
			InsertTreeNode(&(ptn->ptnLeft), pvx);
		else if (CompareVoxels(x, y, z, xCur, yCur, zCur) > 0)
			InsertTreeNode(&(ptn->ptnRight), pvx);
		else
			{
			printf("Error: Trying to insert a voxel which is already in the tree. \n");
			exit(1);
			}
		}
}
TN *
PtnGetLeftChild(TN *ptn)
{
	if (ptn == NULL)
		return NULL;
	return (ptn->ptnLeft);
			
}
TN *
PtnGetRightChild(TN *ptn)
{
	if (ptn == NULL)
		return NULL;
	return (ptn->ptnRight);
}
VX *
PvxFreeNode(TN *ptn)
{
	if (ptn == NULL)
		return NULL;
	VX *pvx = ptn->pvx;
	free(ptn);
	return pvx;
}


void
FreeNodesFunction(TN *ptn, void *pv)
{
	PvxFreeNode(ptn);
}
void
FreeVoxelsAndNodesFunction(TN *ptn, void *pv)
{
	VX *pvx = PvxFreeNode(ptn);
	void *pvData = PvDestroyVoxel(pvx);
	if (pvData != NULL)
		free(pvData);
}
void 
PrintVoxel(TN *ptn, void *pv)
{
	if (ptn == NULL)
		return;
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	printf("X = %i, Y = %i, Z = %i, voxel value (vl) = %f, tag = %d\n", XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx), VlFromPvx(pvx), TgFromPvx(pvx));
	if (PvxFindVoxel(ptn, XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx)) == NULL)
		printf("Not supposed to happen\n");
}
void
PrintVoxelToFile(TN *ptn, void *pvFileHandle)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	FILE *pfl = (FILE *) pvFileHandle;
	fprintf(pfl, "%i 	%i 	  %i\n", XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx));
}
void
WriteVoxel(TN *ptn, void *pv)
{
	FILE *pfh = (FILE *) pv;
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	fprintf(pfh, "%i %i %i %f\n", XFromPvx(pvx),YFromPvx(pvx),ZFromPvx(pvx),VlFromPvx(pvx));
}
VX *
PvxDeleteTreeNode(TN **pptn) 
{
	TN *ptnSav = *pptn; 
    if ((*pptn)->ptnLeft == NULL) 
		{
        *pptn = (*pptn)->ptnRight;
        return PvxFreeNode(ptnSav);
		} 
	else if ((*pptn)->ptnRight == NULL) 
		{
		*pptn = (*pptn)->ptnLeft;
        return PvxFreeNode(ptnSav);
		} 
	else  // 2 children
		{
		/* find inorder predecessor */
		TN **pptnPred = &(*pptn)->ptnLeft;
		while ((*pptnPred)->ptnRight != NULL) 
			pptnPred = &(*pptnPred)->ptnRight;

		/* Swap values */
		VX *pvxSav = (*pptnPred)->pvx;
		(*pptnPred)->pvx = (*pptn)->pvx;
		(*pptn)->pvx = pvxSav;

		return PvxDeleteTreeNode(pptnPred);
		}
}

/* #####   FUNCTION DEFINITIONS  -  EXPORTED FUNCTIONS   ############################ */

VX *
PvxFindVoxel(TN *ptn, int x, int y, int z)
{
	if (ptn == NULL)
		return NULL;	// not found
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	if (CompareVoxels(x, y, z, XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx)) < 0)
		return PvxFindVoxel(PtnGetLeftChild(ptn), x, y, z);
	if (CompareVoxels(x, y, z, XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx)) > 0)
		return PvxFindVoxel(PtnGetRightChild(ptn), x, y, z);
	return pvx;
}
void
TraverseTree(TN *ptn, void (*pfnVisitAction)(TN *ptn, void *pvData), void *pvData)
{
	if (ptn == NULL)
		return;
	TN *ptnRight = PtnGetRightChild(ptn);
	TN *ptnLeft = PtnGetLeftChild(ptn);

	/* visit action */
	pfnVisitAction(ptn, pvData);

	/* recurse */
	TraverseTree(ptnLeft, (*pfnVisitAction), pvData);
	TraverseTree(ptnRight, (*pfnVisitAction), pvData);
}
void
TraverseTreePostOrder(TN *ptn, void (*pfnVisitAction)(TN *ptn, void *pvData), void *pvData)
{
	if (ptn == NULL)
		return;
	
	/* recurse */
	TraverseTreePostOrder(PtnGetLeftChild(ptn), (*pfnVisitAction), pvData);
	TraverseTreePostOrder(PtnGetRightChild(ptn), (*pfnVisitAction), pvData);

	/* visit action */
	pfnVisitAction(ptn, pvData);

}
void
FreeTree(TN *ptn)
{
	TraverseTreePostOrder(ptn, &FreeNodesFunction, NULL);
}
void
FreeTreeAndVoxels(TN *ptn)
{
	TraverseTreePostOrder(ptn, &FreeVoxelsAndNodesFunction, NULL);
}
VX *
PvxFromPtn(TN *ptn)
{
	if (ptn == NULL)
		return NULL;
	return ptn->pvx;
}
void
PrintVoxelTreeToTextFile(TN *ptn, int it)
{
	char rgch[100];
	sprintf(rgch,"voxeltree_%d.txt", it);
	FILE *pfl = fopen(rgch, "w+");

	printf("printing voxel tree to %s...\n", rgch);
	TraverseTree(ptn, &PrintVoxelToFile, pfl);
	fclose(pfl);
	printf("printing voxel tree to %s...done\n", rgch);
}
void
PrintVoxelTree(TN *ptn)
{
	printf("printing voxel tree...\n");
	TraverseTree(ptn, &PrintVoxel, NULL);
	printf("printing voxel tree...done\n");
}
void 
InsertVoxel(TN **pptn, VX *pvxNew)
{
	if (pvxNew == NULL)
		return;

	//TN *ptn = *pptn;
	/* return if the voxel is already in the tree */
	//int keyNew = KeyFromRgco(RgcoFromPvx(pvxNew));	
	//void *pv = PvFindElement(ptn, keyNew);
	
	//VX *pvx = PvxFindVoxel(ptn, XFromPvx(pvxNew), YFromPvx(pvxNew), ZFromPvx(pvxNew));
	//if (pvx != NULL)
		//return;
	
	/* insert voxel into the tree */
	InsertTreeNode(pptn,  pvxNew);
	if (*pptn == NULL)
		printf("ptn is Null in InsertVoxel\n");
}	
void
CreateLinksWithVoxelsInTree(TN *ptn, VX *pvx)
{
	if (pvx == NULL)
		{
		printf("Trying to find neighbour pointers for a NULL voxel.\n");
		return;
		}
	if (ptn == NULL)
		{
		printf("Trying to find neighbour pointers in a NULL tree.\n");
		return;
		}

	/* find its neighbours in the tree and set bidirectional links */
	VX *pvxNeigh = NULL;
	int *rgcoNeigh = NULL;
	int wy = 0;
	for (wy = 0; wy < 6; wy++)
		{
		rgcoNeigh = RgcoGetNeighbourCoordinates(RgcoFromPvx(pvx), wy);
		pvxNeigh = PvxFindVoxel(ptn, rgcoNeigh[0], rgcoNeigh[1], rgcoNeigh[2]);
		free(rgcoNeigh);
		if (pvxNeigh != NULL)
			SetVoxelsAsNeighbours(pvx, pvxNeigh, wy);
		}
}
void 
WriteVoxelTree(TN *ptn, char *szFilename)
{
	FILE *pfh = fopen(szFilename, "w+");
	TraverseTree(ptn, &WriteVoxel, pfh); 
	fclose(pfh);
}
VX *
PvxRemoveVoxel(TN **pptn, int x, int y, int z)
{
	if (*pptn == NULL)
		return NULL;	// not found

	/*VX *pvx = PvxFromPtn(*pptn);*/
	VX *pvx = (*pptn == NULL) ? NULL : (*pptn)->pvx;
	//int x = XFromPvx(pvxRem);
	//int y = YFromPvx(pvxRem);
	//int z = ZFromPvx(pvxRem);

	if (CompareVoxels(x, y, z, XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx)) < 0)
		{
		return PvxRemoveVoxel(&(*pptn)->ptnLeft, x, y, z);
		}

	if (CompareVoxels(x, y, z, XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx)) > 0)
		{
		return PvxRemoveVoxel(&(*pptn)->ptnRight, x, y, z);
		}

	return PvxDeleteTreeNode(pptn);
}
void
Set26VoxelNeighbours(TN *ptn, void *pvTreeRoot)
{
	TN *ptnTreeRoot = (TN *) pvTreeRoot;
	if (ptnTreeRoot == NULL)
		printf("tree is empty\n");
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	int x = XFromPvx(pvx);
	int y = YFromPvx(pvx);
	int z = ZFromPvx(pvx);
	VX *pvx26Neigh = NULL;
	VX **rgpvx = (VX **) malloc(sizeof(VX *) * 27);

	int k,j,i;
	for(k=0; k<3; k++)
		for (j=0; j<3; j++)
			for (i=0; i<3; i++)
				{
				pvx26Neigh = PvxFindVoxel(ptnTreeRoot, x-1+i,y-1+j,z-1+k);
				rgpvx[i+3*j+9*k] = pvx26Neigh;
				//if (pvx26Neigh != NULL && pvx26Neigh->tg != 9)
					//printf("tag = %i\n", pvx26Neigh->tg);
					//assert(TgFromPvx(pvx26Neigh) == 0);
				}

	SetVoxelData(pvx, rgpvx);
}

//void
//Get26VoxelNeighbours(VX *pvx, VX **rgpvx)
//{
	//VX *pvxStart = PvxGetNeighbour(PvxGetNeighbour(PvxGetNeighbour(pvx, wyZMinus1), wyYMinus1), wyXMinus1); 
	//VX *pvxX = pvxStart;
	//VX *pvxY = pvxStart;
	//VX *pvxZ = pvxStart;
	//int k,j,i;
	//for(k=0; k<3; k++)
		//{
		//for (j=0; j<3; j++)
			//{
			//for (i=0; i<3; i++)
				//{
				//rgpvx[i+3*j+9*k] = pvxX;
				//pvxX = PvxGetNeighbour(pvxX, wyXPlus1);
				//}
			//pvxY = PvxGetNeighbour(pvxY, wyYPlus1);
			//pvxX = pvxY;
			//}
		//pvxZ = PvxGetNeighbour(pvxZ, wyZPlus1);
		//pvxX = pvxZ;
		//pvxY = pvxZ;
		//}
//}



