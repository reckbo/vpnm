#include <stdlib.h>
#include <malloc.h>
#include <math.h>
#include <stdio.h>
#include "voxel.h"
#include "VoxelTree.h"

typedef struct GK GK;
struct GK
{
	double *rgvlKernel;
	double vlKernelSum;
	int wy;
	int offset;
	int ivlVoxelValueToSet;
	int ivlVoxelValueToUse;
};

double 
gaussian(double x, double sigma, double center)
{
  double result;
  
  result = exp(-0.5 * pow((x - center)/sigma, 2.0));
  return result;
}
void 
MakeGaussianKernel(double sigma, double range, double center, double kernel[], int offset)
{
  int position;
  
  for (position = 0; position <= 2*offset; position++)
    kernel[position] = gaussian((double)(position - offset),sigma, center);  
}
void
MakeTempStorageAtVoxel(TN *ptn, void *pv)
{
	double *rgvl = (double *) malloc(sizeof(double)*3);
	if (rgvl == NULL)
		{
		printf("Failed to make temp storage at voxel in MakeTempStorageAtVoxel(...)\n");
		exit(1);
		}
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	rgvl[0] = VlFromPvx(pvx);
	rgvl[1] = rgvl[0];
	rgvl[2] = rgvl[0];
	SetVoxelData(pvx, rgvl);
}
void
ClearTempStorageAtVoxel(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	double *rgvl = NULL;
    rgvl = (double *) PvGetVoxelData(pvx);
	if (rgvl == NULL) printf("temp storage is NULL\n");
	SetVoxelValue(pvx, rgvl[0]);
	SetVoxelData(pvx, NULL);
	free(rgvl);
}
void
Convolve1D(TN *ptn, void *pvKernel)
{
	GK *pgk = (GK *) pvKernel;
	if (pgk == NULL)
		{
		printf("Kernel is NULL\n");
		return;
		}
	if (ptn == NULL)
		{
		printf("Error: NULL voxel tree passed to Convolve1D.\n");
		exit(1);
		}

	double vlConvolveSum = 0;
	double *rgvl = NULL;
	int wy = pgk->wy;
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	if (pvx == NULL)
		printf("Error: tree node has NULL voxel\n");
	VX *pvxToUpdate = NULL;
	int ivl = 0;

	while (ivl < (2*pgk->offset+1))
		{
		if (pvx == NULL)
			return;
		rgvl = (double *) PvGetVoxelData(pvx);
		if (rgvl == NULL)
			{
			printf("No temp storage at voxel needed by Convolve1D.\n");
			printf("x=%i, y=%i, z=%i, ivl=%i, offset=%i\n", XFromPvx(pvx),YFromPvx(pvx),ZFromPvx(pvx),ivl,pgk->offset);
			exit(1);	
			}
		//printf("x=%i, y=%i, z=%i, ivl=%i, offset=%i\n", XFromPvx(pvx),YFromPvx(pvx),ZFromPvx(pvx),ivl,pgk->offset);
		vlConvolveSum += (pgk->rgvlKernel[ivl] * rgvl[pgk->ivlVoxelValueToUse]);
		if (ivl == pgk->offset)
			pvxToUpdate = pvx;
		pvx = PvxGetNeighbour(pvx, wy);
		ivl++;
		}

		rgvl = (double *) PvGetVoxelData(pvxToUpdate);
		rgvl[pgk->ivlVoxelValueToSet] = vlConvolveSum/(pgk->vlKernelSum);
}
void
GaussianSmooth(TN *ptn, double sigma, double range)
{
	GK *pgk = (GK *) malloc(sizeof(GK));
	if (pgk == NULL)
		return;
	pgk->rgvlKernel = (double *) malloc( sizeof(double) * (2*range*sigma+1) );	
	pgk->offset = (int)(range * sigma);
	MakeGaussianKernel(sigma, range, 0.0, pgk->rgvlKernel, pgk->offset);
	pgk->vlKernelSum = 0;
	int i = 0;
	for(i = 0; i <= 2*pgk->offset; i++)
		{
		pgk->vlKernelSum += pgk->rgvlKernel[i];
		}

	TraverseTree(ptn, &MakeTempStorageAtVoxel, NULL);	

	pgk->ivlVoxelValueToUse = 0;
	pgk->ivlVoxelValueToSet = 1;
	pgk->wy = wyXPlus1;
	TraverseTree(ptn, &Convolve1D, pgk);

	pgk->ivlVoxelValueToUse = 1;
	pgk->ivlVoxelValueToSet = 2;
	pgk->wy = wyYPlus1;
	TraverseTree(ptn, &Convolve1D, pgk);

	pgk->ivlVoxelValueToUse = 2;
	pgk->ivlVoxelValueToSet = 0;
	pgk->wy = wyZMinus1;
	TraverseTree(ptn, &Convolve1D, pgk);

	TraverseTree(ptn, &ClearTempStorageAtVoxel, NULL);

	free(pgk->rgvlKernel);
	free(pgk);
}
