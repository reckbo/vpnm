

//#include "IM.h"


void Dump_Position_of_Real_Filament(Coordinates **Position, int Fil_Num, double t);

void Dump_Position_of_Thinned_Filament(Coordinates **Position, int Fil_Num, int Iteration);

void Swap_Filament(Coordinates **Fil, int FilNum);

int Locate_Closest_First_Voxel(Coordinates **Filament_Array, double xcord, double ycord, double zcord, int *which_fil);

int Locate_Closest_Voxel(Coordinates **Filament_Array, double xcord, double ycord, double zcord, int *which_fil);

void  Robustly_Check_Direction_of_Ordering(Coordinates **Previous_Filament, Coordinates **Current_Filament, int Fil);

void Solve_Linear_System(int n, double A[NODES], double B[NODES], double C[NODES], double F[NODES]);

void Periodic_Smoothing_Spline(Coordinates **Position, double weight);

//void Thin_Height_For_Display(double_TYPE ***Height, int Iteration_Number);

//void Update_Current_Filament(TN *ptn, Coordinates **P, int Iteration_Number);  

