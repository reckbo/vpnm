/**************************************************************************

   File:                list.h

   Author(s):           Sylvain Bouix and Kaleem Siddiqi

   Created:             Feb 2000

   Last Revision:       $Date: 2006/09/18 13:39:56 $

   Description: 

   $Revision: 1.1.1.1 $

   $Log: list.h,v $
   Revision 1.1.1.1  2006/09/18 13:39:56  eckbo
   Imported source code.

   Revision 1.1.1.1  2006-09-17 23:43:31  ryan
   Imported source code.

   Revision 1.1.1.1  2006-09-17 02:39:57  ryan
   Import of vortex filament project

   Revision 1.1.1.1  2006-09-16 23:13:56  ryan


   Revision 1.1.1.1  2006/04/19 19:51:21  eckbo
   Previously backed up code before the version that made the single elliptical
   vortex movie broke.


	
   Copyright (c) 2000 by Sylvain Bouix and Kaleem Siddiqi, Centre for
   Intelligent Machines, McGill University, Montreal, QC.  Please see
   the copyright notice included in this distribution for full
   details.

 **************************************************************************/

typedef struct Cell *list;

typedef struct Cell {
  int x;
  int y;
  int z;
  float value;
  list prev;
  list next;
} cell;


list createList();
list insertCell(cell *, list);
list deleteCell(cell *, list);

