/************************************************************************

   File:		heap.c

   Author(s):		Sylvain Bouix and Kaleem Siddiqi

   Created:		Feb 2000

   Last Revision:	$Date: 2006/10/18 16:04:56 $

   Description:	

   $Revision: 1.2 $

   $Log: heap.c,v $
   Revision 1.2  2006/10/18 16:04:56  eckbo
   removed files, added mymath.c,h, cleaned up header includes

   Revision 1.1.1.1  2006/09/18 13:39:56  eckbo
   Imported source code.

   Revision 1.1.1.1  2006-09-17 23:43:31  ryan
   Imported source code.

   Revision 1.1.1.1  2006-09-17 02:39:57  ryan
   Import of vortex filament project

   Revision 1.1.1.1  2006-09-16 23:13:56  ryan


   Revision 1.1.1.1  2006/04/19 19:51:21  eckbo
   Previously backed up code before the version that made the single elliptical
   vortex movie broke.


	
   Copyright (c) 2000 by Sylvain Bouix and Kaleem Siddiqi, Centre for
   Intelligent Machines, McGill University, Montreal, QC.  Please see
   the copyright notice included in this distribution for full
   details.

 ***********************************************************************/
#include "voxel.h"
#include "heap.h"
#include <stdio.h>
#include <stdlib.h>

unsigned int parent (unsigned int i){
  return((int)(i/2));
}

unsigned int left (unsigned int i) {
  return ((2*i));
}

unsigned int right(unsigned int i){
  return ((2*i)+1);
}

void heapify(heap *A, unsigned int i) {
  unsigned int l,r,largest;
  point temp;

  l = left(i);
  r = right(i);
  if ((l <= A->size) && (A->keys[l].value > A->keys[i].value))
    largest = l;
  else
    largest = i;
  
  if ((r <= A->size) && (A->keys[r].value > A->keys[largest].value))
    largest = r;
  
  if (largest != i) {
    temp             = A->keys[i];
    A->keys[i]       = A->keys[largest];
    A->keys[largest] = temp;

    heapify(A,largest); 
  } 
}

void buildHeap (heap *A, point * input, unsigned int size) {
  unsigned int i;
  for (i = 0; i<size; i++) {
    A->keys[i+1] = input[i];
  }
    
  A->size = size;
  for (i = ((size/2)); i >=1 ; i--)
    heapify(A,i);
}

point heapExtractMax (heap *A) {
  point max;

  if (A->size < 1 ){
    printf("Heap Underflow\n");
    exit(0);
  }

  max        = A->keys[1];
  A->keys[1] = A->keys[A->size];

  A->size = A->size-1;
  heapify(A,1);
  return max;
}
 
void heapInsert (heap *A, point key) {
  unsigned int i;

  A->size= A->size+1;
  i = A->size;
  
  while ((i>1) && (A->keys[parent(i)].value < key.value)) {
    A->keys[i] = A->keys[parent(i)];
    i = parent(i);
  }

  A->keys[i] = key; 
}

















