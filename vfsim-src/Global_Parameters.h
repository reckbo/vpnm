
/*-----------------------------------------------------------------------------
 *  MODIFIED HEADER FILE FOR 2 FILAMENTS ABOVE CRITICAL HALF ANGLE
 *-----------------------------------------------------------------------------*/


/* Abeer's dimensions for ellipse*/
//#define X_Res 340
//#define Y_Res 340
//#define Z_Res 200

/* My dimensions for ellipse */
//#define X_Res 150
//#define Y_Res 150
//#define Z_Res 75

//#define X_Res 350
//#define Y_Res 350
//#define Z_Res 150

//#define X_Res 75
//#define Y_Res 75
//#define Z_Res 50


//#define X_Res 310
////#define Y_Res 380
//#define Y_Res 420  //****MOD****//
//#define Z_Res 360

//This is used to find the min. distance of voxels from the filament
#define LARGE_NUMBER 10000.0

//This timestep is the one that stabilizes the sim. without ANY time integ. technique
//#define ORIGINAL_TIMESTEP 0.001
//This is the timstep to be used in the time-integration technique
//#define TIMESTEP 0.01
//#define TIMESTEP 0.005
//#define NUM_OF_ITERATIONS 1000

/*define the parameter that determines which version of the Biot_savart intetgral to use in evolving the filament
 'h' indicates that we want to use the high order Biot_Savart while 'd' indicates that we want to use the discrete version*/
//#define BIOT_SAVART_ORDER 'h'
//#define BIOT_SAVART_ORDER 'd'

/*the following is the threshold number of nodes per filament bellow which, we need to add more nodes and use interpolating
  splines in order to maintain resoluation on the filament. This regridding is used in collaberation with the Biot-Savart
  simulation when evolving filaments.*/
//#define N_PER_FIL 500
//#define N_PER_FIL 628 	//original for ellipse

//when using the Hybrid simulation we need to thin every N iterations where n is the min. # of iter. needed to move by 1 voxel  
//#define THIN_EVERY 3

//#define NUM_OF_NODES 800
//#define X_CENTER_OF_FILAMENT 120.0  //X and Y center of filament in voxels
//#define Y_CENTER_OF_FILAMENT1 120.0
//#define Y_CENTER_OF_FILAMENT2 340.0
//#define Z_CENTER_OF_FILAMENT 150.0
//#define RADIUS 100.0               //Radius of filament in voxels
//#define RADIUS 140.0               //original for ellipse 
//#define INV_RADIUS 1.0/RADIUS 
/*This is the distance over which central differences are taken to compute the gradient.
  The distance is equivalent to 2 voxels -scaled to the actual distance*/
//#define INV_DELTA RADIUS/2.0  

//These constants are to initialize the arrays of the type Coordinates defined later
//#define NumOfFil 2 //this is 1 plus the number of filaments simulated because i index filament arrays from 1 not 0
#define NODES 10000  //this is an upper limit on the number of nodes per filament
//#define CAPACITY (NumOfFil-1)*NODES  //This is an upper limit over the total # of nodes for ALL filaments

//#define ALPHA 0.2065 
//#define CORE 0.1
//#define CORE 0.1*75
//#define CORE_SQUARED CORE*CORE
//#define BSDEN_TERM ALPHA*2.0*CORE_SQUARED
//#define PI M_PI  //form the math.h library
//#define COEF -1.0/(4.0*PI)
//#define COEF -75/(4.0*PI)


//this is an upper bound on the number of branch clusters present on our grid 
//#define MAX_BRANCH 10
//this is an upper bound on the number of branch voxels that could constitue a branch cluster 
//#define BRANCH_CLUSTER 10


//this determines the order of the RungeKutta time integ. scheme to be used. Both second and fourth order implementations are posiible
//#define RUNGE_KUTTA_ORDER 2

/*Since we are using a Real distance function, we don't have a certain surface that we follow, instead, we can choose
  any thickness from the filament and follow that as it evolves. For technical reasons,the following constant is that
  thickness that we wish to follow. In addition, the Narrow Band will be taken around that thickness.*/
//#define THICKNESS 0.03 
//#define THICKNESS 4 
//#define THICKNESS 4 


/*This constant is set to the distance around surface (zero level set) over which update of the Height function will be performed.
  When we use a Real distance function that calculates the real distance of each voxel from a filament, we don't get a zero-set.
  Therefore, we have to decide the "distance" from our filament around which we'll take the NarrowBand limit to be*/
//#define Range_of_NarrowBand 0.07  //this is around 7 voxels when filament radius=100 voxels
//#define Range_of_NarrowBand 0.04  //original for ellipse
//#define Range_of_NarrowBand 0.05  
//#define NarrowBand_Size 2000000 //estimated initial size of NarrowBand 
/*This is the number of voxels where if our Level-set is within it from the NarroBand, we reinitialize the Height function.
  This number should also be GREATER than the equivalent number of voxels that our surfaces can move during one iteration.
  This prevents "skipping" the NarrowBand boundary in any given iteration.*/
//#define NB_SLACK 3.0  

/*Define the weight used in smoothing splines i.e. how far do we want our spline to be from our data set.
  As WEIGHT goes to infinity we approach interpolating the data set and as WEIGHT approaches 0 we get maximal 
  smooothing and we are reduced to 1 point since our filaments are closed*/
//#define WEIGHT RADIUS
//#define WEIGHT RADIUS //original for ellipse


/*Directory definitions for dumping information*/
#define HEIGHT_DIRECTORY "."
#define FILAMENT_DIRECTORY "."
#define THIN_FIL_DIRECTORY "."
#define REAL_FIL_DIRECTORY "."

#define MAX_FILAMENTS 10



/* Global variables - these stay constant during runtime */
extern int cvxSubSampleResolution;
extern double lCoreRadius;
extern double alpha;
extern double ll_tCirculation;
//extern char  chBiotSavartIntegral;
extern int  chBiotSavartIntegral;
extern double dtTimeStep;
extern double dtSmallTimeStep;
extern double tMax;
extern double tMin;
extern double cvxResolution;
extern int fHighOrderIntegration;
extern double lNarrowBandRadius;
extern double lNarrowBandBoundary;
//extern double lTubeRadius;
extern int fUseHybrid;
extern double dtThinEvery;
extern double dtDumpEvery; 
extern double vlSplineWeight;
extern double ReynoldsNumber;

//#define tgNotInNarrowBand 		3

#undef   MAX
#define  MAX( x, y )  ( ((x) >= (y)) ? (x) : (y) )
#undef   MIN
#define  MIN( x, y )  ( ((x) <= (y)) ? (x) : (y) )
#define EPSILON 0.0001   
#define double_EQ(x,v) (((v - EPSILON) < x) && (x <( v + EPSILON)))

#define tgInsideBarrier 	4 //if (lDist < lCoreRadius)
#define tgOutsideBarrier 	5 //if (lDist > lCoreRadius)
#define tgNonBarrier 		6 //if (lDist >= (lCoreRadius-(lNarrowBandBoundary/cvxResolution)) 
							  // && lDist  < (lCoreRadius+(lNarrowBandBoundary/cvxResolution)))
#define tgBoundary 			7 //voxels adjacent to the narrow band, both inside and outside of it.

#define tgJunctionVoxel 	8 //if the voxel is part of a junction
#define tgKeep 				9 
#define tgLooseEndConnected 11			 
#define tgDelete 			10 

#define TRUE 1
#define FALSE 0
#define LARGE 1e9


