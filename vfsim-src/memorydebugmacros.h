#ifndef __MEMORYDEBUGMACROS__
#define __MEMORYDEBUGMACROS__
#ifdef DEBUGMEM
#include "malloc.h"
#define FREE(ptr) printf("Freeing " #ptr " %x\n",ptr);\
                  free(ptr);
#define MALLOC(ptr,number,type) \
        if ((ptr=(type *)malloc((number)*sizeof(type)))\
             ==(type *)NULL) {\
            fprintf(stderr,"Out of Memory");\
	    exit(-1);\
	}\
        printf("Allocating " #ptr "%x with %d " #type " (%d) bytes\n",ptr,\
                (number),(number)*sizeof(type));
#define CALLOC(ptr,number,type) \
        printf("CAllocating " #ptr " with %d " #type " (%d) bytes\n",\
        (number),(number)*sizeof(type));\
        if (( ptr=(type *)calloc((number), sizeof(type)))\
	    ==(type *)NULL) {\
	  fprintf(stderr,"Out of Memory");\
	  exit(-1);\
	}
#define REALLOC(ptr,number,type) \
        printf("REAllocating " #ptr " with %d " #type " (%d) bytes\n",\
                (number),(number)*(sizeof(type)));\
	if (( ptr=(type *)realloc(ptr,(number)*(sizeof(type))))\
              ==(type *)NULL) {\
	  fprintf(stderr,"Out of Memory");\
	  exit(-1);\
	      }
#else
#include <malloc.h>
#define FREE(ptr) free(ptr);
#define MALLOC(ptr,number,type) \
        if ((ptr=(type *)malloc((number)*sizeof(type)))\
             ==(type *)NULL) {\
            fprintf(stderr,"Out of Memory");\
	    exit(-1);\
	}
#define CALLOC(ptr,number,type) \
        if (( ptr=(type *)calloc((number), sizeof(type)))\
	    ==(type *)NULL) {\
	  fprintf(stderr,"Out of Memory");\
	  exit(-1);\
	}
#define REALLOC(ptr,number,type) \
	if (( ptr=(type *)realloc(ptr,(number)*sizeof(type)))\
              ==(type *)NULL) {\
	  fprintf(stderr,"Out of Memory");\
	  exit(-1);\
	      }
#endif
#endif
