void
ComputeAndSaveNewVoxelValue4thOrder(TN *ptn, void *pvBoundaryIsHitFlag)
{
	/*******************OBSOLETE FUNCTION **************************/
	int *pfBoundaryIsHit = (int *) pvBoundaryIsHitFlag;

	VX *pvx = PvxFromPtn(ptn);
	RK *prk = (RK *) PvGetVoxelData(pvx);	
	/* use 4 Runge-Kutta time derivatives to find new voxel value */
	float vlNew = VlFromPvx(pvx) + (dtTimeStep/6.0) * (prk->rgdvl_dt[1] + 
										2.0* prk->rgdvl_dt[2] +
										2.0* prk->rgdvl_dt[3] + 
											 prk->rgdvl_dt[4] );

	//printf("time derivative in ComputeAndSaveNewVoxelValue = %f, vlTrialStep=%f,\n", prk->rgdvl_dt[1], dtTimeStep*prk->rgdvl_dt[]+VlFromPvx(pvx));
	SetVoxelValue(pvx, vlNew);
	
	/* check to see whether the front being tracked as hit a boundary voxel */
	//if ( (vlNew-3) < 0.5 && TgFromPvx(pvx) == tgBarrier)
		//*pfBoundaryIsHit = TRUE;
}
void
ComputeAndSaveNewVoxelValue1stOrder(TN *ptn, void *pvBoundaryIsHitFlag)
{
	/*******************OBSOLETE FUNCTION **************************/

	int *pfBoundaryIsHit = (int *) pvBoundaryIsHitFlag;
	VX *pvx = PvxFromPtn(ptn);
	RK *prk = (RK *) PvGetVoxelData(pvx);	

	int wy=0;
	for (wy=0; wy<6; wy++)
		{
		if ( PvxGetNeighbour(pvx, wy) == NULL)
			{
			RK *prkOpposite = (RK *) PvGetVoxelData(PvxGetNeighbour(pvx, WyGetOppositeNeighbourDirection(wy)));
			prk->rgdvl_dt[1] = prkOpposite->rgdvl_dt[1];
			}	
		}
	
	float vlNew = VlFromPvx(pvx) + dtTimeStep*prk->rgdvl_dt[1];

	SetVoxelValue(pvx, vlNew);
	
	//if ( (vlNew > lCoreRadius) && TgFromPvx(pvx) == tgBarrier)
		//*pfBoundaryIsHit = TRUE;
}
int
FUpdateFront4thOrder(TN *ptn, Coordinates **Position)
{
	printf("Performing 4th order update...\n");

	Coordinates **New_Position = (Coordinates **) malloc(sizeof(Coordinates *)*Position[0][0].xcord);
	int irgfn = 0;
	for (irgfn = 0; irgfn < Position[0][0].xcord; irgfn++)
		{
		New_Position[irgfn] = (Coordinates *) malloc(sizeof(Coordinates)*NODES);
		}

	int fBoundaryIsHit = 0;
	int idvl_dtRungeKuttaStep;

	idvl_dtRungeKuttaStep = 1;
	ComputeFluidVelocityAtVoxels(ptn, Position); 
	TraverseTree(ptn, &CalculateRungeKuttaDeriv, &idvl_dtRungeKuttaStep);
	TraverseTree(ptn, &TakeNewTrialStep, &idvl_dtRungeKuttaStep);

	idvl_dtRungeKuttaStep = 2;
	Accurate_NewFilament_Position(Position, New_Position, 0.5*dtTimeStep, FALSE);
	ComputeFluidVelocityAtVoxels(ptn, New_Position); 
	TraverseTree(ptn, &CalculateRungeKuttaDeriv, &idvl_dtRungeKuttaStep);
	TraverseTree(ptn, &TakeNewTrialStep, &idvl_dtRungeKuttaStep);

	idvl_dtRungeKuttaStep = 3;
	TraverseTree(ptn, &CalculateRungeKuttaDeriv, &idvl_dtRungeKuttaStep);
	TraverseTree(ptn, &TakeNewTrialStep, &idvl_dtRungeKuttaStep);

	idvl_dtRungeKuttaStep = 4;
	Accurate_NewFilament_Position(Position, New_Position, 1.0*dtTimeStep, FALSE);
	ComputeFluidVelocityAtVoxels(ptn, New_Position); 
	TraverseTree(ptn, &CalculateRungeKuttaDeriv, &idvl_dtRungeKuttaStep);

	TraverseTree(ptn, &ComputeAndSaveNewVoxelValue4thOrder, &fBoundaryIsHit);

	for (irgfn = 0; irgfn < Position[0][0].xcord; irgfn++)
		{
		free(New_Position[irgfn]);
		}

	return fBoundaryIsHit;
}
int
FUpdateFront1stOrder(TN *ptn, Coordinates **Position)
{
	printf("Performing 1st order update...\n");

	int fBoundaryIsHit = 0;
	int idvl_dtRungeKuttaStep;

	ComputeFluidVelocityAtVoxels(ptn, Position); 

	idvl_dtRungeKuttaStep = 1;
	TraverseTree(ptn, &CalculateRungeKuttaDeriv, &idvl_dtRungeKuttaStep);
	TraverseTree(ptn, &ComputeAndSaveNewVoxelValue1stOrder, &fBoundaryIsHit);

	Accurate_NewFilament_Position(Position, Position, dtTimeStep, TRUE);

	return fBoundaryIsHit;
}
void
SaveVoxelsToDisk(TN *ptn, float t, char *sz)
{
	IM_TYPE *HeightPointer = im_create("/dev/null", FLOAT, X_Res, Y_Res, Z_Res); 
	FLOAT_TYPE ***Height = (FLOAT_TYPE ***) im_alloc3D(HeightPointer, FLOAT);
	int x, y, z;
	/* initialize volume to 0 */
	for (x=0; x<X_Res; x++)
		for (y=0; y<Y_Res; y++)
			for (z=0; z<Z_Res; z++)
				Height[z][y][x] = -0.5;

	/* preorder traversal */
	TraverseTree(ptn, &SetHeightArray, (void *) Height);
	
	//IM_TYPE *Height_Image;
	//printf("Dumping narrow band to file...\n");
	//sprintf(filename,"%s/%s%06f.im", "", sz, t);
	//printf("%s%06f.im: %i by %i by %i\n", sz, t, xMax, yMax, zMax);
	//Height_Image = im_create(filename, FLOAT, xMax, yMax, zMax);
	//im_write(Height_Image, FLOAT, (char *) &(Current_Height[0][0][0]));
	//printf("Dumping narrow band to file...done\n");
	Dump_Height(Height, t, sz, X_Res, Y_Res, Z_Res);
    im_free3D((char ***) Height);
}
void
SaveVoxelsToDiskSS(int sc, TN *ptn, float t, char *sz)
{
	IM_TYPE *HeightPointer = im_create("/dev/null", FLOAT, sc*X_Res, sc*Y_Res, sc*Z_Res); 
	FLOAT_TYPE ***Height = (FLOAT_TYPE ***) im_alloc3D(HeightPointer, FLOAT);
	int x, y, z;
	/* initialize volume to 0 */
	for (x=0; x<sc*(X_Res); x++)
		for (y=0; y<sc*(Y_Res); y++)
			for (z=0; z<sc*(Z_Res); z++)
				Height[z][y][x] = 1.0;

	/* preorder traversal */
	TraverseTree(ptn, &SetHeightArray, (void *) Height);
	//TraverseTree(ptnTreeRoot, Height);
	//Dump_Height(Height, it);
	Dump_Height(Height, t, sz, sc*X_Res, sc*Y_Res, sc*Z_Res);
    im_free3D((char ***) Height);
}
void
SetHeightArray(TN *ptn, void *pv)
{
	if (ptn == NULL)
		return;
	VX *pvx = PvxFromPtn(ptn);
	int x = XFromPvx(pvx);
	int y = YFromPvx(pvx);
	int z = ZFromPvx(pvx);
	//printf("x = %d, y = %d, z = %d \n", x, y, z);
	float ***Height = (float ***) pv;
	Height[z][y][x] = VlFromPvx(pvx);
}

