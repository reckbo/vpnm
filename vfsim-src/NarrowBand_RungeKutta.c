//#include <malloc.h>
#include <stdlib.h>
#include <stdio.h>
#include "memorydebugmacros.h"
#include <math.h>
#include <fibheap.h>
#include "assert.h"
#include <pthread.h>

#include "mymath.h"

#include "voxel.h"

#include "VoxelTree.h"

#include "Global_Parameters.h"
#include "fmm.h"
#include "NarrowBand_RungeKutta.h"
#include "FluidVelocityRoutines.h"
//#include "Dump_Info.h"
//#include "Filament_Functions.h"
#include "thin_ryan.h"
#include "debug.h"
#include "IM.h"
#include "IM_convert.h"


int dxTrans,dyTrans,dzTrans;

#define MAX_JUNCTION_VOXELS 10
#define MAX_JUNCTIONS 		20
#define MAX_LINKING_VOXELS  50

extern void Dump_Position_of_Real_Filament(Coordinates **Position, int Fil_Num, double t);
extern void GaussianSmooth(TN *, double, double);
extern void  Robustly_Check_Direction_of_Ordering(Coordinates **Previous_Filament, Coordinates **Current_Filament, int Fil);
extern void  My_Robustly_Check_Direction_of_Ordering(Coordinates **Previous_Filament, Coordinates **Current_Filament, int Fil);
extern void Swap_Filament(Coordinates **Fil, int FilNum);
TN *ptnRoot;
TN *ptnExtGlobal;
double dvxMax;
double dvxMin;
double dvxRKMax;
double dvxRKMin;
int tgMaxDist;
double rgr[4];
double rgdvlVoxelsTraversed[2];
double lDistMin;
double lDistMax;
Coordinates **GlobalPosition;
int iGlobalPosition;

typedef struct FIL {
  TN **pptnInt; 
	TN **pptnIntBoundary;
	TN **pptnNB;
	TN **pptnExtBoundary;
  Coordinates *Position;
  int i;
} FIL;


//#undef   MAX
//#define  MAX( x, y )  ( ((x) >= (y)) ? (x) : (y) )
//#undef   MIN
//#define  MIN( x, y )  ( ((x) <= (y)) ? (x) : (y) )
//
//#define tgInsideBarrier 	4
//#define tgOutsideBarrier 	5
//#define tgNonBarrier 		6 
//#define tgBoundary 		7
//
//#define TRUE 1
//#define FALSE 0
//#define LARGE 1e9


/*************************************************************************************************************	      
 This is an auxliary function for funciton Robustly_Check_Direction_of_Ordering below. 
 Given a node position given by xcord, ycord and zcord, this function finds the node in array 
 Filament_Array whose position is closest to the given position. It returns the array position of this node.
 The function also updates the integer "which_fil" that indicates the number of the filament where we found the
 current node.
****************************************************************************************************************/
/* int Locate_Closest_Voxel(Coordinates **Filament_Array, double xcord, double ycord, double zcord, int *which_fil) */
/* { */
/*  */
  /* int filnum, i, j, index, fil_index; */
  /* double Min_Dist, xdist, ydist, zdist, distance; */
/*  */
/*  */
/*  */
  /* //get the total number of filaments stored in Filament_Array */
  /* filnum = (int)Filament_Array[0][0].xcord; */
/*  */
  /* //initialize Min_Dist to a very large number */
  /* Min_Dist = LARGE_NUMBER; */
/*  */
/*  */
  /* //go through every voxel Filament_Array and find the array position of the closest voxel to the given location */
  /* for(i=1;i<=filnum;i++) */
    /* for(j=1;j<=(int)Filament_Array[i][0].xcord;j++) */
      /* { */
	/* xdist = xcord - Filament_Array[i][j].xcord; */
	/* ydist = ycord - Filament_Array[i][j].ycord; */
	/* zdist = zcord - Filament_Array[i][j].zcord; */
/*  */
	/* distance = sqrt((xdist*xdist) + (ydist*ydist) + (zdist*zdist)); */
/*  */
	/* if(distance<Min_Dist) */
	  /* { */
	    /* Min_Dist = distance; */
	    /* index = j; */
	    /* fil_index = i; */
	  /* } */
      /* } */
/*  */
  /*  */
  /* //update the number of the filament where the current node was found */
  /* *which_fil = fil_index; */
/*  */
  /* //return the closest array position of the given voxel */
  /* return(index); */
/*  */
/* } */
/*************************************************************************************************************	      
 This is an auxliary function for funciton Robustly_Check_Direction_of_Ordering below. 
 Given a node location described by xcord, ycord and zcord, this function finds the node in array 
 Filament_Array whose position is closest to the given location. It returns the array position of this node
 provided that it's location is in the "middle" of the located filament i.e. at some distance from both
 the start and end voxel locations of the coresponding filament. This critical distance is 20 voxels 
 since all our filaments contain at least 400 voxels (400>>20). If the closet array location turns out to be
 too close to either the start or end locations, then the function returns 0.(the 2D array  Filament_Array
 is not zero-indexed so node location 0 doesn't represent a valid array entry)
 The function also updates the integer "which_fil" that indicates the number of filament where we found the
 current node (once we find a suitable first node). If the current node is not a "suitable" first
 node, then we don't modify "which_fil" and we leave it with it's initial value. 
****************************************************************************************************************/
/* int Locate_Closest_First_Voxel(Coordinates **Filament_Array, double xcord, double ycord, double zcord, int *which_fil) */
/* { */
/*  */
  /* int filnum, i, j, node_index, fil_index, fil_nodes; */
  /* double Min_Dist, xdist, ydist, zdist, distance; */
/*  */
/*  */
/*  */
  /* //get the total number of filaments stored in Filament_Array */
  /* filnum = (int)Filament_Array[0][0].xcord; */
/*  */
  /* //initialize Min_Dist to a very large number */
  /* Min_Dist = LARGE_NUMBER; */
/*  */
/*  */
  /* //go through every voxel Filament_Array and find the array position of the closest voxel to the given location */
  /* for(i=1;i<=filnum;i++) */
    /* for(j=1;j<=(int)Filament_Array[i][0].xcord;j++) */
      /* { */
	/* xdist = xcord - Filament_Array[i][j].xcord; */
	/* ydist = ycord - Filament_Array[i][j].ycord; */
	/* zdist = zcord - Filament_Array[i][j].zcord; */
/*  */
	/* distance = sqrt((xdist*xdist) + (ydist*ydist) + (zdist*zdist)); */
/*  */
	/* if(distance<Min_Dist) */
	  /* { */
	    /* Min_Dist = distance; */
	    /* node_index = j; */
	    /* fil_index = i; */
	  /* } */
      /* } */
/*  */
/*  */
/*  */
  /*return the closest array position of the given voxel provided this location is 
    within 20 voxels from the start and end locations of the filament, otherwise return 0.
    We need to find a first location which is some distance away from the danger zones
    of the start and end locations, that's why we pick a "first" location which is in
    the "middle" of our given filament.*/
  /* fil_nodes = (int)Filament_Array[fil_index][0].xcord; */
/*  */
  /* if((node_index>20)&&(node_index<(fil_nodes-20))) */
    /* { */
      /* *which_fil=fil_index; */
      /* return(node_index); */
    /* } */
  /* else */
    /* return(0); */
/*  */
/* } */
/*************************************************************************************************************	      
 This function provides a second method for checking that the thinned filament/s nodes are ordered in 
 a consistent way. We need to make sure that the sequence in which the nodes are placed in array Current_Filament
 are consistent with the initial ordering. Current_Filament contains the node positions whose ordering we need
 to check. Previous_Filament contains the evolved real filament corresponding to the thinned torus contained
 in Current_FIlament. We assume that the ordering in Previous_Filament is correct and we use it make sure
 that the ordering in Current_Filament is correct. 
 This is done by taking 2 "consecutive" nodes from Previous_Filament and making sure that the nodes closest
 to these 2 nodes in array Current_Filament are also consecutive in the same way.
****************************************************************************************************************/
/* void  Robustly_Check_Direction_of_Ordering(Coordinates **Previous_Filament, Coordinates **Current_Filament, int Fil) */
/* { */
/*  */
  /* int first_location, second_location, found_it=0, i=0, j, equal=1, which_fil1,  which_fil2, Different_fils=1; */
  /* double xcord, ycord, zcord; */
/*  */
   /*Pick a suitable "first" voxel to be a reference point in the quest for the right direction of ordering. */
    /* This is achieved by going through the nodes in our current filament ordering, and making picking */
    /* a node whose corresponding node in the previous filament ordering, is located at some distance */
    /* from the start and end voxels of the corresponding filament. This ensures that we start with  */
    /* "first" voxel which is some distance away from the danger zones since the start and end locations */
     /* are limiting cases and could introduce some problems for this method of order checking.*/ 
/*  */
  /* while(Different_fils)    //we can only compare the 2 node locations provided they come from the SAME original filament */
    /* { */
      /* while(found_it==0) */
	/* { */
	  /* //go to the next node location in our current ordering  */
	  /* i++; */
	  /*  */
	  /* //small check that we haven't exceeded the number of nodes in the current filament */
	  /* if(i>(int)Current_Filament[Fil][0].xcord) */
	    /* { */
	      /* printf("\n!!There is some defect in the ordering code!!\n"); */
	      /* exit(0); */
	    /* } */
	  /*  */
	  /* xcord = Current_Filament[Fil][i].xcord; */
	  /* ycord = Current_Filament[Fil][i].ycord; */
	  /* zcord = Current_Filament[Fil][i].zcord; */
	  /*  */
	  /*  */
	  /*return the closest array location in array Previous_Filament corresponding to the given position 
	    provided it's some distance from the edges of the corresponding filament.If it's too close to
	    the edges, then zero will be returned*/
	  /* found_it = Locate_Closest_First_Voxel(Previous_Filament, xcord, ycord, zcord, &which_fil1); */
	  /*  */
	/* } */
      /*  */
      /*once a good "first" location is found, we proceed to check the consistency of the ordering by checking 
	the next voxels in our current ordering where also the "next" voxels in our previous ordering*/
      /* first_location = found_it; */
      /*  */
      /* //let j denote our valid location in the current filament ordering */
      /* j = i; */
      /*  */
      /* while(equal) */
	/* { */
	  /*  */
	  /* //proceed to the next voxels */
	  /* ++j; */
	  /*  */
	  /* //get the current locations of the second voxel in the array containing the current ordering of our voxels  */
	  /* xcord = Current_Filament[Fil][j].xcord; */
	  /* ycord = Current_Filament[Fil][j].ycord; */
	  /* zcord = Current_Filament[Fil][j].zcord; */
	  /*  */
	  /*  */
	  /* //take the second voxel in the array containing the current ordering of voxels */
	  /* second_location = Locate_Closest_Voxel(Previous_Filament, xcord, ycord, zcord, &which_fil2); */
	  /*  */
	  /* //check whether the closest second location is different from the one closest to the first location */
	  /* if(second_location!=first_location)  */
	    /* equal=0; */
	/* } */
/*  */
      /*Now we can check that both nodes came from the SAME original filament because we can only compare order 
	of nodes provided they came from the same filament in the previous iteration*/
      /* if(which_fil1!=which_fil2) */
	/* { */
	  /* //need to repeat the above but we set our first voxel to be the current second location */
	  /* i=j-1; */
	  /* found_it=0; */
	  /* equal=1; */
	/* } */
      /* else   //exit loop since we've found a suitable first and second locations */
	/* Different_fils=0; */
      /*  */
    /* } */
  /*  */
  /* //check that the "second" location is in the correct position, if not-> swap current voxel locations */
  /* if(second_location<first_location) */
    /* Swap_Filament(Current_Filament, Fil); */
  /*  */
/* } */

int
TmsFromT(double t)
{
	return (int) rint(1000*t);
}
void
WriteVoxelTreeT(TN *ptn, char *szPrefix, double t)
{
	char szFilename[40];
	int tms = TmsFromT(t);
	sprintf(szFilename, "%s-%04i.raw", szPrefix, tms); 
	printf("Saving %s\n", szFilename);
	WriteVoxelTree(ptn, szFilename);
}
void
WriteVoxelWithVelocity(TN *ptn, void *pv)
{
	FILE *pfh = (FILE *) pv;
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	RK *prk = (RK *) pvx->pvData;
	/* print velocities at first runge-kutta step */
	fprintf(pfh, "%i %i %i %f %f %f %f %f %f %f %f %f %f\n", XFromPvx(pvx),YFromPvx(pvx),ZFromPvx(pvx),VlFromPvx(pvx), prk->rgdco_dtFluidVelocity[1][0], prk->rgdco_dtFluidVelocity[1][1], prk->rgdco_dtFluidVelocity[1][2], prk->rgdco_dtNormalVelocity[1][0], prk->rgdco_dtNormalVelocity[1][1], prk->rgdco_dtNormalVelocity[1][2], prk->rgdvl_dcoGradient[1][0], prk->rgdvl_dcoGradient[1][1], prk->rgdvl_dcoGradient[1][2]);
}
void 
WriteVoxelTreeWithVelocities(TN *ptn, char *szFilename)
{
	FILE *pfh = fopen(szFilename, "w+");
	TraverseTree(ptn, &WriteVoxelWithVelocity, pfh); 
	fclose(pfh);
}
void
WriteVoxelTreeWithVelocitiesT(TN *ptn, char *szPrefix, double t)
{
	char szFilename[40];
	int tms = TmsFromT(t);
	sprintf(szFilename, "%s-%04i.raw", szPrefix, tms); 
	printf("Saving %s\n", szFilename);
	WriteVoxelTreeWithVelocities(ptn, szFilename);
}
void 
WriteFilaments(Coordinates **position, int irgfn, char *szFilename)
{
  if (irgfn > (int) position[0][0].xcord)
	return;

  FILE *pfh = NULL;
  pfh = fopen(szFilename, "w+");

  int m = 1;

  for(m=1; m <= (int)position[irgfn][0].xcord; m++)
	  {
	  fprintf(pfh,"%f  %f  %f\n",position[irgfn][m].xcord, position[irgfn][m].ycord, position[irgfn][m].zcord);
	  }

  fclose(pfh);
}
void
WriteFilamentsT(Coordinates **P, double t, char *szPrefixMessage)
{
	char szFilename[40];
	int irgfn;
	int tms = TmsFromT(t);
	for(irgfn=1; irgfn <= (int)P[0][0].xcord; irgfn++)
		{
		sprintf(szFilename, "fil%i-%04i.raw", irgfn, tms); 
		printf("%sSaving %s\n", szPrefixMessage, szFilename);
		WriteFilaments(P, irgfn, szFilename);
		}
}
//void
//MakeAndSetTestVelocityField(TN *ptn, void *pv)
//{
	//VX *pvx = PvxFromPtn(ptn);
	//RK *prk = (RK *) PvGetVoxelData(pvx);
	//prk->rgdco_dtFluidVelocity[0] = 0;
	//prk->rgdco_dtFluidVelocity[1] = 0;
	//prk->rgdco_dtFluidVelocity[2] = 1;
//}
void
FreeVoxelData(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	free(PvGetVoxelData(pvx));
	SetVoxelData(pvx, NULL);
}
void 
My_Solve_Linear_System(int n, double A[NODES], double B[NODES], double C[NODES], double F[NODES])
{

  //working space arrays
  double D[NODES], E[NODES], H[NODES];

  double z, h1, h3, h2, h4, h5, hh1, hh3, hhh1, sum;
  int nm1, nm2, nm3, nm4, nm5, k, km1, km2, kp1, i;

  nm1=n-1;
  nm2=n-2;
  nm3=n-3;
  nm4=n-4;

  z=A[1];



  A[1]=F[1]/z;
  D[1]=C[nm1]/z;
  E[1]=B[n]/z;
  F[1]=C[nm1];
  H[1]=B[n];
  h3=B[1];
  h1=C[1];
  h4=B[1]/z;
  B[1]=h4;
  C[1]=C[1]/z;

  z=A[2] - (h3*h4);



  A[2]=(F[2]-h3*A[1])/z;
  D[2]=-(h3*D[1])/z;
  E[2]=(C[n]- h3*E[1])/z;
  F[2]=-F[1]*h4;
  H[2]=C[n]-H[1]*h4;
  hh3=h3;
  h3=B[2]-h1*h4;
  h4=(B[2]-hh3*C[1])/z;
  B[2]=h4;

  if(n > 5)
    {
      nm5=n-5;

      for(k=3;k<=nm3;k++)
	{
	  km1=k-1;
	  km2=k-2;
	  kp1=k+1;
	  hh1=h1;
	  h1=C[km1];
	  C[km1]=C[km1]/z;
	  h2=C[km2];
	  z=A[k] - hh1*h2 - h3*h4;



	  A[k]=(F[k]-hh1*A[km2]-h3*A[km1])/z;
	  D[k]=-(hh1*D[km2] + h3*D[km1])/z;
	  E[k]=-(hh1*E[km2] + h3*E[km1])/z;
	  F[k]=-F[km2]*h2-F[km1]*h4;
	  H[k]=-H[km2]*h2-H[km1]*h4;
	  hh3=h3;
	  h3=B[k]- h1*h4;
	  h4=(B[k]-hh3*C[km1])/z;
	  B[k]=h4;
	}

      hhh1=hh1;
      hh1=h1;
      h1=C[nm3]-F[nm5]*C[nm5]-F[nm4]*B[nm4];
      C[nm3]=(C[nm3]-hhh1*D[nm5]-hh3*D[nm4])/z;
    }//end if

  else
    {
      hh1=h1;
      h1=C[nm3]-F[nm4]*B[nm4];
      C[nm3]=(C[nm3]-hh3*D[nm4])/z;
    }

  h2=C[nm4];
  z=A[nm2]-hh1*h2 - h3*h4;



  A[nm2]=(F[nm2]-hh1*A[nm4]-h3*A[nm3])/z;
  hh3=h3;
  h3=B[nm2]-F[nm4]*h2-h1*h4;
  h4=(B[nm2]-D[nm4]*hh1-hh3*C[nm3])/z;
  B[nm2]=h4;
  hhh1=hh1;
  hh1=h1;
  h1=C[nm2]-H[nm4]*h2-H[nm3]*B[nm3];
  C[nm2]=(C[nm2]-hhh1*E[nm4]-hh3*E[nm3])/z;

  sum=0.0;
  
  for(i=1;i<=nm4;i++)
    sum=sum+F[i]*D[i];

  z=A[nm1]-hh1*C[nm3]-h3*h4-sum;



  sum=0.0;

  for(i=1;i<=nm4;i++)
    sum=sum+F[i]*A[i];


  A[nm1]=(F[nm1]-sum-hh1*A[nm3]-h3*A[nm2])/z;

  sum=0.0;

  for(i=1;i<=nm4;i++)
    sum=sum + F[i]*E[i];


  h5=B[nm1];
  h4=(h5-h3*C[nm2]-sum-hh1*E[nm3])/z;
  B[nm1]=h4;

  sum=0.0;

  for(i=1;i<=nm4;i++)
      sum=sum+H[i]*D[i];

  hh3=h3;
  h3=h5-h1*B[nm2]-sum-H[nm3]*C[nm3];

  sum=0.0;

  for(i=1;i<=nm3;i++)
    sum=sum+H[i]*E[i];



  z=A[n]-h1*C[nm2]-h3*h4-sum;


  sum=0.0;
  for(i=1;i<=nm3;i++)
    sum=sum+H[i]*A[i];


  A[n]=(F[n]-sum-h1*A[nm2]-h3*A[nm1])/z;
  F[n]=A[n];
  F[nm1]=A[nm1]-B[nm1]*F[n];
  F[nm2]=A[nm2]-B[nm2]*F[nm1]-C[nm2]*F[n];
  F[nm3]=A[nm3]-B[nm3]*F[nm2]-C[nm3]*F[nm1]-E[nm3]*F[n];

 for(i=nm4;i>=1;i=i-1)
   F[i]=A[i]-B[i]*F[i+1]-C[i]*F[i+2]-D[i]*F[nm1]-E[i]*F[n];


}
void 
My_Periodic_Smoothing_Spline(Coordinates **Position, double weight)
{
	//printf("in periodic smoothing spline...\n");
	//the following is the data structure needed as workspace for the algorithm
	double Filament[NODES][4], A[NODES], B[NODES], C[NODES], D[NODES], Y2[NODES], S[NODES];
	int Fil_Num, i, nodes, n1, n2, j, w, k, k1;
	double seg_length, ds, h, h1, h2, h3, h4, r1, r2, p, s;
	//double dsavg, snew;
	//int ns;

	//set the weights; these are the same value for all nodes i.e. polynomials
	p=1.0/weight;  
	//get the total number of filaments
	Fil_Num=(int)Position[0][0].xcord;
	//printf("num of fils = %i\n", Fil_Num);

	//smooth each filament (we assume each filament is closed 3D curve)
	//for(i=irgfn;i<=irgfn;i++)
	for(i=1;i<=Fil_Num;i++)
		{

		//get the number of nodes in the current filament such that the last node is equal to the first node i.e. periodic 
		nodes=(int)Position[i][0].xcord + 1;

		n1=nodes-1;
		n2=nodes-2;

		/*transfer the nodes of the current filament to a temporary storage 2d-array of doubles
		  This is done in order to make the subsequent calculations easier since the x, y and z 
		  coordinates of node j are accessed by Filament[j][1], Filament[j][2] and Filament[j][3].*/
		for(j=1;j<=n1;j++)
			{
			Filament[j][1]=Position[i][j].xcord;
			Filament[j][2]=Position[i][j].ycord;
			Filament[j][3]=Position[i][j].zcord;
			}

		//let the last element in array Filament be identical to the first one (this structure is needed for algorithm)
		Filament[nodes][1]=Position[i][1].xcord;
		Filament[nodes][2]=Position[i][1].ycord;
		Filament[nodes][3]=Position[i][1].zcord;


		//we need to reparametrize our 3d-curve (filament) by S 
		//we store the cumulative length of our filament in array S as such
		S[1]=0.0;
		for(j=2;j<=n1;j++)
			{
			seg_length = sqr(Filament[j][1]-Filament[j-1][1])
				+  sqr(Filament[j][2]-Filament[j-1][2])
				+  sqr(Filament[j][3]-Filament[j-1][3]);

			ds=sqrt(seg_length);

			//add the length of this node to the total length of the filament
			S[j]= S[j-1] + ds;
			}

		//fill-up the last entry of array S (it's more efficient this way)
		seg_length = sqr(Filament[1][1]-Filament[n1][1])
			+  sqr(Filament[1][2]-Filament[n1][2])
			+  sqr(Filament[1][3]-Filament[n1][3]);

		ds=sqrt(seg_length);

		//add the length of this node to the total length of the filament
		S[nodes]= S[n1] + ds;   

		//smooth each coordinate (x,y,z) using splines
		for(w=1;w<=3;w++)
			{
			h1 = 1.0/(S[nodes]-S[n1]);
			h2 = 1.0/(S[2]-S[1]);
			r1=(Filament[1][w] - Filament[n1][w])*h1;

			//compute preparations 
			for(k=1;k<=n1;k++)
				{
				k1=k+1;
				s=h1+h2;

				if(k < n1)
					{
					h3=1.0/(S[k+2]-S[k1]);
					r2=(Filament[k1][w] - Filament[k][w])*h2;
					}
				else
					{
					h3=1.0/(S[2]-S[1]);
					r2=(Filament[1][w] - Filament[n1][w])*h2;
					}

				A[k]=(2.0/h1) + (2.0/h2) + 6.0*(h1*h1*p + s*s*p + h2*h2*p);

				if(k < n1)
					B[k]=(1.0/h2) - 6.0*h2*(p*s + p*(h2+h3));

				if(k < n2)
					C[k]=6.0*p*h2*h3;

				//array Y2 contains the values of the second derivatives
				Y2[k]=6.0*(r2-r1);

				if(k==1)
					{
					h4=1.0/(S[n1]-S[n2]);
					B[n1]=1.0/h1 - 6.0*h1*(p*(h1+h4) + p*(h1+h2));
					C[n2]=6.0*p*h4*h1;
					}

				if(k==2)
					C[n1]=6.0*p*h1/(S[nodes]-S[n1]);

				h1=h2;
				h2=h3;
				r1=r2;

				}//end for loop over k

			/*now that we have the values of the second derivatives stored in Y2, we proceed
			  to solve the system of linear equations. The solution will be outputed in array Y2 too*/
			//printf("about to enter solve linear system\n");
			My_Solve_Linear_System(n1, A, B, C, Y2);
			//printf("done solve linear system\n");

			Y2[nodes]=Y2[1];

			h1=(Y2[1]-Y2[n1])/(S[nodes]-S[n1]);

			//now that we've solved th equations, we proceed to calculate the polynomial coefficients

			for(k=1;k<=n1;k++)
				{
				k1=k+1;
				h=1.0/(S[k1]-S[k]);
				B[k]=h;
				h2=h*(Y2[k1]-Y2[k]);
				D[k]=h2/6.0;
				A[k]=Filament[k][w]-(h2-h1)*p;
				C[k]=Y2[k]/2.0;
				h1=h2;
				}
			A[nodes]=A[1];

			for(k=1;k<=n1;k++)
				{
				k1=k+1;
				h=B[k];
				B[k]=(A[k1]-A[k])*h - (Y2[k1] + 2.0*Y2[k])/(6.0*h);
				}

			/*we now have the coefficient of the polynomials for the given filament and given coordinate. We can update our nodes and 
//			  adjust the coordinate value of all nodes in order to smooth them. Array A, B, C and D contain the coefficients. We'll update the 
//			  original array Position to impose lasting changes.*/
//
//
//
//			//let's try to place nodes ON the produced smoothing spline
			if(w==1)
				{
				//smooth-out the xcord. of the current filament's nodes
				for(j=1;j<=n1;j++)
					{
					//place a node on the cubic polynomial, halfway between each pair of original knots
					ds=(S[j+1]-S[j])/2.0;
					Position[i][j].xcord= A[j] + ds*B[j] + ds*ds*C[j] + ds*ds*ds*D[j];		
					}	  
				}

			else if(w==2)
				{	      
				//smooth-out the ycord. of the current filament's nodes
				for(j=1;j<=n1;j++)
					{
					//place a node on the cubic polynomial, halfway between each pair of original knots
					ds=(S[j+1]-S[j])/2.0;
					Position[i][j].ycord= A[j] + ds*B[j] + ds*ds*C[j] + ds*ds*ds*D[j];		
					}	  
				}

			else if(w==3)
				{	     
				//smooth-out the zcord. of the current filament's nodes
				for(j=1;j<=n1;j++)
					{
					//place a node on the cubic polynomial, halfway between each pair of original knots
					ds=(S[j+1]-S[j])/2.0;
					Position[i][j].zcord= A[j] + ds*B[j] + ds*ds*C[j] + ds*ds*ds*D[j];		
					}	  	      
				}	  
			}//finish smoothing over all coordinates for the given filament
		}//end smoothing all filaments
}
void
WriteFilamentToFilamentArray(VX *pvxStart, Coordinates **Thinned_Filament, int irgfn)
{
	VX **rgpvx = NULL;
	int fBackToStart = 0;
	int ipvx = 0;
	int ifn = 1;
	//int vl = 0;

	/* Walk through filament */
	VX *pvx = pvxStart;
	do
		{
		//printf("in order loop.\n");
		pvx->tg = 1; 
		//if (irgfn == 2)
			//SetVoxelValue(pvx, 5);
		Thinned_Filament[irgfn][ifn].xcord = XFromPvx(pvx)/cvxResolution/cvxSubSampleResolution;
		Thinned_Filament[irgfn][ifn].ycord = YFromPvx(pvx)/cvxResolution/cvxSubSampleResolution;
		Thinned_Filament[irgfn][ifn].zcord = ZFromPvx(pvx)/cvxResolution/cvxSubSampleResolution;
		Thinned_Filament[irgfn][ifn].iFilament = pvx->iFilament;
		ifn++;
		rgpvx = (VX **) PvGetVoxelData(pvx);
		fBackToStart = TRUE;
		/* for each 26 neighbour */
		for (ipvx=0; ipvx<27; ipvx++)
			{
			//if (irgfn == 2 && rgpvx[ipvx] != NULL)
				//{
				//VX *pvxn = rgpvx[ipvx];
				//printf("irgfn=2, 26 neigh tag=%i, x=%i, y=%i, z=%i\n", TgFromPvx(pvxn), XFromPvx(pvxn), YFromPvx(pvxn), ZFromPvx(pvxn));
				//}
			if (rgpvx[ipvx] != pvx && rgpvx[ipvx] != NULL && TgFromPvx(rgpvx[ipvx]) == 0)
				{
				pvx = rgpvx[ipvx];
				fBackToStart = FALSE;
				break;
				}
			}
		}
	while (!fBackToStart);
	//while (!fBackToStart && ifn < 1500);
	//printf("done order loop\n");
	
	Thinned_Filament[irgfn][0].xcord = ifn-1;
}
void
FindNonWrittenVoxel(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	if (TgFromPvx(pvx) == 0)
		{
		VX **ppvxStart = (VX **) pv;
		*ppvxStart = pvx;
		return;
		}
}
int
CvxGetNumberOf26Neighbours(VX *pvx)
{
	VX **rgpvx = (VX **) PvGetVoxelData(pvx);
	assert(rgpvx != NULL);
	int cvx = 0;
	int ipvx=0;
	/* for each 26 neighbour */
	for (ipvx=0; ipvx<27; ipvx++)
		{
		if (rgpvx[ipvx] != NULL && rgpvx[ipvx] != pvx)
			cvx++;
		}
	return cvx;
}
void
TagJunction(VX *pvx, VX **rgpvxAdjacent)
{
	pvx->tg = tgJunctionVoxel;
	pvx->vl = 5.0; //this is used for debugging, since the value won't ever be used.

	VX **rgpvxNeighbours = (VX **) PvGetVoxelData(pvx);
	assert(rgpvxAdjacent != NULL);
	VX *pvx26Neigh = NULL;
	int cvx = 0;
	int ipvx=0;

	/* 
	 * For each neighbour 
	 */
	for (ipvx=0; ipvx<27; ipvx++)
		{
		pvx26Neigh = rgpvxNeighbours[ipvx];
		if (pvx26Neigh != NULL && pvx26Neigh != pvx) //if neighbour is not empty and not itself
			{
			cvx = CvxGetNumberOf26Neighbours(pvx26Neigh);
			/* Add voxel to array if its an adjacent voxel, the beginning of a branch */
			if (cvx == 2)
				{
				int ipvxAdjacent = 0;
				while(rgpvxAdjacent[ipvxAdjacent] != NULL) 
					ipvxAdjacent++;
				//printf("ipvxAdjacent = %i\n", ipvxAdjacent);
				if (ipvxAdjacent > 3)
					{
					printf("Error: A junction cannot have more than 4 adjacent voxels.");
					exit(0);
					}	
				assert(pvx26Neigh != NULL);
				rgpvxAdjacent[ipvxAdjacent] = pvx26Neigh;
				continue;
				}
			printf(" 		Traversing junction: this voxel has %i neighbours.\n", cvx);
			assert(cvx == 3 || cvx == 4);
			/* Recursively call TagJunction on neighbour that hasn't yet been recognized as a junction voxel */
			if (pvx26Neigh->tg != tgJunctionVoxel)
				TagJunction(pvx26Neigh, rgpvxAdjacent);
			}
		}
}
void
FindJunctions(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;

	/* return if voxel has already been recognized as a junction voxel */
	if (pvx->tg == tgJunctionVoxel)
		return;
	/* return if voxel is a filament branch voxel */
	if (CvxGetNumberOf26Neighbours(pvx) < 3)
		return;

	/* found an unrecognized junction voxel */
	VX **rgpvxAdjacent = (VX **) malloc(sizeof(VX *)*5);
	int ipvx=0;
	for(ipvx=0; ipvx<5; ipvx++)
		rgpvxAdjacent[ipvx] = NULL;
	/*
	 * Tag junction voxels
	 */
	TagJunction(pvx, rgpvxAdjacent);

	VX ***rgrgpvx = (VX ***) pv;

	int c = 0;
	while(*rgrgpvx != NULL)
		{
		rgrgpvx++;
		c++;
		}
	//printf("counting junctions %i\n", c);
	*rgrgpvx = rgpvxAdjacent;
}
int
CvxTagBranchAndGetLength(VX *pvxAdjacent)
{
	int cvx = 1;
	VX *pvx = pvxAdjacent;
	VX *pvx26Neigh = NULL;
	VX **rgpvx26Neighbours = NULL;
	int ipvx = 0;

	/* if the adjacent voxel is already tagged "tgKeep",
	 * it implies either a previous branch loops back to the
	 * same junction (in other words, the two ends of a 
	 * loop are adjacent to the junction) OR it has been kept
	 * as a branch from a previously processed junction.  In any case,
	 * we return a large number so that the same branch 
	 * isn't processed twice.
	 */
	if (pvx->tg == tgKeep)
		return LARGE_NUMBER;

	do
		{
		pvx->tg = tgKeep;
		rgpvx26Neighbours = (VX **) PvGetVoxelData(pvx);
		/* for each 26 neighbour */
		for (ipvx=0; ipvx<27; ipvx++)
			{
			pvx26Neigh = rgpvx26Neighbours[ipvx];
			if (pvx26Neigh != pvx && pvx26Neigh != NULL &&  pvx26Neigh->tg != tgJunctionVoxel && pvx26Neigh->tg != tgKeep)
				{
				pvx = pvx26Neigh;
				cvx++;
				break;
				}
			}
		}
	while (pvx->tg != tgKeep);

	return cvx;
}
void 
GetLine3D (int x0, int y0, int z0, int x1, int y1, int z1, VX **rgpvx)
{
    // starting point of line
    register int x = x0, y = y0, z = z0;

    // direction of line
    int dx = x1-x0, dy = y1-y0, dz = z1-z0;

    // increment or decrement depending on direction of line
    register int sx = (dx > 0 ? 1 : (dx < 0 ? -1 : 0));
    register int sy = (dy > 0 ? 1 : (dy < 0 ? -1 : 0));
    register int sz = (dz > 0 ? 1 : (dz < 0 ? -1 : 0));

    // decision parameters for voxel selection
    dx = abs(dx); dy = abs(dy); dz = abs(dz);
    register int ax = 2*dx, ay = 2*dy, az = 2*dz;
    register int decx, decy, decz;

    // determine largest direction component, single-step related variable
    int max = dx, var = 0;
    if ( dy > max ) { max = dy; var = 1; }
    if ( dz > max ) { max = dz; var = 2; }

    // traverse Bresenham line
	int ipvx=0;
    switch ( var ) {
    case 0:  // single-step in x-direction
        for (decy=ay-dx, decz=az-dx; ; x += sx, decy += ay, decz += az) {
			rgpvx[ipvx] = PvxCreateVoxel(x,y,z,0.0);
			ipvx++;

            // take Bresenham step
            if ( x == x1 ) break;
            if ( decy >= 0 ) { decy -= ax; y += sy; }
            if ( decz >= 0 ) { decz -= ax; z += sz; }
        }
        break;
    case 1:  // single-step in y-direction
        for (decx=ax-dy, decz=az-dy; ; y += sy, decx += ax, decz += az) {
            // routine for displaying (x,y,z) goes here
			rgpvx[ipvx] = PvxCreateVoxel(x,y,z,0.0);
			ipvx++;

            // take Bresenham step
            if ( y == y1 ) break;
            if ( decx >= 0 ) { decx -= ay; x += sx; }
            if ( decz >= 0 ) { decz -= ay; z += sz; }
        }
        break;
    case 2:  // single-step in z-direction
        for (decx=ax-dz, decy=ay-dz; ; z += sz, decx += ax, decy += ay) {
            // routine for displaying (x,y,z) goes here
			rgpvx[ipvx] = PvxCreateVoxel(x,y,z,0.0);
			ipvx++;

            // take Bresenham step
            if ( z == z1 ) break;
            if ( decx >= 0 ) { decx -= az; x += sx; }
            if ( decy >= 0 ) { decy -= az; y += sy; }
        }
        break;
    }
}
VX *
PvxDeleteBranch(TN **pptn, VX *pvxAdjacent)
{
	VX *pvx = pvxAdjacent;
	VX *pvx26Neigh = NULL;
	VX **rgpvx26Neighbours = NULL;
	int ipvx = 0;

	do
		{
		assert(PvxFindVoxel(ptnRoot, XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx)) != NULL);

		/* Tag as tgDelete and get 26 neighbours*/
		pvx->tg = tgDelete;
		rgpvx26Neighbours = (VX **) PvGetVoxelData(pvx);
		
		/* for each 26 neighbour */
		for (ipvx=0; ipvx<27; ipvx++)
			{
			pvx26Neigh = rgpvx26Neighbours[ipvx];
			if (pvx26Neigh == NULL || pvx26Neigh == pvx)
				continue;
			if (pvx26Neigh->tg == tgKeep)
				{
				pvx = pvx26Neigh;
				break;
				}
			}
		}
	while (pvx->tg == tgKeep);
	return pvx;
}
void
LinkBranches(TN **pptn, VX *pvx1, VX *pvx2)
{
	VX **rgpvxLinking = (VX **) malloc(sizeof(VX *)*MAX_LINKING_VOXELS);
	int ipvx=0;
	for(ipvx=0; ipvx<MAX_LINKING_VOXELS; ipvx++)
		rgpvxLinking[ipvx] = NULL;

	/* get voxels that will connect branches together */
	GetLine3D(XFromPvx(pvx1),YFromPvx(pvx1),ZFromPvx(pvx1),XFromPvx(pvx2),YFromPvx(pvx2),ZFromPvx(pvx2), rgpvxLinking);

	//printf("voxel to join: x=%i,y=%i,z=%i\n",XFromPvx(pvx1),YFromPvx(pvx1),ZFromPvx(pvx1));
	//printf("voxel to join: x=%i,y=%i,z=%i\n",XFromPvx(pvx2),YFromPvx(pvx2),ZFromPvx(pvx2));
	int cvx = 0;
	while(*rgpvxLinking != NULL)
		{
		//printf("linking voxel: x=%i,y=%i,z=%i\n", XFromPvx(*rgpvxLinking),YFromPvx(*rgpvxLinking),ZFromPvx(*rgpvxLinking));
		if (*rgpvxLinking != pvx1 && *rgpvxLinking != pvx2)
			{
		   	VX *pvxFind = PvxFindVoxel(*pptn, XFromPvx(*rgpvxLinking),YFromPvx(*rgpvxLinking),ZFromPvx(*rgpvxLinking)); 
			if (pvxFind == NULL)
				{
				(*rgpvxLinking)->tg = tgKeep;
				InsertVoxel(pptn, *rgpvxLinking);
				}
			else
				{
				//assert(pvxFind->tg == tgJunctionVoxel);
				pvxFind->tg = tgKeep;
				}
			}
		rgpvxLinking++;
		cvx++;
		if (cvx >= MAX_LINKING_VOXELS)
			{
			printf("Error: the number of linking voxels has exceeded the maximum expected number of %i.\n", MAX_LINKING_VOXELS);
			}
		}
}
//void
//Process3Junction(TN **pptn, VX **rgpvxAdjacent)
//{
	//int ipvx=0;
	//for(ipvx=0; ipvx<3; ipvx++)
		//{
		//if ((rgpvxAdjacent[ipvx])->tg == tgKeep || (rgpvxAdjacent[ipvx])->tg == tgDelete)
			//return;
		//}
	//if ((rgpvxAdjacent[0])->tg == tgDelete)
		//{
		//LinkBranches(pptn, rgpvxAdjacent[1], rgpvxAdjacent[2]);
		//return;
		//}
	//if ((rgpvxAdjacent[1])->tg == tgDelete)
		//{
		//LinkBranches(pptn, rgpvxAdjacent[0], rgpvxAdjacent[2]);
		//return;
		//}
	//if ((rgpvxAdjacent[2])->tg == tgDelete)
		//{
		//LinkBranches(pptn, rgpvxAdjacent[0], rgpvxAdjacent[1]);
		//return;
		//}
//
	//int rgcvx[3];
	//rgcvx[0] = CvxTagBranchAndGetLength(rgpvxAdjacent[0]);
	//rgcvx[1] = CvxTagBranchAndGetLength(rgpvxAdjacent[1]);
	//rgcvx[2] = CvxTagBranchAndGetLength(rgpvxAdjacent[2]);
	//printf(" 			Branch lengths are %i, %i, %i\n", rgcvx[0], rgcvx[1], rgcvx[2]);
	//if (rgcvx[0] < rgcvx[1] && rgcvx[0] < rgcvx[2]) 
		//{
		//DeleteBranch(pptn, rgpvxAdjacent[0]);
		//LinkBranches(pptn, rgpvxAdjacent[1], rgpvxAdjacent[2]);
		//}
	//else if (rgcvx[1] < rgcvx[0] && rgcvx[1] < rgcvx[2])
		//{
		//DeleteBranch(pptn, rgpvxAdjacent[1]);
		//LinkBranches(pptn, rgpvxAdjacent[0], rgpvxAdjacent[2]);
		//}
	//else if (rgcvx[2] < rgcvx[0] && rgcvx[2] < rgcvx[1])
		//{
		//DeleteBranch(pptn, rgpvxAdjacent[2]);
		//LinkBranches(pptn, rgpvxAdjacent[0], rgpvxAdjacent[1]);
		//}
	//else
		//printf("Have two equal branch lengths out of a 3 branch junction.\n");
	//return;
//}
double
lGetVoxelDistance(VX *pvx1, VX *pvx2)
{
	return sqrt(pow(XFromPvx(pvx1)-XFromPvx(pvx2),2) + pow(YFromPvx(pvx1)-YFromPvx(pvx2),2) + pow(ZFromPvx(pvx1)-ZFromPvx(pvx2),2));
}
//void
//Process4Junction(TN **pptn, VX **rgpvxAdjacent)
//{
	///* We keep all 4 branches, so tag them as 'tgKeep' */
	//int ipvx;
	//int cvx;
	//for (ipvx=0; ipvx<4; ipvx++)
		//{
		//cvx = CvxTagBranchAndGetLength(rgpvxAdjacent[ipvx]);
		//printf(" 			Branch length is %i\n", cvx);
		//}
//
	///* Link the adjacent voxels (which start the branches) that are closest together */
	//double rgl[3];
	//rgl[0] = lGetVoxelDistance(rgpvxAdjacent[0], rgpvxAdjacent[1]);
	//rgl[1] = lGetVoxelDistance(rgpvxAdjacent[0], rgpvxAdjacent[2]);
	//rgl[2] = lGetVoxelDistance(rgpvxAdjacent[0], rgpvxAdjacent[3]);
//
	//if (rgl[0] <= rgl[1] && rgl[0] <= rgl[2])
		//{
		//LinkBranches(pptn, rgpvxAdjacent[0], rgpvxAdjacent[1]);
		//LinkBranches(pptn, rgpvxAdjacent[2], rgpvxAdjacent[3]);
		//return;
		//}
	//else if (rgl[1] <= rgl[0] && rgl[1] <= rgl[2])
		//{
		//LinkBranches(pptn, rgpvxAdjacent[0], rgpvxAdjacent[2]);
		//LinkBranches(pptn, rgpvxAdjacent[3], rgpvxAdjacent[1]);
		//return;
		//}
	//else if (rgl[2] <= rgl[0] && rgl[2] <= rgl[1])
		//{
		//LinkBranches(pptn, rgpvxAdjacent[0], rgpvxAdjacent[3]);
		//LinkBranches(pptn, rgpvxAdjacent[1], rgpvxAdjacent[2]);
		//return;
		//}
//}
//TN *
//PtnGetBranch(VX *pvxAdjacent)
//{
	//int cvx = 1;
	//VX *pvx = pvxAdjacent;
	//VX *pvx26Neigh = NULL;
	//int tg = 0;
//
	//while (pvx->tg != tgProcessed)
		//{
		//tg = pvx->tg;
		//pvx->tg = tgProcessed;
		//rgpvx = (VX **) PvGetVoxelData(pvx);
		///* for each 26 neighbour */
		//for (ipvx=0; ipvx<27; ipvx++)
			//{
			//pvx26Neigh = rgpvx[ipvx];
			//if (pvx26Neigh != NULL &&  pvx26Neigh->tg != tgJunctionVoxel && pvx26Neigh->tg != tgDelete)
				//{
				//pvx->tg = tg;
				//pvx->tg = tgProcessed;
				//pvx = pvx26Neigh;
				//cvx++;
				//break;
				//}
			//}
		//}
//}
void
RemoveDeletedVoxelsAndTag(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	TN **pptn = (TN **) pv;

	if (pvx->tg == tgKeep || pvx->tg == 0 || pvx->tg == tgLooseEndConnected)
		{
		VX *pvxNew = PvxCreateVoxel(XFromPvx(pvx),YFromPvx(pvx),ZFromPvx(pvx),0.0);
		pvxNew->iFilament = pvx->iFilament;
		//pvxNew->tg = tgKeep;
		CreateLinksWithVoxelsInTree(*pptn, pvxNew);
		InsertVoxel(pptn, pvxNew);
		return;
		}
	if (pvx->tg != tgDelete && pvx->tg != tgJunctionVoxel)
		{
		printf("Error: at this point the tag is %i, but is expected to be either tgDelete or tgJunctionVoxel.\n", pvx->tg);
		}
	//else if (pvx->tg == tgDelete || pvx->tg == tgJunctionVoxel) 
		//{
		//PvxRemoveVoxel( pptn, XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx));
		//VX **rgpvx26Neigh = PvDestroyVoxel(pvx); //voxel freed, and links from neighbours are removed as well
		//free(rgpvx26Neigh);
		//}
}
void
PrintTags(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	if (pvx->tg != 0 && pvx->tg != tgKeep && pvx->tg != tgDelete && pvx->tg != tgJunctionVoxel)
		printf("Tag is %i!\n", pvx->tg);
}
//void
//EraseJunctions(TN **pptn)
//{
	///* Make array to store voxels adjacent to junctions (3 or 4 voxels per junction) */
	//VX ***rgrgpvxAdjacent = (VX ***) malloc(sizeof(VX **)*(MAX_JUNCTIONS+1));
	//int irgpvx = 0;
	//for (irgpvx = 0; irgpvx< MAX_JUNCTIONS; irgpvx++)
		//rgrgpvxAdjacent[irgpvx] = NULL;
//
	///* Find the junctions */
	//TraverseTree(*pptn, &FindJunctions, rgrgpvxAdjacent);
//
	///* Save for debugging purposes */
	//WriteVoxelTree(*pptn, "nb-colored-junctions.raw");
//
	///* 
	 //* Process each set of adjacent voxels 
	 //*/
	//irgpvx = 0;
	//int ipvx = 0;
	//while(rgrgpvxAdjacent[irgpvx] != NULL)
		//{
		//ipvx=0;
		//while(rgrgpvxAdjacent[irgpvx][ipvx] != NULL)
			//ipvx++;
		//if (ipvx == 4)
			//{
			//printf(" 		Detected a 4 junction.\n");
			//Process4Junction(pptn, rgrgpvxAdjacent[irgpvx]);
			//}
		//else if (ipvx == 3)
			//{
			//printf(" 		Detected a 3 junction.\n");
			//Process3Junction(pptn, rgrgpvxAdjacent[irgpvx]);
			//}
		//else
			//{
			//printf("Error: junction has %d adjacent voxels!\n", ipvx);
			//}
		//irgpvx++;
		//}
//
	//TraverseTree(*pptn, &PrintTags, NULL);
//
	///* Remove deleted voxels */
	//TN *ptnNew = NULL;
	//TraverseTree(*pptn, &RemoveDeletedVoxelsAndTag, &ptnNew);
	//FreeTreeAndVoxels(*pptn); // frees voxels and their data (26 neighbour pointers)
	//*pptn = ptnNew; 
//}
void
DeleteMergingVoxels(VX **rgpvxAdjacent, int cvxMax, VX **rgpvxFreeEnds)
{
	VX *pvx26Neigh = NULL;
	VX **rgpvx26Neighbours = NULL;
	int ipvx = 0;
	int ipvxAdjacent = 0;
	int cvx = 0;
	VX *pvx = NULL;
	int ipvxFreeEnds = 0;


	VX *pvxT = NULL;
	printf("*****Preliminary Check******\n");
	for (ipvxAdjacent=0; ipvxAdjacent<3; ipvxAdjacent++) /* for each adjacent voxel */ 
		{
		pvxT = rgpvxAdjacent[ipvxAdjacent];
		if ((rgpvxAdjacent[ipvxAdjacent])->tg == tgDelete)
			{
			printf("Adjacent voxel %i (%i,%i, %i) is on deleted branch.\n", ipvxAdjacent, XFromPvx(pvxT), YFromPvx(pvxT),ZFromPvx(pvxT));
			continue;
			}
		else
			printf("Adjacent voxel %i (%i, %i,%i) is not on deleted branch.\n", ipvxAdjacent,XFromPvx(pvxT), YFromPvx(pvxT),ZFromPvx(pvxT));
		}


		//int cvxAdjacentTaggedDelete = 0;
		//for (ipvxAdjacent=0; ipvxAdjacent<3; ipvxAdjacent++) /* for each adjacent voxel */ 
			//{
			//if ((rgpvxAdjacent[ipvxAdjacent])->tg == tgDelete)
				//cvxAdjacentTaggedDelete++;
			//}
		//if (cvxAdjacentTaggedDelete == 3)
			//printf("Error: The other junction should not have 3 of its adjcent voxels tagged as tgDelete.\n");
		//if (cvxAdjacentTaggedDelete == 2)
			//{
			//printf("Error: The other junction should not have 2 of its adjcent voxels tagged as tgDelete.\n");
			//exit(1);
			//}


		printf("-------------------------\n");
		for (ipvxAdjacent=0; ipvxAdjacent<3; ipvxAdjacent++) /* for each adjacent voxel */ 
		{
		pvxT = rgpvxAdjacent[ipvxAdjacent];
		if ((rgpvxAdjacent[ipvxAdjacent])->tg == tgDelete)
			{
			printf("Adjacent voxel %i (%i,%i, %i) is on deleted branch.\n", ipvxAdjacent, XFromPvx(pvxT), YFromPvx(pvxT),ZFromPvx(pvxT));
			continue;
			}
		else
			printf("Adjacent voxel %i (%i, %i,%i) is not on deleted branch. Going on to delete it's merging segment.\n", ipvxAdjacent,XFromPvx(pvxT), YFromPvx(pvxT),ZFromPvx(pvxT));

		pvx = rgpvxAdjacent[ipvxAdjacent];
		cvx = 0;
		do /* for each adjacent voxel not part of the linking branch */
			{
			//assert(PvxFindVoxel(ptnRoot, XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx)) != NULL);

			/* Tag as tgDelete and get 26 neighbours*/
			printf("Deleting merging region voxel...\n");
			pvx->tg = tgDelete;
			rgpvx26Neighbours = (VX **) PvGetVoxelData(pvx);
			
			/* for each 26 neighbour */
			for (ipvx=0; ipvx<27; ipvx++)
				{
				pvx26Neigh = rgpvx26Neighbours[ipvx];
				if (pvx26Neigh == NULL || pvx26Neigh == pvx)
					continue;
				//if (pvx26Neigh->tg != tgDelete && pvx26Neigh->tg != tgJunctionVoxel)
				if (pvx26Neigh->tg != tgDelete && pvx26Neigh->tg != tgJunctionVoxel)
					{
					pvx = pvx26Neigh;
					cvx++;
					break;
					}
				}
			}
		while (pvx->tg != tgDelete && cvx < cvxMax);

		assert(pvx != NULL);
		printf("ipvxFreeEnds = %i \n", ipvxFreeEnds);
		rgpvxFreeEnds[ipvxFreeEnds] = pvx;
		ipvxFreeEnds++;

		if (cvx < cvxMax)
			printf(" 		cvx = %i, cvxMax = %i\n", cvx, cvxMax);
		}
}
VX *
PvxDeleteJoiningBranch(TN **pptn, VX **rgpvxAdjacent)
{
	//First check whether this junction's linking branch has been deleted 
	//int ipvx=0;
	//for (ipvx=0; ipvx<3; ipvx++)
		//{
		//if ((rgpvxAdjacent[ipvx])->tg == tgDelete)
			//return;
		//}

	int rgcvx[3];
	rgcvx[0] = CvxTagBranchAndGetLength(rgpvxAdjacent[0]);
	rgcvx[1] = CvxTagBranchAndGetLength(rgpvxAdjacent[1]);
	rgcvx[2] = CvxTagBranchAndGetLength(rgpvxAdjacent[2]);
	printf(" 			Branch lengths are %i, %i, %i\n", rgcvx[0], rgcvx[1], rgcvx[2]);
	if (rgcvx[0] < rgcvx[1] && rgcvx[0] < rgcvx[2]) 
		{
		return PvxDeleteBranch(pptn, rgpvxAdjacent[0]);
		//(rgpvxAdjacent[1])->tg = tgBranchMarker;
		//(rgpvxAdjacent[2])->tg = tgBranchMarker;
		}
	else if (rgcvx[1] < rgcvx[0] && rgcvx[1] < rgcvx[2])
		{
		return PvxDeleteBranch(pptn, rgpvxAdjacent[1]);
		//(rgpvxAdjacent[0])->tg = tgBranchMarker;
		//(rgpvxAdjacent[2])->tg = tgBranchMarker;
		}
	else if (rgcvx[2] < rgcvx[0] && rgcvx[2] < rgcvx[1])
		{
		return PvxDeleteBranch(pptn, rgpvxAdjacent[2]);
		//(rgpvxAdjacent[0])->tg = tgBranchMarker;
		//(rgpvxAdjacent[1])->tg = tgBranchMarker;
		}
	else
		{
		printf("Have two equal branch lengths out of a 3 branch junction.\n");
		exit(0);
		}
}
int
IrgpvxFindAdjacentVoxelArray(VX ***rgrgpvxAdjacent, VX *pvxOtherEnd)
{
	int irgpvx = 0;
	int ipvx = 0;
	while(rgrgpvxAdjacent[irgpvx] != NULL)
		{
		ipvx = 0;
		while(rgrgpvxAdjacent[irgpvx][ipvx] != NULL)
			{
			if (rgrgpvxAdjacent[irgpvx][ipvx] == pvxOtherEnd)
				return irgpvx;
			ipvx++;
			}
		irgpvx++;
		}
	if (1) /* if get here */
		{
		printf("couldn't find the junction this voxel belongs to.\n");
		exit(1);
		}
}
void
Merge(TN **pptn)
{
	/* Make array to store voxels adjacent to junctions */
	VX ***rgrgpvxAdjacent = (VX ***) malloc(sizeof(VX **)*(MAX_JUNCTIONS+1));
	int irgpvx = 0;
	for (irgpvx = 0; irgpvx< MAX_JUNCTIONS; irgpvx++)
		rgrgpvxAdjacent[irgpvx] = NULL;

	/* Find the junctions */
	TraverseTree(*pptn, &FindJunctions, rgrgpvxAdjacent);

	if (rgrgpvxAdjacent[0] == NULL)
		return;

	/* Save for debugging purposes */
	WriteVoxelTree(*pptn, "nb-colored-junctions.raw");


	/* Check that only 3-junctions exists */
	irgpvx = 0;
	int ipvx = 0;
	while(rgrgpvxAdjacent[irgpvx] != NULL)
		{
		ipvx = 0;
		while(rgrgpvxAdjacent[irgpvx][ipvx] != NULL)
			ipvx++;
		printf(" 		Detected a %i junction. \n", ipvx);
		if (ipvx != 3)
			{
			printf(" 		Has to be a 3-junction. Exiting.\n");	
			exit(1);
			}
		irgpvx++;
		}
	//if (irgpvx != 2)
		//{
		//printf(" 		There are %i junctions! exiting.\n", irgpvx);
		//exit(1);
		//}

	int irgpvxMax = irgpvx;

	/* Delete linking branches */
	VX **rgpvxFreeEnds1 = (VX **) malloc(sizeof(VX *)*2);
	VX **rgpvxFreeEnds2 = (VX **) malloc(sizeof(VX *)*2);
	for (irgpvx = 0; irgpvx< 2; irgpvx++)
		rgpvxFreeEnds1[irgpvx] = NULL;
	for (irgpvx = 0; irgpvx< 2; irgpvx++)
		rgpvxFreeEnds2[irgpvx] = NULL;
	double l1 = 0.0;
	double l2 = 0.0;
	VX *pvxOtherEnd = NULL;
	int irgpvxOtherJunction = 0;
	int ipvxAdjacent = 0;
	int cvxAdjacentTaggedDelete = 0;
	printf("There were %i junctions detected.\n", irgpvxMax);
	for (irgpvx=0; irgpvx<irgpvxMax; irgpvx++) /* for each junction */
		{
		printf("=====Junction %i ======\n", irgpvx);
		cvxAdjacentTaggedDelete = 0;
		for (ipvxAdjacent=0; ipvxAdjacent<3; ipvxAdjacent++) /* for each adjacent voxel */ 
			{
			if ((rgrgpvxAdjacent[irgpvx][ipvxAdjacent])->tg == tgDelete)
				cvxAdjacentTaggedDelete++;
			}
		if (cvxAdjacentTaggedDelete == 3)
			continue;
		if (cvxAdjacentTaggedDelete == 2)
			{
			printf("Error: A junction should not have 2 of its adjcent voxels tagged as tgDelete.\n");
			exit(1);
			}

		/* Delete linking branch */
		pvxOtherEnd = PvxDeleteJoiningBranch(pptn, rgrgpvxAdjacent[irgpvx]);
		irgpvxOtherJunction = IrgpvxFindAdjacentVoxelArray(rgrgpvxAdjacent, pvxOtherEnd);
		assert(rgrgpvxAdjacent[irgpvxOtherJunction] != NULL);

		/* Delete merging regions */
		DeleteMergingVoxels(rgrgpvxAdjacent[irgpvx], 6, rgpvxFreeEnds1);
		DeleteMergingVoxels(rgrgpvxAdjacent[irgpvxOtherJunction], 6, rgpvxFreeEnds2);
		assert(rgpvxFreeEnds1[1] != NULL);
		assert(rgpvxFreeEnds2[1] != NULL);

			printf("Free ends from junction 1: (%i,%i,%i) and (%i,%i,%i)\n", XFromPvx(rgpvxFreeEnds1[0]), YFromPvx(rgpvxFreeEnds1[0]), ZFromPvx(rgpvxFreeEnds1[0]), XFromPvx(rgpvxFreeEnds1[1]), YFromPvx(rgpvxFreeEnds1[1]), ZFromPvx(rgpvxFreeEnds1[1]));
			printf("Free ends from junction 2: (%i,%i,%i) and (%i,%i,%i)\n", XFromPvx(rgpvxFreeEnds2[0]), YFromPvx(rgpvxFreeEnds2[0]), ZFromPvx(rgpvxFreeEnds2[0]), XFromPvx(rgpvxFreeEnds2[1]), YFromPvx(rgpvxFreeEnds2[1]), ZFromPvx(rgpvxFreeEnds2[1]));

		/* Link free ends together */
		l1 = lGetVoxelDistance(rgpvxFreeEnds1[0], rgpvxFreeEnds2[0]);
		l2 = lGetVoxelDistance(rgpvxFreeEnds1[1], rgpvxFreeEnds2[0]);
		if (l1 < l2)
			{
			printf("Linking (%i,%i,%i) to (%i,%i,%i).\n", XFromPvx(rgpvxFreeEnds1[0]), YFromPvx(rgpvxFreeEnds1[0]), ZFromPvx(rgpvxFreeEnds1[0]), XFromPvx(rgpvxFreeEnds2[0]), YFromPvx(rgpvxFreeEnds2[0]), ZFromPvx(rgpvxFreeEnds2[0]));
			LinkBranches(pptn, rgpvxFreeEnds1[0], rgpvxFreeEnds2[0]);
			printf("Linking (%i,%i,%i) to (%i,%i,%i).\n", XFromPvx(rgpvxFreeEnds1[1]), YFromPvx(rgpvxFreeEnds1[1]), ZFromPvx(rgpvxFreeEnds1[1]), XFromPvx(rgpvxFreeEnds2[1]), YFromPvx(rgpvxFreeEnds2[1]), ZFromPvx(rgpvxFreeEnds2[1]));
			LinkBranches(pptn, rgpvxFreeEnds1[1], rgpvxFreeEnds2[1]);
			}
		else
			{
			printf("Linking (%i,%i,%i) to (%i,%i,%i).\n", XFromPvx(rgpvxFreeEnds1[0]), YFromPvx(rgpvxFreeEnds1[0]), ZFromPvx(rgpvxFreeEnds1[0]), XFromPvx(rgpvxFreeEnds2[1]), YFromPvx(rgpvxFreeEnds2[1]), ZFromPvx(rgpvxFreeEnds2[1]));
			LinkBranches(pptn, rgpvxFreeEnds1[0], rgpvxFreeEnds2[1]);
			printf("Linking (%i,%i,%i) to (%i,%i,%i).\n", XFromPvx(rgpvxFreeEnds1[1]), YFromPvx(rgpvxFreeEnds1[1]), ZFromPvx(rgpvxFreeEnds1[1]), XFromPvx(rgpvxFreeEnds2[0]), YFromPvx(rgpvxFreeEnds2[0]), ZFromPvx(rgpvxFreeEnds2[0]));
			LinkBranches(pptn, rgpvxFreeEnds1[1], rgpvxFreeEnds2[0]);
			}
		}

	/* Delete merging voxels */
	//VX *rgpvx[tgBranchMarkerMax-tgBranchMarkerStart];
	//int ipvxLinking = 0;
	//for (irgpvx=0; irgpvx<irgpvxMax; irgpvx++) /* for each junction */
		//{
			//for (ipvx=0; ipvx<3; ipvx++) /* for each adjacent voxel */
				//{
				//if ((rgrgpvxAdjacent[irgpvx][ipvx])->tg != tgDelete) /* if not the start of the deleted branch */
					//{
					//rgpvx[ipvxLinking] = PvxDeleteMergingVoxels(rgrgpvxAdjacent[irgpvx][ipvx], 6);
					//assert((rgpvx[ipvxLinking])->tg != tgDelete);
					//(rgpvx[ipvxLinking])->tg = (rgrgpvxAdjacent[irgpvx][irgpvx])->tg; /* tag it with its branch marker integer */
					//ipvxLinking++;
					//}
				//}
		//}
//
		//int ipvxLinkingMax = ipvxLinking;
//
		///* Link filaments together */
		//int ipvxLinking2 = 0;
		//double lMin = 0.0;
		//double l = 0.0;
		//VX *pvxMin = NULL;
		//for (ipvxLinking=0; ipvxLinking<ipvxLinkingMax; ipvxLinking++) /* for each loose end */
			//{
			//lMin = LARGE_NUMBER;
			//for (ipvxLinking2=ipvxLinking+1; ipvxLinking2<ipvxLinkingMax; ipvxLinking2++) /* for each loose end in the rest of the array*/
				//{
				//if ((rgpvx[ipvxLinking2])->tg == tgLooseEndConnected) /* if loose end has already been connected, move to next loose end voxel */
				   //continue;	
				//l = lGetVoxelDistance(rgpvx[ipvxLinking], rgpvx[ipvxLinking2]);
				//if (l < lMin)
					//{
					//lMin = l;
					//pvxMin = rgpvx[ipvxLinking2];
					//}
				//}
			//printf("Linking (%i,%i,%i) to (%i,%i,%i).\n", XFromPvx(rgpvx[ipvxLinking]), YFromPvx(rgpvx[ipvxLinking]), ZFromPvx(rgpvx[ipvxLinking]), XFromPvx(pvxMin), YFromPvx(pvxMin), ZFromPvx(pvxMin));
			//LinkBranches(pptn, rgpvx[ipvxLinking], pvxMin);
			//pvxMin->tg = tgLooseEndConnected;
			//}

	/* Remove deleted voxels */
	TN *ptnNew = NULL;
	TraverseTree(*pptn, &RemoveDeletedVoxelsAndTag, &ptnNew);
	FreeTreeAndVoxels(*pptn); // frees voxels and their data (26 neighbour pointers)
	*pptn = ptnNew; 

}
double
lComputeFilamentLength(Coordinates **Position, int iFilament)
{
	double lFilamentLength = 0.0;
	double lSegmentLength = 0.0;
	double dx,dy,dz;

	/* Compute Length of filament */
	int ifn = 0;
	for (ifn = 1; ifn < (int)Position[iFilament][0].xcord; ifn++)
		{
		//lSegmentLength = lComputeNodeDistance(Position[irgfn][ifn+1], Position[irgfn][ifn]);
		dx = Position[iFilament][ifn+1].xcord - Position[iFilament][ifn].xcord;
		dy = Position[iFilament][ifn+1].ycord - Position[iFilament][ifn].ycord;
		dz = Position[iFilament][ifn+1].zcord - Position[iFilament][ifn].zcord;

		//printf("Position[irgfn][ifn+1].xcord is %f\n", Position[irgfn][ifn+1].xcord);
		//printf("Position[irgfn][ifn+1].ycord is %f\n", Position[irgfn][ifn+1].ycord);
		//printf("Position[irgfn][ifn+1].zcord is %f\n", Position[irgfn][ifn+1].zcord);
		//printf("dx is %f\n", dx);
		//printf("dy is %f\n", dy);
		//printf("dz is %f\n", dz);

		lSegmentLength = sqrt(dx*dx + dy*dy + dz*dz);
		lFilamentLength += lSegmentLength; 
		}
	return lFilamentLength;
}
void 
GetSmoothFilament(TN *ptn, Coordinates **P)
{
	if (ptn == NULL)
		{
		printf("Cannot smooth filament: the thinned filament tree is NULL.\n");
		exit(1);
		}
	
	//int irgfnMost = (int)P[0][0].xcord;	
	Coordinates **Thinned_Filament = (Coordinates **) malloc(sizeof(Coordinates *) * (MAX_FILAMENTS+1));
	int irgfn = 0;
	for (irgfn = 0; irgfn <= MAX_FILAMENTS; irgfn++)
		{
		Thinned_Filament[irgfn] = (Coordinates *) malloc(sizeof(Coordinates)*NODES);
		}

	/*VX *pvxStart = PvxFromPtn(ptn); //start with voxel at tree root*/
	VX *pvxStart = (ptn == NULL) ? NULL : ptn->pvx;

	//printf(" 		Writing skeleton voxels in the tree to filament array...\n");
	//WriteFilamentToFilamentArray(pvxStart, Thinned_Filament);
	//printf(" 			Filament %i written with %i nodes.\n", i, (int)Thinned_Filament[i][0].xcord);

	printf(" 		Writing skeleton voxels in the tree to filament array...\n");
	for(irgfn=1; pvxStart != NULL; irgfn++)
		{
		WriteFilamentToFilamentArray(pvxStart, Thinned_Filament, irgfn);
		Thinned_Filament[0][0].xcord = irgfn;
		pvxStart = NULL;
		TraverseTree(ptn, &FindNonWrittenVoxel, &pvxStart);
		}
	for (irgfn=1; irgfn<=(int) Thinned_Filament[0][0].xcord; irgfn++)
		printf(" 			Filament %i written with %i nodes.\n", irgfn, (int)Thinned_Filament[irgfn][0].xcord);

	TraverseTree(ptn, &FreeVoxelData, ptn);
	//WriteVoxelTree(ptn, "nb-after-order.raw");
	//WriteFilaments(Thinned_Filament, 2, "fil2-before-smoothing.raw");

	/* 
	 * Now we calculate the new core radii 
	 */
	int numOldFilaments = (int)P[0][0].xcord;
	int numNewFilaments = (int)Thinned_Filament[0][0].xcord;
	int rgNumOfSplits[numOldFilaments]; 
	int rgrgFilamentIndexMatrix[numNewFilaments+1][numOldFilaments+1];

	int i, j;
	for(j=0; j<=numOldFilaments; j++)
		{
		rgNumOfSplits[j] = 0;
		for (i=0; i<=numNewFilaments; i++)
			{
			rgrgFilamentIndexMatrix[i][j] = 0;
			}
		}

	int iFilamentOfCurrentNode = 0;
	int ifn = 0;
	for (irgfn=1; irgfn<=numNewFilaments; irgfn++)
		{
		for (ifn=1; ifn<=(int) Thinned_Filament[irgfn][0].xcord; ifn++)
			{
			iFilamentOfCurrentNode = (int) Thinned_Filament[irgfn][ifn].iFilament;
			//printf("iFilamentOfCurrentNode is %i\n", iFilamentOfCurrentNode);
			if (rgrgFilamentIndexMatrix[irgfn][iFilamentOfCurrentNode] == 0)
				{
				//printf("incrementing matrix element (%i, %i)\n", irgfn, iFilamentOfCurrentNode);
					rgrgFilamentIndexMatrix[irgfn][iFilamentOfCurrentNode] = 1;
					rgNumOfSplits[iFilamentOfCurrentNode] += 1;
				}
			}
		}

	int irgfn2 = 0;
	for (irgfn=1; irgfn<=numNewFilaments; irgfn++)
		for (irgfn2=1; irgfn2<=numOldFilaments; irgfn2++)
			printf("Index Matrix (%i, %i) = %i\n", irgfn, irgfn2, rgrgFilamentIndexMatrix[irgfn][irgfn2]);


	for (irgfn2=1; irgfn2<=numOldFilaments; irgfn2++)
		printf(" num of splits for oldf fil %i : %i\n", irgfn2,  rgNumOfSplits[irgfn2]);

	double lll_OldCoreContribution = 0.0;
	int irgfnP = 0;
	double l_NewCoreSize = 0.0;
	double l_OldCoreSize = 0.0;
	double l_NewLength = 0.0;
	double l_OldLength = 0.0;
	for (irgfn=1; irgfn<=numNewFilaments; irgfn++)
		{
		lll_OldCoreContribution = 0.0;
		for (irgfnP=1; irgfnP<=numOldFilaments; irgfnP++)
			{
			//printf("in loop, rgrgFilamentIndexMatrix = %i\n", rgrgFilamentIndexMatrix[irgfn][irgfnP]);
			if (rgrgFilamentIndexMatrix[irgfn][irgfnP] == 1)
				{
				l_OldLength = P[irgfnP][0].ycord;
				l_OldCoreSize = P[irgfnP][0].sigma; 
				lll_OldCoreContribution += ( sqr(l_OldCoreSize) * l_OldLength/rgNumOfSplits[irgfnP] );
				printf("old length is %f\n", l_OldLength);
				printf("old core size %f\n", l_OldCoreSize);
				printf("old core contribution %f\n", lll_OldCoreContribution);
				}
			}
		l_NewLength = lComputeFilamentLength(Thinned_Filament, irgfn);
		Thinned_Filament[irgfn][0].ycord = l_NewLength;
		printf("new length is %f\n", l_NewLength);
		l_NewCoreSize = sqrt(lll_OldCoreContribution/l_NewLength); 
		Thinned_Filament[irgfn][0].sigma = l_NewCoreSize;
		printf("Core size calculated for filament %i with new merging method:%f\n",irgfn, l_NewCoreSize); 
		}

	printf("Smoothing filaments...");
	My_Periodic_Smoothing_Spline(Thinned_Filament, vlSplineWeight);
	printf("done.\n");

	//WriteFilaments(Thinned_Filament, 1, "fil1-after-smoothing.raw");
	
	printf(" 		Check node ordering of filament(s).\n"); 
	for(irgfn=1; irgfn<=numNewFilaments; irgfn++)
		My_Robustly_Check_Direction_of_Ordering(P, Thinned_Filament, irgfn); /*** CHANGE to my modified function */ 

	
	P[0][0].xcord = numNewFilaments;
	for(irgfn=1; irgfn<=numNewFilaments; irgfn++)
		{		    
		printf(" 		Filament %i: %i nodes\n", irgfn, (int)Thinned_Filament[irgfn][0].xcord); 
		P[irgfn][0].xcord = Thinned_Filament[irgfn][0].xcord;
		for(ifn=1; ifn<=(int)Thinned_Filament[irgfn][0].xcord; ifn++)
			{
			P[irgfn][ifn].xcord = Thinned_Filament[irgfn][ifn].xcord;
			P[irgfn][ifn].ycord = Thinned_Filament[irgfn][ifn].ycord;
			P[irgfn][ifn].zcord = Thinned_Filament[irgfn][ifn].zcord;
			}
		}
}
double *
Rgdvl_dcoComputeDifferencesAtVoxel(VX *pvx, char chBackwardOrForward, int fUseTrialStep)
{
	if (!FVoxelHasAllNeighbours(pvx))
		return NULL;

	double vlCurrVoxel = VlFromPvx(pvx);	
	double *rgdvl_dco = (double *) malloc(sizeof(double)*3);
	RK *rgprk[3];  
	RK *rgprkF[3];  

	if (fUseTrialStep == TRUE)
		{
		if (chBackwardOrForward == '-')
			{
			rgprk[0] = (RK *) PvGetVoxelData((PvxGetNeighbour(pvx, wyXPlus1)));  
			rgprk[1] = (RK *) PvGetVoxelData((PvxGetNeighbour(pvx, wyYPlus1)));  
			rgprk[2] = (RK *) PvGetVoxelData((PvxGetNeighbour(pvx, wyZPlus1)));  
			rgdvl_dco[0] = vlCurrVoxel - rgprk[0]->vlTrialStep; 
			rgdvl_dco[1] = vlCurrVoxel - rgprk[1]->vlTrialStep;
			rgdvl_dco[2] = vlCurrVoxel - rgprk[2]->vlTrialStep;
			}
		else if (chBackwardOrForward == '+')
			{
			rgprk[0] = (RK *) PvGetVoxelData((PvxGetNeighbour(pvx, wyXMinus1)));  
			rgprk[1] = (RK *) PvGetVoxelData((PvxGetNeighbour(pvx, wyYMinus1)));  
			rgprk[2] = (RK *) PvGetVoxelData((PvxGetNeighbour(pvx, wyZMinus1)));  
			rgdvl_dco[0] = rgprk[0]->vlTrialStep - vlCurrVoxel; 
			rgdvl_dco[1] = rgprk[1]->vlTrialStep - vlCurrVoxel;
			rgdvl_dco[2] = rgprk[2]->vlTrialStep - vlCurrVoxel;
			}
		else if (chBackwardOrForward == '0')
			{
			rgprk[0] = (RK *) PvGetVoxelData((PvxGetNeighbour(pvx, wyXMinus1)));  
			rgprk[1] = (RK *) PvGetVoxelData((PvxGetNeighbour(pvx, wyYMinus1)));  
			rgprk[2] = (RK *) PvGetVoxelData((PvxGetNeighbour(pvx, wyZMinus1)));  
			rgprkF[0] = (RK *) PvGetVoxelData((PvxGetNeighbour(pvx, wyXPlus1)));  
			rgprkF[1] = (RK *) PvGetVoxelData((PvxGetNeighbour(pvx, wyYPlus1)));  
			rgprkF[2] = (RK *) PvGetVoxelData((PvxGetNeighbour(pvx, wyZPlus1)));  
			rgdvl_dco[0] = rgprkF[0]->vlTrialStep - rgprk[0]->vlTrialStep; 
			rgdvl_dco[1] = rgprkF[1]->vlTrialStep - rgprk[1]->vlTrialStep;
			rgdvl_dco[2] = rgprkF[2]->vlTrialStep - rgprk[2]->vlTrialStep;
			}
		}
	else
		{
		if (chBackwardOrForward == '-')
			{
			rgdvl_dco[0] = vlCurrVoxel - VlFromPvx(PvxGetNeighbour(pvx, wyXMinus1)); 
			rgdvl_dco[1] = vlCurrVoxel - VlFromPvx(PvxGetNeighbour(pvx, wyYMinus1));
			rgdvl_dco[2] = vlCurrVoxel - VlFromPvx(PvxGetNeighbour(pvx, wyZMinus1));
			}
		else if (chBackwardOrForward == '+')
			{
			rgdvl_dco[0] = VlFromPvx(PvxGetNeighbour(pvx, wyXPlus1)) - vlCurrVoxel; 
			rgdvl_dco[1] = VlFromPvx(PvxGetNeighbour(pvx, wyYPlus1)) - vlCurrVoxel;
			rgdvl_dco[2] = VlFromPvx(PvxGetNeighbour(pvx, wyZPlus1)) - vlCurrVoxel;
			}
		else if (chBackwardOrForward == '0')
			{
			VX *pvxNeigh = PvxGetNeighbour(pvx, wyZMinus1);
			assert(ZFromPvx(pvxNeigh) == (ZFromPvx(pvx) - 1));
			rgdvl_dco[0] = VlFromPvx(PvxGetNeighbour(pvx, wyXPlus1)) - VlFromPvx(PvxGetNeighbour(pvx, wyXMinus1)); 
			rgdvl_dco[1] = VlFromPvx(PvxGetNeighbour(pvx, wyYPlus1)) - VlFromPvx(PvxGetNeighbour(pvx, wyYMinus1));
			rgdvl_dco[2] = VlFromPvx(PvxGetNeighbour(pvx, wyZPlus1)) - VlFromPvx(PvxGetNeighbour(pvx, wyZMinus1));
			}

		}
	return rgdvl_dco;
}
void
CalculateRungeKuttaDerivTestCase(TN *ptn, void *pvRungeKuttaStep)
{
	int idvl_dtRungeKuttaStep = *((int *) pvRungeKuttaStep);
	/*VX *pvxCurrVoxel = PvxFromPtn(ptn);*/
	VX *pvxCurrVoxel = (ptn == NULL) ? NULL : ptn->pvx;

	int fUseTrialStep = 0;
	if (idvl_dtRungeKuttaStep == 1)
		fUseTrialStep = FALSE;
	else if (idvl_dtRungeKuttaStep == 2 || idvl_dtRungeKuttaStep == 3 || idvl_dtRungeKuttaStep == 4)
		fUseTrialStep = TRUE;

	double *rgdvl_dcoNeg = Rgdvl_dcoComputeDifferencesAtVoxel(pvxCurrVoxel, '-', fUseTrialStep);
	double *rgdvl_dcoPos = Rgdvl_dcoComputeDifferencesAtVoxel(pvxCurrVoxel, '+', fUseTrialStep);
	
	
	RK *prk = (RK *) PvGetVoxelData(pvxCurrVoxel);
	if (rgdvl_dcoPos == NULL || rgdvl_dcoNeg == NULL)
		{
		prk->rgdvl_dt[idvl_dtRungeKuttaStep] = 0;
		return;
		}

	double dvl_dxPos = rgdvl_dcoPos[0];
	double dvl_dyPos = rgdvl_dcoPos[1];
	double dvl_dzPos = rgdvl_dcoPos[2];
	double dvl_dxNeg = rgdvl_dcoNeg[0];
	double dvl_dyNeg = rgdvl_dcoNeg[1];
	double dvl_dzNeg = rgdvl_dcoNeg[2];
	double delvl = 0;
	double vlSpeed = -1;

	if (vlSpeed <= 0)
		{
		delvl = sqrt( pow(MAX(dvl_dxPos,0.0),2) + 
					  pow(MIN(dvl_dxNeg,0.0),2) + 
					  pow(MAX(dvl_dyPos,0.0),2) + 
					  pow(MIN(dvl_dyNeg,0.0),2) + 
					  pow(MIN(dvl_dzNeg,0.0),2) + 
					  pow(MAX(dvl_dzPos,0.0),2) );     
		}
	else
		{
		delvl = sqrt( pow(MAX(dvl_dxNeg,0.0),2) + 
					  pow(MIN(dvl_dxPos,0.0),2) + 
					  pow(MAX(dvl_dyNeg,0.0),2) + 
					  pow(MIN(dvl_dyPos,0.0),2) + 
					  pow(MIN(dvl_dzPos,0.0),2) + 
					  pow(MAX(dvl_dzNeg,0.0),2) ); 
		}	
	
	prk->rgdvl_dt[idvl_dtRungeKuttaStep] = vlSpeed * delvl;
	//printf("step = %i, delvl = %f, time derivative = %f\n", idvl_dtRungeKuttaStep, delvl, prk->rgdvl_dt[idvl_dtRungeKuttaStep]);

	//SetRKTimeDerivative(ptn, vlSpeed*delvl, *pchRungeKuttaStep);
	
	//if (*pchRungeKuttaStep == '2')
		//printf("step = %c, delvl = %f, time derivative = %f\n", *pchRungeKuttaStep, delvl, Dvl_dtGetRKTimeDerivative(pvc, *pchRungeKuttaStep));

//	if (vlSpeed*delvl == 0.0)
//		{
//		printf("dvl_dxPos =%f, dvl_dyPos = %f, dvl_dzPos = %f\n", dvl_dxPos, dvl_dyPos, dvl_dzPos);
//		printf("dvl_dxNeg =%f, dvl_dyNeg = %f, dvl_dzNeg = %f\n", dvl_dxNeg, dvl_dyNeg, dvl_dzNeg);
//		exit(0);
//		}

	//printf("time derivative  = %f\n", vlSpeed*delvl);
	//if(itGlobal == 1)
	//	{
	//	itGlobal = 0;
	//	}
	free(rgdvl_dcoNeg);
	free(rgdvl_dcoPos);
}
void
TakeNewTrialStep(TN *ptn, void *pvRungeKuttaStep)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;

	int idvl_dtRungeKuttaStep = *((int *) pvRungeKuttaStep);
	double rStep = 0;
	if (idvl_dtRungeKuttaStep == 1)
		rStep = 0.5;
	else if (idvl_dtRungeKuttaStep == 2) //4th order update is now obsolete
		rStep = 0.5;
	else if (idvl_dtRungeKuttaStep == 3) //4th order update is now obsolete
		rStep = 1.0;
	else
		{
		printf("Error: Runge-Kutta step can only be 1, 2, or 3 in TakeNewTrialStep(...)\n");
		return;
		}
	RK *prk = (RK *) PvGetVoxelData(pvx);
	prk->vlTrialStep = (rStep*dtTimeStep*prk->rgdvl_dt[idvl_dtRungeKuttaStep]) + VlFromPvx(pvx);

	//printf("step = %i, time derivative in TakeNewTrialStep = %f, vlTrialStep=%f,\n", idvl_dtRungeKuttaStep, prk->rgdvl_dt[idvl_dtRungeKuttaStep], rStep*dtTimeStep*prk->rgdvl_dt[idvl_dtRungeKuttaStep]+VlFromPvx(pvx));
}
void
TakeNewTrialStep_Boundary(TN *ptn, void *pvRungeKuttaStep)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;

	int idvl_dtRungeKuttaStep = *((int *) pvRungeKuttaStep);
	double rStep = 0;

	if (idvl_dtRungeKuttaStep == 1)
		rStep = 0.5;
	else if (idvl_dtRungeKuttaStep == 2)
		rStep = 0.5;
	else if (idvl_dtRungeKuttaStep == 3)
		rStep = 1.0;
	else
		{
		printf("Error: Runge-Kutta step can only be 1, 2, or 3 in TakeNewTrialStep(...)\n");
		return;
		}

	RK *prk = (RK *) PvGetVoxelData(pvx);
	RK *prkNeighToCopy = (RK *) PvGetVoxelData(prk->pvxNeighToCopy);
	prk->vlTrialStep = (rStep*dtTimeStep*prkNeighToCopy->rgdvl_dt[idvl_dtRungeKuttaStep]) + VlFromPvx(pvx);
}
void
CalculateRungeKuttaDeriv(TN *ptn, void *pvRungeKuttaStep)
{
	int idvl_dtRungeKuttaStep = *((int *) pvRungeKuttaStep);
	/*VX *pvxCurrVoxel = PvxFromPtn(ptn);*/
	VX *pvxCurrVoxel = (ptn == NULL) ? NULL : ptn->pvx;
	RK *prk = (RK *) PvGetVoxelData(pvxCurrVoxel);

	/* Take a trial step depending on runge-kutta step */
	int fUseTrialStep = 0;
	if (idvl_dtRungeKuttaStep == 1)
		fUseTrialStep = FALSE;
	else if (idvl_dtRungeKuttaStep == 2 || idvl_dtRungeKuttaStep == 3 || idvl_dtRungeKuttaStep == 4)
		fUseTrialStep = TRUE;

	/* Get central difference of Psi at this voxel */
	double *rgdvl_dcoCentral = Rgdvl_dcoComputeDifferencesAtVoxel(pvxCurrVoxel, '0', fUseTrialStep);

	/* Make sure central difference is non null */
	if (rgdvl_dcoCentral == NULL)
		{
		printf("Central difference is NULL! Returning 0.\n");
		prk->rgdvl_dt[idvl_dtRungeKuttaStep] = 0;
		return;
		}

	/* Normalize central difference to get gradient */
	int idvl_dco;
	for (idvl_dco=0; idvl_dco<3; idvl_dco++)
		rgdvl_dcoCentral[idvl_dco] = cvxResolution*rgdvl_dcoCentral[idvl_dco]/2;

	/* Get fluid velocity at this voxel */
	double *rgdco_dt = prk->rgdco_dtFluidVelocity[idvl_dtRungeKuttaStep]; 

	/* the time derivative, Psi_t, is the dot product of the gradient and fluid velocity: grad(Psi) . V */
	double vlDotProduct = (rgdco_dt[0]*rgdvl_dcoCentral[0] + rgdco_dt[1]*rgdvl_dcoCentral[1] + rgdco_dt[2]*rgdvl_dcoCentral[2]);

	/* Debug */
	double vlGradientMagnitude = sqrt(pow(rgdvl_dcoCentral[0],2) + pow(rgdvl_dcoCentral[1],2) + pow(rgdvl_dcoCentral[2],2));
	prk->rgdco_dtNormalVelocity[idvl_dtRungeKuttaStep][0] = vlDotProduct * rgdvl_dcoCentral[0] / (sqr(vlGradientMagnitude)+0.000001);	
	prk->rgdco_dtNormalVelocity[idvl_dtRungeKuttaStep][1] = vlDotProduct * rgdvl_dcoCentral[1] / (sqr(vlGradientMagnitude)+0.000001);
	prk->rgdco_dtNormalVelocity[idvl_dtRungeKuttaStep][2] = vlDotProduct * rgdvl_dcoCentral[2] / (sqr(vlGradientMagnitude)+0.000001);

	prk->rgdvl_dcoGradient[idvl_dtRungeKuttaStep][0] = rgdvl_dcoCentral[0];
	prk->rgdvl_dcoGradient[idvl_dtRungeKuttaStep][1] = rgdvl_dcoCentral[1];
	prk->rgdvl_dcoGradient[idvl_dtRungeKuttaStep][2] = rgdvl_dcoCentral[2];
	/* End debug */

	/* store Psi_t at voxel (Psi_t = -F|grad(Psi_t)|) */
	prk->rgdvl_dt[idvl_dtRungeKuttaStep] = (-1) * vlDotProduct;

//	if (rgdvl_dcoNeg != NULL && rgdvl_dcoPos != NULL)
//		{
//		printf("%f %f %f\n", rgdvl_dcoPos[0], rgdvl_dcoPos[1], rgdvl_dcoPos[2]);
//		vlDotProduct = MAX(-rgdco_dt[0],0)*rgdvl_dcoNeg[0] + MIN(-rgdco_dt[0],0)*rgdvl_dcoPos[0] + 
//					   MAX(-rgdco_dt[1],0)*rgdvl_dcoNeg[1] + MIN(-rgdco_dt[0],0)*rgdvl_dcoPos[1] + 
//					   MAX(-rgdco_dt[2],0)*rgdvl_dcoNeg[2] + MIN(-rgdco_dt[0],0)*rgdvl_dcoPos[2];
//		}
	
	
	/* Debug */
	double dl_dt = sqrt(pow(rgdco_dt[0],2) + pow(rgdco_dt[1],2) + pow(rgdco_dt[2],2));
	if (dl_dt*dtTimeStep*cvxResolution > dvxMax)
		{
		dvxMax = dl_dt*dtTimeStep*cvxResolution;
		tgMaxDist = pvxCurrVoxel->tg;
		}
	if (dl_dt*dtTimeStep*cvxResolution < dvxMin)
		dvxMin = dl_dt*dtTimeStep*cvxResolution;
	/* End debug */

	//printf("fluid velocity*dt (voxels): dx_dt = %f, dydt = %f, dzdt = %f\n", rgdco_dt[0]*cvxResolution*dtTimeStep, dtTimeStep*rgdco_dt[1]*cvxResolution, dtTimeStep*rgdco_dt[2]*cvxResolution);
	//printf("time derivative at voxel = %f\n", vlDotProduct);

	//free(rgdvl_dcoPos);
	//free(rgdvl_dcoNeg);
	free(rgdvl_dcoCentral);
}
void
SetRKOfNeighbourToCopy(TN *ptn, void *pv)
{
	char *pch = (char *) pv;
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;

	double vlMinOrMax;
	if (*pch == 'i')
		vlMinOrMax = -100000;
	else if (*pch == 'o')
		vlMinOrMax =  100000;

	VX *pvxNeighToCopy = NULL;
	VX *pvxNeigh = NULL;
	double vlNeigh = 0.0;
	int tgNeigh = 0;
	int wy;

	for (wy=0; wy < 6; wy++)
		{
		pvxNeigh = PvxGetNeighbour(pvx,wy);
		if (pvxNeigh != NULL)
			{
			tgNeigh = TgFromPvx(pvxNeigh);
			if (*pch == 'i' && tgNeigh == tgInsideBarrier)
				{
				vlNeigh = VlFromPvx(pvxNeigh);
				if (vlNeigh > vlMinOrMax)
					{
					vlMinOrMax = vlNeigh;
					pvxNeighToCopy = pvxNeigh;
					}
				}
			else if (*pch == 'o' && tgNeigh == tgOutsideBarrier)
				{
				vlNeigh = VlFromPvx(pvxNeigh);
				if (vlNeigh < vlMinOrMax)
					{
					vlMinOrMax = vlNeigh;
					pvxNeighToCopy = pvxNeigh;
					}
				}
			}
		}

	if (pvxNeighToCopy == NULL) 
		{	
		printf("Error found: Boundary voxel can't find neighbouring barrier voxel.\n");
		exit(1);
		}

	RK *prk = (RK *) PvGetVoxelData(pvx);
	prk->pvxNeighToCopy = pvxNeighToCopy;
}
void
SaveNewVoxelValue2ndOrder_Boundary(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	RK *prk = (RK *) PvGetVoxelData(pvx);
	RK *prkNeighToCopy = (RK *) PvGetVoxelData(prk->pvxNeighToCopy);
	double vlNew = VlFromPvx(pvx) + dtTimeStep*prkNeighToCopy->rgdvl_dt[2];
	SetVoxelValue(pvx, vlNew);
}
void
SaveNewVoxelValue2ndOrder(TN *ptn, void *pvBoundaryIsHitFlag)
{
	//int *pfBoundaryIsHit = (int *) pvBoundaryIsHitFlag;
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;

	/* Update Voxel Value */
	RK *prk = (RK *) PvGetVoxelData(pvx);	
	double vlNew = VlFromPvx(pvx) + dtTimeStep*prk->rgdvl_dt[2];
	SetVoxelValue(pvx, vlNew);

	/* check to see whether the front being tracked has hit a boundary voxel */
	//int tg = TgFromPvx(pvx);
	//if ( (vlNew < lTubeRadius && tg == tgOutsideBarrier) ||
		 //(vlNew > lTubeRadius && tg == tgInsideBarrier) )
		//{
		//*pfBoundaryIsHit = TRUE;
		//}

	/* Debug */
	//if (VlFromPvx(pvx) > lTubeRadius)
		//return;
	double dvlVoxelDistance = dtTimeStep*prk->rgdvl_dt[2]*cvxResolution;
	double dvlVoxelDistanceMag = fabs(dvlVoxelDistance);
	//printf("RK Update = %f\n", dtTimeStep * prk->rgdvl_dt[2]*cvxResolution);
	//printf("Absolute RK Update = %f\n", ABS(dvlVoxelDistance));
	rgdvlVoxelsTraversed[0]++;
	rgdvlVoxelsTraversed[1] += dvlVoxelDistanceMag;
	if (dvlVoxelDistanceMag > dvxRKMax)
		{
		dvxRKMax = dvlVoxelDistanceMag;
		}
	if (dvlVoxelDistanceMag < dvxRKMin)
		{
		dvxRKMin = dvlVoxelDistanceMag;
		}
	/* End debug */
}
//void
//AddVelocities(TN *ptn, void *pv)
//{
	//VX *pvx = PvxFromPtn(ptn);
	//if (pvx->vl > lTubeRadius)
		//return;
	//RK *prk = (RK *)PvGetVoxelData(pvx);
	//rgr[0]++;
	//rgr[1]+= prk->rgdco_dtFluidVelocity[1][0];
	//rgr[2]+= prk->rgdco_dtFluidVelocity[1][1];
	//rgr[3]+= prk->rgdco_dtFluidVelocity[1][2];
//}
//void
//PrintAvgVelocity(TN *ptn)
//{
	//rgr[0] = 0.0;
	//rgr[1] = 0.0;
	//rgr[2] = 0.0;
	//rgr[3] = 0.0;
//
	//TraverseTree(ptn, &AddVelocities, NULL);
	//printf(" 		Average velocity up to now (%i data points): dxdt=%f, dydt=%f, dzdt=%f\n", (int)rgr[0], rgr[1]/rgr[0], rgr[2]/rgr[0], rgr[3]/rgr[0]);
//}
double
lComputeNodeDistance(Coordinates P1, Coordinates P2)
{

	double dx = P2.xcord - P1.xcord;
	double dy = P2.ycord - P1.ycord;
	double dz = P2.zcord - P1.zcord;
	printf("P2.xcord is %f\n", P2.xcord);
	printf("P2.ycord is %f\n", P2.ycord);
	printf("P2.zcord is %f\n", P2.zcord);
	printf("P1.xcord is %f\n", P1.xcord);
	printf("P1.ycord is %f\n", P1.ycord);
	printf("P1.zcord is %f\n", P1.zcord);
	//printf("dx is %f\n", dx);
	//printf("dy is %f\n", dy);
	//printf("dz is %f\n", dz);

	return sqrt(dx*dx + dy*dy + dz*dz);
}
void
UpdateCoreRadii(Coordinates **Position)
{
	//int irgfnMost = (int) Position[0][0].xcord;
	int irgfn = 0;
	//double dx, dy, dz;
	//double dlSigmaDiffusion = 0.0;
	//int ifn = 0;
	double lFilamentLength = 0.0;

	for (irgfn=1; irgfn<=Position[0][0].xcord; irgfn++)
		{
		lFilamentLength = lComputeFilamentLength(Position, irgfn);
		/* Calculate new core radius for filament based on volume conservation */
		Position[irgfn][0].sigma = Position[irgfn][0].sigma * sqrt( Position[irgfn][0].ycord / lFilamentLength ); 

		printf("\t\tNew filament length is %f, old filament length is %f, so new core radius before core-spreading is %f for filament %i\n", lFilamentLength, Position[irgfn][0].ycord, Position[irgfn][0].sigma, irgfn);

		Position[irgfn][0].ycord = lFilamentLength; 

		//dlSigmaDiffusion = sqrt( 2 * 2.205 * ll_tCirculation/ReynoldsNumber * dtTimeStep/2.0 );
		//Position[irgfn][0].sigma = lNewCoreRadius + dlSigmaDiffusion; 

		/* Calculate new core radius for filament after diffusion */
		/* d sigma^2
		 * -------- = 2 * 2.205 * kinematic viscosity
		 *    dt
		 */
		/*  
		 *  sigma(n)  = sqrt( sigma(n-1)^2 + dt(2 * 2.205 * kinematic viscosity )
		 */
		double llConstant = 0.5 * dtTimeStep * 2 * 2.205 * (ll_tCirculation/ReynoldsNumber);
		Position[irgfn][0].sigma = sqrt( Position[irgfn][0].sigma*Position[irgfn][0].sigma + llConstant ); 
		printf("\t\tNew core radius after core-spreading is %f for filament %i\n", Position[irgfn][0].sigma, irgfn);
		}
}
void
PrintFilament(Coordinates **Position)
{
	int irgfnMost = (int) Position[0][0].xcord;
	int irgfn = 0;
	int ifn = 0;

	/* for each filament */
	for (irgfn = 1; irgfn <= irgfnMost; irgfn++)
		{
		for (ifn = 1; ifn <= (int)Position[irgfn][0].xcord; ifn++)
			{
			printf("Position[irgfn][ifn].xcord is %f\n", Position[irgfn][ifn].xcord);
			printf("Position[irgfn][ifn].ycord is %f\n", Position[irgfn][ifn].ycord);
			printf("Position[irgfn][ifn].zcord is %f\n", Position[irgfn][ifn].zcord);
			}
		}
}
int
FUpdateFront2ndOrder(TN **rgptnNB, TN **rgptnIntBoundary, TN **rgptnExtBoundary, Coordinates **Position, double t)
{
	int irgfnMost = (int) Position[0][0].xcord;
	Coordinates **New_Position = (Coordinates **) malloc(sizeof(Coordinates *)*(irgfnMost+1));
	int irgfn = 0;
	for (irgfn = 0; irgfn <= irgfnMost; irgfn++)
		{
		New_Position[irgfn] = (Coordinates *) malloc(sizeof(Coordinates)*NODES);
		}

	int idvl_dtRungeKuttaStep;
	int fBoundaryIsHit = 0;

	printf("\n");
	printf("  	--------------------\n");
	printf(" 	Runge Kutta Step 1\n");
	printf(" 	--------------------\n");
	idvl_dtRungeKuttaStep = 1;

	/* for each tree */
	int i = 0;
	for(i=1; i<=(int) Position[0][0].xcord; i++)
		{
		printf(" 	VOXEL TREE FOR FILAMENT %i\n", i);

		printf(" 	1. Calling ComputeBiotSavartIntegral(...) for each voxel in narrow band tree %i...\n", i);
		ComputeFluidVelocityAtVoxels(rgptnNB[i], Position, idvl_dtRungeKuttaStep); //idvl_dtRungeKuttaStep here is for saving right velocities to file 

		/* Debug */
		dvxMax = -LARGE_NUMBER;
		dvxMin = LARGE_NUMBER;
		/* End Debug */

		printf(" 	2. Taking a trial step for each voxel in narrow band tree, interior boundary tree, and exterior boundary tree...");
		TraverseTree(rgptnNB[i], &CalculateRungeKuttaDeriv, &idvl_dtRungeKuttaStep);
		TraverseTree(rgptnNB[i], &TakeNewTrialStep, &idvl_dtRungeKuttaStep);
		TraverseTree(rgptnIntBoundary[i], &TakeNewTrialStep_Boundary, &idvl_dtRungeKuttaStep);
		TraverseTree(rgptnExtBoundary[i], &TakeNewTrialStep_Boundary, &idvl_dtRungeKuttaStep);
		printf("done.\n");
		}

	printf("\n");
	printf(" 	--------------------\n");
	printf(" 	Runge Kutta Step 2\n");
	printf(" 	--------------------\n");
	idvl_dtRungeKuttaStep = 2;

	printf(" 	3. Calling Accurate_NewFilament_Position(...) and evolving the filament(s) through dt/2 = %f...\n", 0.5*dtTimeStep);
	Accurate_NewFilament_Position(Position, New_Position, 0.5*dtTimeStep, FALSE);
	
	printf(" 	 4. Calling UpdateCoreRadii(...) and calculating the new core radii, saving them to the filament array\n");
	UpdateCoreRadii(New_Position);

	for(i=1; i<=(int) Position[0][0].xcord; i++)
		{
		printf(" 	VOXEL TREE FOR FILAMENT %i\n", i);

		printf(" 	5. Calling ComputeBiotSavartIntegral(...) for each narrow band voxel (using the new filament node positions)...");
		ComputeFluidVelocityAtVoxels(rgptnNB[i], New_Position, idvl_dtRungeKuttaStep); 
		printf("done.\n");

		/* Debug */
		dvxRKMax = -LARGE_NUMBER;
		dvxRKMin = LARGE_NUMBER;
		rgdvlVoxelsTraversed[0] = 0.0;
		rgdvlVoxelsTraversed[1] = 0.0;
		/* End Debug */

		printf(" 	6. Calculating Psi_t(2), in narrow band tree, using current fluid velocities and Psi trial steps...");
		TraverseTree(rgptnNB[i], &CalculateRungeKuttaDeriv, &idvl_dtRungeKuttaStep);
		printf("done.\n");

		printf(" 	7. Updating Psi in the narrow band and its boundary:  Psi_new = Psi_old + Psi_t(2)*dt...");
		TraverseTree(rgptnNB[i], &SaveNewVoxelValue2ndOrder, &fBoundaryIsHit);
		TraverseTree(rgptnIntBoundary[i], &SaveNewVoxelValue2ndOrder_Boundary, NULL);
		TraverseTree(rgptnExtBoundary[i], &SaveNewVoxelValue2ndOrder_Boundary, NULL);
		printf("done.\n");
		}

	printf(" 		/* Debug */ \n");
	printf(" 		Tag of voxel for maximum = %i\n", tgMaxDist);
	printf(" 		Maximum fluid velocity * dt * resolution (in filament core) = %f voxels\n", dvxMax);
	printf(" 		Minimum fluid velocity * dt * resolution (in filament core) = %f voxels\n", dvxMin);
	printf(" 		Maximum magnitude RK update in narrow band ((fluid velocity)dot(gradient) * dt * resolution) = %f voxels\n", dvxRKMax);
	printf(" 		Minimum magnitude RK update in narrow band ((fluid velocity)dot(gradient) * dt * resolution) = %f voxels\n", dvxRKMin);
	printf(" 		Average magnitude RK update in narrow band  = %f voxels\n", rgdvlVoxelsTraversed[1]/rgdvlVoxelsTraversed[0]);
	printf(" 		/* End Debug */ \n");

	printf(" 	8. Calling Accurate_NewFilament_Position(...) and evolving the filament(s) through dt/2 = %f...\n", 0.5*dtTimeStep);
	Accurate_NewFilament_Position(New_Position, Position, 0.5*dtTimeStep, FALSE);

	printf(" 	9. Calling UpdateCoreRadii(...) and calculating the new core radii, saving them to the filament array\n");
	UpdateCoreRadii(Position);

	for (irgfn = 0; irgfn <= irgfnMost; irgfn++)
		{
		free(New_Position[irgfn]);
		}

	//return fBoundaryIsHit;
	return 0;
}
void
MarkBoundaryVoxels(TN *ptn, void *pvBoundary)
{
	double *pvlBoundary = (double *) pvBoundary;	

	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	if (VlFromPvx(pvx) > (*pvlBoundary)/cvxResolution)
		{
		//SetVoxelTag(PvxFromPtn(ptn), tgBarrier);	
		}
	else
		{
		//SetVoxelTag(PvxFromPtn(ptn), tgInterior);
		}
}
TN *
PtnVoxelsFromFilaments(Coordinates *Real_Fil, int iFilament)
{
  TN *ptn = NULL;

  //round-off the positions of the real filaments' nodes
  double rx, ry, rz;
  int x, y, z, j; 
  int c = 0;
  for(j=1;j<=(int)Real_Fil[0].xcord;j++)
	  {
	  /* get the (x,y,z) voxel coordinate for each filament position */
	  //		  rx= Real_Fil[i][j].xcord*RADIUS;
	  //		  ry= Real_Fil[i][j].ycord*RADIUS;
	  //		  rz= Real_Fil[i][j].zcord*RADIUS;
	  rx= cvxResolution*Real_Fil[j].xcord;
	  ry= cvxResolution*Real_Fil[j].ycord;
	  rz= cvxResolution*Real_Fil[j].zcord;

	  x=(int)rint(rx);
	  y=(int)rint(ry);
	  z=(int)rint(rz);

	  //printf("x=%i, y=%i, z=%i\n",x,y,z);

	  /* add the voxel to the tree (the function checks if it has already been inserted) */
	  //int rgco[3] = {x, y, z};
	  //AddVoxelToTree(&ptn, rgco, 0);
	  if (PvxFindVoxel(ptn, x, y, z) == NULL)
		  {
		  c++;
		  VX *pvxNew = PvxCreateVoxel(x, y, z, 0.0);
		  SetVoxelTag(pvxNew, tgAlive);
		  pvxNew->iFilament = iFilament;
		  CreateLinksWithVoxelsInTree(ptn, pvxNew);
		  InsertVoxel(&ptn, pvxNew); 
		  }
	  }

  return ptn;
}
double
lGetExactDistanceToFilament(VX *pvx, Coordinates *Position)
{
	int ifn = 1;
	double dxc = 0;
	double dyc = 0;
	double dzc = 0;
	double dist = 0;
	double distMin = 1e9;
	for(ifn=1; ifn<=(int)Position[0].xcord; ifn++)
		{
		dxc = Position[ifn].xcord - ((double)XFromPvx(pvx)/cvxResolution);
		dyc = Position[ifn].ycord - ((double)YFromPvx(pvx)/cvxResolution);
		dzc = Position[ifn].zcord - ((double)ZFromPvx(pvx)/cvxResolution);
		dist = sqrt((dxc*dxc) + (dyc*dyc) + (dzc*dzc));
		if(dist<distMin)
			distMin=dist;
		}
	return distMin;
}
void
CalcExactDistancesInNB(TN *ptn, void *pvPosition)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	Coordinates *Position = (Coordinates *) pvPosition;
	double lDist = lGetExactDistanceToFilament(pvx, Position);
	SetVoxelValue(pvx, lDist);

	/* Debug */
	if (lDist > lDistMax)
		lDistMax = lDist;
	if (lDist < lDistMin)
		lDistMin = lDist;
	/* End debug */

	//if (lDist >= (lTubeRadius-(lNarrowBandBoundary/cvxResolution)) &&
		//lDist < (lTubeRadius+(lNarrowBandBoundary/cvxResolution)))
		//{
		//SetVoxelTag(pvx, tgNonBarrier);
		//return;
		//}
	if (lDist <= Position[0].sigma/2.0)
		{
		SetVoxelTag(pvx, tgInsideBarrier);	
		return;
		}
	if (lDist > Position[0].sigma/2.0)
		{
		SetVoxelTag(pvx, tgOutsideBarrier);	
		return;
		}
}
void
CalcExactDistancesAtBoundary(TN *ptn, void *pvPosition)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	Coordinates *Position = (Coordinates *) pvPosition;
	double lDist = lGetExactDistanceToFilament(pvx, Position);
	SetVoxelValue(pvx, lDist);
	SetVoxelTag(pvx, tgBoundary);
}
void
InsertBoundaryVoxelIntoHeap(TN *ptn, void *pv)
{
	fibheap_t fh = (fibheap_t) pv;	

	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	SetVoxelValue(pvx, LARGE);
	SetVoxelTag(pvx, tgFar);
	fibnode_t fnSav = NULL;

	int wy;
	for (wy=0; wy<6; wy++)
		{
		if (PvxGetNeighbour(pvx, wy) == NULL) //if its a boundary voxel
			{
			SetVoxelValue(pvx, 0.0);
			SetVoxelTag(pvx, tgAlive);
			fnSav = fibheap_insert(fh, 0, pvx);
			SetVoxelData(pvx, fnSav); //voxel points to node and vice versa
			return;
			}
		}
}
void
DoCheckAndGetIntBoundary(TN *ptnNB, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptnNB);*/
	VX *pvx = (ptnNB == NULL) ? NULL : ptnNB->pvx;
	int tg = TgFromPvx(pvx);
	VX *pvxNeigh = NULL;
	int tgNeigh = 0;
	int wy;

	/* for each voxel of the narrow band */
	for(wy=0; wy<6; wy++)
		{
		pvxNeigh = PvxGetNeighbour(pvx, wy);
		if (pvxNeigh == NULL)
			{
			printf("Error in PtnDoCheckAndGetIntBoundary(..): voxel has to have all 6 neighbours.");
			exit(1);
			}
		tgNeigh = TgFromPvx(pvxNeigh);
		if (tg == tgOutsideBarrier && tgNeigh == tgAlive)
			{
			printf("A tgOutsideBarrier voxel shouldn't be next to a tgAlive voxel in ptnNB.\n");
			exit(1);
			}
		//if (tg == tgNonBarrier && tgNeigh == tgAlive)
			//{
			//printf("narrowband error detected:	inner voxel can only have inner or boundary voxels as neighbours. Try making a smaller narrowband boundary radius or a larger narrowband radius.\n");
			//exit(1);
			//}
		if (tg == tgInsideBarrier && tgNeigh == tgAlive)
			{
			TN **pptnIntBoundary = (TN **) pv;
			InsertVoxel(pptnIntBoundary, pvxNeigh);
			SetVoxelTag(pvxNeigh, tgBoundary);
			}
		}
}
void
CreateNarrowBand(Coordinates *Position, TN **pptnInt, TN **pptnIntBoundary, TN **pptnNB, TN **pptnExtBoundary, int iFilament)
{
	printf(" 	1. Rounding filament nodes to nearest voxels and inserting into binary tree...");
	TN *ptnHeap = PtnVoxelsFromFilaments(Position, iFilament); 
	printf("done.\n");

	/* Debug */
	WriteVoxelTree(ptnHeap, "voxels-from-filament-0000.raw");
	/* End debug */

	fibheap_t fh = fibheap_new();
	TraverseTree(ptnHeap, &InsertBoundaryVoxelIntoHeap, fh); //put filament voxels into heap

	double lLocalTubeRadius = Position[0].sigma/2.0;
	printf(" 	(Using tube radius = %f)\n", lLocalTubeRadius);

	/* Create interior voxels inside of the narrow band. All voxels at a FMM (i.e. not exact) distance smaller than 
	 * (lTubeRadius*cvxResolution-lNarrowBandRadius) are created. */
	//double cvxNBStart = lTubeRadius*cvxResolution-lNarrowBandRadius;
	double cvxNBStart = lLocalTubeRadius*cvxResolution-lNarrowBandRadius;

	/* All voxels at FMM distance >= (lTubeRadius*cvxResolution-lNarrowBandRadius) and
	 * 			   < (lTubeRadius*cvxResolution+lNarrowBandRadius)
	 * are created and put into the narrow band.  */
	double cvxNBEnd = lLocalTubeRadius*cvxResolution+lNarrowBandRadius;

	printf(" 	2. Creating interior voxels (all voxels at FMM distance < %f from voxel filament)...\n", cvxNBStart);
	TN *ptnInt = NULL;
	printf("\t\t");
	Fmm_CreateVoxels(&ptnInt, &ptnHeap, fh, cvxNBStart, iFilament); 
	if (ptnInt == NULL)
		printf(" 		No interior voxels were created.\n");
	else
		printf(" 		There were interior voxels created.\n");

	printf(" 	3. Creating narrow band voxels >= %f and < %f...\n", cvxNBStart, cvxNBEnd);
	TN *ptnNB = NULL;
	printf("\t\t");
	Fmm_CreateVoxels(&ptnNB, &ptnHeap, fh, cvxNBEnd, iFilament);

	/* Voxels at FMM distance >= (lTubeRadius*cvxResolution+lNarrowBandRadius) */
	printf(" 	4. Creating exterior voxels (forms exterior narrow band boundary, >= %f)...", cvxNBEnd);
	TN *ptnExtBoundary = ptnHeap;
	printf("done.\n");

	while(!(fh->nodes == 0)) 
		{
		VX *pvx = (VX *) fibheap_extract_min(fh);
		SetVoxelData(pvx, NULL);
		}

	printf(" 	5. Calculating exact distances from filament for voxels in the narrow band and its interior and exterior boundaries...\n");
	lDistMin = LARGE;
	lDistMax = -LARGE;
	TraverseTree(ptnNB, &CalcExactDistancesInNB, Position);  //distinguishes voxels inside and outside the tube
	printf(" 		Minimum distance in narrow band: %f\n", lDistMin);
	printf(" 		Maximum distance in narrow band: %f\n", lDistMax);
	TraverseTree(ptnExtBoundary, &CalcExactDistancesAtBoundary, Position);
	TN *ptnIntBoundary = NULL;
	TraverseTree(ptnNB, &DoCheckAndGetIntBoundary, &ptnIntBoundary); //finds tgAlive voxels adjacent to tgInsideBarrier voxels
	TraverseTree(ptnIntBoundary, &CalcExactDistancesAtBoundary, Position);
	/* Debug */
	//TraverseTree(ptnExtBoundary, &CalcExactDistancesAtBoundary, Position);
	//TraverseTree(ptnNB, &CalcExactDistancesAtBoundary, Position);
	//TraverseTree(ptnInt, &CalcExactDistancesAtBoundary, Position);
	//ptnExtGlobal = ptnExtBoundary;
	/* End debug */

	//TraverseTree(ptn, &ExactDistanceTransform, Position);
	//TraverseTree(ptn, &MarkBoundaryVoxels, &lNarrowBandBoundary);

	/* To summarize, at this point, all the voxels created = ptnInt + ptnNB + ptnExtBoundary.
	 * ptnIntBoundary is a subset of ptnInt, and is the interior boundary of the
	 * narrow band.  The narrow band is 2*lNarrowBandRadius voxels thick, and the
	 * filament core surface should lie roughly in the middle of this layer.
	 */
 *pptnInt = ptnInt;
 *pptnIntBoundary = ptnIntBoundary;
 *pptnNB = ptnNB;
 *pptnExtBoundary = ptnExtBoundary;
	/*pfil->ptnInt = ptnInt;*/
  /*pfil->ptnIntBoundary = ptnIntBoundary;*/
  /*pfil->ptnNB = ptnNB;*/
  /*pfil->ptnExtBoundary = ptnExtBoundary;*/
}
void
MakeRungeKuttaStorage(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	RK *prk = (RK *) malloc(sizeof(RK));
	assert(prk != NULL);
	prk->vlTrialStep = 0;
	prk->rgdco_dtFluidVelocity[1][0] = 0;
	prk->rgdco_dtFluidVelocity[1][1] = 0;
	prk->rgdco_dtFluidVelocity[1][2] = 0;
	prk->rgdco_dtFluidVelocity[2][0] = 0;
	prk->rgdco_dtFluidVelocity[2][1] = 0;
	prk->rgdco_dtFluidVelocity[2][2] = 0;
	SetVoxelData(pvx, prk);
}
void
DestroyRungeKuttaStorage(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	RK *prk = (RK *) PvGetVoxelData(pvx);
	free(prk);
	SetVoxelData(pvx, NULL);
}
void
SubSampleAndInsert(VX *pvx, TN **pptnSS)
{ 
	double x = XFromPvx(pvx);
	double y = YFromPvx(pvx);
	double z = ZFromPvx(pvx);
	
	if (cvxSubSampleResolution == 1)
		{

		VX *pvxNew = PvxCreateVoxel(x, y, z, 0.0);
		SetVoxelTag(pvxNew, tgFar);
		pvxNew->iFilament = pvx->iFilament;
		CreateLinksWithVoxelsInTree(*pptnSS, pvxNew);
		InsertVoxel(pptnSS, pvxNew);
		return;
		}
	

	double dco= 1/((double)cvxSubSampleResolution); //step size between voxels

	double dcoStep = 0; 
	if (cvxSubSampleResolution%2 == 0) //if even
		dcoStep = dco/2;

	double xSSstart = x - dco*(cvxSubSampleResolution/2) + dcoStep;
	double ySSstart = y - dco*(cvxSubSampleResolution/2) + dcoStep;
	double zSSstart = z - dco*(cvxSubSampleResolution/2) + dcoStep;
	//printf("xStart=%f, yStart=%f, zStart=%f\n", xSSstart, ySSstart, zSSstart);

	VX *pvxNew = NULL;
	int i,j,k;
	int xdSS, ydSS, zdSS;
	double xSS, ySS, zSS;
	for (i=0; i<cvxSubSampleResolution; i++)
		for (j=0; j<cvxSubSampleResolution; j++)
			for (k=0; k<cvxSubSampleResolution; k++)
				{
				xSS = xSSstart + i*dco;
				ySS = ySSstart + j*dco;
				zSS = zSSstart + k*dco;
				if (cvxSubSampleResolution%2 == 0) //if even
					{
					xdSS = (int) rint((1/dcoStep)*(xSS)); //multiply to get back integer coordinates
					ydSS = (int) rint((1/dcoStep)*(ySS));
					zdSS = (int) rint((1/dcoStep)*(zSS)); 
					printf("x=%i, y=%i, z=%i\n", xdSS, ydSS, zdSS);
					}
				else //if odd
					{
					xdSS = (int) rint((1/dco)*(xSS));
					ydSS = (int) rint((1/dco)*(ySS));
					zdSS = (int) rint((1/dco)*(zSS)); 
					}
				pvxNew = PvxCreateVoxel(xdSS, ydSS, zdSS, 0.0);
				SetVoxelTag(pvxNew, tgFar);
				CreateLinksWithVoxelsInTree(*pptnSS, pvxNew);
				InsertVoxel(pptnSS, pvxNew);
				}
}
void
SubSampleAll(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	TN **pptnNewTree = (TN **) pv;

	/* This is new in v2-18, required because voxels in distinct spaces are
	 * being written to the same 3D space.	The interior voxels
	 * however should always be unique.
	 */
	double x = XFromPvx(pvx);
	double y = YFromPvx(pvx);
	double z = ZFromPvx(pvx);
	assert(PvxFindVoxel(*pptnNewTree, x, y, z) == NULL);

	SubSampleAndInsert(pvx, pptnNewTree);
}
void
SubSampleNB(TN *ptnNB, void *pvNewTree)
{
	/*VX *pvx = PvxFromPtn(ptnNB);*/
	VX *pvx = (ptnNB == NULL) ? NULL : ptnNB->pvx;

	assert(pvx->iFilament != 0);

	if(VlFromPvx(pvx) > (GlobalPosition[pvx->iFilament][0].sigma/2.0))
		return;

	TN **pptnNewTree = (TN **) pvNewTree;

	/* This is new in v2-18, required because voxels in distinct spaces are
	 * being written to the same 3D space, and thus the same voxel may be
	 * "flagged" twice.
	 */
	double x = XFromPvx(pvx);
	double y = YFromPvx(pvx);
	double z = ZFromPvx(pvx);
	if (PvxFindVoxel(*pptnNewTree, x, y, z) != NULL)
		return;

	SubSampleAndInsert(pvx, pptnNewTree);
}
void
InvertValues(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	SetVoxelValue(pvx, -(VlFromPvx(pvx)));
}
void
InsertIntoNewTree(TN *ptn, void *pv)
{
	TN **pptnCore = (TN **) pv;
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	CreateLinksWithVoxelsInTree(*pptnCore, pvx);
	InsertVoxel(pptnCore, pvx);
}
void
WriteXAxis(TN *ptn, void *pvFile)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	if (YFromPvx(pvx) == 0 && ZFromPvx(pvx) == 0)
		{
		FILE *pfh = (FILE *) pvFile;
		fprintf(pfh, "%i %f\n", XFromPvx(pvx), pvx->vl);
		}
}
void
GetFilamentFromNB(TN **rgptnNB, TN **rgptnInt, Coordinates **Position, double t)
{
	TN *ptnSS = NULL;

	/* debug */
	//TraverseTree(ptnInt, &SubSampleNB, &ptnSS);
	//TraverseTree(ptnExtGlobal, &SubSampleNB, &ptnSS);
	/* end debug */
	
	int i = 0;
	for (i=1; i<=(int)Position[0][0].xcord; i++)
		{
		printf(" 	a. Adding interior voxels to binary object (sub-sample resolution = %i)...", cvxSubSampleResolution);
		TraverseTree(rgptnInt[i], &SubSampleAll, &ptnSS);
		printf("done.\n");

		printf(" 	b. Thresholding narrow band voxels in tree %i: voxels <= tube radius = %f (core radius = %f)...", i, Position[i][0].sigma/2.0, Position[i][0].sigma);
		//iGlobalPosition = iPosition; //ugly global variable hack, but it's quick
		TraverseTree(rgptnNB[i], &SubSampleNB, &ptnSS);
		}
	printf("done.\n");

	/* Debug */
	//TraverseTree(ptnSS, &TestTreeContainment, ptnSS);
	/* End Debug */

	printf(" 	c. Calculating distance transform on sub-sampled voxels...");
	fibheap_t fh = fibheap_new();
	TraverseTree(ptnSS, &InsertBoundaryVoxelIntoHeap, fh);
	Fmm_NotCreateVoxels(fh, LARGE);
	TraverseTree(ptnSS, &InvertValues, NULL);
	printf("done.\n");

	printf(" 	d. Save the sub-sampled voxels to file.\n");
	printf("\t\t");
	//char szPrefix[40];  
	//sprintf(szPrefix, "ptnSS%i", iPosition);
	WriteVoxelTreeT(ptnSS, "ptnSS", t);

	/* Debug Fmm_NotCreateVoxels(...) */
	//FILE *pfh = fopen("fmm-dt-cross-section.raw", "w+");
	//int x;
	//VX *pvx = NULL;
	//for (x=-70; x<71; x++)
		//{
		//pvx = PvxFindVoxel(ptnSS, x, 0, 0);
		//if (pvx != NULL)
			//fprintf(pfh, "%i %f\n", x, pvx->vl);
		//else
			//fprintf(pfh, "%i %f\n", x, 0.0);
		//}
	//TraverseTree(ptnSS, &WriteXAxis, pfh); 
	//fclose(pfh);
	//exit(0);
	
	/* Debug */
	//TraverseTree(ptnSS, &Test6Neighbours, ptnSS);
	/* End Debug */

	printf(" 	e. Thinning sub-sampled voxels...\n");
	printf("\t\t");
	thin(&ptnSS); 

	printf(" 	f. Write the thinned voxel skeleton to file.\n");
	printf("\t\t");
	//sprintf(szPrefix, "skeleton-before-erase%i", iPosition);
	WriteVoxelTreeT(ptnSS, "skeleton-before-erase", t);

	printf(" 	g. Find and delete merged voxels if any, and link branches together.\n");
	TraverseTree(ptnSS, &Set26VoxelNeighbours, ptnSS);
	ptnRoot = ptnSS;
	//EraseJunctions(&ptnSS); //creates a new tree, so the 26 neighbours are not set 
	Merge(&ptnSS);

	printf("	h. Thin to get back true skeleton if linking in previous step inserted extraneous voxels.\n");
	printf("\t\t");
	thin(&ptnSS);
	TraverseTree(ptnSS, &Set26VoxelNeighbours, ptnSS);

	printf(" 	i. Write the voxel skeleton after the merged voxels have been deleted to file.\n");
	printf("\t\t");
	//sprintf(szPrefix, "skeleton-after-erase%i", iPosition);
	//WriteVoxelTreeT(ptnSS, szPrefix, t);
	WriteVoxelTreeT(ptnSS, "skeleton-before-erase", t);

	printf(" 	j. Smooth filament(s) and order nodes correctly.\n");
	GetSmoothFilament(ptnSS, Position); 

	FreeTreeAndVoxels(ptnSS);
}
void
Test(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	if (PvGetVoxelData(pvx) == NULL)
		printf("voxel data is null\n");
}
void
SetRKStorage(TN *ptnIntBoundary, TN *ptnNB, TN *ptnExtBoundary)
{
	TraverseTree(ptnNB, &MakeRungeKuttaStorage, NULL);
	TraverseTree(ptnExtBoundary, &MakeRungeKuttaStorage, NULL);
	TraverseTree(ptnIntBoundary, &MakeRungeKuttaStorage, NULL);
	char ch = 'i';
	TraverseTree(ptnIntBoundary, &SetRKOfNeighbourToCopy, &ch);
	ch = 'o';
	TraverseTree(ptnExtBoundary, &SetRKOfNeighbourToCopy, &ch);
}
void
RunVortexMethod(Coordinates **P)
{
	double t = 0;
	int it = 0;
	int ditDumpEvery = (int) (dtDumpEvery/dtTimeStep);

	/* Debug */
	int irgfnMost = (int)P[0][0].xcord;	
	Coordinates **Thinned_Filament = (Coordinates **) malloc(sizeof(Coordinates *) * (irgfnMost+1));
	int irgfn = 0;
	for (irgfn = 0; irgfn <= irgfnMost; irgfn++)
		{
		Thinned_Filament[irgfn] = (Coordinates *) malloc(sizeof(Coordinates)*NODES);
		}
	/* End debug */
	
	while(1)
		{
		printf("\n------------------------- \n");
		printf("t = %.3f, ITERATION %i\n", t, it);
		printf("------------------------- \n");

		if (it%ditDumpEvery == 0)
			WriteFilamentsT(P, t, "");

		if (t >= tMax)
			return;

		//Accurate_NewFilament_Position(P, P, dtTimeStep, TRUE);
	
		/* Debug Robustly_Check_Direction_of_Ordering */
		Accurate_NewFilament_Position(P, Thinned_Filament, dtTimeStep, FALSE);
		Swap_Filament(Thinned_Filament, 1);

		printf(" 		Checking node ordering of filament(s)..."); 
		for(irgfn=1; irgfn<=(int)Thinned_Filament[0][0].xcord; irgfn++)
			Robustly_Check_Direction_of_Ordering(P, Thinned_Filament, irgfn);
		printf("done.\n");

		P[0][0].xcord = Thinned_Filament[0][0].xcord;
		int i,j;
		for(i=1; i<=(int)Thinned_Filament[0][0].xcord; i++)
			{		    
			printf(" 		Filament %i: %i nodes\n", i, (int)Thinned_Filament[i][0].xcord); 
			P[i][0].xcord = Thinned_Filament[i][0].xcord;
			for(j=1; j<=(int)Thinned_Filament[i][0].xcord; j++)
				{
				P[i][j].xcord = Thinned_Filament[i][j].xcord;
				P[i][j].ycord = Thinned_Filament[i][j].ycord;
				P[i][j].zcord = Thinned_Filament[i][j].zcord;
				}
			}
		/* End debug */

		t += dtTimeStep;
		it++;
		}
	exit(0);
}

void
CallCreateNarrowBand(FIL *pfil)
{
    /*CreateNarrowBand(Position[i], &rgptnInt[i], &rgptnIntBoundary[i], &rgptnNB[i], &rgptnExtBoundary[i], i);*/
    CreateNarrowBand(pfil->Position, pfil->pptnInt, pfil->pptnIntBoundary, pfil->pptnNB, pfil->pptnExtBoundary, pfil->i);
    SetRKStorage(*(pfil->pptnIntBoundary), *(pfil->pptnNB), *(pfil->pptnExtBoundary));
}

void
RunHybridMethod(Coordinates **Position)
{
	GlobalPosition = Position;
	//rgr[0] = 0.0;
	//rgr[1] = 0.0;
	//rgr[2] = 0.0;
	//rgr[3] = 0.0;

	printf("\n--------------\n");
	printf("Initialization\n");
	printf("--------------\n");

	TN *rgptnInt[MAX_FILAMENTS]; 
	TN *rgptnIntBoundary[MAX_FILAMENTS];
	TN *rgptnNB[MAX_FILAMENTS];
	TN *rgptnExtBoundary[MAX_FILAMENTS];

  /*FIL *rgpfil[MAX_FILAMENTS];*/
  /*int j = 0;*/
  /*for (j=0; j < MAX_FILAMENTS; j++)*/
  /*{*/
    /*rgpfil[j] = (FIL *) malloc(* sizeof(FIL));*/
  /*}*/

  pthread_t threads[MAX_FILAMENTS];



    rgptnInt[0] = NULL; 
    rgptnIntBoundary[0] = NULL;
    rgptnNB[0] = NULL;
    rgptnExtBoundary[0] = NULL;
    FIL *pfil = malloc(sizeof(FIL));
    pfil->pptnInt = &rgptnInt[0];
    pfil->pptnIntBoundary = &rgptnIntBoundary[0];
    pfil->pptnNB = &rgptnNB[0];
    pfil->pptnExtBoundary = &rgptnExtBoundary[0];
    pfil->Position = Position[1];

    rgptnInt[1] = NULL; 
    rgptnIntBoundary[1] = NULL;
    rgptnNB[1] = NULL;
    rgptnExtBoundary[1] = NULL;
    FIL *pfil1 = malloc(sizeof(FIL));
    pfil1->pptnInt = &rgptnInt[1];
    pfil1->pptnIntBoundary = &rgptnIntBoundary[1];
    pfil1->pptnNB = &rgptnNB[1];
    pfil1->pptnExtBoundary = &rgptnExtBoundary[1];
    pfil1->Position = Position[2];


    printf("FILAMENT %i\n", 0);
    pthread_create(&threads[0], NULL, CallCreateNarrowBand, (void*) pfil);
    printf("\n");
    printf("FILAMENT %i\n", 1);
    pthread_create(&threads[1], NULL, CallCreateNarrowBand, (void*) pfil1);
    printf("\n");

    pthread_join(threads[0], NULL);
    pthread_join(threads[1], NULL);

    rgptnInt[0] = *(pfil->pptnInt);
    rgptnIntBoundary[0] = *(pfil->pptnIntBoundary);
    rgptnNB[0] = *(pfil->pptnNB);
    rgptnExtBoundary[0] = *(pfil->pptnExtBoundary);

    rgptnInt[1] = *(pfil1->pptnInt);
    rgptnIntBoundary[1] = *(pfil1->pptnIntBoundary);
    rgptnNB[1] = *(pfil1->pptnNB);
    rgptnExtBoundary[1] = *(pfil1->pptnExtBoundary);



 int i = 0;
	/*for (i=1; i<=(int)Position[0][0].xcord; i++)*/
  /*{*/
    /*rgptnInt[i] = NULL; */
    /*rgptnIntBoundary[i] = NULL;*/
    /*rgptnNB[i] = NULL;*/
    /*rgptnExtBoundary[i] = NULL;*/
    /*FIL *pfil = malloc(sizeof(FIL));*/
    /*pfil->pptnInt = &rgptnInt[i];*/
    /*pfil->pptnIntBoundary = &rgptnIntBoundary[i];*/
    /*pfil->pptnNB = &rgptnNB[i];*/
    /*pfil->pptnExtBoundary = &rgptnExtBoundary[i];*/
    /*pfil->Position = Position[i];*/
    /*printf("FILAMENT %i\n", i);*/
    /*pthread_create(&threads[i], NULL, CallCreateNarrowBand, (void*) pfil);*/
    /*[>CallCreateNarrowBand(pfil);<]*/
    /*[>CreateNarrowBand(Position[i], &rgptnInt[i], &rgptnIntBoundary[i], &rgptnNB[i], &rgptnExtBoundary[i], i);<]*/
    /*printf("\n");*/
    /*[>SetRKStorage(rgptnIntBoundary[i], rgptnNB[i], rgptnExtBoundary[i]);<]*/
    /*[>SetRKStorage(rgpfil[i]);<]*/
    /*rgptnInt[i] = *(pfil->pptnInt);*/
    /*rgptnIntBoundary[i] = *(pfil->pptnIntBoundary);*/
    /*rgptnNB[i] = *(pfil->pptnNB);*/
    /*rgptnExtBoundary[i] = *(pfil->pptnExtBoundary);*/
/*  }*/


	//TraverseTree(ptnNB, &MakeAndSetTestVelocityField, NULL);

	int it = 0;
	int ditDumpEvery = (int) (dtDumpEvery/dtTimeStep);
	if (ditDumpEvery == 0) { ditDumpEvery = 1; }
	int ditThinEvery = (int) (dtThinEvery/dtTimeStep);
	if (ditThinEvery == 0) { ditThinEvery = 1; }
	double t = tMin;
	char szFilename[40];

	while(1)
		{
		printf("\n------------------------\n");
		printf("t = %.3f, iteration %i\n", t, it);
		printf("------------------------ \n");

		if (it%ditDumpEvery == 0)
			{
			printf("Write filament array to disk.\n");
			WriteFilamentsT(Position, t, "\t");
			printf("Write narrow band voxels in each tree to disk.\n");
			for (i=1; i<=(int)Position[0][0].xcord; i++)
				{
				printf("\t");
				sprintf(szFilename, "nb%i", i);
				WriteVoxelTreeWithVelocitiesT(rgptnNB[i], szFilename, t);
				}
			}

		if (t >= tMax)
			return;

		/*
		 * Update Step
		 */
		if (fHighOrderIntegration)
			{
			printf("\nGoing to perform 2nd order update on the narrow band(s):\n");
			FUpdateFront2ndOrder(rgptnNB, rgptnIntBoundary, rgptnExtBoundary, Position, t);  //this dumps the narrow band also
			}
		else
			{
			//fBoundaryIsHit = FUpdateFront1stOrder(ptnNB, Position);
			printf("first order integration obsolete.\n");
			exit(1);
			}

		t += dtTimeStep;
		it++;

		if (it%ditThinEvery == 0)
			{
			printf("*******************************************\n");

			//if (fBoundaryIsHit)
				//printf("The narrow band boundary was hit.\n");
			//else
			printf("Thinning narrow band.\n");

			for (i=1; i<=(int)Position[0][0].xcord; i++)
				{
				printf("Step 1. Save the narrow band voxels to file.\n");
				printf("\t");
				sprintf(szFilename, "nb-before-thinning%i", i); 
				WriteVoxelTreeT(rgptnNB[i], szFilename, t);
				}

			printf("Step 2. Thin the filament tube(s) to get new filament(s).\n");
			GetFilamentFromNB(rgptnNB, rgptnInt, Position, t);

			for (i=1; i<=(int)Position[0][0].xcord; i++)
				{
				FreeTreeAndVoxels(rgptnInt[i]);
				FreeTree(rgptnIntBoundary[i]);
				FreeTreeAndVoxels(rgptnNB[i]);
				FreeTreeAndVoxels(rgptnExtBoundary[i]);

				printf("Step 3. Create filament core(s) from new filament(s).\n");
        CreateNarrowBand(Position[i], &rgptnInt[i], &rgptnIntBoundary[i], &rgptnNB[i], &rgptnExtBoundary[i], i);
				/*CreateNarrowBand(rgpfil[i]);*/
        SetRKStorage(rgptnIntBoundary[i], rgptnNB[i], rgptnExtBoundary[i]);
				/*SetRKStorage(rgpfil[i]);*/
				}

			printf("******************************************* \n");
			}
		}
}
void 
ReadVoxelTree(TN **pptn, char *szFilename)
{
	FILE *pfh=fopen(szFilename, "r+");
	if (pfh == NULL)
		{
		printf("Couldn't open file %s\n", szFilename);
		exit(1);
		}

	int x,y,z;
	int ifn = 0;
	VX *pvxNew = NULL;
	double vl;
	while(1)  
		{ 
		if(fscanf(pfh,"%i %i %i %lf\n", &x, &y, &z, &vl)==EOF)
			break;
		ifn++;
		//printf("Inserting voxel (%i, %i, %i) into tree.\n", x,y,z);
		//pvxNew = PvxCreateVoxel(x,y,z,vl);
		pvxNew = PvxCreateVoxel(x,y,z,0.0);
		CreateLinksWithVoxelsInTree(*pptn, pvxNew);
		InsertVoxel(pptn, pvxNew);
		}
	printf("Number of voxels read in: %i\n", ifn);
	fclose(pfh);
}
void
TestObjectForHoles(TN *ptn, void *pv)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	int wy;
	//printf("Checking neighbours..\n");
	for (wy=0; wy<6; wy++)
		{
		if (PvxGetNeighbour(pvx, wy) == NULL) //if its a boundary voxel
			{
			assert(double_EQ(pvx->vl, 0.0)); 
			}
		}
}
void
GetMaxMinCoord(TN *ptn, void *pv)
{
	int **rgrgMaxMinCoord = (int **) pv;
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;

	if (XFromPvx(pvx) > rgrgMaxMinCoord[0][0])
		rgrgMaxMinCoord[0][0] = XFromPvx(pvx);
	if (XFromPvx(pvx) < rgrgMaxMinCoord[0][1])
		rgrgMaxMinCoord[0][1] = XFromPvx(pvx);

	if (YFromPvx(pvx) > rgrgMaxMinCoord[1][0])
		rgrgMaxMinCoord[1][0] = YFromPvx(pvx);
	if (YFromPvx(pvx) < rgrgMaxMinCoord[1][1])
		rgrgMaxMinCoord[1][1] = YFromPvx(pvx);

	if (ZFromPvx(pvx) > rgrgMaxMinCoord[2][0])
		rgrgMaxMinCoord[2][0] = ZFromPvx(pvx);
	if (ZFromPvx(pvx) < rgrgMaxMinCoord[2][1])
		rgrgMaxMinCoord[2][1] = ZFromPvx(pvx);
}
void
WriteVoxelToArray(TN *ptn, void *pv)
{
	float ***rg3 = (float ***) pv;
    /*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;
	int x = XFromPvx(pvx) + dxTrans;
	int y = YFromPvx(pvx) + dyTrans;
	int z = ZFromPvx(pvx) + dzTrans;
	//rg3[z + dz][y + dy][x + dx] = 1.0;
	rg3[z][y][x] = 1.0;
	//rg3[z - 107][x + 4][y + 4] = 1.0;
}
void WriteIMFile(FLOAT_TYPE ***Current_Height, int xRes, int yRes, int zRes) 
{
  char filename[] = "array.im"; 
  IM_TYPE *Height_Image;

  printf("\n\nSaving current Height function to file ...\n");

  Height_Image = im_create(filename, FLOAT, xRes, yRes, zRes);
  im_write(Height_Image, FLOAT, (char *) &(Current_Height[0][0][0]));
  printf("Finished writing Height function to file.\n");
}
/*float***
Rg3FromPtn(TN *ptn)
{
	int i;

	int **rgrgMaxMinCoord = (int **) malloc(sizeof(int *)*3);
	for(i = 0; i < 3; i++)
		{
		rgrgMaxMinCoord[i] = (int *) malloc(2 * sizeof(int));
		rgrgMaxMinCoord[i][0] = -LARGE;
		rgrgMaxMinCoord[i][1] = LARGE;
		}
	TraverseTree(ptn, &GetMaxMinCoord, rgrgMaxMinCoord);
	for (i = 0; i<3; i++)
		printf("max: %i min: %i\n", rgrgMaxMinCoord[i][0], rgrgMaxMinCoord[i][1]);
	int cvxXRange = rgrgMaxMinCoord[0][0] - rgrgMaxMinCoord[0][1] + 1;
	int cvxYRange = rgrgMaxMinCoord[1][0] - rgrgMaxMinCoord[1][1] + 1;
	int cvxZRange = rgrgMaxMinCoord[2][0] - rgrgMaxMinCoord[2][1] + 1;
	dx = - rgrgMaxMinCoord[0][1];
	dy = - rgrgMaxMinCoord[1][1];
	dz = - rgrgMaxMinCoord[2][1];
	printf("dx = %i, cvxXRange = %i\n", dx, cvxXRange);
	printf("dy = %i, cvxYRange = %i\n", dy, cvxYRange);
	printf("dz = %i, cvxZRange = %i\n", dz, cvxZRange);

	float ***rg3= (float ***) malloc(sizeof(float **)*cvxZRange);
	int j = 0;
	for(i = 0; i < cvxZRange; i++)
		{
		rg3[i] = (float **) malloc(sizeof(float*) * cvxYRange);
		for(j = 0; j < cvxYRange; j++)
			{
			rg3[i][j] = (float *) malloc(sizeof(float) * cvxXRange);
			assert(rg3[i][j] != NULL);
			}
		}
	int k = 0;
	for(i=0; i<cvxZRange; i++)
		for(j=0; j<cvxYRange; j++)
			for(k=0; k<cvxXRange; k++)
				rg3[i][j][k] = 0.0;

	TraverseTree(ptn, &WriteVoxelToArray, rg3);
	return rg3;
}*/
void
ReadVoxels(char *szFilename)
{
	FILE *pfh=fopen(szFilename, "r+");
	if (pfh == NULL)
		{
		printf("Couldn't open file %s\n", szFilename);
		exit(1);
		}

	int x,y,z;
	int ifn = 0;
	VX *pvxNew = NULL;
	double vl;

	int i;

	int **rgrgMaxMinCoord = (int **) malloc(sizeof(int *)*3);
	for(i = 0; i < 3; i++)
		{
		rgrgMaxMinCoord[i] = (int *) malloc(2 * sizeof(int));
		rgrgMaxMinCoord[i][0] = -LARGE;
		rgrgMaxMinCoord[i][1] = LARGE;
		}

	while(1)  
		{ 
		if(fscanf(pfh,"%i %i %i %lf\n", &x, &y, &z, &vl)==EOF)
			break;
		if (x > rgrgMaxMinCoord[0][0]) rgrgMaxMinCoord[0][0] = x;
		if (x < rgrgMaxMinCoord[0][1]) rgrgMaxMinCoord[0][1] = x;

		if (y > rgrgMaxMinCoord[1][0]) rgrgMaxMinCoord[1][0] = y;
		if (y < rgrgMaxMinCoord[1][1]) rgrgMaxMinCoord[1][1] = y;

		if (z > rgrgMaxMinCoord[2][0]) rgrgMaxMinCoord[2][0] = z;
		if (z < rgrgMaxMinCoord[2][1]) rgrgMaxMinCoord[2][1] = z;
		}
	for (i = 0; i<3; i++)
		printf("max: %i min: %i\n", rgrgMaxMinCoord[i][0], rgrgMaxMinCoord[i][1]);

	rewind(pfh);

	int cvxXRange = rgrgMaxMinCoord[0][0] - rgrgMaxMinCoord[0][1] + 1;
	int cvxYRange = rgrgMaxMinCoord[1][0] - rgrgMaxMinCoord[1][1] + 1;
	int cvxZRange = rgrgMaxMinCoord[2][0] - rgrgMaxMinCoord[2][1] + 1;
	int dxTrans = - rgrgMaxMinCoord[0][1];
	int dyTrans = - rgrgMaxMinCoord[1][1];
	int dzTrans = - rgrgMaxMinCoord[2][1];

	float ***rg3= (float ***) malloc(sizeof(float **) * cvxZRange);
	int j = 0;
	for(i = 0; i < cvxZRange; i++)
		{
		rg3[i] = (float **) malloc(sizeof(float*) * cvxYRange);
		for(j = 0; j < cvxYRange; j++)
			{
			rg3[i][j] = (float *) malloc(sizeof(float) * cvxXRange);
			assert(rg3[i][j] != NULL);
			}
		}
	int k = 0;
	for(i=0; i<cvxZRange; i++)
		for(j=0; j<cvxYRange; j++)
			for(k=0; k<cvxXRange; k++)
				rg3[i][j][k] = 0.0;

	while(1)  
		{ 
		if(fscanf(pfh,"%i %i %i %lf\n", &x, &y, &z, &vl)==EOF)
			break;
		rg3[z + dzTrans][y + dyTrans][x + dxTrans] = 1.0;
		}

	fclose(pfh);

	WriteIMFile((float***) rg3, cvxXRange, cvxYRange, cvxZRange);
}
void
WriteImFileFromVoxelTree2(TN *ptn)
{
	//WriteIMFile((float***) Rg3FromPtn(ptn), cvxYRange, cvxXRange, cvxZRange);
}
void
WriteImFileFromVoxelTree(TN *ptn)
{
	int i;

	int **rgrgMaxMinCoord = (int **) malloc(sizeof(int *)*3);
	for(i = 0; i < 3; i++)
		{
		rgrgMaxMinCoord[i] = (int *) malloc(2 * sizeof(int));
		rgrgMaxMinCoord[i][0] = -LARGE;
		rgrgMaxMinCoord[i][1] = LARGE;
		}
	TraverseTree(ptn, &GetMaxMinCoord, rgrgMaxMinCoord);
	for (i = 0; i<3; i++)
		printf("max: %i min: %i\n", rgrgMaxMinCoord[i][0], rgrgMaxMinCoord[i][1]);
	int cvxXRange = rgrgMaxMinCoord[0][0] - rgrgMaxMinCoord[0][1] + 1;
	int cvxYRange = rgrgMaxMinCoord[1][0] - rgrgMaxMinCoord[1][1] + 1;
	int cvxZRange = rgrgMaxMinCoord[2][0] - rgrgMaxMinCoord[2][1] + 1;
	//int cvxXRange = rgrgMaxMinCoord[0][0] + 5;
	//int cvxYRange = rgrgMaxMinCoord[1][0] + 5;
	//int cvxZRange = rgrgMaxMinCoord[2][0] + 5;
	dxTrans = - rgrgMaxMinCoord[0][1];
	dyTrans = - rgrgMaxMinCoord[1][1];
	dzTrans = - rgrgMaxMinCoord[2][1];
	printf("dx = %i, cvxXRange = %i\n", dxTrans, cvxXRange);
	printf("dy = %i, cvxYRange = %i\n", dyTrans, cvxYRange);
	printf("dz = %i, cvxZRange = %i\n", dzTrans, cvxZRange);

	float ***rg3= (float ***) malloc(sizeof(float **) * cvxZRange);
	int j = 0;
	for(i = 0; i < cvxZRange; i++)
		{
		rg3[i] = (float **) malloc(sizeof(float*) * cvxYRange);
		for(j = 0; j < cvxYRange; j++)
			{
			rg3[i][j] = (float *) malloc(sizeof(float) * cvxXRange);
			assert(rg3[i][j] != NULL);
			}
		}
	int k = 0;
	for(i=0; i<cvxZRange; i++)
		for(j=0; j<cvxYRange; j++)
			for(k=0; k<cvxXRange; k++)
				rg3[i][j][k] = 0.0;

	TraverseTree(ptn, &WriteVoxelToArray, rg3);

	FILE *pfh = fopen("slice.txt", "w+");
	for(i=0; i<cvxZRange; i++)
		for(j=0; j<cvxYRange; j++)
			for(k=0; k<cvxXRange; k++)
			fprintf(pfh, "%i %i %i %f\n", i, j, k, rg3[i][j][k]);

	WriteIMFile((float***) rg3, cvxXRange, cvxYRange, cvxZRange);
}
void
RunDebug(Coordinates **Position)
{
	GlobalPosition = Position;
	//rgr[0] = 0.0;
	//rgr[1] = 0.0;
	//rgr[2] = 0.0;
	//rgr[3] = 0.0;
	double t = 0.370;
	TN *ptnSS = NULL;
	//ReadVoxelTree(&ptnSS, "ptnSS-0350.raw");
	ReadVoxels("ptnSS-0350.raw");

	//ReadVoxelTree(&ptnSS, "skeleton-before-erase-0370.raw");
	//int ***rgrgrg = 

	//fibheap_t fh = fibheap_new();
	//TraverseTree(ptnSS, &InsertBoundaryVoxelIntoHeap, fh);
	//Fmm_NotCreateVoxels(fh, LARGE);
	//TraverseTree(ptnSS, &InvertValues, NULL);
	
	//WriteImFileFromVoxelTree(ptnSS);
	exit(0);

	//thin(&ptnSS); 

	printf(" 	f. Write the thinned voxel skeleton to file.\n");
	printf("\t\t");
	WriteVoxelTreeT(ptnSS, "ptnSSt", 350);
	exit(0);

	printf(" 	g. Find and delete merged voxels if any, and link branches together.\n");
	TraverseTree(ptnSS, &Set26VoxelNeighbours, ptnSS);
	ptnRoot = ptnSS;
	Merge(&ptnSS);
	exit(0);
//
	//GlobalPosition = Position;
	//rgr[0] = 0.0;
	//rgr[1] = 0.0;
	//rgr[2] = 0.0;
	//rgr[3] = 0.0;
//
	//printf("\n--------------\n");
	//printf("Initialization\n");
	//printf("--------------\n");
//
//
	//TN *rgptnInt[MAX_FILAMENTS]; 
	//TN *rgptnIntBoundary[MAX_FILAMENTS];
	//TN *rgptnNB[MAX_FILAMENTS];
	//TN *rgptnExtBoundary[MAX_FILAMENTS];
//
	//char szFilename[40];
	//int i = 0;
	//double t = 0;
	//for (i=1; i<=1; i++)
	//for (i=1; i<=(int)Position[0][0].xcord; i++)
		//{
		//rgptnInt[i] = NULL; 
		//rgptnIntBoundary[i] = NULL;
		//rgptnNB[i] = NULL;
		//rgptnExtBoundary[i] = NULL;
		//printf("FILAMENT %i\n", i);
		//CreateNarrowBand(Position[i], &rgptnInt[i], &rgptnIntBoundary[i], &rgptnNB[i], &rgptnExtBoundary[i], i);
		//printf("\n");
		//SetRKStorage(rgptnIntBoundary[i], rgptnNB[i], rgptnExtBoundary[i]);
		//}
//
	//printf("Thinning narrow band.\n");
//
	//printf("Step 2. Thin the filament tube(s) to get new filament(s).\n");
	//GetFilamentFromNB(rgptnNB, rgptnInt, Position, t);
//
	//for (i=1; i<=(int)Position[0][0].xcord; i++)
		//{
		//FreeTreeAndVoxels(rgptnInt[i]);
		//FreeTree(rgptnIntBoundary[i]);
		//FreeTreeAndVoxels(rgptnNB[i]);
		//FreeTreeAndVoxels(rgptnExtBoundary[i]);
		//}
	//exit(0);

	//SaveVoxelsToDisk(ptnNB, 0, "nb");
	//SaveVoxelsToDisk(ptnInt, 0, "int");
	//SaveVoxelsToDisk(ptnIntBoundary, 0, "intb");
	//SaveVoxelsToDisk(ptnExtBoundary, 0, "extb");
	//exit(0);
	//GetFilamentFromNB(ptnNB, ptnInt, Position);

	//TN *ptnHeap = PtnVoxelsFromFilaments(Position);
	//thin(&ptnHeap);
//
	//fibheap_t fh = fibheap_new();
	//TraverseTree(ptnHeap, &InsertBoundaryVoxelIntoHeap, fh);
//
	//TN *ptnInt = NULL;
	//Fmm_CreateVoxels(&ptnInt, &ptnHeap, fh, lCoreRadius*cvxResolution - lNarrowBandRadius);
//
	//TN *ptnNB = NULL;
	//Fmm_CreateVoxels(&ptnNB, &ptnHeap, fh, lCoreRadius*cvxResolution + lNarrowBandRadius);
//
	//TN *ptnExtBoundary = ptnHeap;
//
	//TraverseTree(ptnNB, &CalcExactDistancesInNB, Position);
	//TraverseTree(ptnExtBoundary, &CalcExactDistancesAtBoundary, Position);
	//TN *ptnIntBoundary = NULL;
	//TraverseTree(ptnNB, &DoCheckAndGetIntBoundary, &ptnIntBoundary);
	//TraverseTree(ptnIntBoundary, &CalcExactDistancesAtBoundary, Position);
	//SaveVoxelsToDisk(ptnNB, 0, "nb");
	//SaveVoxelsToDisk(ptnIntBoundary, 0, "intb");
	//SaveVoxelsToDisk(ptnExtBoundary, 0, "extb");



	//SaveVoxelsToDisk(ptnHeap, 0, "ext");

	//TN *ptn = NULL;
	//VX *pvx = PvxCreateVoxel(1,2,3,4);
	//InsertVoxel(&ptn, pvx);
	//InsertVoxel(&ptn, PvxCreateVoxel(2,2,3,4));
	//InsertVoxel(&ptn, PvxCreateVoxel(3,2,3,4));
	//PvxRemoveVoxel(&ptn, pvx); 
	//PrintVoxelTree(ptn);


	//VX **rgpvx = (VX **) malloc(sizeof(VX *)*100);
	//TN *ptnTest = NULL;
	//int i,j,k;
	//int l;
	//int ipvx = 0;
	//int ipvxMax = 0;
	//VX *pvx = NULL;
//
	//for(i=0; i<10; i++)
		//for(j=0; j<10; j++)
			//for(k=0; k<1; k++)
				//{
				//pvx = PvxCreateVoxel(i,j,k,5);
				//InsertVoxel(&ptnTest, pvx);
				//rgpvx[ipvxMax] = pvx;
				//ipvxMax++;
				//}
	//PrintVoxelTree(ptnTest);
	//for(ipvx = 0; ipvx<ipvxMax; ipvx++)
		//{
		//pvx = rgpvx[ipvx];
		//PvxRemoveVoxel(&ptnTest, XFromPvx(pvx), YFromPvx(pvx), ZFromPvx(pvx));
		//}
	//PrintVoxelTree(ptnTest);
//
	//exit(0);
	//TN *ptnInt = NULL;
	//TN *ptnIntBoundary = NULL;
	//TN *ptnNB = NULL;
	//TN *ptnExtBoundary = NULL;
	//CreateNarrowBand(Position, &ptnInt, &ptnIntBoundary, &ptnNB, &ptnExtBoundary);
//

	//TN *ptn = PtnCreateNarrowBand(Position);
	//SaveVoxelsToDisk(ptn, 0, "dt");
	//TraverseTree(ptn, &BoundaryVoxelTest, ptn);
	//TraverseTree(ptn, &TestTreeContainment, ptn);
	//SaveVoxelsToDisk(ptn, 0, "test");

	/* gradient test */
	//TN *ptn = PtnCreateNarrowBand(Position);
	//TraverseTree(ptn, &MakeRungeKuttaStorage, NULL);
	//TraverseTree(ptn, &SetTrialStep, NULL);
	//TraverseTree(ptn, &CalculateGradient, NULL);
	//TraverseTree(ptn, &SetGradient, NULL);
}


/*
{
*/
//	printf("Smoothing narrow band...\n");
//	double sigma = 2.0;
//	double range = 1;
//	GaussianSmooth(ptn, sigma, range);
//	printf("Smoothing narrow band...done\n");

//VX **rgpvx = (VX **) MALLOC(sizeof(VX *)*X_Res * Y_Res * Z_Res);
//	VX **rgpvx;
//	MALLOC(rgpvx,X_Res/10 * Y_Res/10 * Z_Res,VX *);
//	TN *ptnTest = NULL;
//	int i,j,k;
//	int l;
//	int ipvx = 0;
//	int ipvxMax = 0;
//	VX *pvx = NULL;
//	char * dummy;
//
//	for(i=1; i<X_Res/10; i++)
//		for(j=1; j<Y_Res/10; j++)
//			for(k=1; k<Z_Res; k++)
//				{
//				//printf("Creating voxel \n");
//				pvx = PvxCreateVoxel(i,j,k,5);
//				//MALLOC(dummy, 48, char);
//				//pvx=(VX *)dummy;
//				//free(pvx);
//				InsertVoxel(&ptnTest, pvx);
//				rgpvx[ipvxMax] = pvx;
//				ipvxMax++;
//				}
//	scanf("%d", &l);
//	printf("looping thru array and freeing pointers...\n");
//	//PrintVoxelTree(ptnTest);
//	TraverseTreePostOrder(ptnTest, &DeleteVoxe, NULL);
//	ptnTest = NULL;
////	for(ipvx = 0; ipvx<ipvxMax; ipvx++)
////		{
////		//pvx= rgpvx[ipvx];
////		//printf("Deleting x=%i,y=%i,z=%i\n",XFromPvx(pvx),YFromPvx(pvx),ZFromPvx(pvx));
////		FREE(rgpvx[ipvx]);
////		}
//	scanf("%d", &l);
//	for(i=1; i<X_Res/10; i++)
//		for(j=1; j<Y_Res/10; j++)
//			for(k=1; k<Z_Res; k++)
//				{
//				//printf("Creating voxel \n");
//				pvx = PvxCreateVoxel(i,j,k,5);
//				//MALLOC(dummy, 48, char);
//				//pvx=(VX *)dummy;
//				//free(pvx);
//				InsertVoxel(&ptnTest, pvx);
//				//rgpvx[ipvxMax] = pvx;
//				}
//	//printf("destroying tree...\n");
//	//TraverseTree(ptnTest, &DeleteVoxe, NULL);
//	//DestroyTree(ptnTest);
//	FREE(rgpvx);
//	printf("done.\n");
//	
//	scanf("%d", &l);
//	exit(0);
//	ptnTest = NULL;
//	TN *ptnTest2 = NULL;
//	for(i=1; i<X_Res/10; i++)
//		for(j=1; j<Y_Res/10; j++)
//			for(k=1; k<Z_Res; k++)
//				{
//				printf("here2\n");
//				InsertVoxel(&ptnTest2, PvxCreateVoxel(i, j,k,5));
//				}
//	printf("destroying tree...\n");
//TraverseTreePostOrder(ptnTest, &DeleteVoxe, NULL);
//DestroyTree(ptnTest2);
//exit(0);

//	PrintVoxelTree(ptnTest);
//	TraverseTreePostOrder(ptnTest, &DeleteNodeAndVoxel, NULL);
//	free(ptnTest);
//	VX *pvx= PvxFromPtn(ptnTest);
//	printf("x=%i,y=%i,z=%i\n",XFromPvx(pvx),YFromPvx(pvx),ZFromPvx(pvx));
//
	//free(ptnTest);
	//PrintVoxelTree(ptnTest);
	//exit(0);
			//PrintVoxelTree(ptn);
	//ptnGlobal = ptn;
	//double vl = 4;
	//TraverseTree(ptn, &NeighbourhoodTest, &vl);
	//TraverseTree(ptn, &NeighbourTest, &vl);
	//TraverseTree(ptn, &FindTest, &vl);
	//SaveVoxelsToDisk(ptn, 0);
	//ptn = thin(ptn);
	//DestroyVoxelTree(&ptn);
	//PrintVoxelTree(ptn);
	//exit(0);
	//TraverseTree(ptn, &NeighbourTest, NULL);
	//TraverseTreePostOrder(&ptn, &DeleteNodeAndVoxel, NULL);


//void
//Convolve1D(TN *ptn, void *pvKernel)
//{
//	GK *pgk = (GK *) pvKernel;
//	if (pgk == NULL)
//		{
//		printf("Kernel is NULL\n");
//		return;
//		}
//	if (ptn == NULL)
//		{
//		printf("Error: NULL voxel tree passed to Convolve1D.\n");
//		exit(1);
//		}
//
//	double vlConvolveSum = 0;
//	double *rgvl = NULL;
//	int wy = pgk->wy;
//	VX *pvx = PvxFromPtn(ptn);
//	VX *pvxToUpdate = NULL;
//	int ivl = 0;
//	int index = 0;
//	int x = XFromPvx(pvx);
//	int y = YFromPvx(pvx);
//	int z = ZFromPvx(pvx);
//
//	for (ivl = 0; ivl < 2*pgk->offset+1; ivl++)
//		{
//		index = (int)(ivl - pgk->offset);
//		if (wy == wyXPlus1)
//			pvx = PvxFindVoxel(ptnGlobal, x+index, y, z);
//		if (wy == wyYPlus1)
//			pvx = PvxFindVoxel(ptnGlobal, x, y+index, z);
//		if (wy == wyZPlus1)
//			pvx = PvxFindVoxel(ptnGlobal, x, y, z+index);
//
//		if (pvx == NULL)
//			return;
//		rgvl = (double *) PvGetVoxelData(pvx);
//		if (rgvl == NULL)
//			{
//			printf("No temp storage at voxel needed by Convolve1D.\n");
//			printf("x=%i, y=%i, z=%i, ivl=%i, offset=%i\n", XFromPvx(pvx),YFromPvx(pvx),ZFromPvx(pvx),ivl,pgk->offset);
//			exit(1);	
//			}
//		vlConvolveSum += (pgk->rgvlKernel[ivl] * rgvl[pgk->ivlVoxelValueToUse]);
//		}
//
//	rgvl = (double *) PvGetVoxelData(PvxFromPtn(ptn));
//	rgvl[pgk->ivlVoxelValueToSet] = vlConvolveSum/(pgk->vlKernelSum);
//}
/*
}
*/



