/*---------------------------------------------------------------------------*/
/* Program:  IM_convert.c                                                    */
/*                                                                           */
/* Purpose:  This file contains the image type conversion routines           */
/*           for a new image format called "KUIM".  This image format        */
/*           supports 1D, 2D and 3D images of a variety of data types.       */
/*           The constant and type declarations are in IM.h.                 */
/*                                                                           */
/* Author:   John Gauch                                                      */
/*                                                                           */
/* Date:     March 16, 1994                                                  */
/*                                                                           */
/* Note:     Copyright (C) The University of Kansas, 1994                    */
/*---------------------------------------------------------------------------*/
#include "IM.h"

#define NO_SCALE   1
#define MAX_BYTE    255.0
#define MAX_SHORT    32767.0
#define MAX_INT    32767.0
#define MAX_FLOAT    32767.0
#define MAX_DOUBLE    32767.0
#define ROUND(f) ((f>0) ? (int)(f+0.5) : (int)(f-0.5))

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts SHORT to BYTE.                            */
/*---------------------------------------------------------------------------*/
void Short2Byte(Count, In, Out)
     int Count;
     SHORT_TYPE *In;
     BYTE_TYPE *Out;
{
   int i;
   SHORT_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);
#ifdef NO_SCALE
   if (Scale > 1.0)
      Scale = 1.0;
#endif

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (BYTE_TYPE) ROUND((In[i] - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts INT to BYTE.                              */
/*---------------------------------------------------------------------------*/
void Int2Byte(Count, In, Out)
     int Count;
     INT_TYPE *In;
     BYTE_TYPE *Out;
{
   int i;
   INT_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);
#ifdef NO_SCALE
   if (Scale > 1.0)
      Scale = 1.0;
#endif

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (BYTE_TYPE) ROUND((In[i] - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts FLOAT to BYTE.                            */
/*---------------------------------------------------------------------------*/
void Float2Byte(Count, In, Out)
     int Count;
     FLOAT_TYPE *In;
     BYTE_TYPE *Out;
{
   int i;
   FLOAT_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);
#ifdef NO_SCALE
   if (Scale > 1.0)
      Scale = 1.0;
#endif

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (BYTE_TYPE) ROUND((In[i] - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts DOUBLE to BYTE.                           */
/*---------------------------------------------------------------------------*/
void Double2Byte(Count, In, Out)
     int Count;
     DOUBLE_TYPE *In;
     BYTE_TYPE *Out;
{
   int i;
   DOUBLE_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);
#ifdef NO_SCALE
   if (Scale > 1.0)
      Scale = 1.0;
#endif

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (BYTE_TYPE) ROUND((In[i] - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COMPLEX to BYTE (using only real part).   */
/*---------------------------------------------------------------------------*/
void Complex2Byte(Count, In, Out)
     int Count;
     COMPLEX_TYPE *In;
     BYTE_TYPE *Out;
{
   int i;
   float Min, Max;
   float Temp;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0].re;
   for (i = 0; i < Count; i++)
   {
      Temp = In[i].re;
      if (Min > Temp)
	 Min = Temp;
      else if (Max < Temp)
	 Max = Temp;
   }

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);
#ifdef NO_SCALE
   if (Scale > 1.0)
      Scale = 1.0;
#endif

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (BYTE_TYPE) ROUND((In[i].re - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COLOR to BYTE (using R+G+B).              */
/*---------------------------------------------------------------------------*/
void Color2Byte(Count, In, Out)
     int Count;
     COLOR_TYPE *In;
     BYTE_TYPE *Out;
{
   int i;
   int Min, Max;
   int Temp;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = (In[0].r + In[0].g + In[0].b);
   for (i = 0; i < Count; i++)
   {
      Temp = In[i].r + In[i].g + In[i].b;
      if (Min > Temp)
	 Min = Temp;
      else if (Max < Temp)
	 Max = Temp;
   }

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);
#ifdef NO_SCALE
   if (Scale > 1.0)
      Scale = 1.0;
#endif

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
   {
      Temp = In[i].r + In[i].g + In[i].b;
      Out[i] = (BYTE_TYPE) ROUND((Temp - Min) * Scale);
   }
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts PSEUDO to BYTE (using R+G+B).             */
/*---------------------------------------------------------------------------*/
void Pseudo2Byte(Count, In, Out, R, G, B)
     int Count;
     PSEUDO_TYPE *In;
     BYTE_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i;
   int Min, Max;
   int Temp;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = (R[In[0]] + G[In[0]] + B[In[0]]);
   for (i = 0; i < Count; i++)
   {
      Temp = R[In[i]] + G[In[i]] + B[In[i]];
      if (Min > Temp)
	 Min = Temp;
      else if (Max < Temp)
	 Max = Temp;
   }

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);
#ifdef NO_SCALE
   if (Scale > 1.0)
      Scale = 1.0;
#endif

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
   {
      Temp = R[In[i]] + G[In[i]] + B[In[i]];
      Out[i] = (BYTE_TYPE) ROUND((Temp - Min) * Scale);
   }
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts BYTE to SHORT.                            */
/*---------------------------------------------------------------------------*/
void Byte2Short(Count, In, Out)
     int Count;
     BYTE_TYPE *In;
     SHORT_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (SHORT_TYPE) In[i];
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts INT to SHORT.                             */
/*---------------------------------------------------------------------------*/
void Int2Short(Count, In, Out)
     int Count;
     INT_TYPE *In;
     SHORT_TYPE *Out;
{
   int i;
   INT_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_SHORT / (Max - Min);
   if (Scale > 1.0)
   {
      Scale = 1.0;
      Min = 0;
   }

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (SHORT_TYPE) ROUND(In[i] * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts FLOAT to SHORT.                           */
/*---------------------------------------------------------------------------*/
void Float2Short(Count, In, Out)
     int Count;
     FLOAT_TYPE *In;
     SHORT_TYPE *Out;
{
   int i;
   FLOAT_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_SHORT / (Max - Min);
   if (Scale > 1.0)
   {
      Scale = 1.0;
      Min = 0;
   }

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (SHORT_TYPE) ROUND(In[i] * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts DOUBLE to SHORT.                          */
/*---------------------------------------------------------------------------*/
void Double2Short(Count, In, Out)
     int Count;
     DOUBLE_TYPE *In;
     SHORT_TYPE *Out;
{
   int i;
   DOUBLE_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_SHORT / (Max - Min);
   if (Scale > 1.0)
   {
      Scale = 1.0;
      Min = 0;
   }

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (SHORT_TYPE) ROUND(In[i] * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COMPLEX to SHORT (using only real part).  */
/*---------------------------------------------------------------------------*/
void Complex2Short(Count, In, Out)
     int Count;
     COMPLEX_TYPE *In;
     SHORT_TYPE *Out;
{
   int i;
   float Min, Max;
   float Temp;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0].re;
   for (i = 0; i < Count; i++)
   {
      Temp = In[i].re;
      if (Min > Temp)
	 Min = Temp;
      else if (Max < Temp)
	 Max = Temp;
   }

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_SHORT / (Max - Min);
   if (Scale > 1.0)
   {
      Scale = 1.0;
      Min = 0;
   }

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (SHORT_TYPE) ROUND(In[i].re * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COLOR to SHORT (using R+G+B).             */
/*---------------------------------------------------------------------------*/
void Color2Short(Count, In, Out)
     int Count;
     COLOR_TYPE *In;
     SHORT_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (SHORT_TYPE) (In[i].r + In[i].g + In[i].b);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts PSEUDO to SHORT (using R+G+B).            */
/*---------------------------------------------------------------------------*/
void Pseudo2Short(Count, In, Out, R, G, B)
     int Count;
     PSEUDO_TYPE *In;
     SHORT_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (SHORT_TYPE) (R[In[i]] + G[In[i]] + B[In[i]]);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts BYTE to INT.                              */
/*---------------------------------------------------------------------------*/
void Byte2Int(Count, In, Out)
     int Count;
     BYTE_TYPE *In;
     INT_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (INT_TYPE) (In[i]);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts SHORT to INT.                             */
/*---------------------------------------------------------------------------*/
void Short2Int(Count, In, Out)
     int Count;
     SHORT_TYPE *In;
     INT_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (INT_TYPE) In[i];
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts FLOAT to INT.                             */
/*---------------------------------------------------------------------------*/
void Float2Int(Count, In, Out)
     int Count;
     FLOAT_TYPE *In;
     INT_TYPE *Out;
{
   int i;
   FLOAT_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_INT / (Max - Min);
   if (Scale > 1.0)
   {
      Scale = 1.0;
      Min = 0;
   }

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (INT_TYPE) ROUND(In[i] * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts DOUBLE to INT.                            */
/*---------------------------------------------------------------------------*/
void Double2Int(Count, In, Out)
     int Count;
     DOUBLE_TYPE *In;
     INT_TYPE *Out;
{
   int i;
   DOUBLE_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_INT / (Max - Min);
   if (Scale > 1.0)
   {
      Scale = 1.0;
      Min = 0;
   }

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (INT_TYPE) ROUND(In[i] * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COMPLEX to INT (using only real part).    */
/*---------------------------------------------------------------------------*/
void Complex2Int(Count, In, Out)
     int Count;
     COMPLEX_TYPE *In;
     INT_TYPE *Out;
{
   int i;
   float Min, Max;
   float Temp;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0].re;
   for (i = 0; i < Count; i++)
   {
      Temp = In[i].re;
      if (Min > Temp)
	 Min = Temp;
      else if (Max < Temp)
	 Max = Temp;
   }

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_INT / (Max - Min);
   if (Scale > 1.0)
   {
      Scale = 1.0;
      Min = 0;
   }

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (INT_TYPE) ROUND(In[i].re * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COLOR to INT (using R+G+B).               */
/*---------------------------------------------------------------------------*/
void Color2Int(Count, In, Out)
     int Count;
     COLOR_TYPE *In;
     INT_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (INT_TYPE) (In[i].r + In[i].g + In[i].b);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts PSEUDO to INT (using R+G+B).              */
/*---------------------------------------------------------------------------*/
void Pseudo2Int(Count, In, Out, R, G, B)
     int Count;
     PSEUDO_TYPE *In;
     INT_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (INT_TYPE) (R[In[i]] + G[In[i]] + B[In[i]]);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts BYTE to FLOAT.                            */
/*---------------------------------------------------------------------------*/
void Byte2Float(Count, In, Out)
     int Count;
     BYTE_TYPE *In;
     FLOAT_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (FLOAT_TYPE) In[i];
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts SHORT to FLOAT.                           */
/*---------------------------------------------------------------------------*/
void Short2Float(Count, In, Out)
     int Count;
     SHORT_TYPE *In;
     FLOAT_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (FLOAT_TYPE) (In[i]);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts INT to FLOAT.                             */
/*---------------------------------------------------------------------------*/
void Int2Float(Count, In, Out)
     int Count;
     INT_TYPE *In;
     FLOAT_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (FLOAT_TYPE) In[i];
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts DOUBLE to FLOAT.                          */
/*---------------------------------------------------------------------------*/
void Double2Float(Count, In, Out)
     int Count;
     DOUBLE_TYPE *In;
     FLOAT_TYPE *Out;
{
   int i;
   DOUBLE_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_FLOAT / (Max - Min);
   if (Scale > 1.0)
   {
      Scale = 1.0;
      Min = 0;
   }

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (FLOAT_TYPE) ((In[i] - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COMPLEX to FLOAT (using only real part).  */
/*---------------------------------------------------------------------------*/
void Complex2Float(Count, In, Out)
     int Count;
     COMPLEX_TYPE *In;
     FLOAT_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (FLOAT_TYPE) In[i].re;
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COLOR to FLOAT (using R+G+B).             */
/*---------------------------------------------------------------------------*/
void Color2Float(Count, In, Out)
     int Count;
     COLOR_TYPE *In;
     FLOAT_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (FLOAT_TYPE) (In[i].r + In[i].g + In[i].b);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts PSEUDO to FLOAT (using R+G+B).             */
/*---------------------------------------------------------------------------*/
void Pseudo2Float(Count, In, Out, R, G, B)
     int Count;
     PSEUDO_TYPE *In;
     FLOAT_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (FLOAT_TYPE) (R[In[i]] + G[In[i]] + B[In[i]]);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts BYTE to DOUBLE.                           */
/*---------------------------------------------------------------------------*/
void Byte2Double(Count, In, Out)
     int Count;
     BYTE_TYPE *In;
     DOUBLE_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (DOUBLE_TYPE) In[i];
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts SHORT to DOUBLE.                          */
/*---------------------------------------------------------------------------*/
void Short2Double(Count, In, Out)
     int Count;
     SHORT_TYPE *In;
     DOUBLE_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (DOUBLE_TYPE) In[i];
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts INT to DOUBLE.                            */
/*---------------------------------------------------------------------------*/
void Int2Double(Count, In, Out)
     int Count;
     INT_TYPE *In;
     DOUBLE_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (DOUBLE_TYPE) In[i];
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts FLOAT to DOUBLE.                          */
/*---------------------------------------------------------------------------*/
void Float2Double(Count, In, Out)
     int Count;
     FLOAT_TYPE *In;
     DOUBLE_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (DOUBLE_TYPE) In[i];
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COMPLEX to DOUBLE (using only real part). */
/*---------------------------------------------------------------------------*/
void Complex2Double(Count, In, Out)
     int Count;
     COMPLEX_TYPE *In;
     DOUBLE_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (DOUBLE_TYPE) In[i].re;
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COLOR to DOUBLE (using R+G+B).            */
/*---------------------------------------------------------------------------*/
void Color2Double(Count, In, Out)
     int Count;
     COLOR_TYPE *In;
     DOUBLE_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (DOUBLE_TYPE) (In[i].r + In[i].g + In[i].b);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts PSEUDO to DOUBLE (using R+G+B).           */
/*---------------------------------------------------------------------------*/
void Pseudo2Double(Count, In, Out, R, G, B)
     int Count;
     PSEUDO_TYPE *In;
     DOUBLE_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (DOUBLE_TYPE) (R[In[i]] + G[In[i]] + B[In[i]]);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts BYTE to COMPLEX.                          */
/*---------------------------------------------------------------------------*/
void Byte2Complex(Count, In, Out)
     int Count;
     BYTE_TYPE *In;
     COMPLEX_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
   {
      Out[i].re = (float) In[i];
      Out[i].im = 0.0;
   }
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts SHORT to COMPLEX.                         */
/*---------------------------------------------------------------------------*/
void Short2Complex(Count, In, Out)
     int Count;
     SHORT_TYPE *In;
     COMPLEX_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
   {
      Out[i].re = (float) In[i];
      Out[i].im = 0.0;
   }
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts INT to COMPLEX.                           */
/*---------------------------------------------------------------------------*/
void Int2Complex(Count, In, Out)
     int Count;
     INT_TYPE *In;
     COMPLEX_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
   {
      Out[i].re = (float) In[i];
      Out[i].im = 0.0;
   }
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts FLOAT to COMPLEX.                         */
/*---------------------------------------------------------------------------*/
void Float2Complex(Count, In, Out)
     int Count;
     FLOAT_TYPE *In;
     COMPLEX_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
   {
      Out[i].re = (float) In[i];
      Out[i].im = 0.0;
   }
}
/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts DOUBLE to COMPLEX.                        */
/*---------------------------------------------------------------------------*/
void Double2Complex(Count, In, Out)
     int Count;
     DOUBLE_TYPE *In;
     COMPLEX_TYPE *Out;
{
   int i;
   DOUBLE_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_FLOAT / (Max - Min);
   if (Scale > 1.0)
   {
      Scale = 1.0;
      Min = 0;
   }

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
   {
      Out[i].re = (float) ((In[i] - Min) * Scale);
      Out[i].im = 0.0;
   }
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COLOR to COMPLEX (using R+G+B).           */
/*---------------------------------------------------------------------------*/
void Color2Complex(Count, In, Out)
     int Count;
     COLOR_TYPE *In;
     COMPLEX_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
   {
      Out[i].re = (float) (In[i].r + In[i].g + In[i].b);
      Out[i].im = 0.0;
   }
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts PSEUDO to COMPLEX (using R+G+B).          */
/*---------------------------------------------------------------------------*/
void Pseudo2Complex(Count, In, Out, R, G, B)
     int Count;
     PSEUDO_TYPE *In;
     COMPLEX_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
   {
      Out[i].re = (float) (R[In[i]] + G[In[i]] + B[In[i]]);
      Out[i].im = 0.0;
   }
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts BYTE to COLOR.                            */
/*---------------------------------------------------------------------------*/
void Byte2Color(Count, In, Out)
     int Count;
     BYTE_TYPE *In;
     COLOR_TYPE *Out;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i].r = Out[i].g = Out[i].b = In[i];
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts SHORT to COLOR.                           */
/*---------------------------------------------------------------------------*/
void Short2Color(Count, In, Out)
     int Count;
     SHORT_TYPE *In;
     COLOR_TYPE *Out;
{
   int i;
   SHORT_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i].r = Out[i].g = Out[i].b =
	 (unsigned char) ROUND((In[i] - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts INT to COLOR.                             */
/*---------------------------------------------------------------------------*/
void Int2Color(Count, In, Out)
     int Count;
     INT_TYPE *In;
     COLOR_TYPE *Out;
{
   int i;
   INT_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i].r = Out[i].g = Out[i].b =
	 (unsigned char) ROUND((In[i] - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts FLOAT to COLOR.                           */
/*---------------------------------------------------------------------------*/
void Float2Color(Count, In, Out)
     int Count;
     FLOAT_TYPE *In;
     COLOR_TYPE *Out;
{
   int i;
   FLOAT_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i].r = Out[i].g = Out[i].b =
	 (unsigned char) ROUND((In[i] - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts DOUBLE to COLOR.                          */
/*---------------------------------------------------------------------------*/
void Double2Color(Count, In, Out)
     int Count;
     DOUBLE_TYPE *In;
     COLOR_TYPE *Out;
{
   int i;
   DOUBLE_TYPE Min, Max;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i].r = Out[i].g = Out[i].b =
	 (unsigned char) ROUND((In[i] - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COMPLEX to COLOR (using only real part).  */
/*---------------------------------------------------------------------------*/
void Complex2Color(Count, In, Out)
     int Count;
     COMPLEX_TYPE *In;
     COLOR_TYPE *Out;
{
   int i;
   float Min, Max;
   float Temp;
   float Scale = 1;

   /* Find Min and Max pixel value */
   Min = Max = In[0].re;
   for (i = 0; i < Count; i++)
   {
      Temp = In[i].re;
      if (Min > Temp)
	 Min = Temp;
      else if (Max < Temp)
	 Max = Temp;
   }

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i].r = Out[i].g = Out[i].b =
	 (unsigned char) ROUND((In[i].re - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts PSEUDO to COLOR.                          */
/*---------------------------------------------------------------------------*/
void Pseudo2Color(Count, In, Out, R, G, B)
     int Count;
     PSEUDO_TYPE *In;
     COLOR_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
   {
      Out[i].r = R[In[i]];
      Out[i].g = G[In[i]];
      Out[i].b = B[In[i]];
   }
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts BYTE to PSEUDO.                           */
/*---------------------------------------------------------------------------*/
void Byte2Pseudo(Count, In, Out, R, G, B)
     int Count;
     BYTE_TYPE *In;
     PSEUDO_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i;

   /* Create color map */
   for (i = 0; i < 256; i++)
      R[i] = G[i] = B[i] = i;

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = In[i];
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts SHORT to PSEUDO.                          */
/*---------------------------------------------------------------------------*/
void Short2Pseudo(Count, In, Out, R, G, B)
     int Count;
     SHORT_TYPE *In;
     PSEUDO_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i;
   SHORT_TYPE Min, Max;
   float Scale = 1;

   /* Create color map */
   for (i = 0; i < 256; i++)
      R[i] = G[i] = B[i] = i;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (BYTE_TYPE) ROUND((In[i] - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts INT to PSEUDO.                            */
/*---------------------------------------------------------------------------*/
void Int2Pseudo(Count, In, Out, R, G, B)
     int Count;
     INT_TYPE *In;
     PSEUDO_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i;
   INT_TYPE Min, Max;
   float Scale = 1;

   /* Create color map */
   for (i = 0; i < 256; i++)
      R[i] = G[i] = B[i] = i;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (BYTE_TYPE) ROUND((In[i] - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts FLOAT to PSEUDO.                          */
/*---------------------------------------------------------------------------*/
void Float2Pseudo(Count, In, Out, R, G, B)
     int Count;
     FLOAT_TYPE *In;
     PSEUDO_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i;
   FLOAT_TYPE Min, Max;
   float Scale = 1;

   /* Create color map */
   for (i = 0; i < 256; i++)
      R[i] = G[i] = B[i] = i;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (BYTE_TYPE) ROUND((In[i] - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts DOUBLE to PSEUDO.                         */
/*---------------------------------------------------------------------------*/
void Double2Pseudo(Count, In, Out, R, G, B)
     int Count;
     DOUBLE_TYPE *In;
     PSEUDO_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i;
   DOUBLE_TYPE Min, Max;
   float Scale = 1;

   /* Create color map */
   for (i = 0; i < 256; i++)
      R[i] = G[i] = B[i] = i;

   /* Find Min and Max pixel value */
   Min = Max = In[0];
   for (i = 0; i < Count; i++)
      if (Min > In[i])
	 Min = In[i];
      else if (Max < In[i])
	 Max = In[i];

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (BYTE_TYPE) ROUND((In[i] - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COMPLEX to PSEUDO (using only real part). */
/*---------------------------------------------------------------------------*/
void Complex2Pseudo(Count, In, Out, R, G, B)
     int Count;
     COMPLEX_TYPE *In;
     PSEUDO_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i;
   float Min, Max;
   float Temp;
   float Scale = 1;

   /* Create color map */
   for (i = 0; i < 256; i++)
      R[i] = G[i] = B[i] = i;

   /* Find Min and Max pixel value */
   Min = Max = In[0].re;
   for (i = 0; i < Count; i++)
   {
      Temp = In[i].re;
      if (Min > Temp)
	 Min = Temp;
      else if (Max < Temp)
	 Max = Temp;
   }

   /* Determine scale factor */
   if (Max > Min)
      Scale = MAX_BYTE / (Max - Min);

   /* Convert all pixels */
   for (i = 0; i < Count; i++)
      Out[i] = (BYTE_TYPE) ROUND((In[i].re - Min) * Scale);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts COLOR to PSEUDO.                          */
/*---------------------------------------------------------------------------*/
void Color2Pseudo(Count, In, Out, R, G, B)
     int Count;
     COLOR_TYPE *In;
     BYTE_TYPE *Out;
     unsigned char *R, *G, *B;
{
   int i, next;

   /* Create color map (6 Red * 6 Green * 6 Blue) */
   for (i = 0; i < 216; i++)
   {
      R[i] = 21 + 43 * (i / 36);
      G[i] = 21 + 43 * ((i / 6) % 6);
      B[i] = 21 + 43 * (i % 6);
   }

   /* Fill in rest of color map with zeros */
   for (i = 216; i < 256; i++)
      R[i] = G[i] = B[i] = 0;

   /* Process all pixels */
   for (i = 0; i < (Count - 1); i++)
   {
      /* Convert pixel to nearest RGB bin */
      Out[i] = (In[i].r / 43) * 36 + (In[i].g / 43) * 6 + (In[i].b / 43);

      /* Distribute R error to next pixel */
      next = (int) In[i + 1].r - (int) R[Out[i]] + (int) In[i].r;
      if (next < 0)
	 In[i + 1].r = 0;
      else if (next > 255)
	 In[i + 1].r = 255;
      else
	 In[i + 1].r = next;

      /* Distribute G error to next pixel */
      next = (int) In[i + 1].g - (int) G[Out[i]] + (int) In[i].g;
      if (next < 0)
	 In[i + 1].g = 0;
      else if (next > 255)
	 In[i + 1].g = 255;
      else
	 In[i + 1].g = next;

      /* Distribute B error to next pixel */
      next = (int) In[i + 1].b - (int) B[Out[i]] + (int) In[i].b;
      if (next < 0)
	 In[i + 1].b = 0;
      else if (next > 255)
	 In[i + 1].b = 255;
      else
	 In[i + 1].b = next;
   }

   /* Convert last pixel */
   Out[i] = (In[i].r / 43) * 36 + (In[i].g / 43) * 6 + (In[i].b / 43);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine converts image data types.                         */
/*---------------------------------------------------------------------------*/
void im_convert(Image, Count, In, Out, TypeIn, TypeOut)
     IM_TYPE *Image;
     int Count;
     char *In;
     char *Out;
     int TypeIn, TypeOut;
{
   unsigned char Red[nCMAP];
   unsigned char Green[nCMAP];
   unsigned char Blue[nCMAP];

   /* Parameter checking */
   if (Image == NULL)
      Error("Invalid image pointer specified");

   /* Read input image colormap if needed */
   if (TypeIn == PSEUDO)
      im_get_cmap(Image, Red, Green, Blue);

   /* Convert pixels */
   switch (TypeOut)
   {
   case BYTE:
      switch (TypeIn)
      {
      case BYTE:
	 Error("No type conversion needed");
	 break;
      case SHORT:
	 Short2Byte(Count, In, Out);
	 break;
      case INT:
	 Int2Byte(Count, In, Out);
	 break;
      case FLOAT:
	 Float2Byte(Count, In, Out);
	 break;
      case DOUBLE:
	 Double2Byte(Count, In, Out);
	 break;
      case COMPLEX:
	 Complex2Byte(Count, In, Out);
	 break;
      case COLOR:
	 Color2Byte(Count, In, Out);
	 break;
      case PSEUDO:
	 Pseudo2Byte(Count, In, Out, Red, Green, Blue);
	 break;
      default:
	 Error("Invalid pixel type specified");
	 break;
      };
      break;

   case SHORT:
      switch (TypeIn)
      {
      case BYTE:
	 Byte2Short(Count, In, Out);
	 break;
      case SHORT:
	 Error("No type conversion needed");
	 break;
      case INT:
	 Int2Short(Count, In, Out);
	 break;
      case FLOAT:
	 Float2Short(Count, In, Out);
	 break;
      case DOUBLE:
	 Double2Short(Count, In, Out);
	 break;
      case COMPLEX:
	 Complex2Short(Count, In, Out);
	 break;
      case COLOR:
	 Color2Short(Count, In, Out);
	 break;
      case PSEUDO:
	 Pseudo2Short(Count, In, Out, Red, Green, Blue);
	 break;
      default:
	 Error("Invalid pixel type specified");
	 break;
      };
      break;

   case INT:
      switch (TypeIn)
      {
      case BYTE:
	 Byte2Int(Count, In, Out);
	 break;
      case SHORT:
	 Short2Int(Count, In, Out);
	 break;
      case INT:
	 Error("No type conversion needed");
	 break;
      case FLOAT:
	 Float2Int(Count, In, Out);
	 break;
      case DOUBLE:
	 Double2Int(Count, In, Out);
	 break;
      case COMPLEX:
	 Complex2Int(Count, In, Out);
	 break;
      case COLOR:
	 Color2Int(Count, In, Out);
	 break;
      case PSEUDO:
	 Pseudo2Int(Count, In, Out, Red, Green, Blue);
	 break;
      default:
	 Error("Invalid pixel type specified");
	 break;
      };
      break;

   case FLOAT:
      switch (TypeIn)
      {
      case BYTE:
	 Byte2Float(Count, In, Out);
	 break;
      case SHORT:
	 Short2Float(Count, In, Out);
	 break;
      case INT:
	 Int2Float(Count, In, Out);
	 break;
      case FLOAT:
	 Error("No type conversion needed");
	 break;
      case DOUBLE:
	 Double2Float(Count, In, Out);
	 break;
      case COMPLEX:
	 Complex2Float(Count, In, Out);
	 break;
      case COLOR:
	 Color2Float(Count, In, Out);
	 break;
      case PSEUDO:
	 Pseudo2Float(Count, In, Out, Red, Green, Blue);
	 break;
      default:
	 Error("Invalid pixel type specified");
	 break;
      };
      break;

   case DOUBLE:
      switch (TypeIn)
      {
      case BYTE:
	 Byte2Double(Count, In, Out);
	 break;
      case SHORT:
	 Short2Double(Count, In, Out);
	 break;
      case INT:
	 Int2Double(Count, In, Out);
	 break;
      case FLOAT:
	 Float2Double(Count, In, Out);
	 break;
      case DOUBLE:
	 Error("No type conversion needed");
	 break;
      case COMPLEX:
	 Complex2Double(Count, In, Out);
	 break;
      case COLOR:
	 Color2Double(Count, In, Out);
	 break;
      case PSEUDO:
	 Pseudo2Double(Count, In, Out, Red, Green, Blue);
	 break;
      default:
	 Error("Invalid pixel type specified");
	 break;
      };
      break;

   case COMPLEX:
      switch (TypeIn)
      {
      case BYTE:
	 Byte2Complex(Count, In, Out);
	 break;
      case SHORT:
	 Short2Complex(Count, In, Out);
	 break;
      case INT:
	 Int2Complex(Count, In, Out);
	 break;
      case FLOAT:
	 Float2Complex(Count, In, Out);
	 break;
      case DOUBLE:
	 Double2Complex(Count, In, Out);
	 break;
      case COMPLEX:
	 Error("No type conversion needed");
	 break;
      case COLOR:
	 Color2Complex(Count, In, Out);
	 break;
      case PSEUDO:
	 Pseudo2Complex(Count, In, Out, Red, Green, Blue);
	 break;
      default:
	 Error("Invalid pixel type specified");
	 break;
      };
      break;

   case COLOR:
      switch (TypeIn)
      {
      case BYTE:
	 Byte2Color(Count, In, Out);
	 break;
      case SHORT:
	 Short2Color(Count, In, Out);
	 break;
      case INT:
	 Int2Color(Count, In, Out);
	 break;
      case FLOAT:
	 Float2Color(Count, In, Out);
	 break;
      case DOUBLE:
	 Double2Color(Count, In, Out);
	 break;
      case COMPLEX:
	 Complex2Color(Count, In, Out);
	 break;
      case COLOR:
	 Error("No type conversion needed");
	 break;
      case PSEUDO:
	 Pseudo2Color(Count, In, Out, Red, Green, Blue);
	 break;
      default:
	 Error("Invalid pixel type specified");
	 break;
      };
      break;

   case PSEUDO:
      switch (TypeIn)
      {
      case BYTE:
	 Byte2Pseudo(Count, In, Out, Red, Green, Blue);
	 break;
      case SHORT:
	 Short2Pseudo(Count, In, Out, Red, Green, Blue);
	 break;
      case INT:
	 Int2Pseudo(Count, In, Out, Red, Green, Blue);
	 break;
      case FLOAT:
	 Float2Pseudo(Count, In, Out, Red, Green, Blue);
	 break;
      case DOUBLE:
	 Double2Pseudo(Count, In, Out, Red, Green, Blue);
	 break;
      case COMPLEX:
	 Complex2Pseudo(Count, In, Out, Red, Green, Blue);
	 break;
      case COLOR:
	 Color2Pseudo(Count, In, Out, Red, Green, Blue);
	 break;
      case PSEUDO:
	 Error("No type conversion needed");
	 break;
      default:
	 Error("Invalid pixel type specified");
	 break;
      };
      break;

   default:
      Error("Invalid pixel type specified");
      break;
   }

   /* Save new image colormap if needed */
   if (TypeOut == PSEUDO)
      im_put_cmap(Image, Red, Green, Blue);
}
