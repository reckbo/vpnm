#include <stdio.h>
#include <assert.h>


#ifndef VOXEL
#define VOXEL
#include "voxel.h"
#endif

//#include "malloc.h"
#include "memorydebugmacros.h"

#define TRUE 1
#define FALSE 0

/* #####   DEFINITIONS  -  LOCAL TO THIS SOURCE FILE   ##################### */

int
FNeighboursConsistent(VX *pvx, VX *pvxNeighbour, int wy)
{
	int fNeighboursConsistent = 0;
	int *rgco = RgcoFromPvx(pvx);
	int *rgcoActual= RgcoGetNeighbourCoordinates(rgco, wy);	
	int *rgcoToCheck = RgcoFromPvx(pvxNeighbour);
	if (rgcoToCheck[0] == rgcoActual[0] &&
		rgcoToCheck[1] == rgcoActual[1] &&
		rgcoToCheck[2] == rgcoActual[2])
		fNeighboursConsistent = 1;
	else
		fNeighboursConsistent = 0;
	free(rgcoActual);
	return fNeighboursConsistent;
}

/* #####   DEFINITIONS  -  EXPORTED FUNCTIONS   ############################ */

/*-----------------------------------------------------------------------------
 *  Create functions
 *-----------------------------------------------------------------------------*/
VX *
PvxCreateVoxel(int x, int y, int z, double vl)
{
	VX *pvx = (VX *) malloc(sizeof(VX));
	//VX *pvx;
	//MALLOC(pvx,1,VX);
	assert(pvx != NULL);
	pvx->rgco[0] = x;
	pvx->rgco[1] = y;
	pvx->rgco[2] = z;
	pvx->vl=vl;
	pvx->tg=0;
	pvx->pvData = NULL;
	pvx->iFilament=0;
	int i;
	for(i = 0; i < 6; i++)
		pvx->rgpvxNeighbours[i] = NULL;
	return pvx;
}
VX *
PvxCreateVoxelRgco(int rgco[], double vl)
{
	return PvxCreateVoxel(rgco[0], rgco[1], rgco[2], vl);
}
VX *
PvxCreateNeighbourVoxel(VX *pvx, int wy, double vlNeigh)
{
	/* create neighbour voxel */
    int *rgcoNewNeighbour = RgcoGetNeighbourCoordinates(RgcoFromPvx(pvx), wy);
	VX *pvxNewNeighbour = PvxCreateVoxelRgco(rgcoNewNeighbour, vlNeigh);
	free(rgcoNewNeighbour);
	/* set mutual links */
	SetVoxelsAsNeighbours(pvx, pvxNewNeighbour, wy);

	return pvxNewNeighbour;
}
/*-----------------------------------------------------------------------------
 *  Set functions
 *-----------------------------------------------------------------------------*/
void
SetVoxelValue(VX *pvx, double vlNew)
{
	if (pvx == NULL)
		{
		printf("Error: trying to set value of NULL voxel in voxel.c:SetVoxelValue(...)\n");
		return;
		}
	pvx->vl = vlNew;
}
void
SetVoxelTag(VX *pvx, int tg)
{
	if (pvx == NULL)
		{
		printf("Error: trying to set tag value of NULL voxel in voxel.c:SetVoxelTag(...)\n");
		return;
		}
	pvx->tg = tg;
}
void
SetVoxelData(VX *pvx, void *pvData)
{
	if (pvx == NULL)
		return;
	pvx->pvData = pvData;
}
void
SetVoxelsAsNeighbours(VX *pvx, VX *pvxNeighbour, int wy)
{
	if (pvx != NULL && pvxNeighbour != NULL && wy >= 0 && wy < 6)
		{
		/* first do a consistency check */
		if (!FNeighboursConsistent(pvx, pvxNeighbour, wy))
			{
			printf("Error: trying to set inconsistent neighbours in voxel.c\n");
			return; 		
			}
		pvx->rgpvxNeighbours[wy] = pvxNeighbour;
		pvxNeighbour->rgpvxNeighbours[(wy+3)%6] = pvx;
		}	  
}
void
RemoveNeighbourVoxel(VX *pvx, int wy)
{
	if (pvx == NULL)
		return;
	pvx->rgpvxNeighbours[wy] = NULL;
}
/*-----------------------------------------------------------------------------
 *  Get Functions
 *-----------------------------------------------------------------------------*/
double
VlFromPvx(VX *pvx)
{
	if (pvx == NULL)
		{
		printf("Error: trying to get a value from a non-existent voxel in voxel.c:VlFromPvx(). Returning -1. \n");
		return -1;
		}
	return pvx->vl;
}

//double
//VlGet2ndVoxelValue(VX *pvx)
//{
//	if (pvx == NULL)
//		{
//		printf("Error: trying to get a value from a non-existent voxel in voxel.c:VlFromPvx(). Returning -1. \n");
//		return -1;
//		}
//	return pvx->vl2;
//}
//
//double
//VlGet3rdVoxelValue(VX *pvx)
//{
//	if (pvx == NULL)
//		{
//		printf("Error: trying to get a value from a non-existent voxel in voxel.c:VlFromPvx(). Returning -1. \n");
//		return -1;
//		}
//	return pvx->vl3;
//}

int
TgFromPvx(VX *pvx)
{
	return pvx->tg;
}

int 
XFromPvx(VX *pvx)
{
	if (pvx == NULL)
		{
		printf("Error: trying to get a coordinate from a non-existent voxel in voxel.c:XFromPvx(VX *pvx). Returning -1. \n");
		return -1;
		}
	return pvx->rgco[0];
}
int 
YFromPvx(VX *pvx)
{
	if (pvx == NULL)
		{
		printf("Error: trying to get a coordinate from a non-existent voxel in voxel.c:YFromPvx(VX *pvx). Returning -1. \n");
		return -1;
		}
	return pvx->rgco[1];
}
int 
ZFromPvx(VX *pvx)
{
	if (pvx == NULL)
		{
		printf("Error: trying to get a coordinate from a non-existent voxel in voxel.c:ZFromPvx(VX *pvx). Returning -1. \n");
		return -1;
		}
	return pvx->rgco[2];
}
int *
RgcoFromPvx(VX *pvx)
{
	if (pvx == NULL)
		return NULL;
	return pvx->rgco;
}
VX *PvxGetNeighbour(VX *pvx, int wy)
{
	if (pvx == NULL)
		return NULL;
	return pvx->rgpvxNeighbours[wy];
}
void *
PvGetVoxelData(VX *pvx)
{
	if (pvx == NULL)
		return NULL;
	return pvx->pvData;
}

/*
 * The following function define the up, down, right, left, back and forward directions; any code
 * that uses this library need only use these direction names. 
 * The z-axis is chosen as the vertical axis, the x-axis the horizontal, and the y-axis the depth.
 */
int *
RgcoGetNeighbourCoordinates(int rgco[3], int wy)
{
	if (rgco == NULL)
		{
		printf("Error: parameter rgco is NULL in voxel.c:RgcoGetNeighbourCoordinates(...)\n");
		return NULL;
		}
	int *rgcoNeighbour = (int *) malloc(sizeof(int)*3);
	assert(rgcoNeighbour!=NULL);
	rgcoNeighbour[0] = rgco[0];	  
	rgcoNeighbour[1] = rgco[1];	
	rgcoNeighbour[2] = rgco[2]; 
	switch(wy)
		{
		case wyZPlus1: 		rgcoNeighbour[2]+=1;
			break;
		case wyZMinus1: 	rgcoNeighbour[2]-=1;
			break;
		case wyXPlus1: 		rgcoNeighbour[0]+=1;
			break;
		case wyXMinus1: 	rgcoNeighbour[0]-=1;
			break;
		case wyYPlus1: 		rgcoNeighbour[1]+=1;
			break;
		case wyYMinus1: 	rgcoNeighbour[1]-=1;
		    break;
		default: 		printf("Error: received invalid neighbour argument (int wy) in voxel.c:RgcoGetNeighbourCoordinates(...)\n");
		    break;
		}
	return rgcoNeighbour;
}
int
WyGetOppositeNeighbourDirection(int wy)
{
	return (wy+3)%6;
}
/*-----------------------------------------------------------------------------
 *  Destroy function
 *-----------------------------------------------------------------------------*/
void *
PvDestroyVoxel(VX *pvx)
{
	if(pvx == NULL)
		return NULL;

	VX *pvxNeighbour = NULL;
	int wy = 0;
	for (wy = 0; wy < 6; wy++)
		{
		pvxNeighbour = PvxGetNeighbour(pvx, wy);
		/* remove neighbour's link to this voxel being freed */
		if (pvxNeighbour != NULL)
			RemoveNeighbourVoxel(pvxNeighbour, WyGetOppositeNeighbourDirection(wy));			
		}

	void *pv = PvGetVoxelData(pvx);
	free(pvx);
	return pv;
}
int
FVoxelHasAllNeighbours(VX *pvx)
{
	int wy = 0;
	for (wy = 0; wy < 6; wy++)
		{
		if (PvxGetNeighbour(pvx, wy) == NULL)
			return FALSE;
		}
	return TRUE;
}
int 
CvxGetNumberOfNeighbours(VX *pvx)
{
	int wy = 0;
	int cvx = 0;
	for (wy = 0; wy < 6; wy++)
		{
		if (PvxGetNeighbour(pvx, wy) != NULL)
			cvx++;
		}
	return cvx;
}
