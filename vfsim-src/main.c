#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>

#include "voxel.h"
#include "NarrowBand_RungeKutta.h"
#include "Global_Parameters.h"
#include "math.h"

extern void Dump_Position_of_Real_Filament(Coordinates **Position, int Fil_Num, int Iteration);

/*
 * Global variables
 */
int     fUseHybrid = 1;
int     chBiotSavartIntegral = 'h';
double   dtDumpEvery = 0.025;
double   dtTimeStep = 0.005;
double 	dtSmallTimeStep = 0.001;
double   tMax = 3.0;
double   tMin = 0.0;
double   ll_tCirculation = 1;
double   lCoreRadius = 0.1;
double   alpha = 0.2065;
double 	cvxResolution = 75;
double 	vlSplineWeight = 0.01;
/* Hybrid method related */
int     cvxSubSampleResolution = 1;
int 	fHighOrderIntegration = 0;
//double   lTubeRadius = 0.1;
double   lNarrowBandRadius = 2;
double   lNarrowBandBoundary = 3;
double   dtThinEvery = 0.025;
double 	 ReynoldsNumber = 5000;

int fDebug = 0;

void
AddFilament(Coordinates **P, char *szFilename)
{
	FILE *pfh=fopen(szFilename, "r+");

	if (pfh == NULL)
		{
		printf("Couldn't open file %s\n", szFilename);
		exit(1);
		}

	P[0][0].xcord++;
	int crgfn = P[0][0].xcord; 

	double sigma = 0.0;
	fscanf(pfh,"%lf\n", &sigma);
	P[crgfn][0].sigma = sigma;

	double x,y,z;
	int ifn = 0;
	while(1)  
		{ 
		if(fscanf(pfh,"%lf %lf %lf\n", &x, &y, &z)==EOF)
			break;
		ifn++;
		P[crgfn][ifn].xcord=x;
		P[crgfn][ifn].ycord=y;
		P[crgfn][ifn].zcord=z;
		}

	P[crgfn][0].xcord = ifn;
	printf("Number of filament nodes: %f\n", P[crgfn][0].xcord);
	
	/* Compute Length of filament */
	double lSegmentLength = 0.0;
	double lFilamentLength = 0.0;
	double dx, dy, dz;
	for (ifn = 1; ifn < (int)P[crgfn][0].xcord; ifn++)
		{
		//lSegmentLength = lComputeNodeDistance(Position[irgfn][ifn+1], Position[irgfn][ifn]);
		dx = P[crgfn][ifn+1].xcord - P[crgfn][ifn].xcord;
		dy = P[crgfn][ifn+1].ycord - P[crgfn][ifn].ycord;
		dz = P[crgfn][ifn+1].zcord - P[crgfn][ifn].zcord;
		//printf("P[crgfn][ifn+1].xcord is %f\n", P[crgfn][ifn+1].xcord);
		//printf("P[crgfn][ifn+1].ycord is %f\n", P[crgfn][ifn+1].ycord);
		//printf("P[crgfn][ifn+1].zcord is %f\n", P[crgfn][ifn+1].zcord);
		//printf("dx is %f\n", dx);
		//printf("dy is %f\n", dy);
		//printf("dz is %f\n", dz);
		lSegmentLength = sqrt(dx*dx + dy*dy + dz*dz);
		//printf("Segment Length is %f\n", lSegmentLength);
		lFilamentLength += lSegmentLength; 
		}
	printf("Filament Length is %f for filament %i\n", lFilamentLength, crgfn);
	//printf("Core radius is %f\n", lCoreRadius);
	P[crgfn][0].ycord = lFilamentLength;

	fclose(pfh);
}
int
main (argc, argv)
    int argc;
    char **argv;
{
    int c;

    while (1)
    {
	static struct option long_options[] =
	{
	    {"hybrid", 				no_argument,      &fUseHybrid, 1},
	    {"vortex-method",   	no_argument,      &fUseHybrid, 0},
		{"biotsavart-spline", 	no_argument, 	  &chBiotSavartIntegral, 'h'}, 
		{"biotsavart-line", 	no_argument, 	  &chBiotSavartIntegral, 'd'}, 
		{"debug", 				no_argument, 	  &fDebug, 1}, 
	    {"dump-every",     		required_argument, 0, 1},
	    {"timestep",			required_argument, 0, 2},
	    {"small-timestep",		required_argument, 0, 14},
	    {"time-begin",  		required_argument, 0, 3},
	    {"time-end",			required_argument, 0, 4},
	    {"resolution",    		required_argument, 0, 5},
	    {"circulation",    		required_argument, 0, 6},
	    {"core-radius",    		required_argument, 0, 7},
	    {"spline-weight",   	required_argument, 0, 12},
	    {"reynolds-number",   	required_argument, 0, 15},
	    /* hybrid method related */
	    {"high-order-integration",no_argument, 	   &fHighOrderIntegration, 1},
	    {"no-high-order-integration",no_argument, 	   &fHighOrderIntegration, 0},
	    {"subsample-res",    	required_argument, 0, 8},
	    {"narrowband-radius",   required_argument, 0, 9},
	    {"narrowband-boundary", required_argument, 0, 10},
	    {"thin-every",    		required_argument, 0, 11},
	    {"tube-radius",    		required_argument, 0, 13},
	    {0, 0, 0, 0}
	};
	/* getopt_long stores the option index here. */
	int option_index = 0;

	c = getopt_long (argc, argv, "",
		long_options, &option_index);

	/* Detect the end of the options. */
	if (c == -1)
	    break;

	switch (c)
		{
		case 0:
			//printf ("option %s", long_options[option_index].name);
		case '?':
			/* getopt_long already printed an error message. */
			break;
		case 1:
			dtDumpEvery = atof(optarg);
			break;
		case 2:
			dtTimeStep = atof(optarg);
			break;
		case 3:
			tMin = atof(optarg);
			break;
	 	case 4:
			tMax = atof(optarg);
			break;
	 	case 5:
			cvxResolution = atof(optarg);
			break;
	 	case 6:
			ll_tCirculation = atof(optarg);
			break;
	 	case 7:
			lCoreRadius = atof(optarg);
			break;
	 	case 8:
			cvxSubSampleResolution = atoi(optarg);
			break;
	 	case 9:
			lNarrowBandRadius = atof(optarg);
			break;
	 	case 10:
			lNarrowBandBoundary = atof(optarg);
			break;
	 	case 11:
			dtThinEvery = atof(optarg);
			break;
	 	case 12:
			vlSplineWeight = atof(optarg);
			break;
	 	//case 13:
			//lTubeRadius = atof(optarg);
			//break;
	 	case 14:
			dtSmallTimeStep = atof(optarg);
			break;
	 	case 15:
			ReynoldsNumber = atof(optarg);
			break;
		default:
			abort ();
		}
	}
	
    /* Print any remaining command line arguments (not options). */
    if (optind == argc)
		{
		printf ("Need input files\n");
		exit(1);
		}

	/* create filament array*/
	Coordinates **Position = (Coordinates **) malloc(sizeof(Coordinates *)*MAX_FILAMENTS+1);
	int irgfn = 0;
	for (irgfn = 0; irgfn <= MAX_FILAMENTS; irgfn++)
		Position[irgfn] = (Coordinates *) malloc(sizeof(Coordinates)*NODES);
	Position[0][0].xcord = 0.0;

	printf("\n-----------------------------------------------------\n");
	printf("Reading filament nodes from input files into an array\n");
	printf("-----------------------------------------------------\n");
	while (optind < argc)
		{
		printf("Reading in %s...\n", argv[optind]);
		AddFilament(Position, argv[optind++]);
		}

	if (fDebug)
		{
		RunDebug(Position);
		return 0;
		}

	if (fUseHybrid)
		{
		//if (lCoreRadius*cvxResolution >= lNarrowBandBoundary)
			//{
			//printf("narrowband-boundary must be larger than the core radius multiplied by the resolution\n");
			//exit(1);
			//}
		RunHybridMethod(Position);
		}
	else
		RunVortexMethod(Position);

	return 0;
}


//if (optarg)
				//{
				//if(strcmp(long_options[option_index].name, "dump-every") == 0)
					//dtDumpEvery = atoi(optarg);
				//if(strcmp(long_options[option_index].name, "timestep") == 0)
					//dtTimeStep = atof(optarg);
				//if(strcmp(long_options[option_index].name, "time_begin") == 0)
					//tMin = atof(optarg);
				//if(strcmp(long_options[option_index].name, "time_end") == 0)
					//tMax = atof(optarg);
				//if(strcmp(long_options[option_index].name, "circulation") == 0)
					//ll_tCirculation = atof(optarg);
				//if(strcmp(long_options[option_index].name, "core_radius") == 0)
					//lCoreRadius = atof(optarg);
				//if(strcmp(long_options[option_index].name, "subsample-res") == 0)
					//cvxSubSampleResolution = atoi(optarg);
				//if(strcmp(long_options[option_index].name, "narrowband-radius") == 0)
					//lNarrowBandRadius = atof(optarg);
				//if(strcmp(long_options[option_index].name, "narrowband-boundary") == 0)
					//lNarrowBandBoundary = atof(optarg);
				//if(strcmp(long_options[option_index].name, "thin-every") == 0)
					//dtThinEvery = atoi(optarg);
				//}
//typedef struct FMAP FMAP;
//struct FMAP
//{
	//char *szOptName;
	//void (*pfn)();
//};
//
//void SetHybridFlag(double arg) {fUseHybrid = (int) arg;}
//void SetDumpEvery(double arg) {dtDumpEvery = arg;}
//void SetTimeStep(double arg) {dtTimeStep = arg;}
//void SetTMax(double arg) {dtTimeStep = arg;}
//void SetTMin(double arg) {dtTimeStep = arg;}
//void SetTMin(double arg) {dtTimeStep = arg;}

