typedef struct TN TN;
struct TN
{
  VX *pvx;
  TN *ptnLeft;
  TN *ptnRight;
};

void InsertVoxel(TN **, VX *);
VX *PvxRemoveVoxel(TN **, int, int, int);
void CreateLinksWithVoxelsInTree(TN *, VX *);
VX *PvxFromPtn(TN *);
int TgFromPtn(TN *);
void SetTreeNodeTag(TN *, int);
VX *PvxFindVoxel(TN *, int, int, int);
void TraverseTree(TN *, void (*)(TN *, void *), void *);
void TraverseTreePostOrder(TN *, void (*)(TN *, void *), void *);
void SaveVoxelsToDisk(TN *, double, char *);
void SaveVoxelsToDiskSS(int, TN *, double, char *);
void PrintVoxelTreeToTextFile(TN *, int);
void PrintVoxelTree(TN *);
void FreeTree(TN *);
void FreeTreeAndVoxels(TN *);
void WriteVoxelTree(TN *, char *);
void Set26VoxelNeighbours(TN *, void *);
//void Get26VoxelNeighbours(VX *, VX **);


