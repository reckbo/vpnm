#include <stdlib.h>
#include <math.h>
#include <malloc.h>
#include <stdio.h>
#include <assert.h>
#include "Global_Parameters.h"
#include "fibheap.h"
#include "voxel.h"
#include "VoxelTree.h"
#include "fmm.h" 
#define TRUE 1
#define FALSE 0
//#define LARGE 100 defined in Global_Parameters.h

//void
//InsertVoxelIntoTree(TN *ptn, void *pv)
//{
	//TN **pptnAllVoxels = (TN **) pv;
	//VX *pvx = PvxFromPtn(ptn);
	//CreateLinksWithVoxelsInTree(*pptnAllVoxels, pvx);
	//InsertVoxel(pptnAllVoxels, pvx);
//}
double
vlGetMinVoxelValue(VX *pvx1, VX *pvx2)
{
	double vl1, vl2;
	if (pvx1 == NULL) //implies voxel is 'Far' 
		vl1 = LARGE;
	else
		vl1 = VlFromPvx(pvx1);

	if (pvx2 == NULL)
		vl2 = LARGE;
	else
		vl2 = VlFromPvx(pvx2);

	if (vl1 <= vl2)
		return vl1;
	return vl2;
}
double
SolveEikonalEq(VX *pvxTrialNeigh)
{
	double f = 1; 	/* =h or f = h/W_(iNeigh,jNeigh,kNeigh) for geodesics */

	/*  Find neighbouring U's in upwind direction. */
	VX *pvxRight = PvxGetNeighbour(pvxTrialNeigh, wyRight);
	VX *pvxLeft = PvxGetNeighbour(pvxTrialNeigh, wyLeft);
	double u1 = vlGetMinVoxelValue(pvxRight, pvxLeft);

	VX *pvxUp = PvxGetNeighbour(pvxTrialNeigh, wyUp);
	VX *pvxDown = PvxGetNeighbour(pvxTrialNeigh, wyDown);
	double u2 = vlGetMinVoxelValue(pvxUp, pvxDown);

	VX *pvxForward = PvxGetNeighbour(pvxTrialNeigh, wyForward);
	VX *pvxBack = PvxGetNeighbour(pvxTrialNeigh, wyBack);
	double u3 = vlGetMinVoxelValue(pvxForward, pvxBack);

	/* order so that u1 < u2 < u3. */
	double tmp = 0;
	#define SWAP(a,b) tmp = a; a = b; b = tmp
	#define SWAPIF(a,b) if(a>b) { SWAP(a,b); }
	SWAPIF(u2,u3)
	SWAPIF(u1,u2)
	SWAPIF(u2,u3)

	/*
	 *  Solve the Eikonal equation for the current neighbouring point.
	 * 	The equation is (u-u1)^2+(u-u2)^2+(u-u3)^2 = f^2 = 1, with u >= u3 >= u2 >= u1.
	 *  =>    3*u^2 - 2*(u2+u1+u3)*u - f^2 + u1^2 + u3^2 + u2^2
	 *  => delta = (u2+u1+a3)^2 - 3*(a1^2 + a3^2 + a2^2 - P^2)
	 */
	double delta = (u2+u1+u3)*(u2+u1+u3) - 3*(u1*u1 + u2*u2 + u3*u3 - f*f);
	double uNeigh = 0;
	if( delta>=0 )
		{
		uNeigh = ( u2+u1+u3 + sqrt(delta) )/3.0;
		}
	if( uNeigh <= u3 )
		{
		/*  at least u3 is too large, so we have
		 *  u >= u2 >= u1  and  u < u3 so the equation is 
		 * 	(u-u1)^2+(u-u2)^2 - f^2 = 0
		 * 	=> 2*a^2 - 2*(u1+u2)*a + a1^2+a2^2-P^2
		 *  delta = (u2+u1)^2 - 2*(a1^2 + a2^2 - P^2)
		 */
		delta = (u2+u1)*(u2+u1) - 2*(u1*u1 + u2*u2 - f*f);  // = (u2+u1)^2 - 2(u1^2 + u2^2) = (u2-u1)^2 + 2h^2
		uNeigh = 0;
		if( delta>=0 )
			uNeigh = 0.5 * ( u2+u1 +sqrt(delta) );
		if( uNeigh <= u2 )
			{
			uNeigh = u1 + f;
			}
		}
	return uNeigh;
}
void 
Fmm(TN **pptn, TN **pptnHeap, fibheap_t fh, double vlMost, int iFilament, int fExpand)
{
	fibnode_t fnSav = NULL;
	//if (fExpand)
		//TraverseTree(*pptn, &InsertVoxelIntoTree, &ptnAllVoxels);
	//TraverseTree(*pptn, &CheckTag, NULL);

	double vlMinKey;

	while(fh->nodes != 0)
		{
		/* Extract minimum key from heap */
		vlMinKey = (double) fibheap_min_key(fh);
		if (vlMinKey > vlMost || double_EQ(vlMinKey, vlMost))
			{
			printf("DT has reached bandwidth. vlMinKey = %f, vlBandwidth=%f\n", vlMinKey, vlMost);
			break;
			}

		/* Extract trial point from heap */
		VX *pvxTrial = ( (VX *) fibheap_extract_min(fh)  );
		if (pvxTrial == NULL)
			{
				printf("Error: Trial point is NULL in fm_tree_based_dt.c:Fmm(...)\n");
				exit(1);
			}

		/* Tag it as 'Alive' and set its data pointer to NULL */
		SetVoxelTag(pvxTrial, tgAlive);
		SetVoxelData(pvxTrial, NULL);

		/* Insert it into the main tree data structure and remove it from the tree heap */
		if (fExpand) 
			{
			InsertVoxel(pptn, pvxTrial); 
			PvxRemoveVoxel(pptnHeap, pvxTrial->rgco[0], pvxTrial->rgco[1], pvxTrial->rgco[2]);
			//assert(!PvxFindVoxel(*pptnHeap, XFromPvx(pvxTrial),YFromPvx(pvxTrial), ZFromPvx(pvxTrial)));
			}

		int wy;
		VX *pvxTrialNeigh;
		int fHasNeighbour = 0;
	
		/* For each 6-neighbour */ 
		for( wy=0; wy<6; ++wy )
			{
			fHasNeighbour = 0;
			pvxTrialNeigh = PvxGetNeighbour(pvxTrial, wy);

			if (pvxTrialNeigh != NULL)
				fHasNeighbour = 1;
			if (pvxTrialNeigh == NULL && fExpand) // create neighbour 
				{
				pvxTrialNeigh = PvxCreateNeighbourVoxel(pvxTrial, wy, LARGE);
				pvxTrialNeigh->iFilament = iFilament;
				SetVoxelTag(pvxTrialNeigh, tgFar);
				if (pvxTrialNeigh == NULL)
					{
					printf("Error: Failed to create pvxTrialNeigh in fm_tree_based_dt:Fmm(...). Exiting.\n");
					exit(1);
					}
				CreateLinksWithVoxelsInTree(*pptnHeap, pvxTrialNeigh);
				InsertVoxel(pptnHeap, pvxTrialNeigh);
				fHasNeighbour = 1;
				}

			/* do only for those neighbours not 'Alive' */
			if(fHasNeighbour && TgFromPvx(pvxTrialNeigh) != tgAlive )
				{	
				double uNeigh = SolveEikonalEq(pvxTrialNeigh);
				
				/* Update the neighbouring voxel with new U */
				SetVoxelValue(pvxTrialNeigh, uNeigh);

				/* if voxel is 'Far' then insert it into the heap */
				if (TgFromPvx(pvxTrialNeigh) == tgFar)
					{
					SetVoxelTag(pvxTrialNeigh, tgClose);
					fnSav = fibheap_insert(fh, uNeigh, pvxTrialNeigh); 
					assert(fnSav != NULL);
					SetVoxelData(pvxTrialNeigh, fnSav);
					}
				/* else its already in the heap so its heap key must be updated */
				else if (TgFromPvx(pvxTrialNeigh) == tgClose)
					{
					//printf("voxel value = %f\n", VlFromPvx(pvxTrialNeigh));
					fnSav = (fibnode_t) PvGetVoxelData(pvxTrialNeigh);
					assert(fnSav != NULL);
					fibheap_replace_key(fh, fnSav, uNeigh);
					}
				else
					printf("Error: tag must be 'far' or 'close' for this voxel in Fmm(...)");
				
				} 	//end if	
			}//end for each neighbour

		//if (fExpand)
			//{
			///* check that the trial point now has 6 non NULL neighbours */
			//for (wy = 0; wy < 6; ++wy)
				//{
				//if (PvxGetNeighbour(pvxTrial, wy) == NULL)
					//printf("Error: trial point does not have all its neighbours set in fm_tree_based_dt.c:Fmm()\n");
				//}
			//}

	}//end while
	if (!fExpand)
		assert(fh->nodes == 0);

	/* destroy links to voxels left in the heap */
	//VX *pvxObsolete = NULL;
	//while(!(fh->nodes == 0)) 
		//{
		//pvxObsolete = (VX *) fibheap_extract_min(fh);
		//if (fExpand)
			//UnlinkAndDestroyVoxel(pvxObsolete);
		//}
	//assert(fibheap_empty);
	//if (fExpand)
		//TraverseTreePostOrder(ptnHeapVoxels, &DeleteTreeNode, NULL);
}

/* #####   FUNCTION DEFINITIONS  -  EXPORTED FUNCTIONS   ############################ */

void
Fmm_NotCreateVoxels(fibheap_t fh, double vlMost)
{
	Fmm(NULL, NULL, fh, vlMost, 0, FALSE);
}
void 
Fmm_CreateVoxels(TN **pptn, TN **pptnHeap, fibheap_t fh, double vlMost, int iFilament)
{
	Fmm(pptn, pptnHeap, fh, vlMost, iFilament, TRUE);
}

