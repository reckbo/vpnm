typedef struct VX VX;
struct VX 
{
	int rgco[3];
	double vl;
	int tg;
	void *pvData;
	VX *rgpvxNeighbours[6]; 
	int iFilament; //index of filament associated with voxel tube that voxel lives in
};

/* these are each of the voxel neighbour directions. They are defined this way
 * so that the opposite direction can be calculated with the mod function. */
#define wyUp 	0
#define wyDown 	3
#define wyLeft 	1
#define wyRight 4
#define wyForward 2
#define wyBack 	5

#define wyZPlus1 	0
#define wyZMinus1 	3
#define wyXMinus1 	1
#define wyXPlus1 	4
#define wyYPlus1 	2
#define wyYMinus1 	5



//VX * PvxCreateVoxel(int[], double, int);
//VX * PvxCreateVoxelXYZVl(int, int, int, double);
VX * PvxCreateVoxel(int, int, int, double);
VX * PvxCreateNeighbourVoxel(VX *, int, double);

double VlFromPvx(VX *);
//double VlGet2ndVoxelValue(VX *);
//double VlGet3rdVoxelValue(VX *);
int TgFromPvx(VX *);
int XFromPvx(VX *);
int YFromPvx(VX *);
int ZFromPvx(VX *);
int *RgcoFromPvx(VX *);
void *PvGetVoxelData(VX *);
int WyGetOppositeNeighbourDirection(int);

void SetVoxelValue(VX *, double);
void SetVoxelTag(VX *, int);
void SetVoxelData(VX *, void *);
void SetVoxelsAsNeighbours(VX *, VX *, int);

VX *PvxGetNeighbour(VX *, int);

int *RgcoGetNeighbourCoordinates(int [], int);

void *PvDestroyVoxel(VX *);
void RemoveNeighbourVoxel(VX *, int);

int FVoxelHasAllNeighbours(VX *pvx);

int CvxGetNumberOfNeighbours(VX *pvx);
//int *RgcoGetUpNeighbourCoordinates(int []);
//int *RgcoGetDownNeighbourCoordinates(int []);
//int *RgcoGetLeftNeighbourCoordinates(int []);
//int *RgcoGetRightNeighbourCoordinates(int []);
//int *RgcoGetForwardNeighbourCoordinates(int []);
//int *RgcoGetBackNeighbourCoordinates(int []);
