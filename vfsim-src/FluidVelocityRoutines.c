#include <math.h>
#include <stdio.h>
#include <malloc.h>
#include <assert.h>

#include "voxel.h"
#include "VoxelTree.h"
#include "NarrowBand_RungeKutta.h"
#include "Global_Parameters.h"
#include "FluidVelocityRoutines.h"
//#include "Filament_Functions.h"

//typedef struct RK RK;
//struct RK
//{
	//double vlTrialStep;
	//double rgdvl_dt[5]; //Runge-Kutta derivatives
	//double rgdco_dtFluidVelocity[3];
//};

/* //void */
/* //ComputeBiotSavartIntegral(TN *ptn, void *pvFilamentNodes) */
/* //{ */
	/* //VX *pvx = PvxFromPtn(ptn); */
/* // */
	/* // only compute the velocity at narrow band voxels */ 
	/* //int tg = TgFromPvx(pvx); */
	/* //if (tg != tgInnerVoxel || tg != tgBoundaryVoxel) */
		/* //return; */
/* // */
	/* //RK *prk = (RK *) PvGetVoxelData(pvx); */
	/* //int *rgcodCurrVoxel = RgcoFromPvx(pvx); */
	/* //Coordinates **rgrgfn = (Coordinates **) pvFilamentNodes;	 */
/* // */
	/* //prk->rgdco_dtFluidVelocity[2] = 0.0; */
	/* //prk->rgdco_dtFluidVelocity[0] = 0.0; */
	/* //prk->rgdco_dtFluidVelocity[1] = 0.0; */
/* // */
	/* //double bsden_term = alpha*2.0*(lCoreRadius*lCoreRadius); */
	/* printf("bsden_term=%f\n", bsden_term); */
/* // */
	/* calculate the contributions from all nodes from all rings to the current node location X, Y, Z */
	/* //double rdif[3], rdfxdr[4][NODES], rdif3dxds1, rdif2dxds1; */
	/* //double gamj, rdif2_23, rdif2, bsden, r2d3mr3d2; */
	/* //int irgfnFilament, ifn, jn, index; */
/* // */
	/* //for(irgfnFilament=1; irgfnFilament <= (int) rgrgfn[0][0].xcord; irgfnFilament++) */
		/* //{ */
		/* //gamj = 1.0; */
		/* //jn=0; */
		/* //for(ifn=1; ifn <= (int)rgrgfn[irgfnFilament][0].xcord; ifn++) */
			/* //{ */
			/* int irgcod = 0; */
			/* for (irgcod = 0; irgcod < 3; irgcod++) */
			/* //rdif[1]=( ((double)rgcodCurrVoxel[2])/(cvxResolution) ) - rgrgfn[irgfnFilament][ifn].zcord; */
			/* //rdif[2]=( ((double)rgcodCurrVoxel[0])/(cvxResolution) ) - rgrgfn[irgfnFilament][ifn].xcord; */
			/* //rdif[3]=( ((double)rgcodCurrVoxel[1])/(cvxResolution) ) - rgrgfn[irgfnFilament][ifn].ycord; */
			/* printf("x/res = %f\n", ((double)rgcodCurrVoxel[0])/(cvxResolution)); */
			/* printf("y/res = %f\n", ((double)rgcodCurrVoxel[1])/(cvxResolution)); */
			/* printf("z/res = %f\n", ((double)rgcodCurrVoxel[2])/(cvxResolution)); */
			/* printf("z rdif1 = %f\n", rdif[1]); */
			/* printf("x rdif2 = %f\n", rdif[2]); */
			/* printf("y rdif3 = %f\n", rdif[3]); */
/* // */
			/* //rdif2_23 = (rdif[2]*rdif[2])+(rdif[3]*rdif[3]); */
			/* //rdif2 = (rdif[1]*rdif[1]) + rdif2_23; */
/* // */
			/* //bsden = 1.0/sqrt(pow((rdif2+bsden_term),3)); */
/* // */
			/* //jn++; */
/* // */
			/* use the first derivative that's calculated and stored in array dxds in the cross product of the Biot_Savart integral */
			/* r2d3mr3d2=(rdif[2]*dxds[nrj][ifn].ycord)-(rdif[3]*dxds[nrj][ifn].xcord); */
			/* //r2d3mr3d2=(rdif[2]*rgrgfn[irgfnFilament][ifn].dyc_ds)-(rdif[3]*rgrgfn[irgfnFilament][ifn].dxc_ds); */
			/* //rdfxdr[1][jn] = r2d3mr3d2*bsden; */
/* // */
			/* rdif3dxds1 = rdif[3]*dxds[irgfnFilament][ifn].zcord; */
			/* rdfxdr[2][jn] = (rdif3dxds1-(rdif[1]*dxds[irgfnFilament][ifn].ycord))*bsden; */
			/* //rdif3dxds1 = rdif[3]*rgrgfn[irgfnFilament][ifn].dzc_ds; */
			/* //rdfxdr[2][jn] = (rdif3dxds1-(rdif[1]*rgrgfn[irgfnFilament][ifn].dyc_ds))*bsden; */
			/* printf("rdfxdr[2][jn]=%f, rdif[1]=%f, dyc_ds=%f, bsden=%f\n",rdfxdr[2][jn], rdif[1], rgrgfn[irgfnFilament][ifn].dyc_ds, bsden); */
/* // */
			/* rdif2dxds1 = rdif[2]*dxds[irgfnFilament][ifn].zcord; */
			/* rdfxdr[3][jn]=((rdif[1]*dxds[irgfnFilament][ifn].xcord)-rdif2dxds1)*bsden; */
			/* //rdif2dxds1 = rdif[2]*rgrgfn[irgfnFilament][ifn].dzc_ds; */
			/* //rdfxdr[3][jn]=((rdif[1]*rgrgfn[irgfnFilament][ifn].dxc_ds)-rdif2dxds1)*bsden; */
			/* //} */
/* // */
		/* //jn=0; */
/* // */
		/* add the contribution of each node in the ring considered (irgfnFilament) at the node location (ni) */
		/* //for(ifn=1; ifn <= (int)rgrgfn[irgfnFilament][0].xcord; ifn++) */
			/* //{ */
			/* //jn++; */
/* // */
			/* //if(ifn==(int)rgrgfn[irgfnFilament][0].xcord) */
				/* //index = 1; */
			/* //else */
				/* //index=jn+1; */
/* // */
			/* printf("irgfnFilament = %d, ifn = %d, ho2 = %f\n", irgfnFilament, ifn, rgrgfn[irgfnFilament][ifn].ho2); */
			/* // */
			/* prk->rgdco_dtFluidVelocity[0] += cvxResolution*gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[1][jn]+rdfxdr[1][index]); */
			/* prk->rgdco_dtFluidVelocity[1] += cvxResolution*gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[2][jn]+rdfxdr[2][index]); */
			/* prk->rgdco_dtFluidVelocity[2] += cvxResolution*gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[3][jn]+rdfxdr[3][index]); */
			/*  */
/* // */
			/* //prk->rgdco_dtFluidVelocity[2] += (ll_tCirculation/(4.0*M_PI))*gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[1][jn]+rdfxdr[1][index]); */
			/* //prk->rgdco_dtFluidVelocity[0] += (ll_tCirculation/(4.0*M_PI))*gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[2][jn]+rdfxdr[2][index]); */
			/* //prk->rgdco_dtFluidVelocity[1] += (ll_tCirculation/(4.0*M_PI))*gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[3][jn]+rdfxdr[3][index]); */
/* // */
  /* printf("rdfxdr[2] = %f\n", rdfxdr[2][jn]); */
/* // */
			/* AddToVelocityAtCurrVoxel(pvc, gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[1][jn]+rdfxdr[1][index]), 'z'); */
			/* AddToVelocityAtCurrVoxel(pvc, gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[2][jn]+rdfxdr[2][index]), 'x'); */
			/* AddToVelocityAtCurrVoxel(pvc, gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[3][jn]+rdfxdr[3][index]), 'y'); */
			/* // */
			/* Narrowband_Velocity[v].zcord = Narrowband_Velocity[v].zcord */
				/* +gamj*rgrgfn.ho2[irgcocFilament][irgcoc]*(rdfxdr[1][jn]+rdfxdr[1][index]); */
/*  */
			/* Narrowband_Velocity[v].xcord = Narrowband_Velocity[v].xcord */
				/* +gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[2][jn]+rdfxdr[2][index]); */
/*  */
			/* Narrowband_Velocity[v].ycord = Narrowband_Velocity[v].ycord */
				/* +gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[3][jn]+rdfxdr[3][index]);	       */
/* // */
			/* //} */
		/* //} */
	/* // */
	/* if(VlFromPvx(pvx) <  0.001) */
		/* printf("vl=%f, dz/dt=%f, dx/dt=%f, dy/dt=%f\n", VlFromPvx(pvx), prk->rgdco_dtFluidVelocity[2],prk->rgdco_dtFluidVelocity[0],prk->rgdco_dtFluidVelocity[1]); */
  /* printf("y speed = %f\n", prk->rgdco_dtFluidVelocity[1]); */
  /* printf("x speed = %f\n", prk->rgdco_dtFluidVelocity[0]); */
/* //} */
/* //void  */
/* //ComputeFluidVelocityAtVoxels(TN *ptn, Coordinates **P) */
/* //{ */
   /* // */
  /* //int ifn; */
  /* //for(ifn=1; ifn <= (int)P[1][0].xcord; ifn++) */
	  /* //{ */
	  /* //P[1][ifn].ho2 = 0.0; */
	  /* //P[1][ifn].dxc_ds = 0.0; */
	  /* //P[1][ifn].dyc_ds = 0.0; */
	  /* //P[1][ifn].dzc_ds = 0.0; */
	  /* //} */
/* // */
/* // */
  /* //int i, j, index; */
/* // */
  /* //int FilNum, v, X, Y, Z; */
/* // */
  /* get the total number of filaments stored in our array P */
  /* //FilNum = (int)P[0][0].xcord; */
/* // */
  /* evaluate dxds at the filament nodes */
  /* Coordinates dxds[NumOfFil][NODES]; */
  /* double ho2[NumOfFil][NODES]; */
/* // */
  /* //double rngdxds[4][NODES], ds[NODES]; */
  /* //int nr = 0, nhead = 0, nperrgi = 0; */
  /* //double rngnodes[4][NODES]; */
  /* //for(nr=1; nr<=FilNum; nr++) */
	  /* //{ */
	  /* //i=0; */
	  /* //for(nhead=1;nhead<=(int)P[nr][0].xcord;nhead++) */
		  /* //{ */
		  /* //i=i+1; */
		  /* //rngnodes[1][i] = P[nr][nhead].zcord; */
		  /* //rngnodes[2][i] = P[nr][nhead].xcord; */
		  /* //rngnodes[3][i] = P[nr][nhead].ycord; */
		  /* //} */
      /* //nperrgi = (int)P[nr][0].xcord; */
/* // */
      /* compute the first derivative at each node position in the current  */
	  /* filament and store it in array rngdxds. It also fills up array ds */
      /* //First_Derivative(nperrgi, rngnodes, rngdxds, ds); */
/* // */
      /* //i=0; */
      /* fill up array dxds with the first derivative of the current filament */
	  /* //for(nhead=1; nhead<=nperrgi; nhead++) */
		  /* //{ */
		  /* //i=i+1; */
		  /* //P[nr][nhead].dxc_ds = rngdxds[2][i]; */
		  /* //P[nr][nhead].dyc_ds = rngdxds[3][i]; */
		  /* //P[nr][nhead].dzc_ds = rngdxds[1][i]; */
		  /* dxds[nr][nhead].xcord = rngdxds[2][i]; */
		  /* dxds[nr][nhead].ycord = rngdxds[3][i];	 */
		  /* dxds[nr][nhead].zcord = rngdxds[1][i];	 */
/* // */
		  /* //P[nr][nhead].ho2 = 0.5*ds[i]; */
			/* printf("dzc_ds = %f\n", P[nr][nhead].dzc_ds); */
			/* printf("ds = %f\n", ds[i]); */
			/* int l; */
			/* scanf("%d", &l); */
		  /* //} */
	  /* //} */
 /* //TraverseTree(ptn, &ComputeBiotSavartIntegral, (void *)P); */
/* //} */

/******************************************************************************************
   The following function computes the solution of a linear system of equations having 
   symmetric, cyclically tridiagonal matrix. This is an auxiliary function used by the 
   splining function Create_Spline. It solves the matrix in order to compute the coefficients
   of the cubic polynomials that interpolate the given set of nodes. The coefficients are
   stored in the 3 arrays, b, c and d. The function has to be given the number of nodes, n. 
********************************************************************************************/
void 
Tridiagonal_Matrix(int n, double b[NODES], double c[NODES], double d[NODES])
{
  double bh, ch, ch1, ch2, h, s1, s2, s3, z;
  int n1, k, km1, kp1, k1;


  n1 = n-1;
  z = b[1];
  z=1.0/z;

  bh = b[2];
  b[2] = z*c[1];
  ch1 = c[1];
  ch2 = c[2];
  c[2] = z*c[n];
  d[1] = z*d[1];
  s1 = b[n];
  s2 = d[n];
  s3 = c[n];
  
  for(k=2;k<=n1;k++)
    {
      km1 = k-1;
      kp1 = k+1;
      z = bh-ch1*b[k];
      z = 1.0/z;
      s1 = s1 - s3*c[k];
      s2 = s2 - s3*d[km1];
      s3 = -b[k]*s3;
      bh = b[kp1];
      b[kp1] = z*ch2;
      d[k] = z*(d[k]-ch1*d[km1]);
      ch = ch2;
      ch2 = c[kp1];
      c[kp1] = -z*ch1*c[k];
      ch1=ch;
    }

  s1 = s1-(s3+ch1)*(b[n]+c[n]);
  d[n] = s2-(s3+ch1)*d[n1];
  h = d[n]/s1;
  d[n] = h;
  d[n1] = d[n1]-(b[n]+c[n])*h;

  for(k=n-2;k>=1;k--)
    {
      k1 = k+1;
      d[k] = d[k]- b[k1]*d[k1] - c[k1]*h;
    }
}
/********************************************************************************************************
 The following function computes the first derivative at each node for a given filament.
 The filament nodes are stored in array rngnodes and the number of nodes in this filament
 in given by nperrgi. The function then proceeds to find the derivative at each node position
 and stores it the array rngdxds. In addition, it fills up array ds with the corresponding lengths
 of each pair of line elements. First, the cubic polynomial interpolating each pair of nodes is computed
 by finding the corresponding coefficients using the function Tridiagonal_Matrix. Then the derivative
 of the polynomial is computed at the given node position.
 The function returns arrays rngdxds containing the derivative at each node of the given filament
 AND array ds containing the lengths of the line elements.
*********************************************************************************************************/
void 
First_Derivative(int nperrgi, double rngnodes[4][NODES], double rngdxds[4][NODES], double ds[NODES])
{

  double s[NODES], b[NODES], c[NODES], d[NODES];
  double deltas, e2, f2, e1, f1, h, en1;
  //double s0;
  int i, n, n1, j, k, k1;
 
  //generate parameter
  deltas = pow((rngnodes[1][1] - rngnodes[1][nperrgi]),2)
    + pow((rngnodes[2][1] - rngnodes[2][nperrgi]),2) 
    + pow((rngnodes[3][1] - rngnodes[3][nperrgi]),2);
 
  s[1]=0.0;

  for(i=2;i<=nperrgi;i++)
  {
    deltas=pow((rngnodes[1][i] - rngnodes[1][i-1]),2)
      +pow((rngnodes[2][i] - rngnodes[2][i-1]),2)
      +pow((rngnodes[3][i] - rngnodes[3][i-1]),2);
    
    ds[i-1] = sqrt(deltas);
    s[i] = s[i-1] + ds[i-1];
  }

  deltas = pow((rngnodes[1][1]-rngnodes[1][nperrgi]),2)
    +pow((rngnodes[2][1]-rngnodes[2][nperrgi]),2) 
    +pow((rngnodes[3][1]-rngnodes[3][nperrgi]),2);
	
  ds[nperrgi] = sqrt(deltas);
  s[nperrgi+1] = s[nperrgi] + ds[nperrgi];

  //compute dxds
  n=nperrgi+1;
  n1=nperrgi;

  for(j=1;j<=3;j++)
    {
      rngnodes[j][n] = rngnodes[j][1];

      e1 = s[2] - s[1];
      f1 = rngnodes[j][2] - rngnodes[j][1];
      en1 = s[n] - s[n1];
      b[1] = 2.0*(e1+en1);
      d[1] = e1;
      c[1] = 3.0*(f1/e1-(rngnodes[j][n] - rngnodes[j][n1])/en1);

      for(k=2;k<=n1;k++)
	{
	  k1 = k+1;
	  e2 = s[k1] - s[k];
	  f2 = rngnodes[j][k1] - rngnodes[j][k];
	  d[k] = e2;
	  b[k] = 2.0*(e1+e2);
	  c[k] = 3.0*(f2/e2-f1/e1);
	  e1=e2;
	  f1=f2;
	}

      Tridiagonal_Matrix(n1, b, d, c);
      c[n] = c[1];
      for(k=1;k<=n1;k++)
	{
	  k1=k+1;
	  h = s[k1] - s[k];
	  rngdxds[j][k] = (rngnodes[j][k1]-rngnodes[j][k])/h-h*(2.0*c[k]+c[k1])/3.0;

	}
    }
}
/*********************************************************************************************************************************
The following function updates the position of the nodes after moving one timestep. It takes a 2D array containing the old 
positions and the induced current velocity at each node (stored in 2D array V) and finds the new position of the nodes
after timestep "timestep".
********************************************************************************************************************************/ 
void 
Update_Position(Coordinates **P, Coordinates **V, double timestep)
{
   int i, j, NumofFil;

  //get the number of filaments stored in P
  NumofFil = (int)P[0][0].xcord;
 
  for(i=1;i<=NumofFil;i++)
	  {
	  for(j=1;j<=(int)P[i][0].xcord;j++)
		  {
		  P[i][j].xcord=P[i][j].xcord + timestep*V[i][j].xcord;
		  P[i][j].ycord=P[i][j].ycord + timestep*V[i][j].ycord;
		  P[i][j].zcord=P[i][j].zcord + timestep*V[i][j].zcord;   
		  }
	  }
   
}
void 
HighOrderBiotSavart_Filament(Coordinates **P, Coordinates **V)
{
	int irgfnMost = (int)P[0][0].xcord;
	double ho2[irgfnMost+1][NODES];
	Coordinates dxds[irgfnMost+1][NODES];
	double rngnodes[4][NODES], rngdxds[4][NODES], ds[NODES], rdif[4], rdfxdr[4][NODES];
	double gamj, rdif2_23, rdif2;
	double fy, y, y2;
	double bsden, r2d3mr3d2, rdif3dxds1 , rdif2dxds1;
	int i, j, nhead, nperrgi, nr, nri, nrj, ni, jn, nj, index;


	//evaluate dxds at the filament nodes
	for(nr=1;nr<=irgfnMost;nr++)
		{
		i=0;
		for(nhead=1;nhead<=(int)P[nr][0].xcord;nhead++)
			{
			i=i+1;
			rngnodes[1][i] = P[nr][nhead].zcord;
			rngnodes[2][i] = P[nr][nhead].xcord;
			rngnodes[3][i] = P[nr][nhead].ycord;
			}
		nperrgi = (int)P[nr][0].xcord;

		//compute the first derivative at each node position in the current filament and store it in array rngdxds. It also fills up array ds
		First_Derivative(nperrgi, rngnodes, rngdxds, ds);

		i=0;

		//fill up array dxds with the first derivative of the current filament
		for(nhead=1; nhead<=nperrgi; nhead++)
			{
			//printf("%f, nr=%i, nhead=%i\n", P[0][0].xcord, nr, nhead);
			i=i+1;
			dxds[nr][nhead].xcord = rngdxds[2][i];
			dxds[nr][nhead].ycord = rngdxds[3][i];	
			dxds[nr][nhead].zcord = rngdxds[1][i];	
			ho2[nr][nhead] = 0.5*ds[i];
			}
		}


	//initialize velocity data structure to zero over ALL filaments
	for(i=1;i<=irgfnMost;i++)
		for(j=1;j<=(int)P[i][0].xcord;j++)
			{
			V[i][j].xcord = 0.0;
			V[i][j].ycord = 0.0;
			V[i][j].zcord = 0.0;
			}
	
	double lCurrentCoreRadius = 0;
	double bsden_term;

	for(nri=1;nri<=irgfnMost;nri++) //calculate the induced velocities by solving the higher order Biot-Savart integral for every filament
		{
		lCurrentCoreRadius = P[nri][0].sigma;
		printf("\t\tUsing core radius %f in HighOrderBiotSavart_Filament(...) for filament %i\n", lCurrentCoreRadius, nri);

		//double bsden_term = alpha*2.0*(lCurrentCoreRadius*lCurrentCoreRadius);

		for(ni=1;ni<=(int)P[nri][0].xcord;ni++) //for each node in the current filament
			{
			for(nrj=1;nrj<=irgfnMost;nrj++) //calculate the contributions from all nodes from all rings to the current node location ni 
				{
				gamj = 1.0;
				jn=0;
				if (nrj != nri) //average the two core radii if the filament j is different from filament i
					{
					bsden_term = (lCurrentCoreRadius + P[nrj][0].sigma)/2.0;
					bsden_term = bsden_term*bsden_term;
					bsden_term = alpha*2.0*bsden_term;
					}
				else
					{
					bsden_term = alpha*2.0*(lCurrentCoreRadius * lCurrentCoreRadius);
					}
				for(nj=1;nj<=(int)P[nrj][0].xcord;nj++) //for each node in filament j
					{
					rdif[1]=P[nri][ni].zcord-P[nrj][nj].zcord;
					rdif[2]=P[nri][ni].xcord-P[nrj][nj].xcord;
					rdif[3]=P[nri][ni].ycord-P[nrj][nj].ycord;

					rdif2_23 = (rdif[2]*rdif[2])+(rdif[3]*rdif[3]);
					rdif2 = (rdif[1]*rdif[1]) + rdif2_23;
					bsden = 1.0/sqrt(pow((rdif2+bsden_term),3));

					/* 
					 * f(y) = y^3 (5 + 2y^2)
					 *       ---------------
					 *       2(y^2 + 1)^(5/2)
					 *
					 *   y = |x - r(s)|
					 *       ----------
					 *         \sigma
					 */
					y2 = rdif2/(lCurrentCoreRadius*lCurrentCoreRadius);
					fy = (pow(y2,1.5) * (5 + 2*y2)) / ( 2*pow(y2 + 1, 2.5) );

					y = sqrt(y2);
					//printf("y = %f, f(y) = %f\n", y, fy);
					bsden = bsden*fy;
					//bsden = bsden;

					jn++;

					//use the first derivative that's calculated and stored in array dxds in the cross product of the Biot_Savart integral
					r2d3mr3d2=(rdif[2]*dxds[nrj][nj].ycord)-(rdif[3]*dxds[nrj][nj].xcord);
					rdfxdr[1][jn] = r2d3mr3d2*bsden;

					rdif3dxds1 = rdif[3]*dxds[nrj][nj].zcord;
					rdfxdr[2][jn] = (rdif3dxds1-(rdif[1]*dxds[nrj][nj].ycord))*bsden;

					rdif2dxds1 = rdif[2]*dxds[nrj][nj].zcord;
					rdfxdr[3][jn]=((rdif[1]*dxds[nrj][nj].xcord)-rdif2dxds1)*bsden;

					}

				jn=0;

				//add the contribution of each node in the ring considered (nrj) at the node location (ni)
				for(nj=1;nj<=(int)P[nrj][0].xcord;nj++)
					{
					jn=jn+1;

					if(nj==(int)P[nrj][0].xcord)
						index = 1;
					else
						index=jn+1;

					V[nri][ni].zcord=V[nri][ni].zcord
						+gamj*ho2[nrj][nj]*(rdfxdr[1][jn]+rdfxdr[1][index]);

					V[nri][ni].xcord=V[nri][ni].xcord
						+gamj*ho2[nrj][nj]*(rdfxdr[2][jn]+rdfxdr[2][index]);

					V[nri][ni].ycord=V[nri][ni].ycord
						+gamj*ho2[nrj][nj]*(rdfxdr[3][jn]+rdfxdr[3][index]);

					}
				}
			}
		}

	//multiply the induced velocity contributions with the circulation of the filaments 
	for(i=1;i<=irgfnMost;i++)
		for(j=1;j<=(int)P[i][0].xcord;j++)      
			{
			V[i][j].xcord=ll_tCirculation*V[i][j].xcord/(4*M_PI);
			V[i][j].ycord=ll_tCirculation*V[i][j].ycord/(4*M_PI);
			V[i][j].zcord=ll_tCirculation*V[i][j].zcord/(4*M_PI);
			}
	printf("(spline)dz/dt=%f, dy/dt=%f, dx/dt=%f\n", V[1][1].zcord, V[1][1].ycord, V[1][1].xcord);
}
void 
DiscreteBiotSavart_Filament(Coordinates **P, Coordinates **V)
{
  int i, count, nhead, ntail, x, y, k, t;
  //int j;
  double dr2, rdif2,rdifdr, bsden, c1, c2, c3;
  int irgfnMost = P[0][0].xcord;
  Coordinates dr, rdif, rdfxdr, temp[irgfnMost][NODES];

  double bsden_term = alpha*2.0*(lCoreRadius*lCoreRadius);

  //get the number of filaments stored in P
  irgfnMost = (int)P[0][0].xcord;
  
  //initialize array V to zero
  for(x=1;x<=irgfnMost;x++)
    for(y=1;y<=(int)P[x][0].xcord;y++)
      {   
	V[x][y].xcord=0.0;
        V[x][y].ycord=0.0;
        V[x][y].zcord=0.0;        
      }
  
  //go through every filament to get it's contribution at a given point
  for(k=1;k<=irgfnMost;k++)
    { 
      
      //initialize array temp to zero since we're about to consider contributions of the line segments of new filament
      for(x=1;x<=irgfnMost;x++)
	for(y=1;y<=(int)P[x][0].xcord;y++)
	  {   
	    temp[x][y].xcord=0.0;
	    temp[x][y].ycord=0.0;
	    temp[x][y].zcord=0.0;        
	  }
      

      //This contains the number of nodes in the current filament
      ntail=P[k][0].xcord;
      count=ntail;  
      
      //go through every line segment of the current filament to get it's contibution   
      for(nhead=1;nhead<=count;nhead++)
	{
	  //find magnitude of segment under consideration
	  dr.xcord=P[k][nhead].xcord-P[k][ntail].xcord;
	  dr.ycord=P[k][nhead].ycord-P[k][ntail].ycord;
	  dr.zcord=P[k][nhead].zcord-P[k][ntail].zcord;
	  //find |e| squared
	  dr2=(dr.xcord*dr.xcord)+(dr.ycord*dr.ycord)+(dr.zcord*dr.zcord);  
	  
	  //the following loop adds the contribution of the segment under consideration to each node of each filament
	  for(i=1;i<=irgfnMost;i++)
	    for(t=1;t<=(int)P[i][0].xcord;t++)
	      {
		rdif.xcord=P[i][t].xcord-P[k][ntail].xcord;
		rdif.ycord=P[i][t].ycord-P[k][ntail].ycord;
		rdif.zcord=P[i][t].zcord-P[k][ntail].zcord;
		rdif2=(rdif.xcord*rdif.xcord)+(rdif.ycord*rdif.ycord)+(rdif.zcord*rdif.zcord);
		
		rdifdr=(dr.xcord*rdif.xcord)+(dr.ycord*rdif.ycord)+(dr.zcord*rdif.zcord);
		bsden= rdif2 + bsden_term;
		c1=(bsden+dr2)-(2.0*rdifdr);
		c2=((dr2-rdifdr)/sqrt(c1)) + (rdifdr/sqrt(bsden));
		c3=c2/((dr2*bsden)-(rdifdr*rdifdr));
		
		rdfxdr.xcord=(rdif.ycord*dr.zcord)-(rdif.zcord*dr.ycord);
		rdfxdr.ycord=(rdif.zcord*dr.xcord)-(rdif.xcord*dr.zcord);
		rdfxdr.zcord=(rdif.xcord*dr.ycord)-(rdif.ycord*dr.xcord);
		
		//array temp contains the accumulated contributions of the current segment at point t
		temp[i][t].xcord= temp[i][t].xcord + c3*rdfxdr.xcord;
		temp[i][t].ycord= temp[i][t].ycord + c3*rdfxdr.ycord; 
		temp[i][t].zcord= temp[i][t].zcord + c3*rdfxdr.zcord; 
	      }     
	  
	  //increment ntail to consider the next line segment of the current filament 
	  ntail=nhead; 
	  
	}
     
      
      //add the velocities to 2D array V which contains the total velocities from all segments
      for(x=1;x<=irgfnMost;x++)
	for(y=1;y<=(int)P[x][0].xcord;y++)
	  {	 
	    V[x][y].xcord=V[x][y].xcord+temp[x][y].xcord;
	    V[x][y].ycord=V[x][y].ycord+temp[x][y].ycord;
	    V[x][y].zcord=V[x][y].zcord+temp[x][y].zcord;
	  }     
      
    }
  
  //multiply velocity by constant coef which includes the circulation of each filament
    //(all fils are assumed to have the same circulation)
  for(x=1;x<=irgfnMost;x++)
    for(y=1;y<=(int)P[x][0].xcord;y++)
      {	 
	  V[x][y].xcord=ll_tCirculation*V[x][y].xcord/(4*M_PI);
	  V[x][y].ycord=ll_tCirculation*V[x][y].ycord/(4*M_PI);
	  V[x][y].zcord=ll_tCirculation*V[x][y].zcord/(4*M_PI);
      }
  
	//printf("z velocity = %f\n", V[1][1].zcord);
	printf("(line) dz/dt=%f, dy/dt=%f, dx/dt=%f\n", V[1][1].zcord, V[1][1].ycord, V[1][1].xcord);
}
void 
Accurate_NewFilament_Position(Coordinates **Position, Coordinates **New_Position, double dt, int Update_Filament)
{
	int i, j, w, Iter;
	//int nrings, nnodes, index=1;
	//2D array used to store positions and velocities of filament voxels
	//Coordinates Velocity[irgfnMost][NODES];

	int irgfnMost = Position[0][0].xcord;
	Coordinates **Velocity = (Coordinates **) malloc(sizeof(Coordinates *)*(irgfnMost+1));
	int irgfn = 0;
	for (irgfn = 0; irgfn <= irgfnMost; irgfn++)
		{
		Velocity[irgfn] = (Coordinates *) malloc(sizeof(Coordinates)*NODES);
		}

	Iter = (int) rint(dt/dtSmallTimeStep);
	printf(" 		Will evolve nodes over time step=%f, in dtSmallStep=%f increments, i.e in dt/dtSmallStep=%f (Iter=%i) iterations.\n",dt, dtSmallTimeStep,(dt/dtSmallTimeStep), Iter);

	//if we request not to change the contents of array Position 
	if(Update_Filament==0)
		{
		/*transfer the contents of array Position to array New_Positon so we can update the positions 
		  without affecting the sacred array Position which will be used again later on in the simulation*/

		//transfer the number of filaments stored in Position[0][0].xcord      
		New_Position[0][0].xcord = Position[0][0].xcord;

		for(i=1; i<=irgfnMost; i++)
			{		    
			New_Position[i][0].xcord = Position[i][0].xcord; //# of nodes
			New_Position[i][0].ycord = Position[i][0].ycord; //length of filament
			New_Position[i][0].sigma = Position[i][0].sigma; //core radius

			for(j=1; j<=(int)Position[i][0].xcord; j++)
				{
				//transfer the node positions of the current fil. to new array
				New_Position[i][j].xcord = Position[i][j].xcord;
				New_Position[i][j].ycord = Position[i][j].ycord;
				New_Position[i][j].zcord = Position[i][j].zcord;
				}//end for 

			}//end for 


		}//end if

	/*find velocity using either the "discrete" version of the Biot_Savart integral OR the higher order Biot_Savart 
	  and update filament stored in array New_Position. The global parameter BIOT_SAVART_ORDER is used to determine
	  which integral to use.
	 */
	for(w=1;w<=Iter;w++)
		{  
		if(chBiotSavartIntegral=='d') //use the discrete BiotSavart	
			DiscreteBiotSavart_Filament(New_Position, Velocity);

		else if(chBiotSavartIntegral=='h') //use the more accurate higher order BiotSavart integral
			HighOrderBiotSavart_Filament(New_Position, Velocity);

		Update_Position(New_Position, Velocity, dtSmallTimeStep);
		//printf("z=%f, dz/dt=%f, timestep=%f\n", New_Position[1][1].zcord, Velocity[1][1].zcord, dtSmallStep);   
		}
}
void
ComputeBiotSavartIntegral(TN *ptn, void *pvFilamentNodes)
{
	/*VX *pvx = PvxFromPtn(ptn);*/
	VX *pvx = (ptn == NULL) ? NULL : ptn->pvx;

	RK *prk = (RK *) PvGetVoxelData(pvx);
	int *rgcodCurrVoxel = RgcoFromPvx(pvx);
	Coordinates **rgrgfn = (Coordinates **) pvFilamentNodes;	

	int idvl_dtRungeKuttaStep = (int)rgrgfn[0][1].xcord;
	assert (idvl_dtRungeKuttaStep == 1 || idvl_dtRungeKuttaStep == 2);

	prk->rgdco_dtFluidVelocity[idvl_dtRungeKuttaStep][2] = 0.0;
	prk->rgdco_dtFluidVelocity[idvl_dtRungeKuttaStep][0] = 0.0;
	prk->rgdco_dtFluidVelocity[idvl_dtRungeKuttaStep][1] = 0.0;

	//double bsden_term = alpha*2.0*(lCoreRadius*lCoreRadius);
	//printf("bsden_term=%f\n", bsden_term);

	//calculate the contributions from all nodes from all rings to the current node location X, Y, Z
	double rdif[3], rdfxdr[4][NODES], rdif3dxds1, rdif2dxds1;
	double gamj, rdif2_23, rdif2, bsden, r2d3mr3d2;
	int irgfnFilament, ifn, jn, index;
	double fy, y, y2;

	/* for every filament */
	double lCurrentCoreRadius = 0.0;
	for(irgfnFilament=1; irgfnFilament <= (int) rgrgfn[0][0].xcord; irgfnFilament++)
		{
		lCurrentCoreRadius = rgrgfn[irgfnFilament][0].sigma;
		double bsden_term = alpha*2.0*(lCurrentCoreRadius*lCurrentCoreRadius);
		gamj = 1.0;
		jn=0;
		/* for every node */
		for(ifn=1; ifn <= (int)rgrgfn[irgfnFilament][0].xcord; ifn++)
			{
			/* Calculate distances to node */
			rdif[1]=( ((double)rgcodCurrVoxel[2])/(cvxResolution) ) - rgrgfn[irgfnFilament][ifn].zcord;
			rdif[2]=( ((double)rgcodCurrVoxel[0])/(cvxResolution) ) - rgrgfn[irgfnFilament][ifn].xcord;
			rdif[3]=( ((double)rgcodCurrVoxel[1])/(cvxResolution) ) - rgrgfn[irgfnFilament][ifn].ycord;
			//printf("x/res = %f\n", ((double)rgcodCurrVoxel[0])/(cvxResolution));
			//printf("y/res = %f\n", ((double)rgcodCurrVoxel[1])/(cvxResolution));
			//printf("z/res = %f\n", ((double)rgcodCurrVoxel[2])/(cvxResolution));
			//printf("z rdif1 = %f\n", rdif[1]);
			//printf("x rdif2 = %f\n", rdif[2]);
			//printf("y rdif3 = %f\n", rdif[3]);

			rdif2_23 = (rdif[2]*rdif[2])+(rdif[3]*rdif[3]);
			rdif2 = (rdif[1]*rdif[1]) + rdif2_23;
			bsden = 1.0/sqrt(pow((rdif2+bsden_term),3));

			/* 
			 * f(y) = y^3 (5 + 2y^2)
			 *       ---------------
			 *       2(y^2 + 1)^(5/2)
			 *
			 *   y = |x - r(s)|
			 *       ----------
			 *         \sigma
			 */
			y2 = rdif2/(lCurrentCoreRadius*lCurrentCoreRadius);
			fy = (pow(y2,1.5) * (5 + 2*y2)) / ( 2*pow(y2 + 1, 2.5) );
			bsden = bsden*fy;
			//bsden = bsden;

			jn++;

			//use the first derivative that's calculated and stored in array dxds in the cross product of the Biot_Savart integral
			//r2d3mr3d2=(rdif[2]*dxds[nrj][ifn].ycord)-(rdif[3]*dxds[nrj][ifn].xcord);
			r2d3mr3d2=(rdif[2]*rgrgfn[irgfnFilament][ifn].dyc_ds)-(rdif[3]*rgrgfn[irgfnFilament][ifn].dxc_ds);
			rdfxdr[1][jn] = r2d3mr3d2*bsden;

			//rdif3dxds1 = rdif[3]*dxds[irgfnFilament][ifn].zcord;
			//rdfxdr[2][jn] = (rdif3dxds1-(rdif[1]*dxds[irgfnFilament][ifn].ycord))*bsden;
			rdif3dxds1 = rdif[3]*rgrgfn[irgfnFilament][ifn].dzc_ds;
			rdfxdr[2][jn] = (rdif3dxds1-(rdif[1]*rgrgfn[irgfnFilament][ifn].dyc_ds))*bsden;
			//printf("rdfxdr[2][jn]=%f, rdif[1]=%f, dyc_ds=%f, bsden=%f\n",rdfxdr[2][jn], rdif[1], rgrgfn[irgfnFilament][ifn].dyc_ds, bsden);

			//rdif2dxds1 = rdif[2]*dxds[irgfnFilament][ifn].zcord;
			//rdfxdr[3][jn]=((rdif[1]*dxds[irgfnFilament][ifn].xcord)-rdif2dxds1)*bsden;
			rdif2dxds1 = rdif[2]*rgrgfn[irgfnFilament][ifn].dzc_ds;
			rdfxdr[3][jn]=((rdif[1]*rgrgfn[irgfnFilament][ifn].dxc_ds)-rdif2dxds1)*bsden;
			}

		jn=0;

		//add the contribution of each node in the ring considered (irgfnFilament) at the node location (ni)
		for(ifn=1; ifn <= (int)rgrgfn[irgfnFilament][0].xcord; ifn++)
			{
			jn++;

			if(ifn==(int)rgrgfn[irgfnFilament][0].xcord)
				index = 1;
			else
				index=jn+1;

			//printf("irgfnFilament = %d, ifn = %d, ho2 = %f\n", irgfnFilament, ifn, rgrgfn[irgfnFilament][ifn].ho2);
			
			prk->rgdco_dtFluidVelocity[idvl_dtRungeKuttaStep][2] += (ll_tCirculation/(4.0*M_PI))*gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[1][jn]+rdfxdr[1][index]);
			prk->rgdco_dtFluidVelocity[idvl_dtRungeKuttaStep][0] += (ll_tCirculation/(4.0*M_PI))*gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[2][jn]+rdfxdr[2][index]);
			prk->rgdco_dtFluidVelocity[idvl_dtRungeKuttaStep][1] += (ll_tCirculation/(4.0*M_PI))*gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[3][jn]+rdfxdr[3][index]);

			//Narrowband_Velocity[v].zcord = Narrowband_Velocity[v].zcord
				//+gamj*rgrgfn.ho2[irgcocFilament][irgcoc]*(rdfxdr[1][jn]+rdfxdr[1][index]);
//			Narrowband_Velocity[v].xcord = Narrowband_Velocity[v].xcord
//				+gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[2][jn]+rdfxdr[2][index]);
//			Narrowband_Velocity[v].ycord = Narrowband_Velocity[v].ycord
//				+gamj*rgrgfn[irgfnFilament][ifn].ho2*(rdfxdr[3][jn]+rdfxdr[3][index]);	      
			}
		}
	
	//if(VlFromPvx(pvx) <  0.001)
		//printf("vl=%f, dz/dt=%f, dx/dt=%f, dy/dt=%f\n", VlFromPvx(pvx), prk->rgdco_dtFluidVelocity[2],prk->rgdco_dtFluidVelocity[0],prk->rgdco_dtFluidVelocity[1]);
  //printf("y speed = %f\n", prk->rgdco_dtFluidVelocity[1]);
  //printf("x speed = %f\n", prk->rgdco_dtFluidVelocity[0]);
}
void 
ComputeFluidVelocityAtVoxels(TN *ptn, Coordinates **P, int idvl_dtRungeKuttaStep)
{
  int ifn;
  for(ifn=1; ifn <= (int)P[1][0].xcord; ifn++)
	  {
	  P[1][ifn].ho2 = 0.0;
	  P[1][ifn].dxc_ds = 0.0;
	  P[1][ifn].dyc_ds = 0.0;
	  P[1][ifn].dzc_ds = 0.0;
	  }

  int i;
  /* commented out to get rid of warnings (variables not used) */
  //int j, index;
  //int v,X, Y, Z;

  //get the total number of filaments stored in our array P
  int FilNum = (int)P[0][0].xcord;

  //evaluate dxds at the filament nodes
  //Coordinates dxds[NumOfFil][NODES];
  //double ho2[NumOfFil][NODES];

  double rngdxds[4][NODES], ds[NODES];
  int nr = 0, nhead = 0, nperrgi = 0;
  double rngnodes[4][NODES];
  for(nr=1; nr<=FilNum; nr++)
	  {
	  i=0;
	  for(nhead=1;nhead<=(int)P[nr][0].xcord;nhead++)
		  {
		  i=i+1;
		  rngnodes[1][i] = P[nr][nhead].zcord;
		  rngnodes[2][i] = P[nr][nhead].xcord;
		  rngnodes[3][i] = P[nr][nhead].ycord;
		  }
      nperrgi = (int)P[nr][0].xcord;

      //compute the first derivative at each node position in the current 
	  //filament and store it in array rngdxds. It also fills up array ds
      First_Derivative(nperrgi, rngnodes, rngdxds, ds);

      i=0;
      //fill up array dxds with the first derivative of the current filament
	  for(nhead=1; nhead<=nperrgi; nhead++)
		  {
		  i=i+1;
		  P[nr][nhead].dxc_ds = rngdxds[2][i];
		  P[nr][nhead].dyc_ds = rngdxds[3][i];
		  P[nr][nhead].dzc_ds = rngdxds[1][i];
		  //dxds[nr][nhead].xcord = rngdxds[2][i];
		  //dxds[nr][nhead].ycord = rngdxds[3][i];	
		  //dxds[nr][nhead].zcord = rngdxds[1][i];	

		  P[nr][nhead].ho2 = 0.5*ds[i];
			//printf("dzc_ds = %f\n", P[nr][nhead].dzc_ds);
			//printf("ds = %f\n", ds[i]);
			//int l;
			//scanf("%d", &l);
		  }
	  }

 P[0][1].xcord = idvl_dtRungeKuttaStep;  //to save right velocities
 TraverseTree(ptn, &ComputeBiotSavartIntegral, (void *)P);
}
