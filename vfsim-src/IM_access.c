/*---------------------------------------------------------------------------*/
/* Program:  IM_access.c                                                     */
/*                                                                           */
/* Purpose:  This file contains the image creation and access routines       */
/*           for a new image format called "KUIM".  This image format        */
/*           supports 1D, 2D and 3D images of a variety of data types.       */
/*           The constant and type declarations are in IM.h.                 */
/*                                                                           */
/* Author:   John Gauch                                                      */
/*                                                                           */
/* Date:     Oct 25, 1995  (JPEG part by Chunyen Liu)                        */
/*                                                                           */
/* Note:     Copyright (C) The University of Kansas, 1995                    */
/*---------------------------------------------------------------------------*/
#include "IM.h"
#include "IM_convert.h"
#include  <stdlib.h>  
/*---------------------------------------------------------------------------*/
/* Purpose:  This routine prints an error message and aborts.                */
/*---------------------------------------------------------------------------*/

void Error(Message)
     char *Message;
{
   //MessageBox(Message);
//   exit(0);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  These routines swaps byte order of various data types.          */
/*---------------------------------------------------------------------------*/
void Swap2(Value)
     unsigned char *Value;
{
   unsigned char Temp = Value[0];
   Value[0] = Value[1];
   Value[1] = Temp;
}

void Swap4(Value)
     unsigned char *Value;
{
   unsigned char Temp = Value[0];
   Value[0] = Value[3];
   Value[3] = Temp;
   Temp = Value[1];
   Value[1] = Value[2];
   Value[2] = Temp;
}

void Swap8(Value)
     unsigned char *Value;
{
   unsigned char Temp = Value[0];
   Value[0] = Value[7];
   Value[7] = Temp;
   Temp = Value[1];
   Value[1] = Value[6];
   Value[6] = Temp;
   Temp = Value[2];
   Value[2] = Value[5];
   Value[5] = Temp;
   Temp = Value[3];
   Value[3] = Value[4];
   Value[4] = Temp;
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine returns the size of a pixel type in bytes.         */
/*---------------------------------------------------------------------------*/
int PixSize(PixType)
     int PixType;
{
   switch (PixType)
   {
   case BYTE:
      return (sizeof(BYTE_TYPE));
   case SHORT:
      return (sizeof(SHORT_TYPE));
   case INT:
      return (sizeof(INT_TYPE));
   case FLOAT:
      return (sizeof(FLOAT_TYPE));
   case DOUBLE:
      return (sizeof(DOUBLE_TYPE));
   case COMPLEX:
      return (sizeof(COMPLEX_TYPE));
   case COLOR:
      return (sizeof(COLOR_TYPE));
   case PSEUDO:
      return (sizeof(PSEUDO_TYPE));
   case JPEG_GRAY:
      return (sizeof(JPEG_GRAY));
   case JPEG_RGB:
      return (sizeof(JPEG_RGB));
   default:
      return (0);
   }
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine creates an image.                                  */
/*---------------------------------------------------------------------------*/
IM_TYPE *im_create(Name, PixType, Xdim, Ydim, Zdim)
     char *Name;
     int PixType, Xdim, Ydim, Zdim;
{
   FILE *Fd;
   IM_TYPE *Image;
   int i;

   /* Parameter checking */
   if ((Name == NULL) || (strcmp(Name, "") == 0))
      Error("No file name specified");
   if ((PixType < MINTYPE) || (PixType > MAXTYPE))
      Error("Invalid pixel type specified");
   if ((Xdim < 0) || (Ydim < 0) || (Zdim < 0))
      Error("Invalid image dimensions specified");

   /* Open image data file */
   if (strcmp(Name, "/dev/null") != 0)
      if ((Fd = fopen(Name, "r")) != NULL)
      {
	 fclose(Fd);
	 Error("Image file already exists");
      }
   if ((Fd = fopen(Name, "wb")) == NULL)
      Error("Can not open image file");

   /* Allocate image header */
   if ((Image = (IM_TYPE *) malloc((unsigned) sizeof(IM_TYPE))) == NULL)
      Error("Could not allocate image header");

   /* Initialize image header */
   Image->Fd = Fd;
   Image->Machine = 1;
   Image->Version = 1;
   Image->PixType = PixType;
   Image->Xdim = Xdim;
   Image->Ydim = Ydim;
   Image->Zdim = Zdim;
   for (i = 0; i < nTITLE; i++)
      Image->Title[i] = '\0';
   for (i = 0; i < nCMAP; i++)
   {
      Image->Red[i] = Image->Green[i] = Image->Blue[i] = i;
   }

   /* Return image pointer */
   return (Image);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine opens an image.                                    */
/*---------------------------------------------------------------------------*/
IM_TYPE *im_open(Name, PixType, Xdim, Ydim, Zdim, DimCnt)
     char *Name;
     int *PixType, *Xdim, *Ydim, *Zdim, *DimCnt;
{
   int Size, ok = 0;
   FILE *Fd;
   IM_TYPE *Image;

   /* Parameter checking */
   if ((Name == NULL) || (strcmp(Name, "") == 0))
      Error("No file name specified");

   /* Open image data file */
   if ((Fd = fopen(Name, "rb")) == NULL)
      Error("Can not open image file");

   /* Allocate image header */
   if ((Image = (IM_TYPE *) malloc((unsigned) sizeof(IM_TYPE))) == NULL)
      Error("Could not allocate image header");

    
   rewind(Fd);
   Image->Fd = Fd;
   
   /* Read image header from file (excluding Fd) */
   Size = sizeof(IM_TYPE) - sizeof(FILE *);
   if ((int) fread((char *) Image, 1, Size, Fd) != Size)
      Error("Could not read image header");
   Image->Fd = Fd;

   /* Swap header bytes as needed */
   if (Image->Machine != 1)
   {
      Swap2(&(Image->Version));
      Swap2(&(Image->PixType));
      Swap2(&(Image->Xdim));
      Swap2(&(Image->Ydim));
      Swap2(&(Image->Zdim));
   }

   /* Copy header information */
   *PixType = Image->PixType;
   *Xdim = Image->Xdim;
   *Ydim = Image->Ydim;
   *Zdim = Image->Zdim;
   if (*Xdim > 1 && *Ydim > 1 && *Zdim > 1)
      *DimCnt = 3;
   else if (*Xdim > 1 && *Ydim > 1)
      *DimCnt = 2;
   else if (*Xdim > 1)
      *DimCnt = 1;
   else
      *DimCnt = 0;

   /* Return image pointer */
   return (Image);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine returns the image title.                           */
/*---------------------------------------------------------------------------*/
void im_get_title(Image, Title)
     IM_TYPE *Image;
     char *Title;
{
   /* Parameter checking */
   if (Image == NULL)
      Error("Invalid image pointer specified");
   if (Title == NULL)
      Error("Invalid title address specified");
   if (strlen(Image->Title) >= nTITLE)
      Error("Image title is too long");

   /* Data copy */
   strcpy(Title, Image->Title);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine replaces the image title.                          */
/*---------------------------------------------------------------------------*/
void im_put_title(Image, Title)
     IM_TYPE *Image;
     char *Title;
{
   /* Parameter checking */
   if (Image == NULL)
      Error("Invalid image pointer specified");
   if (Title == NULL)
      Error("Invalid title string specified");
   if (strlen(Title) >= nTITLE)
      Error("Image title is too long");

   /* Data copy */
   strcpy(Image->Title, Title);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine returns the image color map.                       */
/*---------------------------------------------------------------------------*/
void im_get_cmap(Image, Red, Green, Blue)
     IM_TYPE *Image;
     unsigned char *Red, *Green, *Blue;
{
   int i;

   /* Parameter checking */
   if (Image == NULL)
      Error("Invalid image pointer specified");
   if ((Red == NULL) || (Green == NULL) || (Blue == NULL))
      Error("Invalid color map pointer specified");

   /* Data copy */
   for (i = 0; i < nCMAP; i++)
   {
      Red[i] = Image->Red[i];
      Green[i] = Image->Green[i];
      Blue[i] = Image->Blue[i];
   }
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine replaces the image color map.                      */
/*---------------------------------------------------------------------------*/
void im_put_cmap(Image, Red, Green, Blue)
     IM_TYPE *Image;
     unsigned char *Red, *Green, *Blue;
{
   int i;

   /* Parameter checking */
   if (Image == NULL)
      Error("Invalid image pointer specified");
   if ((Red == NULL) || (Green == NULL) || (Blue == NULL))
      Error("Invalid color map pointer specified");

   /* Data copy */
   for (i = 0; i < nCMAP; i++)
   {
      Image->Red[i] = Red[i];
      Image->Green[i] = Green[i];
      Image->Blue[i] = Blue[i];
   }
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine produces a 1D image access array.                  */
/*---------------------------------------------------------------------------*/
char *im_alloc1D(Image, PixType)
     IM_TYPE *Image;
     int PixType;
{
   int i, Count, Size;
   char *Data;

   /* Parameter checking */
   if (Image == NULL)
      Error("Invalid image pointer specified");
   if ((PixType < MINTYPE) || (PixType > MAXTYPE))
      Error("Invalid pixel type specified");

   /* Allocate data buffer */
   Count = Image->Xdim * Image->Ydim * Image->Zdim;
   Size = PixSize(PixType);
   if ((Data = (char *) malloc((unsigned) (Count * Size))) == NULL)
      Error("Could not allocate image buffer");
   for (i = 0; i < Count * Size; i++)
      Data[i] = 0;
   /* Since calloc does not work on suns */
   return (Data);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine produces a 2D image access array.                  */
/*---------------------------------------------------------------------------*/
char **im_alloc2D(Image, PixType)
     IM_TYPE *Image;
     int PixType;
{
   char *Data1, **Data2;
   int y, Count, Size, Offset;

   /* Parameter checking */
   if (Image == NULL)
      Error("Invalid image pointer specified");
   if ((PixType < MINTYPE) || (PixType > MAXTYPE))
      Error("Invalid pixel type specified");

   /* Allocate row access buffer */
   Count = Image->Ydim * Image->Zdim;
   Size = sizeof(char *);
   if ((Data2 = (char **) malloc((unsigned) (Count * Size))) == NULL)
      Error("Could not allocate row access buffer");

   /* Initialize row access buffer */
   Offset = Image->Xdim * PixSize(PixType);
   Data1 = im_alloc1D(Image, PixType);
   for (y = 0; y < Count; y++)
      Data2[y] = Data1 + y * Offset;
   return (Data2);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine produces a 3D image access array.                  */
/*---------------------------------------------------------------------------*/
char ***im_alloc3D(Image, PixType)
     IM_TYPE *Image;
     int PixType;
{
   char *Data1, **Data2, ***Data3;
   int y, z, Count, Size, Offset;

   /* Parameter checking */
   if (Image == NULL)
      Error("Invalid image pointer specified");
   if ((PixType < MINTYPE) || (PixType > MAXTYPE))
      Error("Invalid pixel type specified");

   /* Allocate row access buffer */
   Count = Image->Ydim * Image->Zdim;
   Size = sizeof(char *);
   if ((Data2 = (char **) malloc((unsigned) (Count * Size))) == NULL)
      Error("Could not allocate row access buffer");

   /* Initialize row access buffer */
   Offset = Image->Xdim * PixSize(PixType);
   Data1 = im_alloc1D(Image, PixType);
   for (y = 0; y < Count; y++)
      Data2[y] = Data1 + y * Offset;

   /* Allocate slice access buffer */
   Count = Image->Zdim;
   if ((Data3 = (char ***) malloc((unsigned) (Count * Size))) == NULL)
      Error("Could not allocate slice access buffer");

   /* Initialize slice access buffer */
   Offset = Image->Ydim;
   for (z = 0; z < Count; z++)
      Data3[z] = &(Data2[z * Offset]);
   return (Data3);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine frees space from 1D image access array.            */
/*---------------------------------------------------------------------------*/
void im_free1D(Data)
     char *Data;
{
   free(Data);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine frees space from 2D image access array.            */
/*---------------------------------------------------------------------------*/
void im_free2D(Data)
     char **Data;
{
   free(Data[0]);
   free(Data);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine frees space from 3D image access array.            */
/*---------------------------------------------------------------------------*/
void im_free3D(Data)
     char ***Data;
{
   free(Data[0][0]);
   free(Data[0]);
   free(Data);
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine reads pixels from an image.                        */
/*---------------------------------------------------------------------------*/
void im_read(Image, PixType, Data)
     IM_TYPE *Image;
     int PixType;
     char *Data;
{
   int i, Count, Size;
   char *Temp;//, *Pixel;

   /* Parameter checking */
   if (Image == NULL)
      Error("Invalid image pointer specified");
   if ((PixType < MINTYPE) || (PixType > MAXTYPE))
      Error("Invalid pixel type specified");
   if (Data == NULL)
      Error("Invalid pixel data pointer specified");
   if (Image->Fd == NULL)
      Error("File can not be read");

    
   /* Allocate data buffer if needed */
   if (Image->PixType != PixType)
      Temp = im_alloc1D(Image, Image->PixType);
   else
      Temp = Data;

   /* Read pixel data */
   Count = Image->Xdim * Image->Ydim * Image->Zdim;
   Size = PixSize(Image->PixType);
   if (fread(Temp, Size, Count, Image->Fd) == 0)
      Error("Could not read pixel data");

   /* Swap byte order if necessary */
   if (Image->Machine == 256)
   {
      if (Image->PixType == SHORT)
	 for (i = 0; i < Count; i++)
	    Swap2(&(Temp[i * 2]));
      if ((Image->PixType == INT) || (Image->PixType == FLOAT))
	 for (i = 0; i < Count; i++)
	    Swap4(&(Temp[i * 4]));
      if (Image->PixType == COMPLEX)
	 for (i = 0; i < Count * 2; i++)
	    Swap4(&(Temp[i * 4]));
      if (Image->PixType == DOUBLE)
	 for (i = 0; i < Count; i++)
	    Swap8(&(Temp[i * 8]));
      /* No swapping needed for BYTE, COLOR or PSEUDO */
   }

   /* Convert data format if needed */
   if (Image->PixType != PixType)
   {
      im_convert(Image, Count, Temp, Data, Image->PixType, PixType);
      im_free1D(Temp);
   }

   /* Close image */
   fclose(Image->Fd);
   Image->Fd = NULL;
}

/*---------------------------------------------------------------------------*/
/* Purpose:  This routine writes pixels to an image.                         */
/*---------------------------------------------------------------------------*/
void im_write(Image, PixType, Data)
     IM_TYPE *Image;
     int PixType;
     char *Data;
{
   int Count, Size;
   char *Temp = NULL;

   /* Parameter checking */
   if (Image == NULL)
      Error("Invalid image pointer specified");
   if ((PixType < MINTYPE) || (PixType > MAXTYPE))
      Error("Invalid pixel type specified");
   if (Data == NULL)
      Error("Invalid pixel data pointer specified");
   if (Image->Fd == NULL)
      Error("File can not be written");
 
   /* Convert data format if needed */
   if (Image->PixType != PixType)
   {
      Temp = im_alloc1D(Image, Image->PixType);
      Count = Image->Xdim * Image->Ydim * Image->Zdim;
      im_convert(Image, Count, Data, Temp, PixType, Image->PixType);
      Data = Temp;
   }

   /* Write image header */
   Size = sizeof(IM_TYPE) - sizeof(FILE *);
   if ((int) fwrite((char *) Image, 1, Size, Image->Fd) != Size)
      Error("Could not write image header");

   /* Write pixel data */
   Count = Image->Xdim * Image->Ydim * Image->Zdim;
   Size = PixSize(Image->PixType);
   if ((int) fwrite(Data, Size, Count, Image->Fd) != Count)
      Error("Could not write pixel data");
   if (Image->PixType != PixType)
      im_free1D(Temp);

   /* Close image */
   fclose(Image->Fd);
   Image->Fd = NULL;
}
