/************************************************************************

   File:		list.c

   Author(s):		Sylvain Bouix and Kaleem Siddiqi

   Created:		Feb 2000

   Last Revision:	$Date: 2006/09/18 13:39:56 $

   Description:	

   $Revision: 1.1.1.1 $

   $Log: list.c,v $
   Revision 1.1.1.1  2006/09/18 13:39:56  eckbo
   Imported source code.

   Revision 1.1.1.1  2006-09-17 23:43:31  ryan
   Imported source code.

   Revision 1.1.1.1  2006-09-17 02:39:57  ryan
   Import of vortex filament project

   Revision 1.1.1.1  2006-09-16 23:13:56  ryan


   Revision 1.1.1.1  2006/04/19 19:51:21  eckbo
   Previously backed up code before the version that made the single elliptical
   vortex movie broke.


	
   Copyright (c) 2000 by Sylvain Bouix and Kaleem Siddiqi, Centre for
   Intelligent Machines, McGill University, Montreal, QC.  Please see
   the copyright notice included in this distribution for full
   details.

 ***********************************************************************/
#include <stdlib.h>
#include "list.h"

list createList() {
  return NULL;
}

list insertCell(cell *aCell, list aList) {
  list newList;
  list Next;
  list Prev;

  if (aList==NULL) {
    aCell->next=NULL;
    aCell->prev=NULL;
    newList=aCell;
    return newList;
  }
  else {
    newList = aList;
    Next = aList;
    Prev = NULL;
    while (Next!=NULL) {
      Prev = Next;
      Next = Next->next;
    }
    /*    while ((Next!=NULL)&&(aCell->value < Next->value)) {
      Prev = Next;
      Next = Next->next;
    }
    */
    aCell->prev = Prev;
    aCell->next = Next;
    
    if (Next!=NULL)
      Next->prev = aCell;

    if (Prev!=NULL)
      Prev->next = aCell;
    else
      newList = aCell;

    return newList;
  }
}

list deleteCell(cell *aCell, list aList) {
  list head;

  head = aList;

  if (aCell->prev!=NULL)
    (aCell->prev)->next = (aCell->next);
  else
    head = aCell->next;
  
  if (aCell->next!=NULL)
    (aCell->next)->prev = (aCell->prev);

  return head;

}

